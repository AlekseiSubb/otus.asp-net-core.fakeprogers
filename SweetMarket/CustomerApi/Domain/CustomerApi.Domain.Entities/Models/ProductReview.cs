﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models;

/// <summary>
/// Отзыв покупателя на продук
/// </summary>
public class ProductReview : IEntity<int>
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// Оценка
    /// </summary>
    [Range(1, 5)]
    public int Score { get; set; }

    /// <summary>
    /// Краткое описание
    /// </summary>
    public string? Summary { get; set; }

    /// <summary>
    /// ссылка на хранилище картинок
    /// </summary>
    public Guid? ImageGuid { get; set; }

    /// <summary>
    /// Ссылка на покупателя
    /// </summary>
    public int CustomerId { get; set; }
    public Customer Customer { get; set; }

    /// <summary>
    /// Ссылка на продукт
    /// </summary>
    public int ProductId { get; set; }
    public Product Product { get; set; }

}