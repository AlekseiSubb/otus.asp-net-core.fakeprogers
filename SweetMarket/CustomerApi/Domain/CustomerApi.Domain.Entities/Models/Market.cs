﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
#pragma warning disable CS8618

namespace CustomerApi.Domain.Entities.Models;

/// <summary>
/// Модель магазина (витрина продукции)
/// </summary>
public class Market : IEntity<int>
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int Id { get; set; }

    /// <summary>
    /// Идентификатор владельца
    /// </summary>
    [NotMapped]
    public int OwnerId { get; set; } //Удалить потом нахер
    /// <summary>
    /// Идентификатор владельца
    /// </summary>
    [NotMapped]
    public User Owner { get; set; }

    /// <summary>
    /// Наименование
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// Описание
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// ссылка на хранилище картинок
    /// </summary>
    public Guid ImageGuid { get; set; }

    /// <summary>
    /// Уникальный код магазина внутри системы
    /// </summary>
    public string Code { get; set; } = default!;

    /// <summary>
    /// список продукции
    /// </summary>
    public List<Product> Products { get; set; } = new List<Product>();

    /// <summary>
    /// список кондитеров
    /// </summary>
    public List<User> Сonfectioners { get; set; } = new List<User>();
}