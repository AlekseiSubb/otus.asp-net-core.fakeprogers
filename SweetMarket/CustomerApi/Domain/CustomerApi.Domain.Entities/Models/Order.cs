﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models
{

    /// <summary>
    /// Заказ
    /// </summary>
    public class Order : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Номер заказа
        /// </summary>
        public string OrderNumber { get; set; } = default!;

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Ссылка на детали оплаты - сделал что оплаты может не быть
        /// </summary>
        public int PaymentDetailsId { get; set; } //Сделать теневым
        public PaymentDetails? PaymentDetails { get; set; }

        /// <summary>
        /// Ссылка на информацию по доставке - доставки может не быть
        /// </summary>
        public Shipping? Shipping { get; set; } //= default!;

        /// <summary>
        /// Список позиций заказа
        /// </summary>
        public ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

        /// <summary>
        /// статус заказа пока стринг - потом табличку надо сделать со статусами
        /// </summary>
        public string? State { get; set; }

        /// <summary>
        /// Дата создания заказа
        /// </summary>
        public DateTime DateCreate { get; set; }


        /// <summary>
        /// Дата готовности заказа
        /// </summary>
        public DateTime DateReady { get; set; }

        /// <summary>
        /// Ссылка на Заказчика
        /// </summary>
        public int CustomerId { get; set; } 
        public Customer Customer { get; set; } = default!; 
    }
}
