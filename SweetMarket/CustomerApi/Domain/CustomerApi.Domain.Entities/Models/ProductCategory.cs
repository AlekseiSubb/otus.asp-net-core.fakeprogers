﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerApi.Domain.Entities.Models;

/// <summary>
/// Модель типов продукции (Торт, пироженное, мороженное...)
/// </summary>
public class ProductCategory : IEntity<int>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int Id { get; set; }

    /// <summary>
    /// Наименование
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// Описание
    /// </summary>
    public string Description { get; set; } = default!;

    /// <summary>
    /// Уникальный код категории внутри системы
    /// </summary>
    public string Code { get; set; } = default!;

    /// <summary>
    /// Список продуктов данной категории
    /// </summary>
    public ICollection<Product> Products { get; set; } = new List<Product>();
}