﻿using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models
{
    //Сделать теневой
    /// <summary>
    /// Связующая табличка Addon(Дополнения к продукции) - OrderItem (позицию заказа)
    /// </summary>
    public class OrderItemAddon : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Ссылка на дополнение к продукции
        /// </summary>
        public int AddonId { get; set; }

        /// <summary>
        /// Ссылка на позицию заказа
        /// </summary>
        public int OrderItemId { get; set; }

        /// <summary>
        /// Комментарий к дополнению. Может отсутствовать.
        /// </summary>
        public string? Comment { get; set; }
    }
}
