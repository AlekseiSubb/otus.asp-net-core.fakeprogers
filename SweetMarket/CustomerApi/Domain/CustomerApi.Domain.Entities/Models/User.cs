namespace CustomerApi.Domain.Entities.Models;

public class User : IEntity<int>
{
    /// <summary>
    /// �������������
    /// </summary>
   //[Key]
    public int Id { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public string FullName { get; set; }

    /// <summary>
    /// ������ �� ����������
    /// </summary>
    public Customer? Customer { get; set; }

    /// <summary>
    /// ������ �� �������
    /// </summary>
    public int? MarketId { get; set; }
    public Market? Market { get; set; }
}