﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models
{
    /// <summary>
    /// Детали продажы
    /// </summary>
    public class PaymentDetails : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Ссылка на заказ
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// пусть пока способ оплаты только карта
        /// </summary>
        public int? Card { get; set; }

        public string PaymentType { get; set; }

        /// <summary>
        /// Сумма транзакции
        /// </summary>
        public decimal Value { get; set; }

        public string? BankName { get; set; } = default!;

        public DateTimeOffset PaymentDate { get; set; }
    }
}
