﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models;

/// <summary>
/// Модель возможные дополнений к продукции (надпись, особая упаковка, оформление)
/// </summary>
public class Addon : IEntity<int>
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// Наименование
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Описание
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Стоимость
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// Список продуктов которые возможны с данным допником
    /// </summary>
    public ICollection<Product> Products { get; set; } = new List<Product>();

    /// <summary>
    /// Ссылки на позиции к заказу
    /// </summary>
    public ICollection<OrderItemAddon> OrderItemAddons { get; set; } = new List<OrderItemAddon>();

}