﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models
{
    /// <summary>
    /// Позиции в заказе
    /// </summary>
    public class OrderItem : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Колличество единиц выбранной продукции в шт
        /// </summary>
        //[Range(1, UInt16.MaxValue)] потом навесить
        public int Quantity { get; set; }

        /// <summary>
        /// Комментарий к позиции заказа
        /// </summary>
        public string? Comment { get; set; }

        /// <summary>
        /// Ссылка на заказ
        /// </summary>
        public int OrderId { get; set; } //убрать нафиг, дабы не нарушать принципы SOLID
        public Order Order { get; set; } = default!; //убрать нафиг, дабы не нарушать принципы SOLID

        /// <summary>
        /// Ссылка на продукцию
        /// </summary>
        public int ProductId { get; set; } //сделать теневым
        public Product Product { get; set; } = default!;

        /// <summary>
        /// Ссылка на дополнение к продукции
        /// </summary>
        public ICollection<OrderItemAddon> OrderItemAddon { get; set; } = new List<OrderItemAddon>();

        public decimal Price { get; set; }

    }
}
