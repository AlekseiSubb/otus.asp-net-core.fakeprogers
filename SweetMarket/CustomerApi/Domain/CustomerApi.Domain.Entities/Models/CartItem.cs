﻿using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models
{
    /// <summary>
    /// Позиции в корзине они подобны позициям в заказе
    /// </summary>
    public class CartItem : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Колличество
        /// </summary>
        public int Qu { get; set; }

        /// <summary>
        /// Ссылка на корзину 
        /// </summary>
        public int ShoppingSessionId { get; set; }
        public ShoppingSession ShoppingSession { get; set; }

        /// <summary>
        /// Ссылка на продукцию
        /// </summary>
        public int ProductId { get; set; }
        public Product Product { get; set; } = default!;

        public decimal Price { get; set; }

    }
}
