﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models
{
    /// <summary>
    /// Корзина в более общем смысле
    /// то что пользователь накидал в корзину, 
    /// что то может удалиться и по этому не ровно заказу
    /// </summary>
    public class ShoppingSession : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Ссылка на покупателя
        /// </summary>
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime Created { get; set; }


        /// <summary>
        /// Список позиций в корзине
        /// </summary>
        public ICollection<CartItem> CartItems { get; set; } = new List<CartItem>();

        public bool Deleted { get; set; }
    }
}
