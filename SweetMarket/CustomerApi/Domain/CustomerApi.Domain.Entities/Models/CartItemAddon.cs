﻿using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models;

/// <summary>
/// Модель позици дополнений к продукции
/// </summary>
public class CartItemAddon : IEntity<int>
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// Детали дополнения
    /// </summary>
    public string? Details { get; set; }

    public int AddonId { get; set; }
    public int CartItemId { get; set; }
}