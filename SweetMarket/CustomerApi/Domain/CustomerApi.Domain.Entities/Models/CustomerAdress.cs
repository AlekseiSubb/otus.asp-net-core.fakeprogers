using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models;

public class CustomerAdress : IEntity<int>
{
    [Key]
    public int Id { get; set; }
    public string? Adress { get; set; }
}