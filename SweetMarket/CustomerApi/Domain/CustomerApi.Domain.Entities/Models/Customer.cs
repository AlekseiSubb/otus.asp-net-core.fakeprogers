using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Domain.Entities.Models;

/// <summary>
/// Покупатель
/// </summary>
public class Customer : IEntity<int>
{
    [Key]
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Mail { get; set; }

    public int? CustomerAdressId { get; set; }
    public CustomerAdress CustomerAdress { get; set; }

    public int? UserId { get; set; }
    public User? User { get; set; }

    public ICollection<Order> Orders { get; set; } = new List<Order>();
}