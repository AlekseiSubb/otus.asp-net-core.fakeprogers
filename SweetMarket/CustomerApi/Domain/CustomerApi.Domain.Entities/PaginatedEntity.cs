﻿using System.Collections.Generic;

namespace CustomerApi.Domain.Entities;

/// <summary>
/// Сущность с пейджинацией
/// </summary>
public class PaginatedEntity<T, TId> where T : IEntity<TId>
{
    /// <summary>
    /// Элементы
    /// </summary>
    public List<T> Items { get; set; } = new List<T>();
    /// <summary>
    /// Элементов на странице
    /// </summary>
    public int ItemsPerPage { get; set; }
    /// <summary>
    /// Текущая страница
    /// </summary>
    public int CurrentPage { get; set; }
    /// <summary>
    /// Страниц всего
    /// </summary>
    public int TotalPages { get; set; }
}