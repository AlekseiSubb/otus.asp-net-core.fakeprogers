﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System;
using Xunit;
using FluentAssertions;
using System.Linq;

using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Repositories.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Implementations;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NLog;

namespace UnitTests.WebApi.Services
{
    public class Customer_Tests : IClassFixture<TestFixture>
    {
        private readonly CustomerService _customerService;
        private readonly Mock<ICustomerRepository> _customerRepositoryMock;
        private readonly IMapper _mapper;

        public Customer_Tests(TestFixture testFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _customerRepositoryMock = fixture.Freeze<Mock<ICustomerRepository>>();

            var serviceProvider = testFixture.ServiceProvider;
            //_customerRepositoryMock = serviceProvider.GetService<ICustomerRepository>();
            _mapper = serviceProvider.GetService<IMapper>();
            _customerService = new CustomerService(_customerRepositoryMock.Object, _mapper);
        }

        public Customer CreateBaseCustomer(int id, string name)
        {
            return new Customer
            {
                Id = id,
                Name = name,
                Mail = "testCustomer1Mail@mail.ru",
                UserId = id,
                CustomerAdressId = 1
            };

        }


        //по идеи тут особой логике в сервисе нет - тут нужно тестить сам репозиторий InMemory 
        [Fact]
        public async void GetCustomerByIdAsync_CustomerIsNotFound_ReturnNull()
        {
            //Arrange
            int id = 1;
            Customer customer = null;

            _customerRepositoryMock.Setup(r => r.GetAsync(id))
                .ReturnsAsync(customer);

            //Act
            var response = await _customerService.GetCustomerByIdAsync(id);

            //Assert
            _customerRepositoryMock.Verify(r => r.GetAsync(id));

            //var okObjectResult = response as OkObjectResult;
            Assert.Null(response);

            //response.Should().BeAssignableTo<NotFoundResult>();

        }
        [Fact]
        public async void GetCustomerByIdAsync_CustomerExisted_ShouldReturnExpectedCustomer()
        {
            //Arrange
            int id = 1;
            Customer customer = CreateBaseCustomer(id, $"test{id}");

            _customerRepositoryMock.Setup(repo => repo.GetAsync(id))
                .ReturnsAsync(customer);

            //Act
            var response = await _customerService.GetCustomerByIdAsync(id);

            //Assert

            _customerRepositoryMock.Verify(r => r.GetAsync(id));

            //var okObjectResult = response as OkObjectResult;
            Assert.NotNull(response);

            var model = response as CustomerLiteDto;
            Assert.NotNull(model);
            Assert.NotNull(model.Name);
            Assert.NotEmpty(model.Name);

        }

        [Fact]
        public async void GetAllAsync_CustomersExisted_ShouldReturnExpectedListCustomers()
        {
            //Arrange
            List<Customer> customers = new List<Customer>
            {
                CreateBaseCustomer(1, "test1"),
                CreateBaseCustomer(2, "test2")
            };

            _customerRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(customers);

            //Act
            var response = await _customerService.GetCustomersAsync();

            //Assert

            _customerRepositoryMock.Verify(r => r.GetAllAsync());

            //var okObjectResult = response as OkObjectResult;
            Assert.NotNull(response);

            //okObjectResult.StatusCode.Should().Be(200);

            //Assert.IsType<List<CustometLiteDto>>(okObjectResult.Value);
            var model = response as ICollection<CustomerLiteDto>;
            Assert.NotNull(model);
            Assert.True(model.Count > 0);

        }

        [Fact]
        public async void CreateCustomerAsync_CustomerEdit_ShouldReturnNewId()
        {
            //Arrange
            Customer newCustomer = CreateBaseCustomer(1, "test1");
            CustomerEditDto editDto = new CustomerEditDto
            {
                Name = newCustomer.Name,
                Mail = newCustomer.Mail,
                CustomerAdressId = newCustomer.CustomerAdressId
            };

            Customer addedCustomer = _mapper.Map<CustomerEditDto, Customer>(editDto);
            _customerRepositoryMock.Setup(repo => repo.AddAsync(addedCustomer))
                .ReturnsAsync(newCustomer);

            //Act
            var response = await _customerService.CreateCustomerAsync(editDto);


            //Assert
            Assert.NotNull(response);
            //Assert.True(response > 0);
        }
    }
}
