﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System;
using Xunit;
using FluentAssertions;
using System.Linq;

using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Repositories.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Implementations;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using CustomerApi.Infrastructure.Repositories.Implementations;
using CustomerApi.Services.Abstractions;
using Shouldly;

namespace UnitTests.WebApi.Services
{
    public class ShoppingSession_Tests : IClassFixture<TestFixture>
    {
        private readonly ShoppingSessionService _shoppingSessionService;
        private readonly Mock<IShoppingSessionRepository> _shoppingSessionRepository;
        private readonly Mock<IProductRepository> _productRepository;
        private readonly Mock<ICustomerRepository> _customerRepository;
        private readonly IMapper _mapper;

        public ShoppingSession_Tests(TestFixture testFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _shoppingSessionRepository = fixture.Freeze<Mock<IShoppingSessionRepository>>();
            _productRepository = fixture.Freeze<Mock<IProductRepository>>();
            _customerRepository = fixture.Freeze<Mock<ICustomerRepository>>();

            var serviceProvider = testFixture.ServiceProvider;
            _mapper = serviceProvider.GetService<IMapper>();
            _shoppingSessionService = new ShoppingSessionService(_shoppingSessionRepository.Object,_productRepository.Object, _customerRepository.Object, _mapper);
        }
        public ShoppingSession CreateBaseShoppingSession()
        {
            return new ShoppingSession
            {
                Id = 1,
                CustomerId = 1,
                Created = DateTime.Now,
                CartItems = new List<CartItem> {
                    new CartItem{ Id = 1, Price = 10 , ProductId = 1, Qu = 1, ShoppingSessionId = 1},
                    new CartItem{ Id = 2, Price = 20 , ProductId = 2, Qu = 1, ShoppingSessionId = 1}
                }
            };
        }

        [Fact]
        public async void GetByIdSessionAsync_CartIsNotFound_ReturnsNull()
        {
            //Arrange
            int id = -10;
            ShoppingSession cart = null;

            _shoppingSessionRepository.Setup(r => r.GetByIdSessionAsync(id))
                .ReturnsAsync(cart);

            //Act
            var response = await _shoppingSessionService.GetByIdSessionAsync(id);

            //Assert
            _shoppingSessionRepository.Verify(r => r.GetByIdSessionAsync(id));

            Assert.Null(response);
        }
        [Fact]
        public async void GetOrderByIdAsync_OrderExisted_ShouldReturnExpectedOrder()
        {
            //Arrange
            int id = 1;
            ShoppingSession cart = CreateBaseShoppingSession();

            _shoppingSessionRepository.Setup(repo => repo.GetByIdSessionAsync(id))
                .ReturnsAsync(cart);

            //Act
            var model = await _shoppingSessionService.GetByIdSessionAsync(id);

            //Assert

            _shoppingSessionRepository.Verify(r => r.GetByIdSessionAsync(id));
            Assert.NotNull(model);

            model.Should().BeAssignableTo<ShoppingSessionDto>();
            Assert.NotNull(model);
            Assert.NotNull(model.CustomerId > 0);
            Assert.True(model.Id > 0);
            Assert.True(model.CartItems.Count > 0);
        }


    }
}
