﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System;
using Xunit;
using FluentAssertions;
using System.Linq;

using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Repositories.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Implementations;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using CustomerApi.Infrastructure.Repositories.Implementations;
using CustomerApi.Services.Abstractions;
using Shouldly;

namespace UnitTests.WebApi.Services
{
    public class Order_Tests : IClassFixture<TestFixture>
    {
        private readonly OrderService _orderService;
        private readonly Mock<IOrderRepository> _orderRepositoryMock;
        private readonly Mock<IShoppingSessionRepository> _shoppingSessionRepository;
        private readonly IMapper _mapper;

        public Order_Tests(TestFixture testFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _orderRepositoryMock = fixture.Freeze<Mock<IOrderRepository>>();
            _shoppingSessionRepository = fixture.Freeze<Mock<IShoppingSessionRepository>>();

            var serviceProvider = testFixture.ServiceProvider;
            _mapper = serviceProvider.GetService<IMapper>();
            _orderService = new OrderService(_orderRepositoryMock.Object, _mapper, _shoppingSessionRepository.Object, null);
        }
        public Order CreateBaseOrder()
        {
            return new Order
            {
                Id = 1,
                OrderNumber = "market1_1",
                Description = "Тестовый заказа Market1",
                Price = 0,
                PaymentDetailsId = 1,
                DateCreate = DateTime.Now,
                State = "Не оплачен",
                CustomerId = 1
            };

        }
        public ShoppingSession CreateBaseShoppingSession()
        {
            return new ShoppingSession
            {
                Id = 1,
                CustomerId = 1,
                Created = DateTime.Now,
                CartItems = new List<CartItem> {
                    new CartItem{ Id = 1, Price = 10 , ProductId = 1, Qu = 1, ShoppingSessionId = 1},
                    new CartItem{ Id = 2, Price = 20 , ProductId = 2, Qu = 1, ShoppingSessionId = 1}
                }
            };
        }

        [Fact]
        public async void GetOrderByIdAsync_OrderIsNotFound_ReturnsNull()
        {
            //Arrange
            int id = -10;
            Order order = null;

            _orderRepositoryMock.Setup(r => r.GetAsync(id))
                .ReturnsAsync(order);

            //Act
            var response = await _orderService.GetOrderByIdAsync(id);

            //Assert
            _orderRepositoryMock.Verify(r => r.GetAsync(id));

            Assert.Null(response);
        }

        [Fact]
        public async void GetOrderByIdAsync_OrderExisted_ShouldReturnExpectedOrder()
        {
            //Arrange
            int id = 1;
            Order order = CreateBaseOrder();

            _orderRepositoryMock.Setup(repo => repo.GetAsync(id))
                .ReturnsAsync(order);

            //Act
            var response = await _orderService.GetOrderByIdAsync(id);

            //Assert

            _orderRepositoryMock.Verify(r => r.GetAsync(id));
            Assert.NotNull(response);

            var model = response as OrderDto;
            Assert.NotNull(model);
            Assert.NotNull(model.OrderNumber);
            Assert.NotEmpty(model.OrderNumber);
            Assert.True(model.CustomerId > 0);
            Assert.True(model.Id > 0);
        }

        [Fact]
        public async void CreateOrderAsync_CreateOrderDto_With_Not_Exist_Cart_ShouldReturnZero()
        {
            //Arrange
            CreateOrderDto createOrderDto = new CreateOrderDto()
            {
                IdShoppingSession = 1,
                Description = "тест создания с несуществующей корзиной",
                DateReady = DateTime.Now

            };
            ShoppingSession cart = null;

            _shoppingSessionRepository.Setup(repo => repo.GetByIdSessionAsync(createOrderDto.IdShoppingSession))
                .ReturnsAsync(cart);

            //Act
            var response = await _orderService.CreateOrderAsync(createOrderDto);

            //Assert

            _shoppingSessionRepository.Verify(repo => repo.GetByIdSessionAsync(createOrderDto.IdShoppingSession));
            Assert.NotNull(response);
            Assert.True(response == 0);
        }

        [Fact]
        public async void CreateOrderAsync_CreateOrderDto_With_Exist_Cart_ShouldReturnExpectedOrder()
        {
            //Arrange
            CreateOrderDto createOrderDto = new CreateOrderDto()
            {
                IdShoppingSession = 1,
                Description = "тест создания",
                DateReady = DateTime.Now
            };
            ShoppingSession cart = CreateBaseShoppingSession();

            _shoppingSessionRepository.Setup(repo => repo.GetByIdSessionAsync(createOrderDto.IdShoppingSession))
                .ReturnsAsync(cart);

            //Act
            var response = await _orderService.CreateOrderAsync(createOrderDto);

            //Assert
            _shoppingSessionRepository.Verify(repo => repo.GetByIdSessionAsync(createOrderDto.IdShoppingSession));
            Assert.NotNull(response);
        }

    }
}
