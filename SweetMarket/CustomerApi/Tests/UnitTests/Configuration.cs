﻿using AutoMapper;
using CustomerApi.Infrastructure.Repositories.Implementations;
using CustomerApi.Services.Implementations.Mapping;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Implementations;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CustomerApi.Domain.Entities.Models;

namespace UnitTests
{
    public static class Configuration
    {
        public static IServiceCollection GetServiceCollection(IConfigurationRoot configuration, string serviceName, IServiceCollection serviceCollection = null)
        {
            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
            }
            serviceCollection
                .AddSingleton(configuration)
                .AddSingleton((IConfiguration)configuration)
                .ConfigureAutomapper()
                .ConfigureAllRepositories()
                .ConfigureAllBusinessServices()
                .AddLogging(builder =>
                {
                    builder.ClearProviders();
                    builder.AddConfiguration(configuration.GetSection("Logging"));
                    builder
                        .AddFilter("Microsoft", LogLevel.Warning)
                        .AddFilter("System", LogLevel.Warning);
                })
                .AddMemoryCache();
            return serviceCollection;
        }

        //public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        //{
        //    var serviceProvider = new ServiceCollection()
        //        .AddEntityFrameworkInMemoryDatabase()
        //        .BuildServiceProvider();
        //    services.AddDbContext<TradingContext>(options =>
        //    {
        //        options.UseInMemoryDatabase("InMemoryDb", builder => { });
        //        options.UseInternalServiceProvider(serviceProvider);
        //    });
        //    services.AddTransient<DbContext, TradingContext>();
        //    return services;
        //}


        private static IServiceCollection ConfigureAutomapper(this IServiceCollection services) => services
            .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

        private static IServiceCollection ConfigureAllRepositories(this IServiceCollection services) => services
            .AddTransient<ICustomerRepository, CustomerRepository>()
            .AddTransient<IOrderRepository, OrderRepository>()
            .AddTransient<IShoppingSessionRepository, ShoppingSessionRepository>();

        private static IServiceCollection ConfigureAllBusinessServices(this IServiceCollection services) => services
            .AddTransient<ICustomerService, CustomerService>()
            .AddTransient<IOrderService, OrderService>()
            .AddTransient<IShoppingSessionService, ShoppingSessionService>();

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
            cfg.AddProfiles(new List<Profile>() 
            { 
                new CustomerMappingsProfile(), 
                new OrderMappingsProfile(), 
                new ShoppingSessionProfile()
            });
                   
            });
            //configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
