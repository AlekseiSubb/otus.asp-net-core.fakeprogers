﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;
using CustomerApi.Services.Contracts.Product;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.Extensions.Logging;

namespace CustomerApi.Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;
        private readonly ILogger<ProductService> _logger;

        public ProductService(IMapper mapper, IProductRepository productRepository, ILogger<ProductService> logger)
        {
            _mapper = mapper;
            _productRepository = productRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<ProductDto>> GetProductAsync()
        {
            var items = await _productRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductDto>>(items);
        }

        public async Task<ProductDto> GetProductByIdAsync(int id)
        {
            var item = await _productRepository.GetAsync(id);
            return _mapper.Map<Product, ProductDto>(item);
        }

        public async Task<PagingDto<ProductDto>> GetPaged(ProductCategoryFilterDto filterDto)
        {
            var entities = await _productRepository.GetPagedAsync(filterDto);
            return _mapper.Map<PaginatedEntity<Product, int>, PagingDto<ProductDto>>(entities);
        }

        public async Task<int> GetQuantity(int idCategory)
        {
            var qu = await _productRepository.GetQuantity(idCategory);
            return qu;
        }

        public async Task<int> CreateAsync(ProductEditDto productEditDto)
        {
            var entity = _mapper.Map<ProductEditDto, Product>(productEditDto);

            _logger.LogInformation("Начало сохранения Product");
            Product res = await _productRepository.AddAsync(entity);
            _logger.LogInformation("Конец сохранения Product");

            return res.Id;
        }

        public async Task<bool> UpdateAsync(ProductEditDto productEditDto)
        {
            var entity = _mapper.Map<ProductEditDto, Product>(productEditDto);

            _logger.LogInformation("Обновление Product начало");
            bool result = await _productRepository.UpdateAsync(entity);
            _logger.LogInformation("Обновление Product конец");

            if (!result)
            {
                _logger.LogError("Ошибка обновления Product");
            }

            return result;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            _logger.LogInformation("Удаление Product начало");
            bool result = await _productRepository.DeleteAsync(id);
            _logger.LogInformation("Удаление Product конец");

            if (!result)
            {
                _logger.LogError("Ошибка удаления Product");
            }
            return result;
        }

        public async Task<ProductLiteDto?> PublishIntoShop(int id)
        {
            _logger.LogInformation("Публикация Product начало");

            var product = await _productRepository.GetAsync(id);
            if (product is null) throw new Exception("Продукт не найден");

            product.IsPublishedInMarket = true;

            bool resultUpdate = await _productRepository.UpdateAsync(product);

            _logger.LogInformation("Публикация Product конец");

            if (!resultUpdate)
            {
                return null;
            }

            return _mapper.Map<Product, ProductLiteDto>(product);
        }

        public async Task<ProductLiteDto?> UnPublishIntoShop(int id)
        {
            _logger.LogInformation("Публикация Product начало");

            var product = await _productRepository.GetAsync(id);
            if (product is null) throw new Exception("Продукт не найден");

            product.IsPublishedInMarket = false;

            bool resultUpdate = await _productRepository.UpdateAsync(product);

            _logger.LogInformation("Публикация Product конец");

            if (!resultUpdate)
            {
                return null;
            }

            return _mapper.Map<Product, ProductLiteDto>(product);
        }
    }
}
