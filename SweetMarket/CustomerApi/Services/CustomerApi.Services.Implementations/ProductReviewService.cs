﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;

namespace CustomerApi.Services.Implementations
{
    public class ProductReviewService : IProductReviewService
    {
        private readonly IMapper _mapper;
        private readonly IProductReviewRepository _productReviewRepository;

        public ProductReviewService(IMapper mapper, IProductReviewRepository productReviewRepository)
        {
            _mapper = mapper;
            _productReviewRepository = productReviewRepository;
        }

        public async Task<ProductReviewDto> GetByIdOrTrowAsync(int id)
        {
            ProductReview productReview = await _productReviewRepository.GetAsync(id);

            //TODO продумать набор исключений для бизнес-логики 
            if (productReview is null)
            {
                throw new InvalidOperationException();
            }

            return _mapper.Map<ProductReview, ProductReviewDto>(productReview);
        }

        public async Task<int> CreateAsync(ProductReviewCreateDto productReviewDto)
        {
            ProductReview entity = _mapper.Map<ProductReviewCreateDto, ProductReview>(productReviewDto);
            ProductReview res = await _productReviewRepository.AddAsync(entity);
            return res.Id;
        }

        public async Task<PagingDto<ProductReviewDto>> GetPagedAsync(PaginationFilterDto filterDto)
        {
            PaginatedEntity<ProductReview, int> entities = await _productReviewRepository.GetPagedAsync(filterDto);
            return _mapper.Map<PaginatedEntity<ProductReview, int>, PagingDto<ProductReviewDto>>(entities);
        }
    }
}
