using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerApi.Services.Implementations;

public class CustomerService : ICustomerService
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;

    public CustomerService(ICustomerRepository customerRepository, IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }

    public async Task<CustomerLiteDto> GetCustomerByIdAsync(int id)
    {
        var item = await _customerRepository.GetAsync(id);
        return _mapper.Map<Customer, CustomerLiteDto>(item);
    }

    public async Task<CustomerLiteDto> GetCustomerByUserIdAsync(int id)
    {
        var item = await _customerRepository.GetByUserIdAsync(id);
        return _mapper.Map<Customer, CustomerLiteDto>(item);
    }

    public async Task<ICollection<CustomerLiteDto>> GetCustomersAsync()
    {
        var items = await _customerRepository.GetAllAsync();

        return _mapper.Map<ICollection<Customer>, ICollection<CustomerLiteDto>>(items);
    }

    public async Task<int> CreateCustomerAsync(CustomerEditDto customerEditDto)
    {
        var item = _mapper.Map<CustomerEditDto, Customer>(customerEditDto);
        var addItem = await _customerRepository.AddAsync(item);
        return addItem.Id;
    }

    public async Task UpdateCustomerAsync(int id, CustomerEditDto customerEditDto)
    {
        var model = await _customerRepository.GetAsync(id);
        model.Name = customerEditDto.Name;
        model.Mail = customerEditDto?.Mail;
        model.CustomerAdressId = customerEditDto?.CustomerAdressId;
        await _customerRepository.UpdateAsync(model);
    }

    public async Task DeleteCustomerAsync(int id)
    {
        await _customerRepository.DeleteAsync(id);
    }
}