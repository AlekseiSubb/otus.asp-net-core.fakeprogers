﻿using AutoMapper;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CustomerApi.Services.Implementations
{
    /// <inheritdoc />
    public class MarketService : IMarketService
    {
        private readonly IMapper _mapper;
        private readonly IMarketRepository _marketRepository;
        private readonly ILogger<MarketService> _logger;

        public MarketService(IMapper mapper, IMarketRepository marketRepository, ILogger<MarketService> logger)
        {
            _mapper = mapper;
            _marketRepository = marketRepository;
            _logger = logger;
        }

        public async Task<PagingDto<MarketShortDto>> GetPaged(PaginationFilterDto filterDto)
        {
            //Не понятно где правильно логировать, здесь или только в репозитории или вообще везде
            _logger.LogInformation("Начало получения магазина");
            var entities = await _marketRepository.GetPagedAsync(filterDto);
            _logger.LogInformation("Конец получения магазина");

            return _mapper.Map<PaginatedEntity<Market, int>, PagingDto<MarketShortDto>>(entities);
        }

        public async Task<MarketDto> GetById(int id)
        {
            _logger.LogInformation("Начало получения магазина");
            var market = await _marketRepository.GetAsync(id);
            _logger.LogInformation("Конец получения магазина");

            return _mapper.Map<Market, MarketDto>(market);
        }

        public async Task<int> Create(MarketEditDto marketDto)
        {
            var entity = _mapper.Map<MarketEditDto, Market>(marketDto);

            _logger.LogInformation("Начало сохранения магазина");
            Market res = await _marketRepository.AddAsync(entity);
            _logger.LogInformation("Конец сохранения магазина");

            return res.Id;
        }

        public async Task<bool> Update(MarketEditDto marketDto)
        {
            var entity = _mapper.Map<MarketEditDto, Market>(marketDto);

            _logger.LogInformation("Обновление магазина начало");
            bool result = await _marketRepository.UpdateAsync(entity);
            _logger.LogInformation("Обновление магазина конец");
          
            if (!result)
            {
                _logger.LogError("Ошибка обновления магазина");
            }

            return result;
        }

        public async Task<bool> Delete(int id)
        {
            _logger.LogInformation("Удаление магазина начало");
            bool result = await _marketRepository.DeleteAsync(id);
            _logger.LogInformation("Удаление магазина конец");

            if (!result)
            {
                _logger.LogError("Ошибка удаления магазина");
            }
            return result;
        }
    }
}