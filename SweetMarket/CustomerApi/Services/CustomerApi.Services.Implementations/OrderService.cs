﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IShoppingSessionRepository _cartRepository;
        private readonly IMapper _mapper;
        private readonly ISendEndpointProvider _sendEndpointProvider;

        public OrderService(IOrderRepository orderRepository, 
                            IMapper mapper, 
                            IShoppingSessionRepository cartRepository,
                            ISendEndpointProvider sendEndpointProvider)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
            _cartRepository = cartRepository;
            _sendEndpointProvider = sendEndpointProvider;
        }
        public async Task<OrderDto> GetOrderByIdAsync(int id)
        {
            var item = await _orderRepository.GetAsync(id);
            return _mapper.Map<Order, OrderDto>(item);
        }

        public async Task<IEnumerable<OrderDto>> GetOrdersAsync()
        {
            var items = await _orderRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(items);
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список заказов. </returns>
        public async Task<IEnumerable<OrderDto>> GetPaged(OrderFilterDto filterDto)
        {
            IEnumerable<Order> entities = await _orderRepository.GetPagedAsync(filterDto);
            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(entities);
        }

        /// <summary>
        /// На основание корзины создаем заказ
        /// </summary>
        /// <param name="createrderDto"></param>
        /// <returns></returns>
        public async Task<int> CreateOrderAsync(CreateOrderDto createOrderDto)
        {
            ShoppingSession cart =  await _cartRepository.GetByIdSessionAsync(createOrderDto.IdShoppingSession);
            if(cart == null)
                return 0;
            ShoppingSessionDto cartDto =  _mapper.Map<ShoppingSessionDto>(cart);

            // мапим модель корзины на модель заказа
            var orderDto = _mapper.Map<OrderDto>(cartDto); 
            var model = _mapper.Map<Order>(orderDto);
            model.Description = createOrderDto.Description;
            model.PaymentDetails = new PaymentDetails { PaymentType = createOrderDto.PaymentType};
            model.OrderNumber = "client" + model.CustomerId.ToString().PadLeft(5, '0');

            //посчитать сумму 
            model.Price = model.OrderItems.Sum(oi => oi.Price);

            model.Id = 0;
            Order addItem = await _orderRepository.AddAsync(model);

            //генирация номера заказа 
            addItem.OrderNumber = "client" + model.CustomerId.ToString().PadLeft(5, '0') + "-" + model.Id.ToString().PadLeft(5, '0');
            await _orderRepository.UpdateAsync(addItem);

            //отправка сообщения в Market - тут если в заказе покупателя продукция из разных магазинов то формируем сообщение для каждого со своей продукцией 
            var grups = addItem.OrderItems
                .GroupBy(i => i.Product.MarketId)
                .Select(g => new OrderCreateCommand
                {
                    CustomerOrderId = addItem.Id,
                    MarketId = g.Key,
                    Description = addItem.Description,
                    DateReady = createOrderDto.DateReady,
                    OrderItems = g.Select(p => new OrderItemCommand { 
                                            ProductId = p.ProductId,
                                            Comment = p.Comment,
                                            Price = p.Price,
                                            Quantity = p.Quantity }).ToList()
                });
            foreach (var gr in grups)
                await SendCreateOrderCommand(gr);


            return addItem.Id;
        }

        public async Task<int> CreateOrderAsyncOld(OrderDto orderDto)
        {
            var model = _mapper.Map<Order>(orderDto);
            model.Id = 0;
            Order addItem = await _orderRepository.AddAsync(model);
            return addItem.Id;
        }

        public async Task UpdateOrderAsync(int id, OrderDto orderDto)
        {
            var model = _mapper.Map<Order>(orderDto);
            model.Id = id;
            bool res = await _orderRepository.UpdateAsync(model);

            model = await _orderRepository.GetAsync(id);
            if (model == null)
                return;

            //отправка сообщения в Market - тут если в заказе покупателя продукция из разных магазинов то формируем сообщение для каждого со своей продукцией 
            var grups = model.OrderItems
                .GroupBy(i => i.Product.MarketId)
                .Select(g => new OrderUpdateCommand
                {
                    CustomerOrderId = model.Id,
                    MarketId = g.Key,
                    Description = model.Description,
                    DateReady = orderDto.DateReady,
                    OrderItems = g.Select(p => new OrderItemCommand
                    {
                        ProductId = p.ProductId,
                        Comment = p.Comment,
                        Price = p.Price,
                        Quantity = p.Quantity
                    }).ToList()
                });
            foreach (var gr in grups)
                await SendUpdateOrderCommand(gr);

        }

        public async Task DeleteOrderAsync(int id)
        {
            var model = await _orderRepository.GetAsync(id);
            if (model == null)
                return;

            //отправка сообщения в Market - тут если в заказе покупателя продукция из разных магазинов то формируем сообщение для каждого со своей продукцией 
            var grups = model.OrderItems
                .GroupBy(i => i.Product.MarketId)
                .Select(g => new OrderDeleteCommand
                {
                    CustomerOrderId = model.Id,
                    MarketId = g.Key
                });
            foreach (var gr in grups)   
                await SendDeleteOrderCommand(gr);

            await _orderRepository.DeleteAsync(id);
        }

        private async Task SendCreateOrderCommand(OrderCreateCommand command)
        {
            var uri = new Uri("queue:Cmd.Order.MarketApi.OrderCreateCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }
        private async Task SendUpdateOrderCommand(OrderUpdateCommand command)
        {
            var uri = new Uri("queue:Cmd.Order.MarketApi.OrderUpdateCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }
        private async Task SendDeleteOrderCommand(OrderDeleteCommand command)
        {
            var uri = new Uri("queue:Cmd.Order.MarketApi.OrderDeleteCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }

    }
}




