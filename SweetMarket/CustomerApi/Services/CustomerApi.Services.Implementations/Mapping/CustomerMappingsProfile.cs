using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;
using System.Collections.Generic;

namespace CustomerApi.Services.Implementations.Mapping;

public class CustomerMappingsProfile : Profile
{
    public CustomerMappingsProfile()
    {
        CreateMap<Customer, CustomerLiteDto>();

        CreateMap<CustomerLiteDto, Customer>(MemberList.None)
            .ForMember(d => d.UserId, map => map.Ignore())
            .ForMember(d => d.User, map => map.Ignore())
            .ForMember(d => d.CustomerAdress, map => map.Ignore())
            .ForMember(d => d.CustomerAdressId, map => map.Ignore());
        CreateMap<CustomerEditDto, Customer>()
            .ForMember(d => d.UserId, map => map.Ignore())
            .ForMember(d => d.User, map => map.Ignore());

        CreateMap<Customer, CustomerEditDto>(MemberList.None);
    }
}