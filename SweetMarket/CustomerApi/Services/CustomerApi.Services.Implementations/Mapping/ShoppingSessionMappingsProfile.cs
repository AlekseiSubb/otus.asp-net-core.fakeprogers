using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;
using System;

namespace CustomerApi.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности корзины.
    /// </summary>
    public class ShoppingSessionProfile : Profile
    {
        public ShoppingSessionProfile()
        {
            CreateMap<ShoppingSessionDto, ShoppingSession>().ReverseMap();

            CreateMap<CartItemDto, CartItem>();
            CreateMap<CartItem, CartItemDto>()
                .ForMember(d => d.Id, map => map.MapFrom(c => c.Id))
                .ForMember(d => d.Qu, map => map.MapFrom(c => c.Qu))
                .ForMember(d => d.Price, map => map.MapFrom(c => c.Price))
                .ForMember(d => d.MarketName, map => map.MapFrom(c => c.Product.Name))
                .ForMember(d => d.ProductName, map => map.MapFrom(c => c.Product.Market.Name));



            CreateMap<CustomerDto, Customer>();
            CreateMap<Customer, CustomerDto>()
                .ForMember(d => d.CustomerAdress, map => map.MapFrom(c => c.CustomerAdress.Adress));
        }
    }
}
