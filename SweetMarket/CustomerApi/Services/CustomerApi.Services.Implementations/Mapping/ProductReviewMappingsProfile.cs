﻿using AutoMapper;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Implementations.Mapping
{
    public class ProductReviewMappingsProfile : Profile
    {
        public ProductReviewMappingsProfile()
        {
            CreateMap<ProductReview, ProductReviewDto>();

            CreateMap<ProductReviewCreateDto, ProductReview>();

            CreateMap<PaginatedEntity<ProductReview, int>, PagingDto<ProductReviewDto>>(MemberList.None);
        }
    }
}
