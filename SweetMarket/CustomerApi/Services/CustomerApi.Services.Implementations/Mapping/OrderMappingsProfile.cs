using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности заказа.
    /// </summary>
    public class OrderMappingsProfile : Profile
    {
        public OrderMappingsProfile()
        {
            CreateMap<OrderDto, Order>().ReverseMap();
            CreateMap<OrderItemDto, OrderItem>().ReverseMap();
            CreateMap<PaymentDetailsDto, PaymentDetails>().ReverseMap();

            //мапим корзину на заказ
            CreateMap<ShoppingSessionDto, OrderDto>()
                .ForMember(o => o.Id, map => map.Ignore())
                .ForMember(o => o.PaymentDetails, map => map.Ignore())
                .ForMember(o => o.PaymentDetailsId, map => map.Ignore())
                .ForMember(o => o.ShippingId, map => map.Ignore())
                .ForMember(o => o.Description, map => map.Ignore())
                .ForMember(o => o.OrderNumber, map => map.Ignore())
                .ForMember(o => o.Price, map => map.Ignore())
                .ForMember(o => o.OrderItems, map => map.MapFrom(s => s.CartItems))
                .ForMember(o => o.CustomerId, map => map.MapFrom(s => s.Customer.Id));

            CreateMap<CartItemDto, OrderItemDto>()
                .ForMember(o => o.Id, map => map.Ignore())
                .ForMember(o => o.ProductId, map => map.MapFrom(s => s.ProductId))
                .ForMember(o => o.Quantity, map => map.MapFrom(s => s.Qu))
                .ForMember(o => o.Comment, map => map.Ignore())
                .ForMember(o => o.Price, map => map.MapFrom(s => s.Price));
        }
    }
}
