﻿using AutoMapper;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Product;

namespace CustomerApi.Services.Implementations.Mapping
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductLiteDto, Product>();
            CreateMap<Product, ProductLiteDto>();

            CreateMap<Product, ProductDto>().ReverseMap();

            CreateMap<PaginatedEntity<Product, int>, PagingDto<ProductDto>>().ReverseMap();
        }
    }
}