﻿using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;
namespace CustomerApi.Services.Implementations.Mapping;

/// <summary>
/// Профиль автомаппера для сущности категории продуктов
/// </summary>
public class ProductCategoryMappingProfile : Profile
{
    public ProductCategoryMappingProfile()
    {
        CreateMap<ProductCategory, ProductCategoryDto>();


        //var newItem = new Order
        //{
        //    Description = orderModel.Description,
        //    OrderNumber = orderModel.OrderNumber,
        //    //Shipping
        //    //PaymentDetails
        //    Price = orderModel.Price,

        //};
        //newItem.OrderItems = orderModel.OrderItems.Select(q => new OrderItem
        //{
        //    Order = newItem,
        //    ProductId = q.ProductId,
        //    Quantity = q.Quantity,
        //    OrderItemAddon = q.Addons.Select(q => new OrderItemAddon
        //    {
        //        AddonId = q.AddonId,
        //        Comment = q.Comment,
        //    }).ToList(),
        //    Comment = q.Comment
        //}).ToList();
        CreateMap<ProductCategoryDto, ProductCategory>();
    }
}

