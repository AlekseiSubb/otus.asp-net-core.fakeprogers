﻿using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerApi.Services.Implementations
{
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductCategoryService> _logger;

        public ProductCategoryService(
            IProductCategoryRepository productCategoryRepository,
            IMapper mapper,
            ILogger<ProductCategoryService> logger)
        {
            _productCategoryRepository = productCategoryRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ProductCategoryDto> GetProductCategoryByIdAsync(int id)
        {
            _logger.LogInformation("Начало получения ProductCategory");
            var item = await _productCategoryRepository.GetAsync(id);
            _logger.LogInformation("Конец получения ProductCategory");

            return _mapper.Map<ProductCategory, ProductCategoryDto>(item);
        }

        public async Task<IEnumerable<ProductCategoryDto>> GetProductCategoryAsync()
        {
            _logger.LogInformation("Начало получения списка ProductCategory");
            var items = await _productCategoryRepository.GetAllAsync();
            _logger.LogInformation("Конец получения списка ProductCategory");

            return _mapper.Map<IEnumerable<ProductCategory>, IEnumerable<ProductCategoryDto>>(items);
        }

        public async Task<int> CreateProductCategoryAsync(ProductCategoryDto productCategoryDto)
        {
            var model = _mapper.Map<ProductCategory>(productCategoryDto);
            
            _logger.LogInformation("Начало сохранения ProductCategory");
            ProductCategory createdItem = await _productCategoryRepository.AddAsync(model);
            _logger.LogInformation("Конец сохранения ProductCategory");

            return createdItem.Id;
        }

        public async Task<bool> UpdateProductCategoryAsync(ProductCategoryDto productCategoryDto)
        {
            ProductCategory item = await _productCategoryRepository.GetAsync(productCategoryDto.Id);
            item.Name = productCategoryDto.Name;
            item.Description = productCategoryDto.Description;

            bool result = await _productCategoryRepository.UpdateAsync(item);
            if (!result)
            {
                _logger.LogError("Ошибка обновления ProductCategory");
            }

            return result;

        }

        public async Task<bool> DeleteProductCategoryAsync(int id)
        {
            _logger.LogInformation("Удаление ProductCategory начало");
            bool result = await _productCategoryRepository.DeleteAsync(id);
            _logger.LogInformation("Удаление ProductCategory конец");

            if (!result)
            {
                _logger.LogError("Ошибка удаления ProductCategory");
            }
            return result;
        }
    }
}
