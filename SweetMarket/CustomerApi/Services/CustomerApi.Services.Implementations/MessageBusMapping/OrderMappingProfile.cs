﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Domain.Entities.Models;

namespace CustomerApi.Services.Implementations.Mapping;

internal class OrderMappingProfile : Profile
{
    public OrderMappingProfile()
    {
        CreateMap<Order, OrderCreateCommand>();
        CreateMap<Order, OrderUpdateCommand>();
    }
}

