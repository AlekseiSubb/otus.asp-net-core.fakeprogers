﻿using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Services.Implementations
{
    public class ShoppingSessionService : IShoppingSessionService
    {
        private readonly IShoppingSessionRepository _cartRepository;
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;
        private readonly ICustomerRepository  _customerRepository;


        public ShoppingSessionService(IShoppingSessionRepository cartRepository, 
                                    IProductRepository productRepository, 
                                    ICustomerRepository  customerRepository,
                                    IMapper mapper)
        {
            _cartRepository = cartRepository;
            _productRepository = productRepository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }
        /// Получить корзину по id клиента 
        public async Task<ShoppingSessionDto> GetByIdCustomerAsync(int id)
        {
            var item = await _cartRepository.GetByIdCustomerAsync(id);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }

        /// Получить все корзины по id клиента 
        public async Task<IEnumerable<ShoppingSessionDto>> GetAllByIdCustomerAsync(int id)
        {
            var item = await _cartRepository.GetAllByIdCustomerAsync(id);
            return _mapper.Map<IEnumerable<ShoppingSession>, IEnumerable<ShoppingSessionDto>>(item);
        }

        // Получить содержимое корзины id корзины
        public async Task<ShoppingSessionDto> GetByIdSessionAsync(int id)
        {
            var item = await _cartRepository.GetByIdSessionAsync(id);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }


        /// создать пустую корзину
        public async Task<ShoppingSessionDto> CreateAsync(int idCustomer)
        {
            // проверить что такой покупатель есть:
            var customer = await _customerRepository.GetAsync(idCustomer);
            if(customer == null)
                return null;

            var item = await _cartRepository.CreateAsync(idCustomer);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }


        //Добавить продукт в корзину
        public async Task<bool> AddProductAsync(int idShoppingSession, int addedProductId, int qu)
        {
            var product = await _productRepository.GetAsync(addedProductId);
            if (product == null)
                return false;

            var res = await _cartRepository.AddProductAsync(idShoppingSession, product, qu);
            if( res == null)
                return false;
            else 
                return true;
        }

        //Удалить позицию из корзины
        public async Task<ShoppingSessionDto> DeleteCartItemAsync(int idShoppingSession, int cartItemId)
        {
            //проверить что такая позиция есть в корзине - сделано в репозитории

            var item = await _cartRepository.DeleteCartItemAsync(idShoppingSession, cartItemId);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }

        /// Удалить все позиции из корзины
        public async Task<ShoppingSessionDto> DeleteCartItemsAsync(int idShoppingSession)
        {
            var item = await _cartRepository.DeleteCartItemsAsync(idShoppingSession);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }

        /// Удалить все корзиный для данного Заказчика
        public async Task<bool> DeleteCartByIdCustomer(int idCustomer)
        {
            return await _cartRepository.DeleteSessionsAsync(idCustomer);
        }

        /// Удалить все корзиный для данного Заказчика
        public async Task<bool> DeleteCartAsync(int idShoppingSession)
        {
            return await _cartRepository.DeleteAsync(idShoppingSession);
        }

        /// Изменить количество для позиции
        public async Task<ShoppingSessionDto> UpdateQuCartItemAsync(int idShoppingSession, int cartItemId, int qu)
        {
            var item = await _cartRepository.UpdateQuCartItemAsync(idShoppingSession, cartItemId, qu);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }

        /// Изменить количество для всех позиций
        public async Task<ShoppingSessionDto> UpdateQuCartItemsAsync(int idShoppingSession, int qu)
        {
            var item = await _cartRepository.UpdateQuCartItemsAsync(idShoppingSession, qu);
            return _mapper.Map<ShoppingSession, ShoppingSessionDto>(item);
        }

    }
}




