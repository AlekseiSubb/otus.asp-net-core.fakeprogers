﻿namespace Contracts.MessageBus.Commands
{
    public record OrderDeleteCommand
    {
        public int CustomerOrderId { get; init; }
        public int MarketId { get; init; }
    }
}
