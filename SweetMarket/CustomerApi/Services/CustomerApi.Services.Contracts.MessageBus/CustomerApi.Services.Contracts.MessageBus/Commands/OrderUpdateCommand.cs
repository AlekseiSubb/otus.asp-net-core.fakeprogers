﻿namespace Contracts.MessageBus.Commands
{
    public record OrderUpdateCommand
    {
        public int CustomerOrderId { get; init; }

        public int MarketId { get; init; }

        public string? Description { get; init; }

        public DateTime DateReady { get; set; }

        public ICollection<OrderItemCommand> OrderItems { get; set; } = new List<OrderItemCommand>();

    }
}
