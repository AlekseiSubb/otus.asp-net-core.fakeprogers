﻿using CustomerApi.Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerApi.Services.Abstractions;
public interface IShoppingSessionService
{
    Task<ShoppingSessionDto> CreateAsync(int idCustomer);
    Task<bool> AddProductAsync(int idShoppingSession, int addedProductId, int qu);
    Task<ShoppingSessionDto> DeleteCartItemAsync(int idShoppingSession, int cartItemId);
    Task<ShoppingSessionDto> DeleteCartItemsAsync(int idShoppingSession);
    Task<bool> DeleteCartByIdCustomer(int idCustomer);
    Task<bool> DeleteCartAsync(int idShoppingSession);
    Task<ShoppingSessionDto> GetByIdCustomerAsync(int id);
    Task<IEnumerable<ShoppingSessionDto>> GetAllByIdCustomerAsync(int id);
    Task<ShoppingSessionDto> GetByIdSessionAsync(int id);
    Task<ShoppingSessionDto> UpdateQuCartItemAsync(int idShoppingSession, int cartItemId, int qu);
    Task<ShoppingSessionDto> UpdateQuCartItemsAsync(int idShoppingSession, int qu);
}
