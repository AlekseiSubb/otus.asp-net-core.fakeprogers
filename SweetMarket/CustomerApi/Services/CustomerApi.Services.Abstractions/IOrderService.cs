﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Abstractions;
public interface IOrderService
{
    Task<int> CreateOrderAsync(CreateOrderDto orderDto);
    Task<int> CreateOrderAsyncOld(OrderDto orderDto);
    Task DeleteOrderAsync(int id);
    Task<OrderDto> GetOrderByIdAsync(int id);
    Task<IEnumerable<OrderDto>> GetOrdersAsync();
    Task<IEnumerable<OrderDto>> GetPaged(OrderFilterDto filterDto);
    Task UpdateOrderAsync(int id, OrderDto orderDto);
}
