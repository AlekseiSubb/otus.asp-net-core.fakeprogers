﻿using System.Threading.Tasks;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Abstractions
{
    public interface IProductReviewService
    {
        Task<ProductReviewDto> GetByIdOrTrowAsync(int id);

        Task<int> CreateAsync(ProductReviewCreateDto productReviewDto);

        Task<PagingDto<ProductReviewDto>> GetPagedAsync(PaginationFilterDto filterDto);
    }
}
