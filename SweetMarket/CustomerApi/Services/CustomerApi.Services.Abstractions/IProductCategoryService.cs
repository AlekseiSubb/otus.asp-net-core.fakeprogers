﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Abstractions;

public interface IProductCategoryService
{
    Task<ProductCategoryDto> GetProductCategoryByIdAsync(int id);
    Task<IEnumerable<ProductCategoryDto>> GetProductCategoryAsync();
    Task<int> CreateProductCategoryAsync(ProductCategoryDto productCategoryDto);
    Task<bool> UpdateProductCategoryAsync(ProductCategoryDto productCategoryDto);
    Task<bool> DeleteProductCategoryAsync(int id);
}
