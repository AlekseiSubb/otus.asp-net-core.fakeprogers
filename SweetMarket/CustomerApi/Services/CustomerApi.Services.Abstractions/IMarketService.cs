using System.Threading.Tasks;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;

namespace CustomerApi.Services.Abstractions
{
    /// <summary>
    /// Cервис работы с магазинами (интерфейс)
    /// </summary>
    public interface IMarketService
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов. </returns>
        Task<PagingDto<MarketShortDto>> GetPaged(PaginationFilterDto filterDto);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО магазина</returns>
        Task<MarketDto> GetById(int id);
        
        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="marketDto"></param>
        /// <returns></returns>
        Task<int> Create(MarketEditDto marketDto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="marketDto">ДТО магазина</param>
        Task<bool> Update(MarketEditDto marketDto);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<bool> Delete(int id);
    }
}