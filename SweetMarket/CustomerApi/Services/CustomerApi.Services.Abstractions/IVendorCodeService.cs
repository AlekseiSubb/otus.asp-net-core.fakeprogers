﻿using System;
using System.Threading.Tasks;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Abstractions;

public interface IVendorCodeService : IDisposable
{
    /// <summary>
    /// Генерирует код магазина
    /// </summary>
    /// <remarks>
    /// Длина кода – 5 символов
    /// Пока используется Id магазина.
    /// </remarks>
    /// <param name="dto"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public Task<string> GenerateMarketCode();

    public Task<string> GenerateProductCategoryCode();



    public Task<string> GenerateProductCode();

    public Task<string> GetVendorCode(VendorCodeDto dto);

    /// <summary>
    /// Парсит артикул
    /// </summary>
    /// <remarks>
    /// Пишется латиницей в верхнем регистре и цифрами 0-9 Запрещены символы: I, O, J, то есть всего 33 варианта на символ.
    /// Структура:
    /// 5 символов – магазин (28480320 вариантов), символы не должны повторяться
    /// 7 символов – категория в магазине (21531121920 вариантов, символы не должны повторяться)
    /// 6 символов – рандомное значение, символы могут повторяться (1291467969 вариантов)
    /// Длина – 18 символов
    /// </remarks>
    /// <returns></returns>
    public Task<VendorCodeDto> ParseCode(string code);
}
