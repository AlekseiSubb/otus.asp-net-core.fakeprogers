﻿using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;
using CustomerApi.Services.Contracts.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerApi.Services.Abstractions
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> GetProductAsync();
        Task<ProductDto> GetProductByIdAsync(int id);
        Task<PagingDto<ProductDto>> GetPaged(ProductCategoryFilterDto filterDto);
        Task<int> GetQuantity(int idCategory);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="productEditDto"></param>
        /// <returns></returns>
        Task<int> CreateAsync(ProductEditDto productEditDto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="productEditDto">ДТО продукта</param>
        Task<bool> UpdateAsync(ProductEditDto productEditDto);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<bool> DeleteAsync(int id);

        /// <summary>
        /// Публикация продукта
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<ProductLiteDto?> PublishIntoShop(int id);

        /// <summary>
        /// Снять с публикации
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<ProductLiteDto?> UnPublishIntoShop(int id);
    }
}
