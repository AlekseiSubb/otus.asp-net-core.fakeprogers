using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Abstractions;

/// <summary>
/// Сервис работы с покупателями
/// </summary>
public interface ICustomerService
{
    Task<CustomerLiteDto> GetCustomerByIdAsync(int id);
    Task<CustomerLiteDto> GetCustomerByUserIdAsync(int id);
    Task<ICollection<CustomerLiteDto>> GetCustomersAsync();
    Task<int> CreateCustomerAsync(CustomerEditDto customerEditDto);
    Task UpdateCustomerAsync(int id, CustomerEditDto customerEditDto);
    Task DeleteCustomerAsync(int id);
}