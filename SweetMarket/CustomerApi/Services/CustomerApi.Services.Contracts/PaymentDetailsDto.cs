﻿using System;

namespace CustomerApi.Services.Contracts
{
    public class PaymentDetailsDto
    {
        public int Id { get; set; }
        public int Card { get; set; }
        public decimal Value { get; set; }
        public string BankName { get; set; } = default!;
        public DateTimeOffset PaymentDate { get; set; }
    }
}
