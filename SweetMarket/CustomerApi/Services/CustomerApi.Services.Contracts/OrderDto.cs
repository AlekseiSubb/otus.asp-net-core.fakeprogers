﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Services.Contracts;
public class OrderDto
{
    public int Id { get; set; }
    public string OrderNumber { get; set; } = default!;
    public string Description { get; set; }
    public decimal Price { get; set; }
    public int? PaymentDetailsId { get; set; }
    public int? ShippingId { get; set; } //надо наверное создать
    public DateTime DateCreate { get; set; }
    public DateTime DateReady { get; set; }
    public PaymentDetailsDto PaymentDetails { get; set; }
    public ICollection<OrderItemDto> OrderItems { get; set; } = new List<OrderItemDto>();
    public int CustomerId { get; set; }

}

