﻿namespace CustomerApi.Services.Contracts
{
    public class ProductCategoryDto
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// Уникальный код категории внутри системы
        /// </summary>
        public string Code { get; set; } = default!;
    }
}
