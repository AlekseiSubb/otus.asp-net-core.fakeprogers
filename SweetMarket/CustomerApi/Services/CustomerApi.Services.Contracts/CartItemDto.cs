﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Services.Contracts;

public class CartItemDto
{
    public int Id { get; set; }
    public int Qu { get; set; }
    public int ProductId { get; set; }
    public string ProductName { get; set; }
    public string MarketName { get; set; }
    public decimal Price { get; set; }
}


