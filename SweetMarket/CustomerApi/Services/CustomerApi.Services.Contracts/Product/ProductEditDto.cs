﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerApi.Services.Contracts.Product
{
    public class ProductEditDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public Guid? ImageGuid { get; set; }

        public string Code { get; set; }

        public int MarketId { get; set; }

        public int? ProductCategoryId { get; set; }

        public bool IsPublishedInMarket { get; set; }

        public DateTime CreatedAt { get; set; }

        public int CreatedBy { get; set; }

        public bool IsDeleted { get; set; }
    }
}
