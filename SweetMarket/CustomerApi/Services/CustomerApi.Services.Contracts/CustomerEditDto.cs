namespace CustomerApi.Services.Contracts;

public class CustomerEditDto
{
    public string Name { get; set; }
    public string? Mail { get; set; }
    public int? CustomerAdressId { get; set; }

}