﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Services.Contracts
 {
        public class CreateOrderDto
        {
            public int IdShoppingSession { get; set; }
            public string Description { get; set; }
            public DateTime DateReady { get; set; }
            public string PaymentType { get; set; }
            public string Promocode { get; set; }
        }
}

