﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerApi.Services.Contracts.Market
{
    public class MarketEditDto
    {
        public int Id { get; init; }

        public string Name { get; init; }

        public string? Description { get; init; }

        //может быть картинку лучше не при создании маркета сразу крепить, а подгружать отдельной кнопкой?
        public Guid? ImageGuid { get; init; }

        public string Code { get; init; }
    }
}
