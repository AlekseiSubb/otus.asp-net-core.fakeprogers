﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Services.Contracts.Market
{
    /// <summary>
    /// ДТО магазина
    /// </summary>
    public class MarketDto
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; } = default!;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ссылка на хранилище картинок
        /// </summary>
        public Guid ImageGuid { get; set; }

        /// <summary>
        /// список продукции
        /// </summary>
        public List<int> Products { get; set; } = new List<int>();
        //public List<ProductDto> Products { get; set; } = new List<ProductDto>();

        /// <summary>
        /// список кондитеров
        /// </summary>
        public List<int> Сonfectioners { get; set; } = new List<int>();
        //public List<UserDto> Сonfectioners { get; set; } = new List<UserDto>();

        /// <summary>
        /// Уникальный код магазина внутри системы
        /// </summary>
        public string Code { get; set; } = default!;
    }
}