namespace CustomerApi.Services.Contracts
{
    public class MessageDto
    {
        public string Content { get; set; }
    }
}