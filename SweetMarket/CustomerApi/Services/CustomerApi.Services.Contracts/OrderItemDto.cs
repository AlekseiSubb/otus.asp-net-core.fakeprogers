﻿namespace CustomerApi.Services.Contracts;
public class OrderItemDto
{
    public int Id { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }
    public string? Comment { get; set; }
    public decimal Price { get; set; }
}

