﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerApi.Services.Contracts
{
    public class ProductCategoryFilterDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string MarketName { get; set; }

        public string CategoryName { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
