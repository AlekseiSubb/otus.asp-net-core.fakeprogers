﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Services.Contracts;
public class ShoppingSessionDto
{
    public int Id { get; set; }
    public int CustomerId { get; set; }
    public CustomerDto Customer { get; set; }
    public DateTime Created { get; set; }
    public ICollection<CartItemDto> CartItems { get; set; } 
}

