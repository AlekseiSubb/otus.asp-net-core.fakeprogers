﻿namespace CustomerApi.Services.Contracts;

public class OrderItemAddonDto
{
    public int Id { get; set; }
    public int AddonId { get; set; }
    public string? Comment { get; set; }
}

