﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Services.Contracts;

public class CustomerDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Mail { get; set; }
    public string CustomerAdress { get; set; }
}


