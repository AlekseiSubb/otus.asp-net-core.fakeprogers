﻿using System.Threading.Tasks;

namespace CustomerApi.Services.Repositories.Abstractions;
/// <summary>
/// Получение идентификаторов сущностей с уникальным кодом
/// </summary>
public interface ICodeEntityRepository
{
    public Task<int?> GetIdByCode(string code);
}
