﻿using System.Threading.Tasks;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Repositories.Abstractions
{
    public interface IProductReviewRepository : IRepository<ProductReview, int>
    {
        Task<PaginatedEntity<ProductReview, int>> GetPagedAsync(PaginationFilterDto filterDto);
    }
}
