using CustomerApi.Domain.Entities.Models;
using System.Threading.Tasks;

namespace CustomerApi.Services.Repositories.Abstractions;

public interface ICustomerRepository : IRepository<Customer, int>
{
    Task<Customer> GetByUserIdAsync(int UserId);
}