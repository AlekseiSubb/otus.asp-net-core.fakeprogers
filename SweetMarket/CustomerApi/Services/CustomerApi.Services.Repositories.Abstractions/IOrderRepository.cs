﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Repositories.Abstractions
{
    public interface IOrderRepository : IRepository<Order, int>
    {
        Task<List<Order>> GetPagedAsync(OrderFilterDto filterDto);
        Task<List<Order>> GetAllAsync();
        Task<Order> GetAsync(int id);
    }
}
