﻿using CustomerApi.Domain.Entities.Models;

namespace CustomerApi.Services.Repositories.Abstractions
{
    public interface IProductCategoryRepository : IRepository<ProductCategory, int>, ICodeEntityRepository
    {
    }
}
