﻿using CustomerApi.Domain.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CustomerApi.Services.Repositories.Abstractions;

public interface IShoppingSessionRepository
{
    Task<ShoppingSession> AddProductAsync(int idShoppingSession, Product addedProduct, int qu);
    Task<ShoppingSession> CreateAsync(int idCustomer);
    Task<ShoppingSession> DeleteCartItemAsync(int idShoppingSession, int cartItemId);
    Task<ShoppingSession> DeleteCartItemsAsync(int idShoppingSession);
    Task<bool> DeleteSessionsAsync(int idCustomer);
    Task<bool> DeleteAsync(int idShoppingSession);
    Task<ShoppingSession> GetByIdCustomerAsync(int idCustomer);
    Task<IEnumerable<ShoppingSession>> GetAllByIdCustomerAsync(int idCustomer);
    Task<ShoppingSession> GetByIdSessionAsync(int idShoppingSession);
    Task<ShoppingSession> UpdateQuCartItemAsync(int idShoppingSession, int cartItemId, int qu);
    Task<ShoppingSession> UpdateQuCartItemsAsync(int idShoppingSession, int qu);
}
