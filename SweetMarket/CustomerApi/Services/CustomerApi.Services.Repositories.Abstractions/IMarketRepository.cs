﻿using System.Threading.Tasks;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с магазинами
    /// </summary>
    public interface IMarketRepository : IRepository<Market, int>, ICodeEntityRepository
    {
        public Task<PaginatedEntity<Market, int>> GetPagedAsync(PaginationFilterDto filterDto);
    }
}
