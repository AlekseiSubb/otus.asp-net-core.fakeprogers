﻿using CustomerApi.Domain.Entities.Models;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Services.Repositories.Abstractions
{
    public interface IProductRepository : IRepository<Product, int>, ICodeEntityRepository
    {
        Task<PaginatedEntity<Product, int>> GetPagedAsync(ProductCategoryFilterDto filterDto);
        Task<int> GetQuantity(int idCategory);
    }
}
