﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;

namespace CustomerApi.ConsumersMapping
{
    public class ProductCategoryMappingsProfile : Profile
    {
        public ProductCategoryMappingsProfile()
        {
            CreateMap<ProductCategoryCreateCommand, ProductCategoryDto>();
            CreateMap<ProductCategoryUpdateCommand, ProductCategoryDto>();
        }
    }
}
