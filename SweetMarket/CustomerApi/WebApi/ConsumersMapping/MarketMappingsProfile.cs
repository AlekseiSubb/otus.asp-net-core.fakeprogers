﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Contracts.Market;

namespace CustomerApi.ConsumersMapping
{
    public class MarketMappingsProfile : Profile
    {
        public MarketMappingsProfile()
        {
            CreateMap<MarketCreateCommand, MarketEditDto>();
            CreateMap<MarketUpdateCommand, MarketEditDto>();
        }
    }
}
