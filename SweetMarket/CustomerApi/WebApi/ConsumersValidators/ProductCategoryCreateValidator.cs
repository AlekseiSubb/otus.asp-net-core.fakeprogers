﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class ProductCategoryCreateValidator : AbstractValidator<ProductCategoryCreateCommand>
    {
        public ProductCategoryCreateValidator()
        {
            ConfigureIdValidator();
            ConfigureNameValidator();
            ConfigureDescriptionValidator();
            ConfigureCodeValidator();
        }

        public void ConfigureIdValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.Id)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Id)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.Id)} должно быть не меньше {minIdValue}");
        }

        public void ConfigureNameValidator()
        {
            const int minLength = 3;
            const int maxLength = 256;
            RuleFor(s => s.Name)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть пустым")
                .MinimumLength(minLength)
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть не короче {minLength} символов")
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть длиннее {maxLength} символов");
        }

        public void ConfigureDescriptionValidator()
        {
            const int maxLength = 4096;
            RuleFor(s => s.Description)
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Description)} не должно быть длиннее {maxLength} символов");
        }

        public void ConfigureCodeValidator()
        {
            RuleFor(s => s.Code)
                .Length(7)
                .WithMessage(s => $"Поле {nameof(s.Code)} должно быть длинной в 7 символов");
        }
    }
}
