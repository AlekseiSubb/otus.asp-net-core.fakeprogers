﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class ProductDeleteValidator : AbstractValidator<ProductDeleteCommand>
    {
        public ProductDeleteValidator()
        {
            ConfigureIdValidator();
        }

        public void ConfigureIdValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.ProductId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.ProductId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.ProductId)} должно быть не меньше {minIdValue}");
        }
    }
}
