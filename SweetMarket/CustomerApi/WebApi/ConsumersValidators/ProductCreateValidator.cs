﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class ProductCreateValidator : AbstractValidator<ProductCreateCommand>
    {
        public ProductCreateValidator()
        {
            ConfigureIdValidator();
            ConfigureNameValidator();
            ConfigureDescriptionValidator();
            ConfigureCodeValidator();
            ConfigureMarketIdValidator();
        }

        public void ConfigureIdValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.Id)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Id)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.Id)} должно быть не меньше {minIdValue}");
        }

        public void ConfigureNameValidator()
        {
            const int minLength = 3;
            const int maxLength = 256;
            RuleFor(s => s.Name)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть пустым")
                .MinimumLength(minLength)
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть не короче {minLength} символов")
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть длиннее {maxLength} символов");
        }

        public void ConfigureDescriptionValidator()
        {
            const int maxLength = 4096;
            RuleFor(s => s.Description)
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Description)} не должно быть длиннее {maxLength} символов");
        }

        public void ConfigureCodeValidator()
        {
            RuleFor(s => s.Code)
                .Length(6)
                .WithMessage(s => $"Поле {nameof(s.Code)} должно быть длинной в 6 символов");
        }

        public void ConfigureMarketIdValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.MarketId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.MarketId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.MarketId)} должно быть не меньше {minIdValue}");
        }
    }
}
