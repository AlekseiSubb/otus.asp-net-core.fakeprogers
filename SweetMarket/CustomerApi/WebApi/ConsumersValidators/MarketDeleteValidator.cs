﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class MarketDeleteValidator : AbstractValidator<MarketDeleteCommand>
    {
        public MarketDeleteValidator()
        {
            ConfigureIdValidator();
        }

        public void ConfigureIdValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.MarketId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.MarketId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.MarketId)} должно быть не меньше {minIdValue}");
        }
    }
}
