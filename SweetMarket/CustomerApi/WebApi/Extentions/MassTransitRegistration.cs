﻿using CustomerApi.Consumers;
using CustomerApi.Domain.Entities.Models;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CustomerApi.Extentions
{
    public static class MassTransitRegistration
    {
        public static IServiceCollection AddMassTransitServices(this IServiceCollection serviceCollection, ConfigurationManager configurationManager)
        {
            AddServicesInternal(serviceCollection, configurationManager);

            return serviceCollection;
        }

        private static void AddServicesInternal(IServiceCollection serviceCollection, ConfigurationManager configurationManager)
        {
            serviceCollection.AddMassTransit(q =>
            {
                q.AddConsumer<MarketCreateConsumer>();
                q.AddConsumer<MarketDeleteConsumer>();
                q.AddConsumer<MarketUpdateConsumer>();

                q.AddConsumer<ProductCategoryCreateConsumer>();
                q.AddConsumer<ProductCategoryDeleteConsumer>();
                q.AddConsumer<ProductCategoryUpdateConsumer>();

                q.AddConsumer<ProductCreateConsumer>();
                q.AddConsumer<ProductDeleteConsumer>();
                q.AddConsumer<ProductPublishConsumer>();
                q.AddConsumer<ProductUnPublishConsumer>();
                q.AddConsumer<ProductUpdateConsumer>();

                q.UsingRabbitMq((context, config) =>
                {
                    config.Host(configurationManager["RabbitMq:Host"],
                        virtualHost: configurationManager["RabbitMq:VirtualHost"], q =>
                        {
                            q.Username(configurationManager["RabbitMq:Username"]);
                            q.Password(configurationManager["RabbitMq:Password"]);
                        });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.MarketCreateCommand", e =>
                    {
                        e.ConfigureConsumer<MarketCreateConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.MarketDeleteCommand", e =>
                    {
                        e.ConfigureConsumer<MarketDeleteConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.MarketUpdateCommand", e =>
                    {
                        e.ConfigureConsumer<MarketUpdateConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductCategoryCreateCommand", e =>
                    {
                        e.ConfigureConsumer<ProductCategoryCreateConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductCategoryDeleteCommand", e =>
                    {
                        e.ConfigureConsumer<ProductCategoryDeleteConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductCategoryUpdateCommand", e =>
                    {
                        e.ConfigureConsumer<ProductCategoryUpdateConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductCreateCommand", e =>
                    {
                        e.ConfigureConsumer<ProductCreateConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductDeleteCommand", e =>
                    {
                        e.ConfigureConsumer<ProductDeleteConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductPublishCommand", e =>
                    {
                        e.ConfigureConsumer<ProductPublishConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductUnPublishCommand", e =>
                    {
                        e.ConfigureConsumer<ProductUnPublishConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });

                    config.ReceiveEndpoint("Cmd.Customer.CustomerApi.ProductUpdateCommand", e =>
                    {
                        e.ConfigureConsumer<ProductUpdateConsumer>(context);
                        e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                    });
                });
            });
        }
    }
}
