using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.Repositories.Implementations;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Implementations;
using CustomerApi.Services.Repositories.Abstractions;

namespace CustomerApi.Extentions;

public static class AddCustomerExtentions
{
    public static IServiceCollection AddCustomerModel(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<IRepository<Customer, int>, Repository<Customer, int>>();
        serviceCollection.AddTransient<IRepository<CustomerAdress, int>, Repository<CustomerAdress, int>>();
        serviceCollection.AddTransient<IRepository<User, int>, Repository<User, int>>();
        serviceCollection.AddTransient<IRepository<Market, int>, Repository<Market, int>>();
        return serviceCollection;
    }

    public static IServiceCollection AddCustomerService(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<ICustomerService, CustomerService>();
        serviceCollection.AddScoped<ICustomerRepository, CustomerRepository>();
        return serviceCollection;
    }
}