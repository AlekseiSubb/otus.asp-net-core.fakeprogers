using System.IdentityModel.Tokens.Jwt;
using CustomerApi.Domain.Entities.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Infrastructure.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NLog.Web;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Implementations;
using CustomerApi.Services.Repositories.Abstractions;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using CustomerApi.Configuration;
using CustomerApi.Models;
using CustomerApi.Settings;
using static CustomerApi.Extentions.AddCustomerExtentions;
using CustomerApi.Extentions;
using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);

AddConfiguration(builder);

//���� ������
builder.Services.AddDbContext<DatabaseContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("PostgreSQL"),
    q => q.MigrationsAssembly("Infrastructure.EntityFramework"));
    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true); //�������� UTC-�����.
    //options.UseSqlServer(builder.Configuration.GetConnectionString("SqlServer"));
    //options.UseSqlite(builder.Configuration.GetConnectionString("SqLite"));
    //options.UseLazyLoadingProxies(); // lazy loading
});

builder.Host.UseNLog(new NLogAspNetCoreOptions
{
    //RemoveLoggerFactoryFilter = false, //�� ��������� true, �� ���� ������� ����������� �������, �� � ��, �� ��� �� ������.
    //ReplaceLoggerFactory = true, //������� ��������� �������
});


builder.Services.AddHealthChecks()
    .AddDbContextCheck<DatabaseContext>();


JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
        options.Audience = "sweetmarket_customer_api";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new()
        {
            RoleClaimType = "role",
            ValidTypes = new[] { "at+jwt" }
        };
    });


RegisterAppServices(builder);

builder.Services.AddMassTransitServices(builder.Configuration);

builder.Services.AddControllers();

//������������� �� ������������ ������������ ��. ���� �� ������
//https://docs.fluentvalidation.net/en/latest/aspnet.html
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddFluentValidationClientsideAdapters();
builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();
builder.Services.Configure<SwaggerGenOptions>(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Title",
        Description = "Description",
        Contact = new OpenApiContact
        {
            Name = "Example Contact",
            Url = new Uri("https://example.com/contact")
        },
        License = new OpenApiLicense
        {
            Name = "Example License",
            Url = new Uri("https://example.com/license")
        },
        Version = "v1"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/authorize"),
                TokenUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/token"),
                Scopes = new Dictionary<string, string>
                {
                    {"openid", ""},
                    {"email", ""},
                    {"profile", ""},
                    {"roles", ""},
                    {"sweetmarket_customer_api.fullaccess", ""}
                },
            },
        },
    });
    options.OperationFilter<AuthorizeCheckOperationFilter>();
});

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseSwagger();

app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "CustomerAPI V1");
    c.RoutePrefix = string.Empty;
    c.OAuthClientId("sweetmarket_web_frontend_client");
    c.OAuthClientSecret("LwZjqN6XCPCtnt0ZKYUiD1jGbRbe2aepqtGfxsuk9D");
    c.EnablePersistAuthorization();
});

//app.UseCors(); //���� �� ����, ����� ��������

app.UseRouting();
//app.UseHttpsRedirection();
//app.UseHealthChecks("/health");

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

//����� ����� � �����, � ����� � ���:
//var provider = app.Services.GetService<IMapper<??>>().ConfigurationProvider;
//provider.AssertConfigurationIsValid();
using (var serviceScope = app.Services.CreateScope())
{
    var initService = serviceScope.ServiceProvider.GetService<ILoadFirstDataService>();
    initService?.InitializeDb();
}
app.Run();

void RegisterAppServices(WebApplicationBuilder webApplicationBuilder)
{
    webApplicationBuilder.Services.AddScoped<ILoadFirstDataService, LoadFirstDataService>();

    webApplicationBuilder.Services.AddScoped<IValidator<OrderFilterModel>, OrderFilterModelValidator>();

    //����������� ��������
    webApplicationBuilder.Services.AddScoped<IMarketRepository, MarketRepository>();

    //����������� �� �������
    webApplicationBuilder.Services.AddTransient<IOrderRepository, OrderRepository>();
    webApplicationBuilder.Services.AddTransient<IRepository<OrderItem, int>, Repository<OrderItem, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<OrderItemAddon, int>, Repository<OrderItemAddon, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<PaymentDetails, int>, Repository<PaymentDetails, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<Shipping, int>, Repository<Shipping, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<ShoppingSession, int>, Repository<ShoppingSession, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<CartItem, int>, Repository<CartItem, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<CartItemAddon, int>, Repository<CartItemAddon, int>>();

    //����������� �� �������
    webApplicationBuilder.Services.AddTransient<IShoppingSessionRepository, ShoppingSessionRepository>();

    //����������� �� ��������
    webApplicationBuilder.Services.AddScoped<IProductRepository, ProductRepository>();
    webApplicationBuilder.Services.AddTransient<IRepository<ProductReview, int>, Repository<ProductReview, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<Addon, int>, Repository<Addon, int>>();
    webApplicationBuilder.Services.AddScoped<IProductCategoryRepository, ProductCategoryRepository>();
    webApplicationBuilder.Services.AddTransient<IProductService, ProductService>();


    //�������
    webApplicationBuilder.Services.AddCustomerModel() //����������� �� ���������
        .AddCustomerService();
    webApplicationBuilder.Services.AddTransient<IProductCategoryService, ProductCategoryService>();
    webApplicationBuilder.Services.AddTransient<IOrderService, OrderService>();

    webApplicationBuilder.Services.AddTransient<IProductReviewService, ProductReviewService>();
    webApplicationBuilder.Services.AddTransient<IProductReviewRepository, ProductReviewRepository>();
    webApplicationBuilder.Services.AddTransient<IShoppingSessionService, ShoppingSessionService>();

    //������� ��������
    webApplicationBuilder.Services.AddTransient<IMarketService, MarketService>();

    webApplicationBuilder.Services.AddTransient<IVendorCodeService, VendorCodeService>();
}

void AddConfiguration(WebApplicationBuilder builder1)
{
    //Configuration
    //�������� ��������� �� ��������� (��������, �� �����, ������ �� ���: https://learn.microsoft.com/ru-ru/aspnet/core/fundamentals/configuration/?view=aspnetcore-7.0#cp)
    builder1.Configuration.Sources.Clear();
    //��������� ����
    builder1.Configuration
        .AddJsonFile("appsettings.json")
        .AddEnvironmentVariables()
        .AddUserSecrets<Program>();
    builder1.Services.Configure<ApplicationSettings>(builder1.Configuration
        .GetSection("")); //��� ������� ���������� ������
}
