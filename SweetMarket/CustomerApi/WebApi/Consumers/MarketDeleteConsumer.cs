﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts.Market;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class MarketDeleteConsumer : IConsumer<MarketDeleteCommand>
    {
        private readonly IMarketService _marketService;
        private readonly IMapper _mapper;
        private readonly ILogger<MarketDeleteConsumer> _logger;
        private readonly IValidator<MarketDeleteCommand> _validator;

        public MarketDeleteConsumer(
            IMarketService marketService,
            IMapper mapper,
            ILogger<MarketDeleteConsumer> logger,
            IValidator<MarketDeleteCommand> validator)
        {
            _marketService = marketService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<MarketDeleteCommand> context)
        {
            _logger.LogInformation("Обработка сообщения MarketDeleteCommand начало");

            MarketDeleteCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации MarketDeleteCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение MarketDeleteCommand");
            }

            await _marketService.Delete(message.MarketId);

            _logger.LogInformation("Обработка сообщения MarketDeleteCommand конец");
        }
    }
}
