﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductCategoryCreateConsumer : IConsumer<ProductCategoryCreateCommand>
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductCategoryCreateConsumer> _logger;
        private readonly IValidator<ProductCategoryCreateCommand> _validator;

        public ProductCategoryCreateConsumer(
            IProductCategoryService productCategoryService,
            IMapper mapper,
            ILogger<ProductCategoryCreateConsumer> logger,
            IValidator<ProductCategoryCreateCommand> validator
            )
        {
            _productCategoryService = productCategoryService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductCategoryCreateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductCategoryCreateCommand начало");

            ProductCategoryCreateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductCategoryCreateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductCategoryCreateCommand");
            }

            ProductCategoryDto productCategoryDto = _mapper.Map<ProductCategoryCreateCommand, ProductCategoryDto>(message);

            await _productCategoryService.CreateProductCategoryAsync(productCategoryDto);

            _logger.LogInformation("Обработка сообщения ProductCategoryCreateCommand конец");
        }
    }
}
