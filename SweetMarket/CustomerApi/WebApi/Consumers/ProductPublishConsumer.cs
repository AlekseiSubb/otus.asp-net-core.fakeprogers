﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductPublishConsumer : IConsumer<ProductPublishCommand>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductPublishConsumer> _logger;
        private readonly IValidator<ProductPublishCommand> _validator;

        public ProductPublishConsumer(
            IProductService productService,
            IMapper mapper,
            ILogger<ProductPublishConsumer> logger,
            IValidator<ProductPublishCommand> validator
            )
        {
            _productService = productService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductPublishCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductPublishCommand начало");

            ProductPublishCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductPublishCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductPublishCommand");
            }

            await _productService.PublishIntoShop(message.ProductId);

            _logger.LogInformation("Обработка сообщения ProductPublishCommand конец");
        }
    }
}
