﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts.Market;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class MarketUpdateConsumer : IConsumer<MarketUpdateCommand>
    {
        private readonly IMarketService _marketService;
        private readonly IMapper _mapper;
        private readonly ILogger<MarketUpdateCommand> _logger;
        private readonly IValidator<MarketUpdateCommand> _validator;

        public MarketUpdateConsumer(
            IMarketService marketService,
            IMapper mapper,
            ILogger<MarketUpdateCommand> logger,
            IValidator<MarketUpdateCommand> validator)
        {
            _marketService = marketService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<MarketUpdateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения MarketUpdateCommand начало");

            MarketUpdateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации MarketUpdateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение MarketUpdateCommand");
            }

            MarketEditDto marketEditDto = _mapper.Map<MarketUpdateCommand, MarketEditDto>(message);

            await _marketService.Update(marketEditDto);

            _logger.LogInformation("Обработка сообщения MarketUpdateCommand конец");
        }
    }
}
