﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductUnPublishConsumer : IConsumer<ProductUnPublishCommand>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductUnPublishConsumer> _logger;
        private readonly IValidator<ProductUnPublishCommand> _validator;

        public ProductUnPublishConsumer(
            IProductService productService,
            IMapper mapper,
            ILogger<ProductUnPublishConsumer> logger,
            IValidator<ProductUnPublishCommand> validator
            )
        {
            _productService = productService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductUnPublishCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductUnPublishCommand начало");

            ProductUnPublishCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductUnPublishCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductUnPublishCommand");
            }

            await _productService.UnPublishIntoShop(message.ProductId);

            _logger.LogInformation("Обработка сообщения ProductUnPublishCommand конец");
        }
    }
}
