﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts.Market;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class MarketCreateConsumer : IConsumer<MarketCreateCommand>
    {
        private readonly IMarketService _marketService;
        private readonly IMapper _mapper;
        private readonly ILogger<MarketCreateConsumer> _logger;
        private readonly IValidator<MarketCreateCommand> _validator;

        public MarketCreateConsumer(
            IMarketService marketService, 
            IMapper mapper, 
            ILogger<MarketCreateConsumer> logger,
            IValidator<MarketCreateCommand> validator)
        {
            _marketService = marketService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<MarketCreateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения MarketCreateCommand начало");

            MarketCreateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);
           
            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации MarketCreateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение MarketCreateCommand");
            }

            MarketEditDto marketEditDto = _mapper.Map<MarketCreateCommand, MarketEditDto>(message);

            await _marketService.Create(marketEditDto);

            _logger.LogInformation("Обработка сообщения MarketCreateCommand конец");
        }
    }
}
