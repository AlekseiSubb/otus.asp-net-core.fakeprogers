﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductCategoryDeleteConsumer: IConsumer<ProductCategoryDeleteCommand>
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductCategoryDeleteConsumer> _logger;
        private readonly IValidator<ProductCategoryDeleteCommand> _validator;

        public ProductCategoryDeleteConsumer(
            IProductCategoryService productCategoryService,
            IMapper mapper,
            ILogger<ProductCategoryDeleteConsumer> logger,
            IValidator<ProductCategoryDeleteCommand> validator

            )
        {
            _productCategoryService = productCategoryService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductCategoryDeleteCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductCategoryDeleteCommand начало");

            ProductCategoryDeleteCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductCategoryDeleteCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductCategoryDeleteCommand");
            }

            await _productCategoryService.DeleteProductCategoryAsync(message.ProductId);

            _logger.LogInformation("Обработка сообщения ProductCategoryDeleteCommand конец");
        }
    }
}
