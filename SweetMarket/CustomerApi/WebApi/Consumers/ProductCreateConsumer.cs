﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Product;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductCreateConsumer : IConsumer<ProductCreateCommand>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductCreateConsumer> _logger;
        private readonly IValidator<ProductCreateCommand> _validator;

        public ProductCreateConsumer(
            IProductService productService,
            IMapper mapper,
            ILogger<ProductCreateConsumer> logger,
            IValidator<ProductCreateCommand> validator
            )
        {
            _productService = productService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductCreateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductCreateCommand начало");

            ProductCreateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductCreateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductCreateCommand");
            }

            ProductEditDto productCategoryDto = _mapper.Map<ProductCreateCommand, ProductEditDto>(message);

            await _productService.CreateAsync(productCategoryDto);

            _logger.LogInformation("Обработка сообщения ProductCreateCommand конец");
        }
    }
}
