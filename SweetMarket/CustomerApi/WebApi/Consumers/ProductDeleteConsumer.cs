﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts.Product;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductDeleteConsumer : IConsumer<ProductDeleteCommand>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductDeleteConsumer> _logger;
        private readonly IValidator<ProductDeleteCommand> _validator;

        public ProductDeleteConsumer(
            IProductService productService,
            IMapper mapper,
            ILogger<ProductDeleteConsumer> logger,
            IValidator<ProductDeleteCommand> validator
            )
        {
            _productService = productService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductDeleteCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductDeleteCommand начало");

            ProductDeleteCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductDeleteCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductDeleteCommand");
            }

            await _productService.DeleteAsync(message.ProductId);

            _logger.LogInformation("Обработка сообщения ProductDeleteCommand конец");
        }
    }
}
