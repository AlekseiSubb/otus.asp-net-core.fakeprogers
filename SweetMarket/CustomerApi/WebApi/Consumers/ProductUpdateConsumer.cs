﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts.Product;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductUpdateConsumer : IConsumer<ProductUpdateCommand>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductUpdateConsumer> _logger;
        private readonly IValidator<ProductUpdateCommand> _validator;

        public ProductUpdateConsumer(
            IProductService productService,
            IMapper mapper,
            ILogger<ProductUpdateConsumer> logger,
            IValidator<ProductUpdateCommand> validator
            )
        {
            _productService = productService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductUpdateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductUpdateCommand начало");

            ProductUpdateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductUpdateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductUpdateCommand");
            }

            ProductEditDto productCategoryDto = _mapper.Map<ProductUpdateCommand, ProductEditDto>(message);

            await _productService.UpdateAsync(productCategoryDto);

            _logger.LogInformation("Обработка сообщения ProductUpdateCommand конец");
        }
    }
}
