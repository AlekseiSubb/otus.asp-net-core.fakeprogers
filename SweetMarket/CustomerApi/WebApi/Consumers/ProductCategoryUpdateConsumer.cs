﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class ProductCategoryUpdateConsumer : IConsumer<ProductCategoryUpdateCommand>
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductCategoryUpdateConsumer> _logger;
        private readonly IValidator<ProductCategoryUpdateCommand> _validator;

        public ProductCategoryUpdateConsumer(
            IProductCategoryService productCategoryService,
            IMapper mapper,
            ILogger<ProductCategoryUpdateConsumer> logger,
            IValidator<ProductCategoryUpdateCommand> validator
            )
        {
            _productCategoryService = productCategoryService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<ProductCategoryUpdateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения ProductCategoryUpdateCommand начало");

            ProductCategoryUpdateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации ProductCategoryUpdateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение ProductCategoryUpdateCommand");
            }

            ProductCategoryDto productCategoryDto = _mapper.Map<ProductCategoryUpdateCommand, ProductCategoryDto>(message);

            await _productCategoryService.UpdateProductCategoryAsync(productCategoryDto);

            _logger.LogInformation("Обработка сообщения ProductCategoryUpdateCommand конец");
        }
    }
}
