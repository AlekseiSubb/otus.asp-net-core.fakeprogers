﻿using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Models.ProductReview;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    public class ProductReviewMappingsProfile : Profile
    {
        public ProductReviewMappingsProfile()
        {
            CreateMap<ProductReviewCreateModel, ProductReviewCreateDto>();

            CreateMap<ProductReviewDto, ProductReviewDetailsModel>();

            CreateMap<PagingDto<ProductReviewDto>, PagingModel<ProductReviewDetailsModel>>();
        }
    }
}
