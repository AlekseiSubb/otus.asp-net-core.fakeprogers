using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности корзиный.
    /// </summary>
    public class ShoppingSessionProfile : Profile
    {
        public ShoppingSessionProfile()
        {
            CreateMap<ShoppingSessionDto, ShoppingSessionModel>().ReverseMap();
            CreateMap<CustomerDto, CustomerModel>().ReverseMap();
            CreateMap<CartItemDto, CartItemModel>().ReverseMap();
        }
    }
}
