using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности заказа.
    /// </summary>
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<CustomerEditModel, CustomerEditDto>().ReverseMap();
            CreateMap<CustomerLiteModel, CustomerLiteDto>().ReverseMap();
        }
    }
}
