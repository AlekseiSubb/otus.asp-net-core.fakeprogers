using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности заказа.
    /// </summary>
    public class OrderMappingsProfile : Profile
    {
        public OrderMappingsProfile()
        {
            CreateMap<OrderDto, OrderModel>().ReverseMap();
            CreateMap<OrderItemDto, OrderItemShortModel>().ReverseMap();
            CreateMap<PaymentDetailsDto, PaymentDetailsModel>().ReverseMap();

            CreateMap<OrderShortModel, OrderDto>();
            CreateMap<OrderFilterModel, OrderFilterDto>();

            CreateMap<CreateOrderModel, CreateOrderDto>();

        }
    }
}
