﻿using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности категории продуктов
    /// </summary>
    public class ProductCategoryMappingProfile : Profile
    {
        public ProductCategoryMappingProfile()
        {
            CreateMap<ProductCategoryModel, ProductCategoryDto>();
            CreateMap<ProductCategoryDto, ProductCategoryModel>();
        }
    }
}
