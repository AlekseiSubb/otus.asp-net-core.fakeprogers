﻿using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Product;

namespace CustomerApi.Mapping
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductDto, ProductModel>().ReverseMap();
            CreateMap<ProductDto, ProductEditModel>().ReverseMap();
            CreateMap<PagingDto<ProductDto>, PagingModel<ProductModel>>();
            CreateMap<PagingDto<ProductDto>, PagingModel<ProductModel>>();
            CreateMap<ProductCategoryFilterDto, ProductCategoryFilterModel>().ReverseMap();
        }
    }
}
