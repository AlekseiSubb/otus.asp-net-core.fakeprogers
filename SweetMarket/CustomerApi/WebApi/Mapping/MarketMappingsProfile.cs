using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для магазина.
    /// </summary>
    public class MarketMappingsProfile : Profile
    {
        public MarketMappingsProfile()
        {
            CreateMap<MarketModel, MarketDto>();
            CreateMap<MarketDto, MarketModel>();
            CreateMap<MarketShortModel, MarketShortDto>();

            CreateMap<PagingDto<MarketShortDto>, PagingModel<MarketShortDto>>();
        }
    }
}
