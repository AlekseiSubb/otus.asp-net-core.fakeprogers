using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для магазина.
    /// </summary>
    public class VendorCodeMappingsProfile : Profile
    {
        public VendorCodeMappingsProfile()
        {
            CreateMap<VendorCodeModel, VendorCodeDto>();
            CreateMap<VendorCodeDto, VendorCodeModel>();
        }
    }
}
