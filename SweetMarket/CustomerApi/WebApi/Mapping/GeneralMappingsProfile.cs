using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для фильтра пейджинации.
    /// </summary>
    public class GeneralMappingsProfile : Profile
    {
        public GeneralMappingsProfile()
        {
            CreateMap<PaginationFilterModel, PaginationFilterDto>().ReverseMap();
        }
    }
}
