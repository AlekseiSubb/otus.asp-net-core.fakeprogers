﻿using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductCatalogController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductService _productService;

        public ProductCatalogController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список карточек продуктов самых популярных товаров
        /// (запрос с пагинацией, указано количество на странице и отступ от начала)
        /// </summary>
        [HttpPost("list")]
        public async Task<IActionResult> GetPopular(ProductCategoryFilterModel filterModel)
        {
            var filterDto = _mapper.Map<ProductCategoryFilterModel, ProductCategoryFilterDto>(filterModel);
            var items = await _productService.GetPaged(filterDto);
            return Ok(_mapper.Map<PagingModel<ProductModel>>(items));
        }
        /// <summary>
        /// Возвращает количество товаров по категории
        /// </summary>
        [HttpGet()]
        public async Task<IActionResult> GetQuantity(int idCategory)
        {
            var qu = await _productService.GetQuantity(idCategory);
            return Ok(qu);
        }
    }
}
