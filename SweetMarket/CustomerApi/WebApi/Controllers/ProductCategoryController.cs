using AutoMapper;
using CustomerApi.Models;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using Microsoft.AspNetCore.Authorization;

namespace CustomerApi.Controllers
{
    /// <summary>
    /// Контроллер для просмотра справочника типов продукции
    /// </summary>

    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductCategoryController : ControllerBase
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IMapper _mapper;
        private readonly IVendorCodeService _vendorCodeService;

        public ProductCategoryController(IProductCategoryService productCategoryService, IMapper mapper, IVendorCodeService vendorCodeService)
        {
            _productCategoryService = productCategoryService;
            _mapper = mapper;
            _vendorCodeService = vendorCodeService;
        }

        /// <summary>
        /// Получить категорию товаров
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var item = await _productCategoryService.GetProductCategoryByIdAsync(id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ProductCategoryDto, ProductCategoryModel>(item));
        }

        /// <summary>
        /// Получить список категорий товаров
        /// </summary>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            var items = await _productCategoryService.GetProductCategoryAsync();
            return Ok(_mapper.Map<List<ProductCategoryModel>>(items));
        }

        /// <summary>
        /// Добавить категорию товаров
        /// </summary>
        //[HttpPost]
        internal async Task<IActionResult> Add(ProductCategoryModel productCategoryModel)
        {
            var validator = new ProductCategoryValidator();
            var validationResult = validator.Validate(productCategoryModel);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);
            var model = _mapper.Map<ProductCategoryDto>(productCategoryModel);
            model.Code = await _vendorCodeService.GenerateProductCategoryCode();
            return Ok(await _productCategoryService.CreateProductCategoryAsync(model));
        }

        /// <summary>
        /// Удалить категорию товаров
        /// </summary>
        //[HttpDelete("{id}")]
        internal async Task<IActionResult> Delete(int id)
        {
            await _productCategoryService.DeleteProductCategoryAsync(id);
            return Ok();
        }
    }
}
