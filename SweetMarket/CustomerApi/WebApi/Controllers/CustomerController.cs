using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using AutoMapper;
using CustomerApi.Models;

namespace CustomerApi.Controllers;

/// <inheritdoc />
[ApiController]
[Route("api/v1/[controller]")]
[Authorize]
public class CustomerController : Controller
{
    private readonly ILogger _logger;
    private readonly ICustomerService _customerService;
    private readonly IMapper _mapper;

    /// <inheritdoc />
    public CustomerController(ICustomerService customerService, ILogger<CustomerController> logger, IMapper mapper)
    {
        _customerService = customerService; 
        _logger = logger;
        _mapper = mapper;
    }

    /// <summary>
    /// TEST
    /// </summary>
    //[HttpGet("TEST")]
    internal void Get()
    {
        _logger.LogTrace("ttrrcc");
        _logger.LogDebug("ddbbgg");
        _logger.LogInformation("iinnff");
        _logger.LogWarning("wwaarr");
        _logger.LogError("eerrrr");
        _logger.LogCritical("ccrrii");
    }

    /// <summary>
    /// TEST
    /// </summary>
    //[HttpGet("TESTEX")]
    internal void Get1()
    {
        throw new Exception("TEST EX");
    }


    /// <summary>
    /// Получить покупателя по индентификатору
    /// </summary>
    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        var item = await _customerService.GetCustomerByIdAsync(id);

        if (item  == null)
            return NotFound();

        return Ok(_mapper.Map<CustomerLiteModel>(item));
    }

    /// <summary>
    /// Получить покупателя по userId
    /// </summary>
    [HttpGet("by_user_id/{id}")]
    public async Task<IActionResult> GetByUserId(int id)
    {
        var item = await _customerService.GetCustomerByUserIdAsync(id);

        if (item == null)
            return NotFound();

        return Ok(_mapper.Map<CustomerLiteModel>(item));
    }

    /// <summary>
    /// Получить список покупателей
    /// </summary>
    [HttpGet("list")]
    public async Task<IActionResult> GetAll()
    {
        var items = await _customerService.GetCustomersAsync();
        return Ok(_mapper.Map<List<CustomerLiteModel>>(items));
    }

    /// <summary>
    /// Добавить покупателя
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> Add(CustomerEditModel customerEditModel)
    {
        var dto = _mapper.Map<CustomerEditDto>(customerEditModel);
        return Ok(await _customerService.CreateCustomerAsync(dto));
    }

    /// <summary>
    /// Изменить покупателя
    /// </summary>
    [HttpPut("{id}")]
    public async Task<IActionResult> Edit(int id, CustomerEditModel customerEditModel)
    {
        var dto = _mapper.Map<CustomerEditDto>(customerEditModel);
        await _customerService.UpdateCustomerAsync(id, dto);
        return Ok();
    }

    /// <summary>
    /// Удалить покупателя
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _customerService.DeleteCustomerAsync(id);
        return Ok();
    }
}