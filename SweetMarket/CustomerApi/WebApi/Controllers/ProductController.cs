﻿using AutoMapper;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Models;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Repositories.Abstractions;
using CustomerApi.Services.Contracts.Product;

namespace CustomerApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductService _productService;

        public ProductController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить продукт по Id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var product = await _productService.GetProductByIdAsync(id);

            if (product is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ProductDto, ProductModel>(product));
        }

        /// <summary>
        /// Получить список всех продуктов товаров для тестирования
        /// </summary>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            var items = await _productService.GetProductAsync();
            return Ok(_mapper.Map<List<ProductModel>>(items));
        }

        ///// <summary>
        ///// Добавить новый продукт
        ///// </summary>
        //[HttpPost]
        //public async Task<IActionResult> Add(ProductModel productModel)
        //{
        //    var validator = new ProductValidator();
        //    var validationResult = validator.Validate(productModel);
        //    if (!validationResult.IsValid)
        //        return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);

        //    var model = _mapper.Map<ProductDto>(productModel);
        //    int newId = await _productService.CreateAsync(model);
        //    if (newId == 0)
        //        return BadRequest();

        //    return Ok(newId);
        //}

        ///// <summary>
        ///// Изменить продукт
        ///// </summary>
        //[HttpPut("{id}")]
        //public async Task<IActionResult> Edit(int id, ProductEditModel productModel)
        //{
        //    var product = await _productService.GetProductByIdAsync(id);

        //    if (product is null)
        //    {
        //        return NotFound();
        //    }
        //    var validator = new ProductValidator();
        //    var validationResult = validator.Validate(_mapper.Map<ProductModel>(productModel));
        //    if (!validationResult.IsValid)
        //        return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);


        //    await _productService.UpdateProductAsync(id, _mapper.Map<ProductDto>(productModel));
        //    return Ok();
        //}

        ///// <summary>
        ///// Удалить продукт
        ///// </summary>
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    await _productService.DeleteProductAsync(id);
        //    return Ok();
        //}
    }
}
