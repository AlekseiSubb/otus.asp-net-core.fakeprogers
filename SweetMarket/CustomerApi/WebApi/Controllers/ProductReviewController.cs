﻿using AutoMapper;
using CustomerApi.Models;
using CustomerApi.Models.ProductReview;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;

namespace CustomerApi.Controllers
{
    /// <summary> Отзывы на товары </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductReviewController : ControllerBase
    {
        private readonly IProductReviewService _productReviewService;
        private readonly IMapper _mapper;

        public ProductReviewController(IProductReviewService productReviewService, IMapper mapper)
        {
            _productReviewService = productReviewService;
            _mapper = mapper;
        }

        /// <summary> API функция "добавить новый отзыв на продукт" </summary>
        [HttpPost]
        public async Task<IActionResult> Add(ProductReviewCreateModel productReviewCreateModel)
        {

            ProductReviewCreateDto productReviewDto = _mapper.Map<ProductReviewCreateDto>(productReviewCreateModel);

            int newProductReviewId = await _productReviewService.CreateAsync(productReviewDto);

            ProductReviewDto newProductReview = await _productReviewService.GetByIdOrTrowAsync(newProductReviewId);

            return Ok(_mapper.Map<ProductReviewDetailsModel>(newProductReview));
        }

        /// <summary> API функция "получить список продуктов с пагинацией" </summary>
        [HttpGet("list")]
        public async Task<IActionResult> GetList([FromQuery] PaginationFilterModel filterModel)
        {
            PaginationFilterDto filterDto = _mapper.Map<PaginationFilterModel, PaginationFilterDto>(filterModel);
            PagingDto<ProductReviewDto> result = await _productReviewService.GetPagedAsync(filterDto);
            return Ok(_mapper.Map<PagingModel<ProductReviewDetailsModel>>(result));
        }
    }
}
