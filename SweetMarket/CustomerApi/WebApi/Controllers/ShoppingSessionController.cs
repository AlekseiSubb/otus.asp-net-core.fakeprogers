﻿using System.Net;
using AutoMapper;
using CustomerApi.Models;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using Microsoft.AspNetCore.Authorization;

namespace CustomerApi.Controllers
{
    /// <summary>
    /// Контроллер заказов
    /// </summary>
    [ApiController]
    [Route("/api/v1/[controller]")]
    [Authorize]
    public class ShoppingSessionController : ControllerBase
    {
        private readonly IShoppingSessionService _cartService;
        private readonly IMapper _mapper;
        //private readonly IValidator<OrderFilterModel> _orderFilterModelValidator;

        public ShoppingSessionController(IShoppingSessionService cartService, IMapper mapper)
        {
            _cartService = cartService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить корзину по id клиента 
        /// </summary>
        [HttpGet("GetByIdCastomer")]
        [ProducesResponseType(typeof(IEnumerable<ShoppingSessionModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetByIdCastomer(int id)
        {
            var item = await _cartService.GetByIdCustomerAsync(id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ShoppingSessionModel>(item));
        }

        /// <summary>
        /// Получить все корзины(и удаленные) по id клиента 
        /// </summary>
        [HttpGet("GetAllByIdCastomer")]
        [ProducesResponseType(typeof(IEnumerable<List<ShoppingSessionModel>>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllByIdCastomer(int id)
        {
            var items = await _cartService.GetAllByIdCustomerAsync(id);

            if (items.Count() == 0)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<List<ShoppingSessionModel>>(items));
        }

        /// <summary>
        /// Получить содержимое корзины id корзины
        /// </summary>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<ShoppingSessionModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetByIdCart(int id)
        {
            var item = await _cartService.GetByIdSessionAsync(id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ShoppingSessionModel>(item));
        }

        /// <summary>
        /// Cоздать пустую корзину
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(int idCustomer)
        {
            var item = await _cartService.CreateAsync(idCustomer);

            if (item == null)
                return BadRequest();
            else
                return Ok(_mapper.Map<ShoppingSessionModel>(item));
        }

        /// <summary>
        /// Добавить продукт в корзину
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <param name="addedProductId"></param>
        /// <param name="qu"></param>
        /// <returns></returns>
        [HttpPost("items")]
        public async Task<IActionResult> AddProduct(int idShoppingSession, int addedProductId, int qu)
        {
            var res = await _cartService.AddProductAsync(idShoppingSession, addedProductId, qu);

            if (!res)
                return NotFound();
            else
                return Ok();
        }

        /// <summary>
        /// Изменить количество для позиции
        /// </summary>
        [HttpPut("items/{id}")]
        public async Task<IActionResult> EditQuCartItem(int idShoppingSession, int id, int qu)
        {
            var item = await _cartService.UpdateQuCartItemAsync(idShoppingSession, id, qu);
            if (item is null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<ShoppingSessionModel>(item));

        }

        /// <summary>
        /// Изменить количество для всех позиций
        /// </summary>
        //[HttpPut("/items/EditQu")]
        internal async Task<IActionResult> EditQuCartItems(int idShoppingSession, int qu)
        {
            var item = await _cartService.UpdateQuCartItemsAsync(idShoppingSession, qu);
            if (item is null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<ShoppingSessionModel>(item));

        }

        /// <summary>
        /// Удалить позицию из корзины
        /// </summary>
        [HttpDelete("items/{id}")]
        public async Task<IActionResult> DeleteCartItem(int idShoppingSession, int id)
        {
            var item = await _cartService.DeleteCartItemAsync(idShoppingSession, id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ShoppingSessionModel>(item));
        }

        /// <summary>
        /// Удалить все позиции из корзины
        /// </summary>
        [HttpDelete("items")]
        public async Task<IActionResult> DeleteCartItems(int idShoppingSession)
        {
            var item = await _cartService.DeleteCartItemsAsync(idShoppingSession);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ShoppingSessionModel>(item));
        }

        /// <summary>
        /// Удалить корзину по id покупателя
        /// </summary>
        [HttpDelete("DeleteCartByIdCustomer")]
        public async Task<IActionResult> DeleteCartByIdCustomer(int idCustomer)
        {
            var item = await _cartService.DeleteCartByIdCustomer(idCustomer);

            if (!item)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить корзину по id корзиный
        /// </summary>
        [HttpDelete()]
        public async Task<IActionResult> DeleteCart(int idShoppingSession)
        {
            var item = await _cartService.DeleteCartAsync(idShoppingSession);

            if (!item)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}