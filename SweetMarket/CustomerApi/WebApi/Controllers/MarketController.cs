﻿using AutoMapper;
using CustomerApi.Models;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services.Abstractions;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Contracts.Market;
using Microsoft.AspNetCore.Authorization;

namespace CustomerApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MarketController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMarketService _service;
        private readonly IVendorCodeService _vendorCodeService;

        public MarketController(IMarketService service, IMapper mapper, IVendorCodeService vendorCodeService)
        {
            _service = service;
            _mapper = mapper;
            _vendorCodeService = vendorCodeService;
        }

        /// <summary>
        /// Получить магазин
        /// </summary>
        //TODO добавить фильтр по удаленным записям, т.к. запись не должна удаляться
        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var identity = User.Identity;

            var market = await _service.GetById(id);

            if (market is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MarketModel>(market));
        }

        /// <summary>
        /// Получить по коду
        /// </summary>
        //TODO добавить фильтр по удаленным записям, т.к. запись не должна удаляться
        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var dto = await _vendorCodeService.ParseCode(code);
            var market = await _service.GetById(dto.MarketId);
            if (market is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MarketModel>(market));
        }

        /// <summary>
        /// Получить список магазинов
        /// </summary>
        [HttpPost("list")]
        public async Task<IActionResult> GetList(PaginationFilterModel filterModel)
        {
            var filterDto = _mapper.Map<PaginationFilterModel, PaginationFilterDto>(filterModel);
            var result = await _service.GetPaged(filterDto);
            return Ok(_mapper.Map<PagingModel<MarketShortDto>>(result));
        }

        /// <summary>
        /// Удалить магазин - через команду от rabbita в сервисе
        /// </summary>
        //[HttpDelete("{id}")]
        internal async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }
    }
}
