﻿using FluentValidation;

namespace CustomerApi.Models
{
    public class ProductModel
    {
        /// <summary>
        /// TODO Временное решение
        /// </summary>
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? ImageGuid { get; set; }

        public decimal Price { get; set; }

        /// <summary>
        /// Может быть кто-то будет вести рецепты не в нашей системе?
        /// Либо не будет вести рецепты вообще
        /// </summary>
        public int? RecipeId { get; set; }

        public int MarketId { get; set; }

        public int ProductCategoryId { get; set; }

        public bool IsPublishedInMarket { get; set; }

        public DateTime CreatedAt { get; set; }

        public int CreatedBy { get; set; }
        /// <summary>
        /// Оценка продкута
        /// </summary>
        /// <remarks>
        /// Что-то вроде средней оценки
        /// </remarks>
        public byte Rating { get; set; }

        /// <summary>
        /// Число коментариев под продуктом
        /// </summary>
        public uint CommntsCount { get; set; }
    }

    public class ProductValidator : AbstractValidator<ProductModel>
    {
        public ProductValidator()
        {
            RuleFor(e => e.Id).Equal(0)
                .WithMessage("Поле {Id} должно быть 0!");

            RuleFor(e => e.Name).NotEmpty()
                .WithMessage("Поле {Name} не должно быть пустым!")
                .MaximumLength(50)
                .WithMessage("Поле {Name} не должно быть больше 20 символов!");

            RuleFor(e => e.Description)
                .MaximumLength(50)
                .WithMessage("Поле {ShortName} не должно быть больше 10 символов!");
        }
    }
}
