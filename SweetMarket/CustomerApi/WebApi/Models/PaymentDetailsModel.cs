﻿namespace CustomerApi.Models
{
    public class PaymentDetailsModel
    {
        public int Card { get; set; }
        public decimal Value { get; set; }
        public string BankName { get; set; } = default!;
        public DateTimeOffset PaymentDate { get; set; }
    }
}
