﻿namespace CustomerApi.Models
{
    public class OrderShortModel
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal Price { get; set; }
        public string State { get; set; } = default!;
        public DateTime DateReady { get; set; }
        public ICollection<OrderItemShortModel> OrderItems { get; set; } = new List<OrderItemShortModel>();
    }
}
