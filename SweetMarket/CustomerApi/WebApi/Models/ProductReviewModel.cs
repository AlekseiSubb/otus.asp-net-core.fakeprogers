using FluentValidation;

namespace CustomerApi.Models;

/// <summary>
/// ������ ��������
/// </summary>
public class MarketModel
{
    /// <summary>
    /// ������������
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// ��������
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// ������ �� ��������� ��������
    /// </summary>
    public Guid ImageGuid { get; set; }

    /// <summary>
    /// Id �� ���������
    /// </summary>
    public int CreatiorId { get; set; }

    /// <summary>
    /// ������ ���������
    /// </summary>
    public List<int> Products { get; set; } = new List<int>();
    //public List<ProductDto> Products { get; set; } = new List<ProductDto>();

    /// <summary>
    /// ������ ����������
    /// </summary>
    public List<int> Confectioners { get; set; } = new List<int>();
    //public List<UserDto> �onfectioners { get; set; } = new List<UserDto>();
}

public class MarketValidator : AbstractValidator<MarketModel>
{
    public MarketValidator()
    {
        RuleFor(e => e.Name).NotEmpty()
                            .WithMessage("���� {Name} �� ������ ���� ������!")
                            .MaximumLength(64)
                            .WithMessage("���� {Name} �� ������ ���� ������ 64 ��������!");

        RuleFor(e => e.ImageGuid).NotEqual(Guid.Empty)
                            .WithMessage("���� {ImageGuid} �� ������ ���� ������ GUID!");

        RuleFor(e => e.Description).MaximumLength(512)
                            .WithMessage("���� {Description} �� ������ ���� ������ 512 ��������!");
    }
}