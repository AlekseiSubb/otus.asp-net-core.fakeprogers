﻿namespace CustomerApi.Models
{
    public class ProductEditModel
    {
        /// <summary>
        /// TODO Временное решение
        /// </summary>
        public int? Id { get; set; }

        public string Name { get; set; } = default!;

        public string Description { get; set; } = default!;

        public Guid? ImageGuid { get; set; }

        public decimal Price { get; set; }

        /// <summary>
        /// TODO имеет смысл фиксировать рецепт при создании?
        /// Может быть кто-то будет вести рецепты не в нашей системе?
        /// Либо не будет вести рецепты вообще
        /// </summary>
        public int? RecipeId { get; set; }

        /// <summary>
        /// TODO фиксировать категорию после создания?
        /// </summary>
        public int ProductCategoryId { get; set; }

        public bool IsPublishedInMarket { get; set; }
    }
}
