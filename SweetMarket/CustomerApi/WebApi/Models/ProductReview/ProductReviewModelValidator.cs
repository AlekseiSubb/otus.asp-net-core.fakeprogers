﻿using FluentValidation;

namespace CustomerApi.Models.ProductReview
{
    public class ProductReviewModelValidator : AbstractValidator<ProductReviewCreateModel>
    {
        public ProductReviewModelValidator()
        {
            ConfigureScoreValidation();
            ConfigureSummaryValidation();
            ConfigureCustomerIdValidation();
            ConfigureProductIdValidation();
        }

        private void ConfigureScoreValidation()
        {
            int minScoreValue = 1;
            int maxScoreValue = 5;
            RuleFor(s => s.Score)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Score)} не должно быть пустым")
                .GreaterThanOrEqualTo(minScoreValue)
                .WithMessage(s => $"Поле {nameof(s.Score)} должно быть не меньше {minScoreValue}")
                .LessThanOrEqualTo(maxScoreValue)
                .WithMessage(s => $"Поле {nameof(s.Score)} должно быть не больше {maxScoreValue}");
        }

        private void ConfigureSummaryValidation()
        {
            const int maxLength = 1024;
            RuleFor(s => s.Summary)
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Summary)} не должно быть длиннее {maxLength} символов");

        }

        private void ConfigureCustomerIdValidation()
        {
            int minIdValue = 1;
            RuleFor(s => s.CustomerId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.CustomerId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.CustomerId)} должно быть не меньше {minIdValue}");
        }

        private void ConfigureProductIdValidation()
        {
            int minIdValue = 1;
            RuleFor(s => s.ProductId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.ProductId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.ProductId)} должно быть не меньше {minIdValue}");
        }
    }
}
