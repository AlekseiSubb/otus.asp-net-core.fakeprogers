﻿namespace CustomerApi.Models.ProductReview
{
    public class ProductReviewDetailsModel
    {
        public int Id { get; set; }

        public int Score { get; set; }

        public string? Summary { get; set; }

        public Guid? ImageGuid { get; set; }

        public int CustomerId { get; set; }

        public int ProductId { get; set; }
    }
}
