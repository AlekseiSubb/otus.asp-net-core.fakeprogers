namespace CustomerApi.Models;

public class CustomerLiteModel
{
    /// <summary>
    /// Индефикатор заказчика
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Имя заказчика
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Почта заказчика
    /// </summary>
    public string? Mail { get; set; }
}