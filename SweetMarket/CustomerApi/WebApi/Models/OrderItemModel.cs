﻿namespace CustomerApi.Models
{
    public class OrderItemModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string? Comment { get; set; }

        public decimal Price { get; set; }
        public List<OrderItemAddonModel> Addons { get; set; } = new List<OrderItemAddonModel>();
    }
}
