﻿using FluentValidation;

namespace CustomerApi.Models;

public class PaginationFilterModel
{
    public int Page { get; set; } = 1;

    public int PageSize { get; set; }
}

public class PaginationFilterValidator : AbstractValidator<PaginationFilterModel>
{
    public PaginationFilterValidator()
    {
        RuleFor(e => e.Page).GreaterThanOrEqualTo(1)
                            .WithMessage($"Поле {{{nameof(PaginationFilterModel.Page)}}} не должно быть меньше 1!")
                            .LessThanOrEqualTo(int.MaxValue - 1)
                            .WithMessage($"Поле {{{nameof(PaginationFilterModel.PageSize)}}} не должно быть больше {int.MaxValue - 1}!"); ;

        RuleFor(e => e.PageSize).GreaterThanOrEqualTo(1)
                            .WithMessage("Поле {{nameof(PaginationFilterModel.PageSize)}} не должно быть меньше 1!")
                            .LessThanOrEqualTo(int.MaxValue)
                            .WithMessage($"Поле {{{nameof(PaginationFilterModel.PageSize)}}} не должно быть больше {int.MaxValue}!");
    }
}