﻿using System;
using System.Collections.Generic;

namespace CustomerApi.Models;
public class ShoppingSessionModel
{
    public int Id { get; set; }
    public int CustomerId { get; set; }
    public CustomerModel Customer { get; set; }
    public DateTime Created { get; set; }
    public ICollection<CartItemModel> CartItems { get; set; } 
}

