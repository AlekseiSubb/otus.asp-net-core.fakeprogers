﻿using CustomerApi.Domain.Entities.Models;

namespace CustomerApi.Models
{
    public class ShippingModel
    {
        public int Id { get; set; }
        public string ContactName { get; set; } = default!;
        public string Address { get; set; } = default!;
        public string Phone { get; set; } = default!;
        public DateTime DateShipping { get; set; }
        public string? TransportCompany { get; set; }
        public string ShippingNumber { get; set; } = default!;
        public string? CustomerComment { get; set; }
        public TimeSpan DeliveryTime { get; set; }
        public DateTimeOffset DeliveryStartTime { get; set; }
        public ShippingStatus Status { get; set; }
    }
}
