﻿namespace CustomerApi.Models
{
    public class ProductCategoryFilterModel
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string MarketName { get; set; }

        public string CategoryName { get; set; }
        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
