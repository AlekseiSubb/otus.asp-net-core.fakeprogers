namespace CustomerApi.Services.Contracts;

public class CustomerEditModel
{
    public string Name { get; set; }
    public string? Mail { get; set; }
    public int? CustomerAdressId { get; set; }

}