﻿namespace CustomerApi.Models
{
    public class OrderItemAddonModel
    {
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal Price { get; set; }
        public string? Comment { get; set; }
    }
}
