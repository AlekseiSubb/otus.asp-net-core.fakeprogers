﻿namespace CustomerApi.Models
{
    public class CreateOrderModel
    {
        public int IdShoppingSession { get; set; }
        public string Description { get; set; }
        public DateTime DateReady { get; set; }
        public string PaymentType { get; set; }
        public string Promocode { get; set; }

    }
}
