﻿namespace CustomerApi.Models
{
    public class OrderItemShortModel
    {
        public string ProductName { get; set; } = default!;
        public string MarketName { get; set; } = default!;
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string? Comment { get; set; }
    }
}
