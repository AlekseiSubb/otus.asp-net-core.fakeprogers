﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomerApi.Domain.Entities.Models;

namespace CustomerApi.Infrastructure.EntityFramework
{
    public static class FakeDataFactory
    {
        //звгрузка по пользователям
        public static IEnumerable<Market> Markets => new List<Market>()
        {
            new()
            {
                Id = 1,
                Name = "Белочка",
                Description = "г.Пермь, Комсомольский проспект, 50",
                OwnerId = 2,
                Code = "ABC01"
            },
            new()
            {
                Id = 2,
                Name = "Эрнест",
                Description = "г.Пермь, Сибирская, 30",
                OwnerId = 6,
                Code = "ABC02"
            },
            new()
            {
                Id = 3,
                Name = "Кофесити",
                Description = "г.Пермь, Луночарского, 10",
                OwnerId = 3,
                Code = "ABC03"
            }
        };
        public static IEnumerable<User> Users => new List<User>()
        {
            new()
            {
                Id = 1,
                Login = "admin",
                Password = "123",
                FullName = "admin"
            },
            new()
            {
                Id = 2,
                Login = "ivanovii",
                Password = "123",
                MarketId = 1,
                FullName = "Иванов Иван Иванович"

            },
            new()
            {
                Id = 3,
                Login = "novokovgk",
                Password = "123",
                MarketId = 3,
                FullName = "Новиков Геннадий Константинович"
            },
            new()
            {
                Id = 4,
                Login = "grigorievmd",
                Password = "123",
                FullName = "Григорьев Максим Даниилович"
            },
             new()
            {
                Id = 5,
                Login = "alexeevaa",
                Password = "123",
                FullName = "Алексеев Алексей Алексеевич"
            },
            new()
            {
                Id = 6,
                Login = "zhukov",
                Password = "123",
                MarketId = 2,
                FullName = "Жуков Даниил Денисович"

            }
        };
        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new()
            {
                Id = 1,
                Name = "Тестовый покупатель 1",
                Mail = "testCustomer1Mail@mail.ru",
                CustomerAdressId = CustomerAdreses.FirstOrDefault(ad => ad.Id == 1)?.Id,
                UserId =  Users.FirstOrDefault(us => us.Login.Contains("grigorievmd"))!.Id
            },
            new()
            {
                Id = 2,
                Name = "Тестовый покупатель 2",
                Mail = "testCustomer2Mail@mail.ru",
                CustomerAdressId = CustomerAdreses.FirstOrDefault(ad => ad.Id == 2)?.Id,
                UserId=  Users.FirstOrDefault(us => us.Login.Contains("alexeevaa"))!.Id
            }
        };
        public static IEnumerable<CustomerAdress> CustomerAdreses => new List<CustomerAdress>()
        {
            new()
            {
                Id = 1,
                Adress = "г. Москва, ул. Ленина 10"
            },
            new()
            {
                Id = 2,
                Adress = "г. Москва, ул. Пушкина 70"
            },
            new()
            {
                Id = 3,
                Adress = "г. Пермь, ул. Сибирская 17"
            },
            new()
            {
                Id = 4,
                Adress = "г. Пермь, ул. Тимерязево 7"
            }
        };

        //загрузка по продуктам
        public static IEnumerable<ProductCategory> ProductCategores => new List<ProductCategory>()
        {
            new()
            {
                Id = 1,
                Name = "Торты",
                Description = "Общая категория для всех тортов",
                Code = "ABC0123"
            },
            new()
            {
                Id = 2,
                Name = "Пирожное",
                Description = "Общая категория для пирожного",
                Code = "ABC0124"
            },
            new()
            {
                Id = 3,
                Name = "Печенье",
                Description = "Общая категория для пирожного",
                Code = "ABC0125"
            },
            new()
            {
                Id = 4,
                Name = "Конфеты ручной работы",
                Description = "Общая категория для пирожного",
                Code = "ABC0126"
            },
            new()
            {
                Id = 5,
                Name = "Шоколад формовой",
                Description = "Общая категория для пирожного",
                Code = "ABC0127"
            },
            new()
            {
                Id = 6,
                Name = "Мороженое",
                Description = "Общая категория для пирожного",
                Code = "ABC0128"
            }
        };
        public static IEnumerable<Addon> Addons => new List<Addon>()
        {
            new()
            {
                Id = 1,
                Name = "Подарочная упаковка",
                Description = "Упаковка продуката",
                Price = 100
            },
            new()
            {
                Id = 2,
                Name = "Надпись на торте заказчика",
                Description = "Для тортов",
                Price = 50
            },
            new()
            {
                Id = 3,
                Name = "Комплект свечек на торт",
                Description = "Свечки для торта 10 шт в пчке",
                Price = 150
            }
        };
        public static IEnumerable<Product> Products => new List<Product>()
        {
            new()
            {
                Id = 1,
                MarketId = 1,
                Name = "Медовик",
                Description = "Бисквитные медовые коржи с начинкой из вареной сгущенки",
                Price = 800,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA001",
                RecipeId = 1,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("76d95dcc-c48a-48be-8e4b-b5f4da0e491c"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 2,
                MarketId = 1,
                Name = "Торт Пикник",
                Description = "Шоколадный",
                Price = 800,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA002",
                RecipeId = 2,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("e436b78d-57a1-4a3f-aa67-9937ee7ffffd"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 3,
                MarketId = 1,
                Name = "Наполеон",
                Description = "Слоёный торт по классическому рецепту с заварным ванильно-сливочным кремом",
                Price = 1045,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA003",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("9de82376-a6cf-4ff3-9d21-db44fe41d7d4"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 4,
                MarketId = 1,
                Name = "Бурбон шоколадный",
                Description = "Шоколадный бисквит без муки, прослойка воздушного мусса из белого и темного шоколада, покрыт шоколадным велюром, оформлен цельным фундуком и карамелью",
                Price = 1560,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA004",
                RecipeId = null,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("91b6ba58-b765-431b-bf36-603b3c316a8f"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 5,
                MarketId = 1,
                Name = "Ганя",
                Description = "Блаженство шоколадного мусса с сочными ягодами малины",
                Price = 1460,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA005",
                RecipeId = null,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("4091ed71-570d-41d6-b470-f22d8731e7f4"),
                IsPublishedInMarket = false
            },
            new()
            {
                Id = 6,
                MarketId = 1,
                Name = "Жизнь в шоколаде",
                Description = "Шоколадный бисквит пропитан вишневым ликером с прослойкой из шоколадного мусса и свежей вишни",
                Price = 1085,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA006",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("4312b111-00ea-4b8c-8616-8382c1a6cfa0"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 7,
                MarketId = 1,
                Name = "Захер",
                Description = "Шоколадный бисквит пропитан коньячным сиропом с прослойкой из шоколадного заварного крема и фруктового джема",
                Price = 875,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA007",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("2c4bd858-b60a-4f93-b6ea-6c3e90ee437f"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 8,
                MarketId = 2,
                Name = "Клубничка",
                Description = "Молочный бисквит, пропитанный сиропом с прослойкой из нежных взбитых сливок, клубника в желе",
                Price = 510,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA008",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("5f264c3f-5a61-4c0a-baa9-94a08dcc631e"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 9,
                MarketId = 2,
                Name = "Матис с малиной",
                Description = "Шоколадный бисквит с прослойкой низкокалорийного йогуртового мусса с малиной и профитролями с ванильным муссом",
                Price = 810,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA009",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("8bb01c0c-7a4b-4b0e-857f-7282bcb9d55d"),
                IsPublishedInMarket = true 
            },
            new()
            {
                Id = 10,
                MarketId = 2,
                Name = "Матисс с фруктами",
                Description = "Шоколадный бисквит с прослойкой низкокалорийного йогуртового мусса с фруктами и профитролями с творожным муссом",
                Price = 990,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA010",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("026701f9-e6a4-4149-859c-eb47d334a470"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 11,
                MarketId = 2,
                Name = "Сметанный",
                Description = "(Домашний рецепт) медовые коржи с прослойкой из сметанного крема",
                Price = 1075,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA011",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("23235a0b-2fda-4e58-87c3-d15ac0364df1"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 12,
                MarketId = 2,
                Name = "Тирамису",
                Description = "Молочный бисквит, пропитанный кофейным сиропом с коньяком с прослойкой из нежных натуральных сливок с сыром маскарпоне",
                Price = 650,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA012",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("3d36c2ab-a4e6-42b9-bba9-f1fbfe1becaf"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 13,
                MarketId = 2,
                Name = "Тифани",
                Description = "Шоколадный бисквит с коньячным сиропом, прослойка из взбитых сливок с шоколадом",
                Price = 45,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA013",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("4ca7269b-782e-41d4-9279-f9305f8c72f7"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 14,
                MarketId = 3,
                Name = "Финансье",
                Description = "Нежный шоколадный бисквит без муки, сливочный крем с вареным сгущенным молоком, грецкими орехами и кусочками безе, покрыт шоколадным велюром",
                Price = 1300,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA014",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("91b6ba58-b765-431b-bf36-603b3c316a8f"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 15,
                MarketId = 3,
                Name = "Чаровница",
                Description = "Нежное ванильно-сливочное суфле с клюквенным парфе",
                Price = 580,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA015",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("c3deb4f1-bfc0-4563-bd42-5c7cf4f10f5e"),
                IsPublishedInMarket = true
            },
            new ()
            {
                Id = 16,
                MarketId = 3,
                Name = "Чизкейк с ягодным соусом",
                Description = "Из сыра Маскарпоне с ягодным соусом",
                Price = 680,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA016",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("18a2590a-ff18-47a5-93b7-a480661f3d3c"),
                IsPublishedInMarket = true
            }
        };
        public static IEnumerable<ProductReview> ProductReviews => new List<ProductReview>()
        {
            new() {
                Id = 1,
                CustomerId = 1,
                ProductId = 1,
                Score = 10,
                Summary = "Отличный торт. Спасибо кондитерам - всем понравился."
            },
            new() {
                Id = 2,
                CustomerId = 1,
                ProductId = 1,
                Score = 10,
                Summary = "Отличный торт. Спасибо кондитерам - всем понравился."
            },
            new() {
                Id = 3,
                CustomerId = 1,
                ProductId = 2,
                Score = 10,
                Summary = "Отличный торт. Спасибо кондитерам - всем понравился."
            },
        };

        //загрузка по заказу
        public static IEnumerable<PaymentDetails> PaymentDetails => new List<PaymentDetails>()
        {
            new()
            {
                Id = 1,
                Card = 23132132,
                Value = 0,
                BankName = "Сбер Банк",
                PaymentDate = DateTime.Now,
                PaymentType = "Наличные"
            },
            new()
            {
                Id = 2,
                Card = 777777,
                Value = 0,
                BankName = "Открытие",
                PaymentDate = DateTime.Now,
                PaymentType = "Картой"
            }


        };
        public static IEnumerable<Order> Orders => new List<Order>()
        {
            new()
            {
                Id = 1,
                OrderNumber = "market1_1",
                Description = "Тестовый заказа Market1",
                Price = 0,
                PaymentDetailsId = 1,
                DateCreate = DateTime.Now,
                State = "Не оплачен",
                CustomerId = 1

            }
        };
        public static IEnumerable<Shipping> Shippings => new List<Shipping>()
        {
            new()
            {
                Id = 1,
                OrderId =  Orders.FirstOrDefault(o => o.Id == 1)!.Id,
                ContactName = "Алексей",
                Address = "г. Пермь, Холмагорская 4",
                Phone = "890588888844",
                DateShipping = DateTime.Now.AddDays(3),
                Status = ShippingStatus.NotStarted,
                CustomerComment = ""
            }
        };
        public static IEnumerable<OrderItem> OrderItems => new List<OrderItem>()
        {
            new()
            {
                Id= 1,
                OrderId = 1,
                Comment = "Тестовый заказ 1 поз 1",
                Quantity = 1,
                ProductId = 1
            },
            new()
            {
                Id= 2,
                OrderId = 1,
                Comment = "Тестовый заказ 1 поз 2",
                Quantity = 2,
                ProductId = 2
            }

        };
        public static IEnumerable<OrderItemAddon> OrderItemAddons => new List<OrderItemAddon>()
        {
            new()
            {
                Id = 1,
                OrderItemId = 1,
                AddonId = 1,
                Comment = "Дополнение к первой позиции: упаковка"
            },
            new()
            {
                Id = 2,
                OrderItemId = 1,
                AddonId = 2,
                Comment = "Дополнение к первой поз: надпись"
            },
            new()
            {
                Id = 3,
                OrderItemId = 1,
                AddonId = 3,
                Comment = "Дополнение к первой поз: свечки"
            }
        };
        public static IEnumerable<ShoppingSession> ShoppingSessions => new List<ShoppingSession>()
        {
            new()
            {
                Id = 1,
                CustomerId = 1,
                Created = DateTime.Now
            }
        };
        public static IEnumerable<CartItem> CartItems => new List<CartItem>()
        {
            new()
            {
                Id = 1,
                ShoppingSessionId = 1,
                ProductId = 1,
                Qu = 1,
                Price = 20,
            },
            new()
            {
                Id = 2,
                ShoppingSessionId = 1,
                ProductId = 2,
                Qu = 1,
                Price = 30,
            }
        };
        public static IEnumerable<CartItemAddon> CartItemAddons => new List<CartItemAddon>()
        {
            new()
            {
                Id = 1,
                CartItemId = 1,
                AddonId = 1,
                Details = "Дополнение к первой позиции: упаковка"
            },
            new()
            {
                Id = 2,
                CartItemId = 1,
                AddonId = 2,
                Details = "Дополнение к первой поз: надпись"
            },
            new()
            {
                Id = 3,
                CartItemId = 1,
                AddonId = 3,
                Details = "Дополнение к первой поз: свечки"
            }
        };
    }
}
