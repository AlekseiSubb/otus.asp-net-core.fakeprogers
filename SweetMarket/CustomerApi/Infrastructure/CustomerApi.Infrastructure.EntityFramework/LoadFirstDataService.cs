﻿using System;
using System.Linq;
using CustomerApi.Domain.Entities.Models;

namespace CustomerApi.Infrastructure.EntityFramework
{
    public interface ILoadFirstDataService
    {
        void InitializeDb();
    }
    public class LoadFirstDataService : ILoadFirstDataService
    {
        private readonly DatabaseContext _dataContext;
        public LoadFirstDataService(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {

            if (_dataContext == null)
                return;

            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            //загрузка начальных данные т.к. у нас ключи генерируются БД то мы не можем грузить данные с ID - это делает БД,
            // по этому нужно создавать обект БД и потом его заполнять или использовать начальную загрузку в DBContext через modelBuilder.Entity<T>().HasData()
            //FakeDataFactory.ProductCategores.ToList().ForEach(categores =>
            //{
            //    _dataContext.ProductCategorys.Add(new ProductCategory { Name = categores.Name, Description = categores.Description });
            //});
            //FakeDataFactory.customerAdreses.ToList().ForEach(adreses =>
            //{
            //    _dataContext.CustomerAdresses.Add(new CustomerAdress { Adress = adreses.Adress });
            //});
            //FakeDataFactory.markets.ToList().ForEach(markets =>
            //{
            //    _dataContext.Markets.Add(new Market { Name = markets.Name, Description = markets.Description });
            //});
            //_dataContext.SaveChanges();
            //FakeDataFactory.users.ToList().ForEach(users =>
            //{
            //    _dataContext.Users.Add(new User { 
            //                        Login = users.Login, 
            //                        Password = users.Password, 
            //                        MarketId = _dataContext.Markets.FirstOrDefault(m => m.Id == 1).Id
            //                        });
            //});
            //_dataContext.SaveChanges();

            //добавление заказк для примера:
            var NewPaymentDetail = new PaymentDetails
            {
                Card = 111111,
                Value = 12000,
                BankName = "Открытие",
                PaymentDate = DateTime.UtcNow,
                PaymentType = "карта"
            };
            var NewOrder1 = new Order
            {
                OrderNumber = "market3_1",
                Description = "Тестовый заказа Market3",
                Price = 12000,
                PaymentDetails = NewPaymentDetail,
                CustomerId = 1
            };
            var NewOrder2 = new Order
            {
                OrderNumber = "market2_1",
                Description = "Тестовый заказа Market2",
                Price = 12000,
                PaymentDetailsId = 2,
                CustomerId = 2
            };
            //_dataContext.PaymentDetails.Add(NewPaymentDetail); можно отдельно не грузить т.к. добавится при добавление Orders
            _dataContext.Orders.AddRange(NewOrder1, NewOrder2);
            _dataContext.SaveChanges();

            //новая доставка на заказ
            _dataContext.Shippings.Add(new Shipping
            {
                OrderId = _dataContext.Orders.FirstOrDefault(o => o.OrderNumber.Contains("market2_1")).Id,
                ContactName = "Алексей2",
                Address = "г. Пермь, Холмагорская 42",
                Phone = "890588888844",
                DateShipping = DateTime.Now.AddDays(5),
                Status = ShippingStatus.NotStarted,
                CustomerComment = ""
            });
            _dataContext.SaveChanges();
        }

    }
}
