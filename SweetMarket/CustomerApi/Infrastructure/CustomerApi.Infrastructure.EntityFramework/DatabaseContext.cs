﻿using CustomerApi.Infrastructure.EntityFramework.Configurations;
using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CustomerApi.Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public sealed class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        /// <summary>
        /// Таблички по заказу
        /// </summary>
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderItemAddon> OrderItemAddons { get; set; }
        public DbSet<PaymentDetails> PaymentDetails { get; set; }
        public DbSet<Shipping> Shippings { get; set; }
        public DbSet<ShoppingSession> ShoppingSessions { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<CartItemAddon> CartItemAddons { get; set; }


        /// <summary>
        /// Таблички по Заказчику
        /// </summary>
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerAdress> CustomerAdresses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Market> Markets { get; set; }


        /// <summary>
        /// Таблички по продукту
        /// </summary>
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<ProductCategory> ProductCategorys { get; set; }
        public DbSet<Addon> Addons { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new MarketConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new ProductCategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ProductReviewConfiguration());
            modelBuilder.ApplyConfiguration(new CartItemConfiguration());
            initDB(modelBuilder);
        }

        private void initDB(ModelBuilder modelBuilder)
        {

            //начальные данные по пользователям
            modelBuilder.Entity<CustomerAdress>().HasData(FakeDataFactory.CustomerAdreses);
            modelBuilder.Entity<Market>().HasData(FakeDataFactory.Markets);
            modelBuilder.Entity<User>().HasData(FakeDataFactory.Users);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);

            //начальные данные по заказу
            modelBuilder.Entity<PaymentDetails>().HasData(FakeDataFactory.PaymentDetails);
            modelBuilder.Entity<Order>().HasData(FakeDataFactory.Orders);
            modelBuilder.Entity<Shipping>().HasData(FakeDataFactory.Shippings);
            modelBuilder.Entity<OrderItem>().HasData(FakeDataFactory.OrderItems);
            modelBuilder.Entity<OrderItemAddon>().HasData(FakeDataFactory.OrderItemAddons);
            modelBuilder.Entity<ShoppingSession>().HasData(FakeDataFactory.ShoppingSessions);
            modelBuilder.Entity<CartItem>().HasData(FakeDataFactory.CartItems);
            modelBuilder.Entity<CartItemAddon>().HasData(FakeDataFactory.CartItemAddons);

            //начальные данные по продукту
            modelBuilder.Entity<ProductCategory>().HasData(FakeDataFactory.ProductCategores);
            modelBuilder.Entity<Addon>().HasData(FakeDataFactory.Addons);
            modelBuilder.Entity<Product>().HasData(FakeDataFactory.Products);
            modelBuilder.Entity<ProductReview>().HasData(FakeDataFactory.ProductReviews);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(message => System.Diagnostics.Debug.WriteLine(message), LogLevel.Information);
        }
    }
}