﻿using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerApi.Infrastructure.EntityFramework.Configurations
{
    internal class CartItemConfiguration : IEntityTypeConfiguration<CartItem>
    {
        public void Configure(EntityTypeBuilder<CartItem> builder)
        {
            builder.ToTable(nameof(CartItem));

            builder.HasKey(t => t.Id);

            builder.HasOne(q => q.ShoppingSession)
             .WithMany(q => q.CartItems)
             .HasForeignKey(q => q.ShoppingSessionId)
             .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
