﻿using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerApi.Infrastructure.EntityFramework.Configurations
{
    internal class OrderItemAddonConfiguration : IEntityTypeConfiguration<OrderItemAddon>
    {
        public void Configure(EntityTypeBuilder<OrderItemAddon> builder)
        {
            builder.ToTable(nameof(OrderItemAddon));

            builder.HasKey(t => t.Id); //Возможно потом будем использовать strongly typed id
        }
    }
}
