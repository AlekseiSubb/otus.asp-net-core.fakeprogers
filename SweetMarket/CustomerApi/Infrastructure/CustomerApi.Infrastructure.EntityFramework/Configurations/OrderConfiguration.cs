﻿using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerApi.Infrastructure.EntityFramework.Configurations
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable(nameof(Order));

            builder.HasKey(t => t.Id); //Возможно потом будем использовать strongly typed id

            builder.HasMany(q => q.OrderItems)
                .WithOne()
                .HasForeignKey("OrderItemId");

            builder.HasOne(q => q.PaymentDetails)
                .WithOne()
                .HasForeignKey<Order>("PaymentDetailsId");

        }
    }
}
