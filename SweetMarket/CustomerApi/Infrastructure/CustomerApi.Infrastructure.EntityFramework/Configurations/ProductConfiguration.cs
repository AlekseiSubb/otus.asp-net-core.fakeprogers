﻿using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerApi.Infrastructure.EntityFramework.Configurations
{
    internal class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(nameof(Product));

            builder.HasKey(t => t.Id); //Возможно потом будем использовать strongly typed id

            builder.Property(q => q.Name)
                .HasMaxLength(64);

            builder.Property(q => q.Description)
                .HasMaxLength(512);

            builder.HasIndex(q => q.Code)
                .IsUnique();
            builder.Property(q => q.Code)
                .HasMaxLength(6);

            builder.HasOne(q => q.Category)
                .WithMany(q => q.Products)
                .HasForeignKey(q => q.ProductCategoryId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(q => q.ProductReviews)
                .WithOne(q => q.Product)
                .HasForeignKey(q => q.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(q => q.OrderItems)
                .WithOne(q => q.Product)
                .HasForeignKey(q => q.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            //builder.HasMany(q => q.CartItems)
            //    .WithOne()
            //    .HasForeignKey(q => q.ProductId)
            //    .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(q => q.Addons)
                .WithMany(q => q.Products)
                .UsingEntity("ProductAddon");
        }
    }
}
