﻿using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerApi.Infrastructure.EntityFramework.Configurations
{
    internal class ProductCategoryConfiguration : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            builder.ToTable(nameof(ProductCategory));

            builder.HasKey(t => t.Id); //Возможно потом будем использовать strongly typed id

            builder.Property(q => q.Name)
                .HasMaxLength(64);

            builder.Property(q => q.Description)
                .HasMaxLength(512);

            builder.Property(q => q.Code)
                .HasMaxLength(7);
        }
    }
}
