﻿using CustomerApi.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerApi.Infrastructure.EntityFramework.Configurations
{
    internal class ProductReviewConfiguration : IEntityTypeConfiguration<ProductReview>
    {
        public void Configure(EntityTypeBuilder<ProductReview> builder)
        {
            builder.ToTable(nameof(ProductReview));

            builder.HasKey(t => t.Id);


            builder.HasOne(q => q.Product)
             .WithMany(q => q.ProductReviews)
             .HasForeignKey(q => q.ProductId)
             .OnDelete(DeleteBehavior.NoAction);

            builder.Property(t => t.Summary)
                .HasMaxLength(1024);
        }
    }
}
