﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Infrastructure.Repositories.Implementations
{
    public class ProductReviewRepository : Repository<ProductReview, int>, IProductReviewRepository
    {
        public ProductReviewRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<PaginatedEntity<ProductReview, int>> GetPagedAsync(PaginationFilterDto filterDto)
        {
            var query = Context.Set<ProductReview>().AsQueryable();
            var paginatedQuery = query
                .Skip((filterDto.Page - 1) * filterDto.PageSize)
                .Take(filterDto.PageSize);

            var items = await paginatedQuery.ToListAsync();
            int itemsCount = await query.CountAsync();
            int totalPages = Convert.ToInt32(Math.Ceiling((double)itemsCount / (double)filterDto.PageSize));

            return new PaginatedEntity<ProductReview, int>
            {
                Items = items,
                CurrentPage = filterDto.Page,
                ItemsPerPage = filterDto.PageSize,
                TotalPages = totalPages == 0 ? 1 : totalPages,
            };
        }
    }
}
