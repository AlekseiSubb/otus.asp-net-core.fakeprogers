﻿using System.Threading.Tasks;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Infrastructure.Repositories.Implementations
{
    public class ProductCategoryRepository : Repository<ProductCategory, int>, IProductCategoryRepository
    {
        public ProductCategoryRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<int?> GetIdByCode(string code) => (await Context.Set<ProductCategory>().FirstOrDefaultAsync(q => q.Code == code))?.Id;
    }
}
