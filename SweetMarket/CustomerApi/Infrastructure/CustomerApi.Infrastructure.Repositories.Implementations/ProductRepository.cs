﻿using System.Linq;
using System;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Infrastructure.Repositories.Implementations
{
    public class ProductRepository : Repository<Product, int>, IProductRepository
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<int?> GetIdByCode(string code) => (await Context.Set<Product>().FirstOrDefaultAsync(q => q.Code == code))?.Id;

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список продукции </returns>
        public async Task<PaginatedEntity<Product, int>> GetPagedAsync(ProductCategoryFilterDto filterDto)
        {
            var query = Context.Set<Product>()
                            .Include(p => p.Category)
                            .Include(p => p.Market)
                            .AsQueryable();
            query = query
                .Skip((filterDto.Page - 1) * filterDto.PageSize)
                .Take(filterDto.PageSize);

            //показываем только опубликованную продукцию
            query = query.Where(c => c.IsPublishedInMarket == true);

            //фильтр по категории продукции
            if (!string.IsNullOrWhiteSpace(filterDto.CategoryName))
            {
                query = query.Where(c => c.Category.Name.Contains(filterDto.CategoryName));
            }

            //фильтр по имени продукта
            if (!string.IsNullOrWhiteSpace(filterDto.Name))
            {
                query = query.Where(c => c.Name.Contains(filterDto.Name));
            }

            //фильтр по имени маркета
            if (!string.IsNullOrWhiteSpace(filterDto.MarketName))
            {
                query = query.Where(c => c.Market.Name.Contains(filterDto.MarketName));
            }

            //фильтр по стоимости
            if (filterDto.Price > 0)
            {
                query = query.Where(c => c.Price >= filterDto.Price);
            }

            var items = await query.ToListAsync();
            var itemsCount = await GetAll(asNoTracking: true).CountAsync();
            var totalPages = Convert.ToInt32(Math.Ceiling((double)itemsCount / (double)filterDto.PageSize));
            return new PaginatedEntity<Product, int>
            {
                Items = items,
                CurrentPage = filterDto.Page,
                ItemsPerPage = filterDto.PageSize,
                TotalPages = totalPages == 0 ? 1 : totalPages,
            };
        }

        /// <summary>
        /// Получить количество товаров по категории
        /// </summary>
        /// <param name="dCategory"> Id категории </param>
        /// <returns> количество  </returns>
        public async Task<int> GetQuantity(int idCategory)
        {
            var query = Context.Set<Product>().AsQueryable();
            int qu = query
                .Where(x => x.ProductCategoryId == idCategory).Count();
            return qu;
        }
    }
}
