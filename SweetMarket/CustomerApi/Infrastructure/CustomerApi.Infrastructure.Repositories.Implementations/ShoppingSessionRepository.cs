﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Infrastructure.Repositories.Implementations
{
    public class ShoppingSessionRepository : Repository<ShoppingSession, int>, IShoppingSessionRepository
    {
        public ShoppingSessionRepository(DatabaseContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить корзину по id клиента 
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public async Task<ShoppingSession> GetByIdCustomerAsync(int idCustomer)
        {
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.CustomerId == idCustomer && !c.Deleted);

            return await query.SingleOrDefaultAsync();
        }

        /// <summary>
        /// Получить все корзины по id клиента 
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ShoppingSession>> GetAllByIdCustomerAsync(int idCustomer)
        {
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.CustomerId == idCustomer).OrderByDescending(c => c.Created);

            return await query.ToListAsync();
        }
        /// <summary>
        /// создать пустую корзину
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public async Task<ShoppingSession> CreateAsync(int idCustomer)
        {
            //проверить если ли для него корзина
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Where(c => c.CustomerId == idCustomer && !c.Deleted);

            //проверяем наличие корзин, если есть то удаляем
            List<ShoppingSession> carts = await query.ToListAsync();
            if (carts != null && carts.Count() > 0)
            {
                await DeleteSessionsAsync(idCustomer);
            }

            //если нет корзины для клиента - создаем пустую
            ShoppingSession newShoppingSession = new ShoppingSession { CustomerId = idCustomer };
            await base.AddAsync(newShoppingSession); //тут нет Customer только Id

            return await GetByIdCustomerAsync(idCustomer);
        }

        /// <summary>
        /// Получить содержимое корзины 
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns></returns>
        public async Task<ShoppingSession> GetByIdSessionAsync(int idShoppingSession)
        {
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.Id == idShoppingSession && !c.Deleted);

            return await query.SingleOrDefaultAsync();
        }

        /// <summary>
        /// Добавить продукт в корзину
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns>корзина со всем содержимым</returns>
        public async Task<ShoppingSession> AddProductAsync(int idShoppingSession, Product addedProduct, int qu)
        {
            //получили корзину
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.Id == idShoppingSession && !c.Deleted);
            ShoppingSession cart = await query.SingleOrDefaultAsync();
            if (cart == null)
                return null;

            //создали новую позицию
            cart.CartItems.Add(new CartItem { ProductId = addedProduct.Id, 
                                                ShoppingSessionId = idShoppingSession, 
                                                Qu = qu,
                                                Price = addedProduct.Price});

            var Ef = Context.Set<ShoppingSession>().Update(cart);
            if (Ef is { })
            {
                await Context.SaveChangesAsync();
            }


            return cart;
        }

        /// <summary>
        /// Удалить позицию из корзины
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns>корзина со всем содержимым</returns>
        public async Task<ShoppingSession> DeleteCartItemAsync(int idShoppingSession, int cartItemId)
        {
            //получили корзину
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.Id == idShoppingSession && !c.Deleted);
            ShoppingSession cart = await query.SingleOrDefaultAsync();
            if(cart == null)
                return null;

            var item = cart.CartItems.Where(ci => ci.Id == cartItemId).FirstOrDefault();
            if(item != null)
            {
                cart.CartItems.Remove(item);
            }

            var Ef = Context.Set<ShoppingSession>().Update(cart);
            if (Ef is { })
            {
                await Context.SaveChangesAsync();
            }
            return cart;
        }

        /// <summary>
        /// Удалить все позиции из корзины
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns>пустая корзина</returns>
        public async Task<ShoppingSession> DeleteCartItemsAsync(int idShoppingSession)
        {
            //получили корзину
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.Id == idShoppingSession && !c.Deleted);
            ShoppingSession cart = await query.SingleOrDefaultAsync();

            if (cart == null)
                return null;

            //удалить позицию
            cart.CartItems.Clear();

            var Ef = Context.Set<ShoppingSession>().Update(cart);
            if (Ef is { })
            {
                await Context.SaveChangesAsync();
            }
            return cart;
        }

        /// <summary>
        /// Удалить все корзиный для данного Заказчика
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns>пустая корзина</returns>
        public async Task<bool> DeleteSessionsAsync(int idCustomer)
        {
            //получили корзину
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Where(c => c.CustomerId == idCustomer);

            List<ShoppingSession> carts = await query.ToListAsync();

            //помечаем на удаление
            carts.ForEach(x => x.Deleted = true);

            if (carts == null)
                return false;

            Context.Set<ShoppingSession>().UpdateRange(carts);
            await Context.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Изменить количество для позиции
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns>корзина со всем содержимым</returns>
        public async Task<ShoppingSession> UpdateQuCartItemAsync(int idShoppingSession, int cartItemId, int qu)
        {
            //получили корзину
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.Id == idShoppingSession && !c.Deleted);
            ShoppingSession cart = await query.SingleOrDefaultAsync();

            if (cart == null)
                return null;

            //изменить количество
            var item = cart.CartItems.FirstOrDefault(x => x.Id == cartItemId);
            if (item != null)
                item.Qu = qu;

            var Ef = Context.Set<ShoppingSession>().Update(cart);
            if (Ef is { })
            {
                await Context.SaveChangesAsync();
            }
            return cart;
        }

        /// <summary>
        /// Изменить количество для всех позиций
        /// </summary>
        /// <param name="idShoppingSession"></param>
        /// <returns>корзина со всем содержимым</returns>
        public async Task<ShoppingSession> UpdateQuCartItemsAsync(int idShoppingSession, int qu)
        {
            //получили корзину
            var query = Context.Set<ShoppingSession>().AsQueryable();
            query = query
                .Include(c => c.CartItems)
                .ThenInclude(ci => ci.Product)
                .ThenInclude(p => p.Market)
                .Include(c => c.Customer)
                .ThenInclude(a => a.CustomerAdress)
                .Where(c => c.Id == idShoppingSession && !c.Deleted);
            ShoppingSession cart = await query.SingleOrDefaultAsync();

            if (cart == null)
                return null;

            //изменить количество
            cart.CartItems.ToList().ForEach(x => x.Qu = qu);

            var Ef = Context.Set<ShoppingSession>().Update(cart);
            if (Ef is { })
            {
                await Context.SaveChangesAsync();
            }
            return cart;
        }
    }
}
