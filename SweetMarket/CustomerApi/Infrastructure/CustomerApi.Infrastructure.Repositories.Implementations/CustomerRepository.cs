using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Infrastructure.Repositories.Implementations;

public class CustomerRepository : Repository<Customer, int>, ICustomerRepository
{
    public CustomerRepository(DatabaseContext context) : base(context)
    {

    }
    public async Task<Customer> GetByUserIdAsync(int UserId)
    {
        var query = Context.Set<Customer>().AsQueryable();
        query = query
         .Where(c => c.UserId == UserId);

        return await query.SingleOrDefaultAsync();
    }

}