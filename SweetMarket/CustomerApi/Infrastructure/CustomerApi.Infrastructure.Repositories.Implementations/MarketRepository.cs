﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с магазинами
    /// </summary>
    public class MarketRepository : Repository<Market, int>, IMarketRepository
    {
        public MarketRepository(DatabaseContext context) : base(context) { }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<PaginatedEntity<Market, int>> GetPagedAsync(PaginationFilterDto filterDto)
        {
            //var query = GetAll().ToList().AsQueryable(); //не работает
            var query = Context.Set<Market>().AsQueryable();
            query = query
                .Skip((filterDto.Page - 1) * filterDto.PageSize)
                .Take(filterDto.PageSize);
            var items = await query.ToListAsync();
            var itemsCount = await GetAll(asNoTracking: true).CountAsync();
            var totalPages = Convert.ToInt32(Math.Ceiling((double)itemsCount / (double)filterDto.PageSize));
            return new PaginatedEntity<Market, int>
            {
                Items = items,
                CurrentPage = filterDto.Page,
                ItemsPerPage = filterDto.PageSize,
                TotalPages = totalPages == 0 ? 1 : totalPages,
            };
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<Market> GetAsync(int id)
        {
            var query = Context.Set<Market>().AsQueryable();
            query = query
                .Include(c => c.Products)
                .Include(q => q.Сonfectioners)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
        public async Task<int?> GetIdByCode(string code) => (await Context.Set<Market>().FirstOrDefaultAsync(q => q.Code == code))?.Id;
    }
}
