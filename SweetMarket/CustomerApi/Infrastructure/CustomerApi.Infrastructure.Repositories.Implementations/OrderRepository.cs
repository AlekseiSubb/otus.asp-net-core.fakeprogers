﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerApi.Domain.Entities.Models;
using CustomerApi.Infrastructure.EntityFramework;
using CustomerApi.Services.Contracts;
using CustomerApi.Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Infrastructure.Repositories.Implementations
{
    public class OrderRepository : Repository<Order, int>, IOrderRepository
    {
        public OrderRepository(DatabaseContext context) : base(context)
        {
        }
        public async Task<List<Order>> GetPagedAsync(OrderFilterDto filterDto)
        {
            //var query = GetAll().ToList().AsQueryable();
            var query = Context.Set<Order>().AsQueryable();

            query = query.Where(c =>  c.CustomerId == filterDto.CustomerId);

            if (!string.IsNullOrWhiteSpace(filterDto.Name))
            {
                query = query.Where(c => filterDto.Name.Contains(c.OrderNumber));
            }

            if (filterDto.Price.HasValue && filterDto.Price >= 0)
            {
                query = query.Where(c => c.Price == filterDto.Price);
            }

            if (!string.IsNullOrWhiteSpace(filterDto.State))
            {
                query = query.Where(c => c.State == filterDto.State);
            }

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            query = query
                    .Include(o => o.OrderItems)
                    .Include(o => o.PaymentDetails);

            if (filterDto.SortByDateCreate)
                query.OrderByDescending(s => s.DateCreate);
            else
                query.OrderByDescending(s => s.Id);

            return await query.ToListAsync();
        }
        public override Task<Order> GetAsync(int id)
        {
            var query = Context.Set<Order>().AsQueryable();
            query = query
                .Include(o => o.OrderItems)
                .ThenInclude(x => x.Product)
                .Include(o => o.PaymentDetails)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }

        public override async Task<List<Order>> GetAllAsync()
        {
            var query = Context.Set<Order>().AsQueryable();
            query = query
                .Include(o => o.OrderItems)
                .Include(o => o.PaymentDetails);
            return await query.ToListAsync();
        }
    }
}
