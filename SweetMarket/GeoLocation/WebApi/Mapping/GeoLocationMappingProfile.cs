using AutoMapper;
using GeoLocation.Api.Models;
using GeoLocation.Services.Contracts;

namespace GeoLocation.Api.Mapping
{
    /// <summary>
    /// Профиль автомаппера для геолокации (пока только город)
    /// </summary>
    public class GeoLocationMappingProfile : Profile
    {
        public GeoLocationMappingProfile()
        {
            CreateMap<GeoLocationDto, GeoLocationModel>().ReverseMap();
        }
    }
}
