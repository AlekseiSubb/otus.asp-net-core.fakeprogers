namespace GeoLocation.Api.Settings
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; } = default!;
    }
}