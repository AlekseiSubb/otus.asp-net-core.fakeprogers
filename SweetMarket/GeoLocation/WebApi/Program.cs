using FluentValidation;
using FluentValidation.AspNetCore;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NLog.Web;
using Services.Implementations;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using GeoLocation.Api.Settings;
using GeoLocation.Infrastructure.EntityFramework;
using GeoLocation.Infrastructure.Repositories.Implementations;
using GeoLocation.Services.Abstractions;
using GeoLocation.Services.Repositories.Abstractions;
using System.IdentityModel.Tokens.Jwt;
using GeoLocation.Api.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);

AddConfiguration(builder);

//���� ������
builder.Services.AddDbContext<DatabaseContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("PostgreSQL"),
        q => q.MigrationsAssembly("Infrastructure.EntityFramework"));
    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true); //�������� UTC-�����.
    //options.UseSqlServer(builder.Configuration.GetConnectionString("SqlServer"));
    //options.UseSqlite(builder.Configuration.GetConnectionString("SqLite"));
    //options.UseLazyLoadingProxies(); // lazy loading
});

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
        options.Audience = "sweetmarket_geolocation_api";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new()
        {
            RoleClaimType = "role",
            ValidTypes = new[] { "at+jwt" }
        };
    });


builder.Host.UseNLog(new NLogAspNetCoreOptions
{
    //RemoveLoggerFactoryFilter = false, //�� ��������� true, �� ���� ������� ����������� �������, �� � ��, �� ��� �� ������.
    //ReplaceLoggerFactory = true, //������� ��������� �������
});


builder.Services.AddHealthChecks()
    .AddDbContextCheck<DatabaseContext>();

RegisterAppServices(builder);

//Rabbit MQ
builder.Services.AddMassTransit(x =>
{
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host("hawk.rmq.cloudamqp.com",  //TODO: ������� � ������������
                "iatvfquz",
                h =>
                {
                    h.Username("iatvfquz");
                    h.Password("G68bk0zxzH0ncOvMlmfyYapLaCqwjiRi");
                });
    });
});


builder.Services.AddControllers();

//������������� �� ������������ ������������ ��. ���� �� ������
//https://docs.fluentvalidation.net/en/latest/aspnet.html
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddFluentValidationClientsideAdapters();
builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();
builder.Services.Configure<SwaggerGenOptions>(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Title",
        Description = "Description",
        Contact = new OpenApiContact
        {
            Name = "Example Contact",
            Url = new Uri("https://example.com/contact")
        },
        License = new OpenApiLicense
        {
            Name = "Example License",
            Url = new Uri("https://example.com/license")
        },
        Version = "v1"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/authorize"),
                TokenUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/token"),
                Scopes = new Dictionary<string, string>
                {
                    {"openid", ""},
                    {"email", ""},
                    {"profile", ""},
                    {"roles", ""},
                    {"sweetmarket_geolocation_api.fullaccess", ""}
                },
            },
        },
    });
    options.OperationFilter<AuthorizeCheckOperationFilter>();
});

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

var app = builder.Build();
var aa = app.Services.GetService<ILogger<string>>();

aa?.LogInformation("inf");
aa?.LogWarning("warn");
aa?.LogError("err");

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseSwagger();

app.UseSwaggerUI();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    c.RoutePrefix = string.Empty;

    c.OAuthClientId("sweetmarket_web_frontend_client");
    c.OAuthClientSecret("LwZjqN6XCPCtnt0ZKYUiD1jGbRbe2aepqtGfxsuk9D");
    c.EnablePersistAuthorization();
});

//app.UseCors(); //���� �� ����, ����� ��������

app.UseRouting();
app.UseHttpsRedirection();
//app.UseHealthChecks("/health");

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

//����� ����� � �����, � ����� � ���:
//var provider = app.Services.GetService<IMapper<??>>().ConfigurationProvider;
//provider.AssertConfigurationIsValid();
using (var serviceScope = app.Services.CreateScope())
{
    var initService = serviceScope.ServiceProvider.GetService<ILoadFirstDataService>();
    initService?.InitializeDb();
}
app.Run();

void RegisterAppServices(WebApplicationBuilder webApplicationBuilder)
{
    webApplicationBuilder.Services.AddScoped<ILoadFirstDataService, LoadFirstDataService>();

    //����������� ��������
    webApplicationBuilder.Services.AddScoped<ICityRepository, CityRepository>();

    webApplicationBuilder.Services.AddTransient<IGeoLocationService, GeoLocationService>();
}

void AddConfiguration(WebApplicationBuilder builder1)
{
    //Configuration
    //�������� ��������� �� ��������� (��������, �� �����, ������ �� ���: https://learn.microsoft.com/ru-ru/aspnet/core/fundamentals/configuration/?view=aspnetcore-7.0#cp)
    builder1.Configuration.Sources.Clear();
    //��������� ����
    builder1.Configuration
        .AddJsonFile("appsettings.json")
        .AddEnvironmentVariables()
        .AddUserSecrets<Program>();
    builder1.Services.Configure<ApplicationSettings>(builder1.Configuration
        .GetSection("")); //��� ������� ���������� ������

    var a = builder1.Configuration.GetConnectionString("PostgreSQL");
}

