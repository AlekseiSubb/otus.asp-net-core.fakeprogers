﻿using System.Net;
using AutoMapper;
using GeoLocation.Api.Models;
using GeoLocation.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GeoLocation.Api.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class GeoLocationController : ControllerBase
    {
        private IGeoLocationService _geoLocationService;
        private IMapper _mapper;

        public GeoLocationController(IGeoLocationService geoLocationService, IMapper mapper)
        {
            _geoLocationService = geoLocationService;
            _mapper = mapper;
        }

        /// <summary>
        /// Город
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<GeoLocationModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var identity = User.Identity;

            var item = await _geoLocationService.GetLocationAsync();

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<GeoLocationModel>(item));
        }

        /// <summary>
        /// Город по IP
        /// </summary>
        [HttpGet("{ip}")]
        [ProducesResponseType(typeof(IEnumerable<GeoLocationModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(string ip)
        {
            var item = await _geoLocationService.GetLocationByIPAsync(ip);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<GeoLocationModel>(item));
        }

        /// <summary>
        /// Получить список городов-миллионеров
        /// </summary>
        /// <returns></returns> 
        [HttpGet(nameof(GetBigСities))]
        [ProducesResponseType(typeof(IEnumerable<GeoLocationModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetBigСities()
        {
            var items = await _geoLocationService.GetBigСitiesAsync();
            return Ok(_mapper.Map<List<GeoLocationModel>>(items));
        }

        /// <summary>
        /// Получить список городов с фильтром по городу
        /// </summary>
        /// <param name="filterModel">фильтр по городу</param>
        /// <returns></returns>
        [HttpGet("GetСitiesWithFilter")]
        public async Task<IActionResult> GetСitiesWithFilter(string filterByCity)
        {
            return Ok(_mapper.Map<List<GeoLocationModel>>(await _geoLocationService.GetСitiesWithFilter(filterByCity)));
        }

    }
}