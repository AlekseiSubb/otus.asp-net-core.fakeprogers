﻿using System.Threading.Tasks;

namespace GeoLocation.Services.Repositories.Abstractions;
/// <summary>
/// Получение идентификаторов сущностей с уникальным кодом
/// </summary>
public interface ICodeEntityRepository
{
    public Task<int?> GetIdByCode(string code);
}
