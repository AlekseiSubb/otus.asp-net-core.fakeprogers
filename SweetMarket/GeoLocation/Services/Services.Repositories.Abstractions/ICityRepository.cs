﻿using GeoLocation.Domain.Entities.Models;


namespace GeoLocation.Services.Repositories.Abstractions
{
    public interface ICityRepository : IRepository<City, int>
    {
    }
}
