﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeoLocation.Services.Contracts;

namespace GeoLocation.Services.Abstractions;
public interface IGeoLocationService
{
    Task<GeoLocationDto> GetLocationByIPAsync(string ip);

    Task<GeoLocationDto> GetLocationAsync();

    Task<IEnumerable<GeoLocationDto>> GetBigСitiesAsync();

    Task<IEnumerable<GeoLocationDto>> GetСitiesWithFilter(string filterBySity);

}
