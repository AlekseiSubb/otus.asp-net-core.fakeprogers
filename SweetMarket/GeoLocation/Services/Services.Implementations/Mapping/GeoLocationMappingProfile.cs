using AutoMapper;
using GeoLocation.Domain.Entities.Models;
using GeoLocation.Services.Contracts;

namespace Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для геолокации (пока только город)
    /// </summary>
    public class GeoLocationMappingProfile : Profile
    {
        public GeoLocationMappingProfile()
        {
            CreateMap<GeoLocationDto, City>()
                .ForMember(c => c.Name, g => g.MapFrom(src => src.City)).ReverseMap();
        }
    }
}
