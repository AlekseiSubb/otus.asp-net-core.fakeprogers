﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using GeoLocation.Domain.Entities.Models;
using GeoLocation.Services.Repositories.Abstractions;
using GeoLocation.Services.Abstractions;
using GeoLocation.Services.Contracts;

namespace Services.Implementations
{
    public class GeoLocationService : IGeoLocationService
    {
        private readonly IMapper _mapper;
        private readonly ICityRepository _cityRepository;

        public GeoLocationService(IMapper mapper, ICityRepository cityRepository)
        {
            _mapper = mapper;
            _cityRepository = cityRepository;
        }

        public async Task<GeoLocationDto> GetLocationByIPAsync(string ip)
        {
            using var client = new HttpClient();
            var response = await client.GetAsync($"https://ipwho.is/{ip}?fields=city&lang=ru");
            string jsonResponse = await response.Content.ReadAsStringAsync();
            dynamic stuff = JObject.Parse(jsonResponse);

            string city = stuff.GetValue("city").ToString();
            if (!string.IsNullOrEmpty(city))
                return new GeoLocationDto { City = city };
            else
                return null;
        }

        public async Task<GeoLocationDto> GetLocationAsync()
        {
            using var client = new HttpClient();
            var response = await client.GetAsync($"https://ipwho.is/?lang=ru");
            string jsonResponse = await response.Content.ReadAsStringAsync();
            dynamic stuff = JObject.Parse(jsonResponse);

            string city = stuff.GetValue("city").ToString();
            if (!string.IsNullOrEmpty(city))
                return new GeoLocationDto { City = city };
            else
                return null;
        }


        /// <summary>
        /// Возвращает города миллионики из БД
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<GeoLocationDto>> GetBigСitiesAsync()
        {
            var items = await _cityRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<City>, IEnumerable<GeoLocationDto>>(items);
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> Фиьтр по городу </param>
        /// <returns> Список городов </returns>
        public async Task<IEnumerable<GeoLocationDto>> GetСitiesWithFilter(string filterByСity)
        {
            using var client = new HttpClient();
            var response = await client.GetAsync($"https://kladr-api.ru/api.php?query={filterByСity}&contentType=city&withParent=0&typeCode=1&limit=10");
            string jsonResponse =  await response.Content.ReadAsStringAsync();
            dynamic stuff = JObject.Parse(jsonResponse);//JsonConvert.DeserializeObject(jsonResponse);

            dynamic address = stuff.result;

            List<GeoLocationDto> result = new List<GeoLocationDto>();
            foreach (JObject item in address) // <-- Note that here we used JObject instead of usual JProperty
            {
                string name = item.GetValue("name").ToString();
                string type = item.GetValue("type").ToString();
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(type) && type.ToLower() == "город")
                    result.Add(new GeoLocationDto { City = name });
            }
            return result;
        }

    }
}




