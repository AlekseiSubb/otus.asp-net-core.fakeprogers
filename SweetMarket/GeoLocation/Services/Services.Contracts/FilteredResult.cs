﻿using System.Collections.Generic;

namespace GeoLocation.Services.Contracts
{
    public class FilteredResult<T>
    {
        public IReadOnlyCollection<T> Records { get; set; }
        public int RecordsTotal { get; set; }
    }
}
