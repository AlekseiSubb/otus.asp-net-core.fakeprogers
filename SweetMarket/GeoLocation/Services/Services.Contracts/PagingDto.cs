﻿using System.Collections.Generic;

namespace GeoLocation.Services.Contracts
{
    /// <summary>
    /// ДТО пейджинации
    /// </summary>
    public class PagingDto<T> where T : class
    {
        /// <summary>
        /// Элементы
        /// </summary>
        public List<T> Items { get; set; } = new List<T>();
        /// <summary>
        /// Элементов на странице
        /// </summary>
        public int ItemsPerPage { get; set; }
        /// <summary>
        /// Текущая страница
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Страниц всего
        /// </summary>
        public int TotalPages { get; set; }
    }
}