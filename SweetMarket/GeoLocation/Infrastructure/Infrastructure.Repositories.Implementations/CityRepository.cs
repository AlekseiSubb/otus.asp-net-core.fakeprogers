﻿using GeoLocation.Domain.Entities.Models;
using GeoLocation.Infrastructure.EntityFramework;
using GeoLocation.Services.Repositories.Abstractions;

namespace GeoLocation.Infrastructure.Repositories.Implementations
{
    public class CityRepository : Repository<City, int>, ICityRepository
    {
        public CityRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
