﻿using GeoLocation.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GeoLocation.Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public sealed class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
       
        public DbSet<City> cities { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            InitDb(modelBuilder);
        }

        private void InitDb(ModelBuilder modelBuilder)
        {
            //города-миллионники
            modelBuilder.Entity<City>().HasData(FakeDataFactory.Cities);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(message => System.Diagnostics.Debug.WriteLine(message), LogLevel.Information);
        }
    }
}