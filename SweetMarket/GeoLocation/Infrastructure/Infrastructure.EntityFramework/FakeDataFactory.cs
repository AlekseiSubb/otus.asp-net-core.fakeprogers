﻿using System.Collections.Generic;
using GeoLocation.Domain.Entities.Models;

namespace GeoLocation.Infrastructure.EntityFramework
{
    public static class FakeDataFactory
    {
        public static IEnumerable<City> Cities => new List<City>()
        {
            new() { Id = 1 , Name = "Москва"},
            new() { Id = 2 , Name = "Санкт-Петербург"},
            new() { Id = 3 , Name = "Новосибирск"},
            new() { Id = 4 , Name = "Екатеринбург"},
            new() { Id = 5 , Name = "Казань"},
            new() { Id = 6 , Name = "Нижний Новгород"},
            new() { Id = 7 , Name = "Челябинск"},
            new() { Id = 8 , Name = "Самара"},
            new() { Id = 9 , Name = "Уфа"},
            new() { Id = 10 , Name = "Ростов-на-Дону"},
            new() { Id = 11 , Name = "Омск"},
            new() { Id = 12 , Name = "Красноярск"},
            new() { Id = 13 , Name = "Воронеж"},
            new() { Id = 14 , Name = "Пермь"},
            new() { Id = 15 , Name = "Волгоград"},
            new() { Id = 16 , Name = "Краснодар"},
            new() { Id = 17 , Name = "Тюмень"},
            new() { Id = 18 , Name = "Саратов"}        
        };
    }
}
