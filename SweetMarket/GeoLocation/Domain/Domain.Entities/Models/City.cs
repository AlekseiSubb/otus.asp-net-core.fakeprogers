﻿using System.ComponentModel.DataAnnotations;

namespace GeoLocation.Domain.Entities.Models;

/// <summary>
/// Список городов-миллионников
/// </summary>
public class City: IEntity<int>
{
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

}