﻿using Services.NotificationService.Domain.Abstractions;
namespace Services.NotificationService.Domain.Notifications.SignalR;
public class SignalRNotificationSettings : INotificationSettings<SignalRContact, SignalRNotification>
{
    //public SignalRContact? Contact { get; set; }
    //public SignalRNotification NotificationType { get; set; } = default!;
    public bool Enabled { get; set; }
}