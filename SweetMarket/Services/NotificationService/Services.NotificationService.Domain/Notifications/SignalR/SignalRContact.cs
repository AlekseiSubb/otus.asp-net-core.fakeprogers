﻿using Services.NotificationService.Domain.Abstractions;

namespace Services.NotificationService.Domain.Notifications.SignalR;

public class SignalRContact : IContact
{
    public required  string ConnectionID { get; set; }
    public required string UserAgent { get; set; }
    public bool Connected { get; set; }
    public Guid ServerId { get; set; }
}