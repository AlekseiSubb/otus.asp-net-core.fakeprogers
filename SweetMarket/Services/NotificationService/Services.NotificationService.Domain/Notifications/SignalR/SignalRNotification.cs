﻿using Services.NotificationService.Domain.Abstractions;
namespace Services.NotificationService.Domain.Notifications.SignalR;
public record SignalRNotification() : INotificationType<SignalRContact>
{
    public string Name => "SignalR";
}