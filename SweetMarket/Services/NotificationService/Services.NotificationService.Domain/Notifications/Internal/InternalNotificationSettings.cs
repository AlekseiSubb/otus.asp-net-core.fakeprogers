﻿using Services.NotificationService.Domain.Abstractions;
namespace Services.NotificationService.Domain.Notifications.Internal;
public class InternalNotificationSettings : INotificationSettings<InternalContact, InternalNotification>
{
    //public InternalContact? Contact { get; set; }
    //public InternalNotification NotificationType { get; set; } = default!;
    public bool Enabled { get; set; }
}