﻿using Services.NotificationService.Domain.Abstractions;

namespace Services.NotificationService.Domain.Notifications.Internal;
public record InternalNotification : INotificationType<InternalContact>
{
    public string Name => "Internal";
}