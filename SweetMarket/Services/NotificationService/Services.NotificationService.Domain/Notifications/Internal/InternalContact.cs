﻿using Services.NotificationService.Domain.Abstractions;

namespace Services.NotificationService.Domain.Notifications.Internal;

public class InternalContact : IContact
{
    public int UserId { get; set; }
}