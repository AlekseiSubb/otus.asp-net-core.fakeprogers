﻿using Services.NotificationService.Domain.Abstractions;

namespace Services.NotificationService.Domain.Notifications.Email;
public record EmailNotification : INotificationType<EmailContact>
{
    public string Name => "Email";
}