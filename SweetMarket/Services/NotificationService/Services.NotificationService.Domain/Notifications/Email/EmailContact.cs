﻿using Services.NotificationService.Domain.Abstractions;

namespace Services.NotificationService.Domain.Notifications.Email;

public class EmailContact : IContact
{
    public string Email { get; set; } = default!;
}