﻿using Services.NotificationService.Domain.Abstractions;
namespace Services.NotificationService.Domain.Notifications.Email;
public class EmailNotificationSettings : INotificationSettings<EmailContact, EmailNotification>
{
    //public EmailContact? Contact { get; set; }
    //public EmailNotification NotificationType { get; set; } = default!;
    public bool Enabled { get; set; }
}