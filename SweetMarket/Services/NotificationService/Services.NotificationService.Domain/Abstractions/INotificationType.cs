﻿namespace Services.NotificationService.Domain.Abstractions;

public interface INotificationType<TContact> where TContact : IContact
{
    public string Name { get; }
}