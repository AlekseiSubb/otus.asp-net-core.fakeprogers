﻿using Services.NotificationService.Domain.Notifications.Email;
using Services.NotificationService.Domain.Notifications.Internal;
using Services.NotificationService.Domain.Notifications.SignalR;
using System.Text.Json.Serialization;

namespace Services.NotificationService.Domain.Abstractions;

[JsonPolymorphic]
[JsonDerivedType(typeof(EmailNotificationSettings), typeDiscriminator: "Email")]
[JsonDerivedType(typeof(InternalNotificationSettings), typeDiscriminator: "Internal")]
[JsonDerivedType(typeof(SignalRNotificationSettings), typeDiscriminator: "SignalR")]
public interface INotificationSettings
{
    public bool Enabled { get; set; }
}

internal interface INotificationSettings<TContact, TNotification> : INotificationSettings where TContact : IContact where TNotification : INotificationType<TContact>
{
    //public TContact? Contact { get; set; }
    //public TNotification NotificationType { get; set; }
}