﻿using Services.NotificationService.Domain.Notifications.Email;
using Services.NotificationService.Domain.Notifications.Internal;
using Services.NotificationService.Domain.Notifications.SignalR;
using System.Text.Json.Serialization;

namespace Services.NotificationService.Domain.Abstractions;

[JsonPolymorphic]
[JsonDerivedType(typeof(EmailContact), typeDiscriminator: "Email")]
[JsonDerivedType(typeof(InternalContact), typeDiscriminator: "Internal")]
[JsonDerivedType(typeof(SignalRContact), typeDiscriminator: "SignalR")]
public interface IContact
{
}