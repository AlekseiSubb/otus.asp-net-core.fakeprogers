﻿using MongoDB.Bson;
using Services.NotificationService.Domain.Abstractions;
namespace Services.NotificationService.Domain;
public class Recipient
{
    public ObjectId Id { get; set; }
    public int UserId { get; set; }
    public bool Blocked { get; set; }
    public List<IContact> Contacts { get; set; } = new List<IContact>();
    public List<INotificationSettings> NotificationSettings { get; set; } = new List<INotificationSettings>();
    public List<Notification> Notifications { get; set; } = new List<Notification>();
    public RecipientInternalNotifications InternalNotifications { get; set; } = new RecipientInternalNotifications();
}