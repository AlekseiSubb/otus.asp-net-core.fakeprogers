﻿using MongoDB.Bson;
namespace Services.NotificationService.Domain;
public class RecipientInternalNotifications
{
    public List<ObjectId> InternalNotificationsUnread { get; set; } = new List<ObjectId>();
    public List<ObjectId> InternalNotificationsRead { get; set; } = new List<ObjectId>();
    public List<ObjectId> InternalNotificationsAll { get; set; } = new List<ObjectId>();
}