﻿using MediatR;

namespace Services.NotificationService.Shared.Commands;

public class SendEmailCommand : IRequest<Unit>
{
    /// <summary>
    /// Имя отправителя
    /// </summary>
    public string FromName { get; set; } = null!;

    /// <summary>
    /// Адрес отправителя
    /// </summary>
    public string? FromAddress { get; set; }

    /// <summary>
    /// Тема сообщения
    /// </summary>
    public string Subject { get; set; } = null!;

    /// <summary>
    /// Тело сообщения
    /// </summary>
    public string Body { get; set; } = string.Empty;

    /// <summary>
    /// Тело сообщения в формате html
    /// </summary>
    public bool HtmlBody { get; set; } = false;

    /// <summary>
    /// Список адресов получателей
    /// </summary>
    public List<string> ToAddresses { get; set; } = new List<string>();

    /// <summary>
    /// Список адресов копии
    /// </summary>
    public List<string> ToAddressesCopy { get; set; } = new List<string>();

    /// <summary>
    /// Список адресов скрытой копии
    /// </summary>
    public List<string> ToAddressesShadowCopy { get; set; } = new List<string>();

    /// <summary>
    /// Список вложений
    /// </summary>
    public List<FileDto>? Attachments { get; set; } = new();
}

/// <summary>
/// Модель файла
/// </summary>
public class FileDto
{
    /// <summary>
    /// Данные
    /// </summary>
    public byte[] Data { get; set; } = null!;
    /// <summary>
    /// Имя файла с расширением
    /// </summary>
    public string FileName { get; set; } = null!;
}