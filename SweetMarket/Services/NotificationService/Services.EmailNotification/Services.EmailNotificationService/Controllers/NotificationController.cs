using MediatR;
using Microsoft.AspNetCore.Mvc;
using Services.EmailNotificationService.Commands;

namespace Services.EmailNotificationService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotificationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public NotificationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public Task Send(SendEmailCommandHandler command) => _mediator.Send(command);
    }
}