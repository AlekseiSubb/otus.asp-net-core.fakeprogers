﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;

namespace Services.EmailNotificationService.Middlewares;

public class CustomExceptionHandlingMiddleware : IMiddleware
{
    private readonly ILogger<CustomExceptionHandlingMiddleware> _logger;
    public CustomExceptionHandlingMiddleware(ILogger<CustomExceptionHandlingMiddleware> logger) => _logger = logger;
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            await HandleExceptionAsync(context, e);
        }
    }

    private static async Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
    {
        var statusCode = GetStatusCode(exception);
        var response = new
        {
            title = GetTitle(exception),
            status = statusCode,
            detail = exception.Message,
            source = exception.Source
        };
        httpContext.Response.ContentType = "application/json";
        httpContext.Response.StatusCode = statusCode;
        await httpContext.Response.WriteAsync(JsonSerializer.Serialize(response));
    }

    private static int GetStatusCode(Exception exception) =>
        exception switch
        {
            BadHttpRequestException => StatusCodes.Status400BadRequest,
            WebException => GetStatusCode(exception as WebException),
            ValidationException => StatusCodes.Status422UnprocessableEntity,
            _ => StatusCodes.Status500InternalServerError
        };
    
    private static int GetStatusCode(WebException? exception)
    {
        if (exception?.Response is HttpWebResponse response) return (int)response.StatusCode;
        return (int)HttpStatusCode.InternalServerError;
    }


    private static string GetTitle(Exception exception) =>
        exception switch
        {
            ApplicationException applicationException => "ApplicationException",
            _ => "Server Error"
        };
}