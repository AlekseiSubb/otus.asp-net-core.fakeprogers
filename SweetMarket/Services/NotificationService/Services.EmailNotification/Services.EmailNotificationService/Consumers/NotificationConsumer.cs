﻿using MassTransit;
using MediatR;
using Services.NotificationService.Shared.Commands;
namespace Services.EmailNotificationService.Consumers;

public class NotificationConsumer : IConsumer<SendEmailCommand>
{
    //private readonly ApplicationDbContext _dbContext;
    private readonly IMediator _mediator;

    public NotificationConsumer(/*ApplicationDbContext dbContext, */IMediator mediator)
    {
        //_dbContext = dbContext;
        _mediator = mediator;
    }

    public Task Consume(ConsumeContext<SendEmailCommand> context)
    {
        //await _mediator.Send(new SendEmailCommand
        //{
        //    FromName = context.Message.FromName,
        //    FromAddress = context.Message.FromAddress,
        //    Subject = context.Message.Subject,
        //    Body = context.Message.Body,
        //    HtmlBody = context.Message.HtmlBody,
        //    ToAddresses = context.Message.ToAddresses,
        //    ToAddressesCopy = context.Message.ToAddressesCopy, //пока нет
        //    ToAddressesShadowCopy = context.Message.ToAddressesShadowCopy, //пока нет
        //    Attachments = context.Message.Attachments?.Select(q => new Commands.FileDto
        //    {
        //        Data = q.Data,
        //        FileName = q.FileName,
        //    }).ToList()
        //});
        _mediator.Send(context);
        return Task.CompletedTask;
    }
}