﻿using MediatR;
using Microsoft.Extensions.Options;
using MimeKit;
using Services.EmailNotificationService.Config;
using Services.NotificationService.Shared.Commands;
using System.Net;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace Services.EmailNotificationService.Commands;

public class SendEmailCommandHandler : IRequestHandler<SendEmailCommand, Unit>
{
    private readonly SenderConfiguration _config;

    public SendEmailCommandHandler(IOptions<SenderConfiguration> options)
    {
        _config = options.Value;
    }

    public async Task<Unit> Handle(SendEmailCommand request, CancellationToken cancellationToken)
    {
        using var client = new MailKit.Net.Smtp.SmtpClient();
        client.LocalDomain = _config.LocalDomain;

        var message = CreateMessage(request);
        //var m1 = CreateMessage(new EmailNotificationDto
        //{
        //    Subject = dto.Subject,
        //    Body = dto.Body,
        //    ToAddresses = dto.ToAddresses,
        //    ToAddressesCopy = dto.ToAddressesCopy,
        //    ToAddressesShadowCopy = dto.ToAddressesShadowCopy,
        //    FromName = "SweetMarket Notification Service",
        //    //FromAddress = "notification-sweet-market@rambler.ru", //sender address rejected
        //    FromAddress = "093a86825b454f8fa65890bc89eebc6@rambler.ru",
        //    HtmlBody = false,
        //});
        await client.ConnectAsync(_config.MailServerAddress, Convert.ToInt32(_config.MailServerPort), MailKit.Security.SecureSocketOptions.Auto).ConfigureAwait(false);
        //await client.ConnectAsync("smtp.rambler.ru", 465, MailKit.Security.SecureSocketOptions.Auto).ConfigureAwait(false);

        await client.AuthenticateAsync(new NetworkCredential(_config.UserId, _config.UserPassword)).ConfigureAwait(false);
        //await client.AuthenticateAsync(new NetworkCredential("093a86825b454f8fa65890bc89eebc6", "KxZJNxeqLkdf_5f")).ConfigureAwait(false);

        await client.SendAsync(message).ConfigureAwait(false);
        await client.DisconnectAsync(true).ConfigureAwait(false);
        return new Unit();
    }

    private MimeMessage CreateMessage(SendEmailCommand command)
    {
        var m = new MimeMessage()
        {
            Subject = command.Subject,
        };

        m.From.Add(new MailboxAddress(command.FromName, !string.IsNullOrWhiteSpace(command.FromAddress) ? command.FromAddress : _config.FromAddressBase));

        foreach (var address in command.ToAddresses)
        {
            m.To.Add(InternetAddress.Parse(address));
        }

        if (command.ToAddressesCopy != null)
        {
            foreach (var address in command.ToAddressesCopy)
            {
                m.Cc.Add(InternetAddress.Parse(address));
            }
        }

        if (command.ToAddressesShadowCopy != null)
        {
            foreach (var address in command.ToAddressesShadowCopy)
            {
                m.Bcc.Add(InternetAddress.Parse(address));
            }
        }

        var bodyBuilder = new BodyBuilder();
        if (command.HtmlBody == true) bodyBuilder.HtmlBody = command.Body;
        else bodyBuilder.TextBody = command.Body;

        if (command.Attachments?.Any() is true)
        {
            foreach (var attachment in command.Attachments)
            {
                bodyBuilder.Attachments.Add(attachment.FileName, attachment.Data);
            }
        }
        m.Body = bodyBuilder.ToMessageBody();

        return m;
    }
}
