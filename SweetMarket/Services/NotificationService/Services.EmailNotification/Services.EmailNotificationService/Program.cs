using MassTransit;
using Services.EmailNotificationService.Consumers;
using Services.EmailNotificationService.Middlewares;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<CustomExceptionHandlingMiddleware>();
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

builder.Services.AddMassTransit(q =>
{
    q.AddConsumer<NotificationConsumer>();
    q.UsingRabbitMq((context, config) =>
    {
        config.Host(builder.Configuration["RabbitMq:Host"], virtualHost: builder.Configuration["RabbitMq:VirtualHost"], q =>
        {
            q.Username(builder.Configuration["RabbitMq:Username"]);
            q.Password(builder.Configuration["RabbitMq:Password"]);
        });

        config.ReceiveEndpoint(builder.Configuration.GetValue<string>("MassTransit:Consumers:NotificationConsumer:Queue")!, e =>
        {
            e.ConfigureConsumer<NotificationConsumer>(context);
            e.UseMessageRetry(r =>
            {
                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
            });
        });
    });
});

var app = builder.Build();

app.UseMiddleware<CustomExceptionHandlingMiddleware>();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
