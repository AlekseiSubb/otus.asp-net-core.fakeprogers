using System.IdentityModel.Tokens.Jwt;
using MassTransit;
using Microsoft.AspNetCore.Diagnostics;
using MongoDB.Bson;
using NLog.Web;
using Services.NotificationService.Application.Consumers;
using Services.NotificationService.Domain;
using Services.NotificationService.Infrastructure;
using Services.NotificationService.Infrastructure.Configurations;
using Services.NotificationService.Infrastructure.Extensions;
using Services.NotificationService.Infrastructure.Repositories;
using Services.NotificationService.Infrastructure.Services;
using Services.NotificationService.MailConfiguration;
using Services.NotificationService.Services;
using Services.NotificationService.Services.NotificationServices;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using Services.NotificationService.Configuration;

var builder = WebApplication.CreateBuilder(args);

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
        options.Audience = "sweetmarket_notification_api";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new()
        {
            RoleClaimType = "role",
            ValidTypes = new[] { "at+jwt" }
        };
    });


//Add config
builder.Services.AddOptions<EmailSenderConfiguration>().Bind(
    builder.Configuration.GetSection("EmailSenderConfiguration"));

builder.Services.AddOptions<SignalRConfiguration>().Bind(
    builder.Configuration.GetSection("SignalR"));

//Background services
builder.Services.AddHostedService<SignalRConnectionCleanerBackgroundService>();


// Add services to the container.
builder.Services.AddMongoDb(builder.Configuration.GetConnectionString("MongoDb"), builder.Configuration.GetValue<string>("MongoDatabaseName"));
builder.Services.AddScoped<ApplicationDbContext>();
builder.Services.AddTransient<IRepository<Recipient, ObjectId>, RecipientRepository>();
builder.Services.AddTransient<IRepository<Notification, ObjectId>, NotificationRepository>();

builder.Services.AddScoped<SignalRConnectionsService>();


//Senders
builder.Services.AddTransient<NotificationServiceFactory>();
builder.Services.AddTransient<INotificationService, InternalNotificationService>();
builder.Services.AddTransient<INotificationService, EmailNotificationService>();
builder.Services.AddTransient<INotificationService, SignalRNotificationService>();
//builder.Services.AddTransient<INotificationService<EmailContact>, EmailNotificationService>();

//builder.Services.AddScoped<NotificationManagerService>();

//builder.Services.AddScoped<EmailNotificationService>();

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies()));

builder.Services.AddMassTransit(q =>
{
    q.AddConsumer<NotificationConsumer>();
    q.UsingRabbitMq((context, config) =>
    {
        config.Host(builder.Configuration["RabbitMq:Host"], virtualHost: builder.Configuration["RabbitMq:VirtualHost"], q =>
        {
            q.Username(builder.Configuration["RabbitMq:Username"]);
            q.Password(builder.Configuration["RabbitMq:Password"]);
        });

        config.ReceiveEndpoint(builder.Configuration.GetValue<string>("MassTransit:Consumers:NotificationConsumer:Queue")!, e =>
        {
            e.ConfigureConsumer<NotificationConsumer>(context);
            e.UseMessageRetry(r =>
            {
                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
            });
        });
    });
});

builder.Services.AddSignalR(options =>
{
    options.EnableDetailedErrors = true;
    options.KeepAliveInterval = TimeSpan.FromSeconds(15);
    options.ClientTimeoutInterval = TimeSpan.FromSeconds(30);
});

builder.Host.UseNLog(new NLogAspNetCoreOptions
{
    //RemoveLoggerFactoryFilter = false, //�� ��������� true, �� ���� ������� ����������� �������, �� � ��, �� ��� �� ������.
    //ReplaceLoggerFactory = true, //������� ��������� �������
});

builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<SwaggerGenOptions>(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/authorize"),
                TokenUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/token"),
                Scopes = new Dictionary<string, string>
                {
                    {"openid", ""},
                    {"email", ""},
                    {"profile", ""},
                    {"roles", ""},
                    {"sweetmarket_notification_api.fullaccess", ""}
                },
            },
        },
    });
    options.OperationFilter<AuthorizeCheckOperationFilter>();
});

var app = builder.Build();

app.UseExceptionHandler(c => c.Run(async context =>
{
    var exception = context.Features.Get<IExceptionHandlerPathFeature>()?.Error;
    var factory = c.ApplicationServices.GetService<ILoggerFactory>();
    var logger = factory?.CreateLogger(AppDomain.CurrentDomain.FriendlyName);
    logger?.LogError(new EventId(222), exception, exception?.Message);
    var response = new { error = exception?.Message ?? "Unknown exception" };
    await context.Response.WriteAsJsonAsync(response);
}));

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.OAuthClientId("sweetmarket_web_frontend_client");
    c.OAuthClientSecret("LwZjqN6XCPCtnt0ZKYUiD1jGbRbe2aepqtGfxsuk9D");
    c.EnablePersistAuthorization();
});

//app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.MapHub<NotificationsHub>("/hubs/notification", options =>
{
    options.ApplicationMaxBufferSize = 1 * 1024 * 1024;

    options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets |
    Microsoft.AspNetCore.Http.Connections.HttpTransportType.LongPolling |
    Microsoft.AspNetCore.Http.Connections.HttpTransportType.ServerSentEvents;

    options.LongPolling.PollTimeout = TimeSpan.FromSeconds(30);
});

app.Run();
