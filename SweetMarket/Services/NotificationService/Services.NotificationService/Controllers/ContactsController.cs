using MassTransit.Initializers;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Notifications.Email;
using Services.NotificationService.Domain.Notifications.Internal;
using Services.NotificationService.Infrastructure.Repositories;
using System.Net;

namespace Services.NotificationService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactsController : ControllerBase
    {
        private readonly IRepository<Recipient, ObjectId> _repository;
        private readonly ILogger _logger;

        public ContactsController(IRepository<Recipient, ObjectId> repository, ILogger<ContactsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserContacts(int userId)
        {
            var recipient = await _repository.GetFirstOrDefaultWhere(q => q.UserId == userId);
            if (recipient == null) return NotFound();
            return Ok(recipient.Contacts);
        }

        //[HttpDelete]
        //public async Task<IActionResult> DeleteContact(int userId)
        //{
        //    var recipient = await _repository.GetFirstOrDefaultWhere(x => x.UserId == userId);
        //    if (recipient == null) return NotFound();
        //    var result = await _repository.DeleteAsync(recipient); //������� ������
        //    return result ? Ok() : NotFound();
        //}

        [HttpPost("AddEmailContact")]
        public async Task<int> AddEmailContact(int userId, string email)
        {
            var recipient = await _repository.GetFirstOrDefaultWhere(q => q.UserId == userId);
            if (recipient == null) throw new Exception();
            var emailContact = recipient.Contacts.FirstOrDefault(q => q is EmailContact) as EmailContact;
            if (emailContact != null) emailContact.Email = email;
            else recipient.Contacts.Add(new EmailContact { Email = email });
            await _repository.UpdateAsync(recipient);
            return recipient.UserId;
        }
    }
}