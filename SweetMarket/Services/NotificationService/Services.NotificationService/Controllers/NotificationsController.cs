using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Services.NotificationService.Application.Commands;
using Services.NotificationService.Domain;
using Services.NotificationService.Infrastructure.Repositories;

namespace Services.NotificationService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotificationsController : ControllerBase
    {
        private readonly IRepository<Notification, ObjectId> _repository;
        private readonly IRepository<Recipient, ObjectId> _recipientRepository;
        private readonly IBus _bus;
        private readonly ILogger<NotificationsController> _logger;
        private readonly IMediator _mediator;

        public NotificationsController(IRepository<Notification, ObjectId> repository, IRepository<Recipient, ObjectId> recipientRepository, IBus bus, ILogger<NotificationsController> logger, IMediator mediator)
        {
            _repository = repository;
            _recipientRepository = recipientRepository;
            _bus = bus;
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet("list")]
        public async Task<IEnumerable<Notification>> GetAllMessages(int userId)
        {
            var recipient = await _recipientRepository.GetFirstOrDefaultWhere(q => q.UserId == userId);
            if (recipient == null) return Enumerable.Empty<Notification>();
            var messages = await _repository.GetRangeByIdsAsync(recipient.InternalNotifications.InternalNotificationsAll);
            return messages.Select(q => new Notification
            {
                Id = q.Id,
                MessageType = q.MessageType,
                Text = q.Text,
                RecievingDate = q.RecievingDate,
            });
        }

        [HttpPost("Send")]
        public async Task SendSmth()
        {
            var c = (await _repository.GetAllAsync()).Count;

            var msg = new SendNotificationCommand
            {
                Text = $"Test message {c + 1}",
                Recipients = new List<int> { 1 },
                MessageType = new Random((int)DateTime.Now.ToFileTimeUtc()).Next(0, 100) % 2 == 0 ? "Order" : "Advertisment",
            };

            var not = await _repository.GetFirstOrDefaultWhere(q => q.Text == msg.Text);
            if (not == null)  await _repository.AddAsync(new Notification
            {
                MessageType = "TestType",
                Text = msg.Text,
                RecievingDate = DateTime.Now,
            });

            var sendEndpoint = await _bus.GetSendEndpoint(new Uri("queue:cmd.Notification.NotificationService.SendNotification"));
            await sendEndpoint.Send(msg);
        }

    }
}