using MassTransit.Initializers;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Services.NotificationService.Domain;
using Services.NotificationService.Infrastructure.Repositories;
using Services.NotificationService.Models;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace Services.NotificationService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RecipientController : ControllerBase
    {
        private readonly IRepository<Recipient, ObjectId> _repository;
        private readonly ILogger _logger;

        public RecipientController(IRepository<Recipient, ObjectId> repository, ILogger<RecipientController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        //delete
        [HttpGet("list")]
        public async Task<IActionResult> GetAllRecipients()
        {
            var identity = User.Identity;

            var search = await _repository.GetAllAsync();
            var result = search.Select(q => new
            {
                q.UserId,
                q.Blocked
            });
            return Ok(result);
        }
        
        [HttpGet("{userId:int}")]
        [ProducesResponseType(typeof(RecipientModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetRecipient(int userId)
        {
            var search = await _repository.GetFirstOrDefaultWhere(q => q.UserId == userId);
            if (search == null) return NotFound();
            return Ok(new RecipientModel
            {
                Blocked = search.Blocked,
                InternalNotificationsCount = search.InternalNotifications.InternalNotificationsAll.Capacity,
                InternalNotificationsUnreadCount = search.InternalNotifications.InternalNotificationsUnread.Capacity,
            });
        }

        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Add(int userId)
        {
            if (await _repository.GetFirstOrDefaultWhere(x => x.UserId == userId) != null) return BadRequest();
            var recipient = new Recipient { UserId = userId };
            await _repository.AddAsync(recipient);
            return Ok(recipient.UserId);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int userId)
        {
            var recipient = await _repository.GetFirstOrDefaultWhere(x => x.UserId == userId);
            if (recipient == null) return NotFound();
            var result = await _repository.DeleteAsync(recipient); //������� ������
            return result ? Ok() : NotFound();
        }
    }
}