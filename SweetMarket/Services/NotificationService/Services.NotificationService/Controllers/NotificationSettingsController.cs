using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Notifications.Email;
using Services.NotificationService.Infrastructure.Repositories;

namespace Services.NotificationService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotificationSettingsController : ControllerBase
    {
        private readonly IRepository<Recipient, ObjectId> _repository;
        private readonly ILogger _logger;

        public NotificationSettingsController(IRepository<Recipient, ObjectId> repository, ILogger<NotificationSettingsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetAllUserNotificationSettings(int userId)
        {
            var recipient = await _repository.GetFirstOrDefaultWhere(q => q.UserId == userId);
            if (recipient == null) return NotFound();
            return Ok(recipient.NotificationSettings);
        }

        //[HttpDelete]
        //public async Task<IActionResult> Delete(int userId)
        //{
        //    var recipient = await _repository.GetFirstOrDefaultWhere(x => x.UserId == userId);
        //    if (recipient == null) return NotFound();
        //    var result = await _repository.DeleteAsync(recipient); //������� ������
        //    return result ? Ok() : NotFound();
        //}

        [HttpPost("AddEmailNotificationSettings")]
        public async Task<int> AddEmailNotificationSettings(int userId, bool enabled)
        {
            var recipient = await _repository.GetFirstOrDefaultWhere(q => q.UserId == userId);
            if (recipient == null) throw new Exception();
            var emailContact = recipient.NotificationSettings.FirstOrDefault(q => q is EmailNotificationSettings) as EmailNotificationSettings;
            if (emailContact != null) emailContact.Enabled = enabled;
            else recipient.NotificationSettings.Add(new EmailNotificationSettings { Enabled = enabled });
            await _repository.UpdateAsync(recipient);
            return recipient.UserId;
        }
    }
}