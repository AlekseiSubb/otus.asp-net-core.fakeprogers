﻿using MediatR;
using MongoDB.Bson;
using Services.NotificationService.Application.Commands;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Notifications.Internal;
using Services.NotificationService.Infrastructure.Repositories;
using Services.NotificationService.Services;
using Services.NotificationService.Services.NotificationServices;

namespace Services.NotificationService.CommandHandlers
{
    public class SendNotificationCommandHandler : IRequestHandler<SendNotificationCommand, Unit>
    {
        private readonly IRepository<Recipient, ObjectId> _recipientRepository;
        private readonly IRepository<Notification, ObjectId> _notificationRepository;
        private readonly NotificationServiceFactory _factory;
        private readonly ILogger _logger;

        public SendNotificationCommandHandler(IRepository<Recipient, ObjectId> recipientRepository, IRepository<Notification, ObjectId> notificationRepository,
            NotificationServiceFactory _factory, ILogger<SendNotificationCommandHandler> logger)
        {
            _recipientRepository = recipientRepository;
            _notificationRepository = notificationRepository;
            this._factory = _factory;
            _logger = logger;
        }

        public async Task<Unit> Handle(SendNotificationCommand request, CancellationToken cancellationToken)
        {
            var recipients = await _recipientRepository.GetWhere(q => request.Recipients.Contains(q.UserId));
            var notification = new Notification
            {
                MessageType = request.MessageType,
                RecievingDate = DateTime.UtcNow,
                Text = request.Text
            };

            await _notificationRepository.AddAsync(notification);

            foreach (var recipient in recipients)
            {
                var notificationServices = _factory.GetNotificationService(recipient);
                foreach(var service in notificationServices)
                {
                    if (service is not InternalNotificationService) await service.Send(notification.Id);
                }
                var internalNotificationService = _factory.GetRequiredNotificationService<InternalContact>();
                internalNotificationService.SetContact(new InternalContact { UserId = recipient.UserId });
                await internalNotificationService.Send(notification.Id);
            }
            return Unit.Value;
        }
    }
}
