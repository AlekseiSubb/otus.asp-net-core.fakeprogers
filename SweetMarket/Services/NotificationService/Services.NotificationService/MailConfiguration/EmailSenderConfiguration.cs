﻿namespace Services.NotificationService.MailConfiguration;

public class EmailSenderConfiguration
{
    public string LocalDomain { get; set; } = null!;

    public string MailServerAddress { get; set; } = null!;
    public string MailServerPort { get; set; } = null!;

    public string UserId { get; set; } = null!;
    public string UserPassword { get; set; } = null!;

    public string FromAddressBase { get; set; } = null!;
}