﻿namespace Services.NotificationService.Models
{
    public class RecipientModel
    {
        public bool Blocked { get; set; }
        public int InternalNotificationsCount { get; set; }
        public int InternalNotificationsUnreadCount { get; set; }
    }
}
