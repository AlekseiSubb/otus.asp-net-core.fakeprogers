﻿using MongoDB.Bson;

namespace Services.NotificationService.Models
{
    public class NotificationsItem
    {
        public ObjectId Id { get; set; }
        public string? Text { get; set; }
        public string? MessageType { get; set; } //Пока Avertisment/Order, задаём ручками
                                                 //public string? Source { get; set; } //Источник (пока не делаем)
        public DateTimeOffset? RecievingDate { get; set; }
    }
}
