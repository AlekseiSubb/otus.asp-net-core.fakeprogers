﻿using MassTransit;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver.Core.Connections;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Abstractions;
using Services.NotificationService.Domain.Notifications.SignalR;
using Services.NotificationService.Infrastructure;
using Services.NotificationService.Infrastructure.Configurations;
using Services.NotificationService.Infrastructure.Repositories;
using System.Net;

namespace Services.NotificationService.Services.NotificationServices
{
    public class SignalRNotificationService : INotificationService<SignalRContact>
    {
        private readonly IHubContext<NotificationsHub, INotificationsHubClient> _hubContext;
        private List<SignalRContact> contacts = new List<SignalRContact>();
        private readonly IRepository<Notification, ObjectId> _notificationRepository;
        private readonly Guid _serverId;
        private readonly ILogger _logger;
        public SignalRNotificationService(IHubContext<NotificationsHub, INotificationsHubClient> hubContext, IOptions<SignalRConfiguration> options, IRepository<Notification, ObjectId> notificationRepository, ILogger<SignalRNotificationService> logger)
        {
            _hubContext = hubContext;
            _serverId = options.Value.ServerId;
            _notificationRepository = notificationRepository;
            _logger = logger;
            //emailQueue = configuration.GetValue<string>("MassTransit:ExternalEndpoints:EmailNotification") ?? throw new Exception("No email notification endpoints declared");
        }

        public async Task Send(ObjectId notificationId)
        {
            if (!contacts.Any()) throw new Exception("Contact was not set");
            var notification = await _notificationRepository.GetByIdAsync(notificationId);
            var contactsToSend = contacts.Where(q => q.Connected && q.ServerId == _serverId);

            //var i = 0;
            //foreach (var contact in contactsToSend)
            //{
            //    await _hubContext.Clients.Client(contact.ConnectionID).RecieveNotification(notification.MessageType ?? "Default", notification.Text ?? "").ConfigureAwait(false);
            //    _logger.LogWarning("{i}. {ConnectionID}\r\n{Subject}, {Message}", i, contact.ConnectionID, notification.MessageType ?? "Default", notification.Text ?? "");
            //    i++;
            //}

            contactsToSend.ToList().ForEach(async q =>
            {
                await _hubContext.Clients.Client(q.ConnectionID)
                .RecieveNotification(notification.MessageType ?? "Default", notification.Text ?? "").ConfigureAwait(false);
            });
        }

        public void SetContact(IContact contact)
        {
            if (contact is SignalRContact signalRContact && !contacts.Any(q => q == signalRContact)) contacts.Add(signalRContact);
        }
        public void SetContacts(List<IContact> contacts) => contacts.ForEach(SetContact);
    }
}
