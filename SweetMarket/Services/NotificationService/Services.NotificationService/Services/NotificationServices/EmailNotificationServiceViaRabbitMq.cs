﻿using MassTransit;
using MongoDB.Bson;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Abstractions;
using Services.NotificationService.Domain.Notifications.Email;
using Services.NotificationService.Infrastructure.Repositories;
using Services.NotificationService.Shared.Commands;

namespace Services.NotificationService.Services.NotificationServices
{
    public class EmailNotificationServiceViaRabbitMq : INotificationService<EmailContact>
    {
        private readonly IBus _bus;
        private readonly string emailQueue;
        private List<IContact> contacts = new List<IContact>();
        private readonly IRepository<Notification, ObjectId> _notificationRepository;

        public EmailNotificationServiceViaRabbitMq(IBus bus, IConfiguration configuration, IRepository<Notification, ObjectId> notificationRepository)
        {
            _bus = bus;
            emailQueue = configuration.GetValue<string>("MassTransit:ExternalEndpoints:EmailNotification") ?? throw new Exception("No email notification endpoints declared");
            _notificationRepository = notificationRepository;
        }

        public async Task Send(ObjectId notificationId)
        {
            if (!contacts.Any()) throw new Exception("Contact was not set");
            foreach (var contact in contacts)
            {
                await SendNotificationToContact(notificationId, contact);
            }
        }

        private async Task SendNotificationToContact(ObjectId notificationId, IContact contact)
        {
            var notification = await _notificationRepository.GetByIdAsync(notificationId);
            var endpoint = await _bus.GetSendEndpoint(new Uri($"queue:{emailQueue}"));
            if (contact == null) throw new Exception("Contact was not sent");
            if (contact is not EmailContact emailContact) return;
            await endpoint.Send(new SendEmailCommand
            {
                FromName = "SweetMarket Notification Service",
                FromAddress = null,
                Subject = "Common",
                Body = notification.Text ?? string.Empty,
                HtmlBody = false,
                ToAddresses = new List<string> { emailContact.Email },
                //ToAddressesCopy = request.ToAddressesCopy, //пока нет
                //ToAddressesShadowCopy = request.ToAddressesShadowCopy, //пока нет
                //Attachments = request.Attachments, //пока нет
            });
        }

        public void SetContact(IContact contact)
        {
            if (!contacts.Any(q => q == contact)) contacts.Add(contact);
        }
        public void SetContacts(List<IContact> contacts) => contacts.ForEach(SetContact);
    }
}
