﻿using MongoDB.Bson;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Abstractions;
using Services.NotificationService.Domain.Notifications.Internal;
using Services.NotificationService.Infrastructure.Repositories;
using System.Net;

namespace Services.NotificationService.Services.NotificationServices
{
    public class InternalNotificationService : INotificationService<InternalContact>
    {
        private List<IContact> contacts = new List<IContact>();
        private readonly IRepository<Recipient, ObjectId> _recipientRepository;
        private readonly IRepository<Notification, ObjectId> _notificationRepository;

        public InternalNotificationService(IRepository<Recipient, ObjectId> repository, IRepository<Notification, ObjectId> notificationRepository)
        {
            _recipientRepository = repository;
            _notificationRepository = notificationRepository;
        }

        public async Task Send(ObjectId notificationId)
        {
            if (!contacts.Any()) throw new Exception("Contact was not set");
            foreach (var contact in contacts)
            {
                await SendNotificationToContact(notificationId, contact);
            }
        }

        private async Task SendNotificationToContact(ObjectId notificationId, IContact contact)
        {
            var notification = await _notificationRepository.GetByIdAsync(notificationId);
            if (contact == null) throw new Exception("Contact was not sent");
            if (contact is not InternalContact internalContact) return;
            var recipient = await _recipientRepository.GetFirstOrDefaultWhere(q => q.UserId == internalContact.UserId);
            if (recipient == null) return;
            recipient.InternalNotifications.InternalNotificationsAll.Add(notificationId);
            recipient.InternalNotifications.InternalNotificationsUnread.Add(notificationId);
            await _recipientRepository.UpdateAsync(recipient);
        }

        public void SetContact(IContact contact)
        {
            if (!contacts.Any(q => q == contact)) contacts.Add(contact);
        }
        public void SetContacts(List<IContact> contacts) => contacts.ForEach(SetContact);
    }
}
