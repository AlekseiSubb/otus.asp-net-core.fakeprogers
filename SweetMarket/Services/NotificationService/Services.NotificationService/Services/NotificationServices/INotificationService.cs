﻿using MongoDB.Bson;
using Services.NotificationService.Domain.Abstractions;
namespace Services.NotificationService.Services.NotificationServices;
public interface INotificationService
{
    public Task Send(ObjectId notificationId);
    public void SetContact(IContact contact);
    public void SetContacts(List<IContact> contacts);
}
public interface INotificationService<TContact> : INotificationService where TContact : IContact
{
}