﻿using MassTransit;
using MimeKit;
using MongoDB.Bson;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Abstractions;
using Services.NotificationService.Domain.Notifications.Email;
using System.Net;
using Services.NotificationService.MailConfiguration;
using Microsoft.Extensions.Options;
using Services.NotificationService.Infrastructure.Repositories;

namespace Services.NotificationService.Services.NotificationServices
{
    public class EmailNotificationService : INotificationService<EmailContact>
    {
        private List<IContact> contacts = new List<IContact>();
        private readonly IRepository<Notification, ObjectId> _notificationRepository;
        private readonly EmailSenderConfiguration _configuration;

        public EmailNotificationService(IOptions<EmailSenderConfiguration> options, IRepository<Notification, ObjectId> notificationRepository)
        {
            _configuration = options.Value ?? throw new Exception("No email sender configuration found");
            _notificationRepository = notificationRepository;
        }

        public async Task Send(ObjectId notificationId)
        {
            if (!contacts.Any()) throw new Exception("Contact was not set");
            foreach (var contact in contacts)
            {
                await SendNotificationToContact(notificationId, contact);
            }
        }
        
        private async Task SendNotificationToContact(ObjectId notificationId, IContact contact)
        {
            using var client = new MailKit.Net.Smtp.SmtpClient();
            client.LocalDomain = _configuration.LocalDomain;

            var notification = await _notificationRepository.GetByIdAsync(notificationId);
            if (contact is not EmailContact emailContact) return;
            var message = CreateMessage(notification, emailContact);
            await client.ConnectAsync(_configuration.MailServerAddress, Convert.ToInt32(_configuration.MailServerPort), MailKit.Security.SecureSocketOptions.Auto).ConfigureAwait(false);
            
            await client.AuthenticateAsync(new NetworkCredential(_configuration.UserId, _configuration.UserPassword)).ConfigureAwait(false);
            
            await client.SendAsync(message).ConfigureAwait(false);
            await client.DisconnectAsync(true).ConfigureAwait(false);
        }

        public void SetContact(IContact contact)
        {
            if (!contacts.Any(q => q == contact)) contacts.Add(contact);
        }
        public void SetContacts(List<IContact> contacts) => contacts.ForEach(SetContact);

        private MimeMessage CreateMessage(Notification notification, EmailContact contact)
        {
            var messageObject = new 
            {
                FromName = "SweetMarket Notification Service",
                FromAddress = _configuration.FromAddressBase,
                Subject = "Common",
                Body = notification.Text ?? string.Empty,
                HtmlBody = false,
                ToAddresses = new List<string> { contact.Email },
                //ToAddressesCopy = request.ToAddressesCopy, //пока нет
                //ToAddressesShadowCopy = request.ToAddressesShadowCopy, //пока нет
                //Attachments = request.Attachments, //пока нет
            };

            var m = new MimeMessage()
            {
                Subject = messageObject.Subject,
            };

            m.From.Add(new MailboxAddress(messageObject.FromName, messageObject.FromAddress));

            foreach (var address in messageObject.ToAddresses)
            {
                m.To.Add(InternetAddress.Parse(address));
            }

            //if (messageObject.ToAddressesCopy != null)
            //{
            //    foreach (var address in messageObject.ToAddressesCopy)
            //    {
            //        m.Cc.Add(InternetAddress.Parse(address));
            //    }
            //}

            //if (messageObject.ToAddressesShadowCopy != null)
            //{
            //    foreach (var address in messageObject.ToAddressesShadowCopy)
            //    {
            //        m.Bcc.Add(InternetAddress.Parse(address));
            //    }
            //}

            var bodyBuilder = new BodyBuilder();
            if (messageObject.HtmlBody == true) bodyBuilder.HtmlBody = messageObject.Body;
            else bodyBuilder.TextBody = messageObject.Body;

            //if (messageObject.Attachments?.Any() is true)
            //{
            //    foreach (var attachment in messageObject.Attachments)
            //    {
            //        bodyBuilder.Attachments.Add(attachment.FileName, attachment.Data);
            //    }
            //}
            m.Body = bodyBuilder.ToMessageBody();

            return m;
        }
    }
}
