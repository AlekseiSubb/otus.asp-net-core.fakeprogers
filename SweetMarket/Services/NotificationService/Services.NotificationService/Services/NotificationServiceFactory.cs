﻿using Services.NotificationService.Domain;
using Services.NotificationService.Services.NotificationServices;
using static MassTransit.ValidationResultExtensions;

namespace Services.NotificationService.Services;
public class NotificationServiceFactory
{
    private readonly List<INotificationService> notificationServices;

    public NotificationServiceFactory(IServiceProvider serviceProvider)
    {
        notificationServices = serviceProvider.GetServices<INotificationService>().ToList();
    }

    public List<INotificationService> GetNotificationService(Recipient recipient)
    {
        var contactGroups = recipient.Contacts.GroupBy(q => q.GetType()).Select(q => q.ToList()).ToList();
        var result = new List<INotificationService>();
        foreach(var contacts in contactGroups)
        {
            var contactType = contacts.FirstOrDefault()?.GetType();
            var notificationSettings = recipient.NotificationSettings
                .FirstOrDefault(q => q.GetType().GetInterfaces()
                .Where(q => q.IsGenericType)?.Any(q => q.GenericTypeArguments?.Any(q => q == contactType) == true) == true);
            if (notificationSettings != null)
            {
                //Тут работа с настройками, пока отправляется всё
            }
            var notificationService = notificationServices.FirstOrDefault(q => q.GetType().GetInterfaces()
            .Where(q => q.IsGenericType).Any(q => q.GenericTypeArguments?.Any(q => q == contactType) == true) == true);
            if (notificationService != null)
            {
                notificationService.SetContacts(contacts);
                result.Add(notificationService);
            }
        }
        return result;
    }

    public INotificationService? GetNotificationService<TContact>(TContact contact)
    {
        if (contact == null) return null;
        var contactType = contact.GetType();
        return notificationServices.FirstOrDefault(q => q.GetType().GetInterfaces()
        .Where(q => q.IsGenericType).Any(q => q.GenericTypeArguments?.Any(q => q == contactType) == true) == true);
    }
    
    public INotificationService GetRequiredNotificationService<TContact>(TContact contact)
    {
        if (contact == null) throw new ArgumentNullException(nameof(ConfigurationRoot));
        var contactType = contact.GetType();
        return notificationServices.First(q => q.GetType().GetInterfaces()
        .Where(q => q.IsGenericType).Any(q => q.GenericTypeArguments?.Any(q => q == contactType) == true) == true);
    }

    public INotificationService? GetNotificationService<TContact>()
    {
        var contactType = typeof(TContact);
        return notificationServices.FirstOrDefault(q => q.GetType().GetInterfaces()
        .Where(q => q.IsGenericType).Any(q => q.GenericTypeArguments?.Any(q => q == contactType) == true) == true);
    }

    public INotificationService GetRequiredNotificationService<TContact>()
    {
        var contactType = typeof(TContact);
        return notificationServices.First(q => q.GetType().GetInterfaces()
        .Where(q => q.IsGenericType).Any(q => q.GenericTypeArguments?.Any(q => q == contactType) == true) == true);
    }
}