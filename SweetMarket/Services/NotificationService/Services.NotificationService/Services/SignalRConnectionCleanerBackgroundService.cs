﻿using Microsoft.Extensions.Options;
using Services.NotificationService.Infrastructure.Configurations;
using Services.NotificationService.Infrastructure.Services;

namespace Services.NotificationService.Services
{
    public class SignalRConnectionCleanerBackgroundService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly TimeSpan _cleanupInterval;
        private readonly Guid _serverId;
        private readonly ILogger _logger;

        public SignalRConnectionCleanerBackgroundService(IServiceProvider serviceProvider, IOptions<SignalRConfiguration> options,
            ILogger<SignalRConnectionCleanerBackgroundService> logger)
        {
            _serviceProvider = serviceProvider;
            var config = options.Value;
            var intervalValue = config.CleanupInterval > 0 && config.CleanupInterval <= int.MaxValue ? (int)config.CleanupInterval : 5;
            var intervalUnit = config.CleanupIntervalUnit ?? IntervalUnit.Seconds;
            _cleanupInterval =  intervalUnit switch
            {
                IntervalUnit.Seconds => TimeSpan.FromSeconds(intervalValue),
                IntervalUnit.Minutes => TimeSpan.FromMinutes(intervalValue),
                IntervalUnit.Hours => TimeSpan.FromHours(intervalValue),
                IntervalUnit.Days => TimeSpan.FromDays(intervalValue),
                _ => TimeSpan.FromSeconds(intervalValue),
            };
            _serverId = config.ServerId;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("SignalR contacts clear interval [{interval}]", _cleanupInterval.ToString("dd\\ hh\\:mm\\:ss"));
            using var scope = _serviceProvider.CreateScope();
            var connectionsService = scope.ServiceProvider.GetRequiredService<SignalRConnectionsService>();
            await connectionsService.ClearAllServerContacts(stoppingToken);
            _logger.LogInformation("All SignalR contacts with server id [{ServerId}] was cleared", _serverId);
            await Task.Delay(_cleanupInterval, stoppingToken);
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("All SignalR disconnected contacts with server id [{ServerId}] was cleared", _serverId);
                await connectionsService.ClearServerDisconnectedContacts(stoppingToken);
                await Task.Delay(_cleanupInterval, stoppingToken);
            }
        }
    }
}
