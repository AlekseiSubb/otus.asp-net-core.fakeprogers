﻿using MediatR;
using Microsoft.Extensions.Logging;
using Services.NotificationService.Application.Commands;
using Services.NotificationService.EmailNotification.Models;

namespace Services.NotificationService.EmailNotification.CommandHandlers;

public class SendEmailMessageCommandHandler : IRequestHandler<SendEmailMessageCommand, Unit>
{
    private readonly EmailNotificationService _notificationService;

    public SendEmailMessageCommandHandler(EmailNotificationService notificationService)
    {
        _notificationService = notificationService;
    }

    public async Task<Unit> Handle(SendEmailMessageCommand request, CancellationToken cancellationToken)
    {
        try
        {
            await _notificationService.SendEmail(new EmailNotificationDto
            {
                FromName = request.FromName,
                FromAddress = request.FromAddress ?? string.Empty,
                Subject = request.MessageSubject,
                Body = request.Text,
                HtmlBody = true,
                ToAddresses = request.ToAddresses,
                ToAddressesCopy = request.ToAddressesCopy,
                ToAddressesShadowCopy = request.ToAddressesShadowCopy,
                Attachments = request.Attachments?.Select(q => new FileDto
                {
                    Data = q.Data,
                    FileName = q.FileName
                }).ToList(),
            });
        }
        catch (Exception e) { Console.WriteLine(e.Message); }
        return Unit.Value;
    }
}