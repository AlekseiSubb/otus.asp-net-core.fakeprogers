﻿namespace Services.NotificationService.EmailNotification.Models;

/// <summary>
/// Модель файла
/// </summary>
public class FileDto
{
    /// <summary>
    /// Данные
    /// </summary>
    public byte[] Data { get; set; } = null!;
    /// <summary>
    /// Имя файла с расширением
    /// </summary>
    public string FileName { get; set; } = null!;
}