﻿using Microsoft.Extensions.Options;
using MimeKit;
using Services.NotificationService.EmailNotification.Models;
using System.Net;

namespace Services.NotificationService.EmailNotification;

public class EmailNotificationService
{
    private readonly SenderConfiguration _config;

    public EmailNotificationService(IOptions<SenderConfiguration> options)
    {
        _config = options.Value;
    }

    public async Task SendEmail(EmailNotificationDto dto)
    {
        using var client = new MailKit.Net.Smtp.SmtpClient();
        client.LocalDomain = _config.LocalDomain;

        var message = CreateMessage(dto);
        //var m1 = CreateMessage(new EmailNotificationDto
        //{
        //    Subject = dto.Subject,
        //    Body = dto.Body,
        //    ToAddresses = dto.ToAddresses,
        //    ToAddressesCopy = dto.ToAddressesCopy,
        //    ToAddressesShadowCopy = dto.ToAddressesShadowCopy,
        //    FromName = "SweetMarket Notification Service",
        //    //FromAddress = "notification-sweet-market@rambler.ru", //sender address rejected
        //    FromAddress = "093a86825b454f8fa65890bc89eebc6@rambler.ru",
        //    HtmlBody = false,
        //});
        await client.ConnectAsync(_config.MailServerAddress, Convert.ToInt32(_config.MailServerPort), MailKit.Security.SecureSocketOptions.Auto).ConfigureAwait(false);
        //await client.ConnectAsync("smtp.rambler.ru", 465, MailKit.Security.SecureSocketOptions.Auto).ConfigureAwait(false);

        await client.AuthenticateAsync(new NetworkCredential(_config.UserId, _config.UserPassword)).ConfigureAwait(false);
        //await client.AuthenticateAsync(new NetworkCredential("093a86825b454f8fa65890bc89eebc6", "KxZJNxeqLkdf_5f")).ConfigureAwait(false);

        await client.SendAsync(message).ConfigureAwait(false);
        await client.DisconnectAsync(true).ConfigureAwait(false);
    }

    private MimeMessage CreateMessage(EmailNotificationDto request)
    {
        var m = new MimeMessage()
        {
            Subject = request.Subject,
        };

        m.From.Add(new MailboxAddress(request.FromName, !string.IsNullOrWhiteSpace(request.FromAddress) ? request.FromAddress : _config.FromAddressBase));

        foreach (var address in request.ToAddresses)
        {
            m.To.Add(InternetAddress.Parse(address));
        }

        if (request.ToAddressesCopy != null)
        {
            foreach (var address in request.ToAddressesCopy)
            {
                m.Cc.Add(InternetAddress.Parse(address));
            }
        }

        if (request.ToAddressesShadowCopy != null)
        {
            foreach (var address in request.ToAddressesShadowCopy)
            {
                m.Bcc.Add(InternetAddress.Parse(address));
            }
        }

        var bodyBuilder = new BodyBuilder();
        if (request.HtmlBody == true) bodyBuilder.HtmlBody = request.Body;
        else bodyBuilder.TextBody = request.Body;

        if (request.Attachments?.Any() is true)
        {
            foreach (var attachment in request.Attachments)
            {
                bodyBuilder.Attachments.Add(attachment.FileName, attachment.Data);
            }
        }
        m.Body = bodyBuilder.ToMessageBody();

        return m;
    }
}