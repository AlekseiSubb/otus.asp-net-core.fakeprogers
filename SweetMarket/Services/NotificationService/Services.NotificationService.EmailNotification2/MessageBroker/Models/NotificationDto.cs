﻿namespace Services.NotificationService.Application.MessageBroker.Models;

public class NotificationDto
{
    public string? Text { get; set; }
    public string? MessageType { get; set; } //Пока Avertisment/Order, задаём ручками
    //public string? Source { get; set; } //Источник (пока не делаем)
    public List<int> Recipients { get; set; } = new List<int>();
}