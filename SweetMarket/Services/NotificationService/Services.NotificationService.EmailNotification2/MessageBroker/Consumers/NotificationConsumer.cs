﻿using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Services.NotificationService.Application.Commands;
using Services.NotificationService.Application.MessageBroker.Models;
using Services.NotificationService.Domain;
using Services.NotificationService.Infrastructure;

namespace Services.NotificationService.Application.MessageBroker.Consumers;

public class NotificationConsumer : IConsumer<NotificationDto>
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMediator _mediator;

    public NotificationConsumer(ApplicationDbContext dbContext, IMediator mediator)
    {
        _dbContext = dbContext;
        _mediator = mediator;
    }

    public async Task Consume(ConsumeContext<NotificationDto> context)
    {
        var toAddresses = await _dbContext.Set<Recipient>()
            .Where(q => context.Message.Recipients.Contains(q.Id) && q.Email != null)
            .Select(q => q.Email ?? string.Empty)
            .ToListAsync();

        await _mediator.Send(new SendNotificationCommand
        {
            Text = context.Message.Text,
            Recipients = context.Message.Recipients,
            RecievingDate = DateTimeOffset.Now,
            MessageType = context.Message.MessageType,
        });

        //await _mediator.Send(new SendEmailMessageCommand
        //{
        //    FromName = "SweetMarket Notification Service",
        //    FromAddress = null,
        //    MessageSubject = context.Message.MessageType ?? "Common",
        //    Text = context.Message.Text ?? string.Empty,
        //    ToAddresses = toAddresses ?? new List<string>(),
        //    //ToAddressesCopy = request.ToAddressesCopy, //пока нет
        //    //ToAddressesShadowCopy = request.ToAddressesShadowCopy, //пока нет
        //    //Attachments = request.Attachments, //пока нет
        //});
    }
}