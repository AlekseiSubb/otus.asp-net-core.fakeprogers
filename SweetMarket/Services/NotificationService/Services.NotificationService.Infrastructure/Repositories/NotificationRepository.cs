﻿using MongoDB.Bson;
using MongoDB.Driver;
using Services.NotificationService.Domain;
using System.Linq.Expressions;
namespace Services.NotificationService.Infrastructure.Repositories;
public class NotificationRepository : IRepository<Notification, ObjectId>
{
    private readonly ApplicationDbContext _context;

    public NotificationRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task AddAsync(Notification entity) => _context.Notifications.InsertOneAsync(entity);

    public async Task<bool> DeleteAsync(Notification entity)
    {
        var document = await _context.Recipients.FindOneAndDeleteAsync(q => q.Id == entity.Id);
        return document != null;
    }

    public Task<List<Notification>> GetAllAsync() =>
        _context.Notifications.Find("{}").ToListAsync();

    public Task<Notification> GetByIdAsync(ObjectId id) =>
        _context.Notifications.Find(q => q.Id == id).SingleAsync();

    public Task<Notification> GetByIdOrDefaultAsync(ObjectId id) =>
        _context.Notifications.Find(q => q.Id == id).SingleOrDefaultAsync();

    public Task<Notification> GetFirstWhere(Expression<Func<Notification, bool>> predicate) =>
        _context.Notifications.Find(predicate).SingleAsync();

    public async Task<Notification?> GetFirstOrDefaultWhere(Expression<Func<Notification, bool>> predicate) =>
        await _context.Notifications.Find(predicate).SingleOrDefaultAsync();

    public Task<List<Notification>> GetRangeByIdsAsync(List<ObjectId> ids) =>
        _context.Notifications.Find(q => ids.Contains(q.Id)).ToListAsync();

    public Task<List<Notification>> GetWhere(Expression<Func<Notification, bool>> predicate) =>
        _context.Notifications.Find(predicate).ToListAsync();

    public async Task<bool> UpdateAsync(Notification entity)
    {
        var document = await _context.Notifications.FindOneAndReplaceAsync(q => q.Id == entity.Id, entity);
        return document != null;
    }
}
