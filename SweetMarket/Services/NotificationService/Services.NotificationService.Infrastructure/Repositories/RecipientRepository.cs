﻿using MongoDB.Bson;
using MongoDB.Driver;
using Services.NotificationService.Domain;
using System.Linq.Expressions;
namespace Services.NotificationService.Infrastructure.Repositories;
public class RecipientRepository : IRepository<Recipient, ObjectId>
{

    private readonly ApplicationDbContext _context;

    public RecipientRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task AddAsync(Recipient entity) => _context.Recipients.InsertOneAsync(entity);

    public async Task<bool> DeleteAsync(Recipient entity)
    {
        var document = await _context.Recipients.FindOneAndDeleteAsync(q => q.Id == entity.Id);
        return document != null;
    }

    public Task<List<Recipient>> GetAllAsync() =>
        _context.Recipients.Find("{}").ToListAsync();

    public Task<Recipient> GetByIdAsync(ObjectId id) =>
        _context.Recipients.Find(q => q.Id == id).SingleAsync();

    public Task<Recipient> GetByIdOrDefaultAsync(ObjectId id) =>
        _context.Recipients.Find(q => q.Id == id).SingleOrDefaultAsync();

    public Task<Recipient> GetFirstWhere(Expression<Func<Recipient, bool>> predicate) =>
        _context.Recipients.Find(predicate).SingleAsync();
    
    public async Task<Recipient?> GetFirstOrDefaultWhere(Expression<Func<Recipient, bool>> predicate) =>
        await _context.Recipients.Find(predicate).SingleOrDefaultAsync();

    public Task<List<Recipient>> GetRangeByIdsAsync(List<ObjectId> ids) =>
        _context.Recipients.Find(q => ids.Contains(q.Id)).ToListAsync();

    public Task<List<Recipient>> GetWhere(Expression<Func<Recipient, bool>> predicate) =>
        _context.Recipients.Find(predicate).ToListAsync();

    public async Task<bool> UpdateAsync(Recipient entity)
    {
        var document = await _context.Recipients.FindOneAndReplaceAsync(q => q.Id == entity.Id, entity);
        return document != null;
    }
}
