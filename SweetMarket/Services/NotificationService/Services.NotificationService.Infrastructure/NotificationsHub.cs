﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Services.NotificationService.Infrastructure.Services;

namespace Services.NotificationService.Infrastructure;

public interface INotificationsHubClient
{
    Task RecieveNotification(string subject, string messageText);
}

public class NotificationsHub : Hub<INotificationsHubClient>
{
    private readonly SignalRConnectionsService _connectionsService;
    private readonly ILogger _logger;

    public NotificationsHub(SignalRConnectionsService connectionsService, ILogger<NotificationsHub> logger)
    {
        _connectionsService = connectionsService;
        _logger = logger;
    }

    public async Task SubscribeOnNotifications(int userId)
    {
        await _connectionsService.ConnectUser(userId, Context.ConnectionId, GetUserAgent()).ConfigureAwait(false);
        _logger.LogInformation("Client [{ConnectionId}] connected", Context.ConnectionId);
    }

    public override async Task OnDisconnectedAsync(Exception? exception)
    {
        await _connectionsService.DisconnectUser(Context.ConnectionId).ConfigureAwait(false);
        _logger.LogInformation(exception, "Client [{ConnectionId}] disconnected", Context.ConnectionId);
    }

    private string GetUserAgent()
    {
        var userAgent = string.Empty;
        try { userAgent = Context?.GetHttpContext()?.Request.Headers["User-Agent"].ToString(); } catch { }
        if (string.IsNullOrWhiteSpace(userAgent)) userAgent = "N/A";
        return userAgent;
    }
}