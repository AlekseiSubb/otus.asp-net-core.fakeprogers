﻿using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Notifications.Email;
using Services.NotificationService.Domain.Notifications.Internal;
using Services.NotificationService.Domain.Notifications.SignalR;

namespace Services.NotificationService.Infrastructure;

public class ApplicationDbContext
{
    private readonly IMongoDatabase _database;

    public ApplicationDbContext(IMongoDatabase database)
    {
        _database = database;
        var objectSerializer = new ObjectSerializer(type => ObjectSerializer.DefaultAllowedTypes(type) ||
        type.Namespace?.StartsWith("Services.NotificationService.Domain") == true);
        BsonSerializer.TryRegisterSerializer(objectSerializer);
        ConfigurePolymorphicMapping();
    }

    private void ConfigurePolymorphicMapping()
    {
        BsonClassMap.TryRegisterClassMap<Recipient>();

        BsonClassMap.TryRegisterClassMap<EmailContact>();
        BsonClassMap.TryRegisterClassMap<EmailNotification>();
        BsonClassMap.TryRegisterClassMap<EmailNotificationSettings>();

        BsonClassMap.TryRegisterClassMap<InternalContact>();
        BsonClassMap.TryRegisterClassMap<InternalNotification>();
        BsonClassMap.TryRegisterClassMap<InternalNotificationSettings>();

        BsonClassMap.TryRegisterClassMap<SignalRContact>();
        BsonClassMap.TryRegisterClassMap<SignalRNotification>();
        BsonClassMap.TryRegisterClassMap<SignalRNotificationSettings>();
    }

    public IMongoCollection<TDocument> GetCollections<TDocument>(string collectionName) => _database.GetCollection<TDocument>(collectionName);

    public IMongoCollection<Recipient> Recipients => GetCollections<Recipient>(nameof(Recipient));
    public IMongoCollection<Notification> Notifications => GetCollections<Notification>(nameof(Notification));
    public IMongoCollection<InternalNotification> InternalNotifications => GetCollections<InternalNotification>(nameof(InternalNotification));

}