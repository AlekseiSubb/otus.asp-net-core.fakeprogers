﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Services.NotificationService.Domain;
using Services.NotificationService.Domain.Notifications.SignalR;
using Services.NotificationService.Infrastructure.Configurations;
using Services.NotificationService.Infrastructure.Repositories;

namespace Services.NotificationService.Infrastructure.Services;

public class SignalRConnectionsService
{
    private readonly ApplicationDbContext _context;
    private readonly IRepository<Recipient, ObjectId> _repository;
    private readonly Guid _serverId;
    private readonly ILogger _logger;

    public SignalRConnectionsService(ApplicationDbContext context, IOptions<SignalRConfiguration> options,
        IRepository<Recipient, ObjectId> repository, ILogger<SignalRConnectionsService> logger)
    {
        _context = context;
        _repository = repository;
        _serverId = options.Value.ServerId;
        _logger = logger;
    }

    public async Task ConnectUser(int userId, string connectionId, string userAgent, CancellationToken? cancellationToken = null)
    {
        try
        {
            if (cancellationToken?.IsCancellationRequested == true) return;
            var user = await _repository.GetFirstWhere(x => x.UserId == userId).ConfigureAwait(false);
            var contact = (SignalRContact?)user.Contacts.FirstOrDefault(q => q is SignalRContact signalRContact && signalRContact.ConnectionID == connectionId);
            if (contact == null) await CreateContact(user, connectionId, true, userAgent);
            else await Connect(connectionId, userAgent, cancellationToken ?? CancellationToken.None);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Could not connect user [{userId}] with [{connectionId}] due exception: [{exception}]", userId, connectionId, ex.Message);
        }
    }

    public Task DisconnectUser(string connectionId, CancellationToken? cancellationToken = null) => Disconnect(connectionId, cancellationToken ?? CancellationToken.None);

    public async Task ClearAllServerContacts(CancellationToken? cancellationToken = null)
    {
        if (cancellationToken?.IsCancellationRequested == true) return;
        var users = await GetUsersWithServerContacts(cancellationToken ?? CancellationToken.None);
        foreach (var user in users)
        {
            if (cancellationToken?.IsCancellationRequested == true) return;
            await ClearUserSignalRContacts(user);
        }
    }

    public async Task ClearServerDisconnectedContacts(CancellationToken? cancellationToken = null)
    {
        if (cancellationToken?.IsCancellationRequested == true) return;
        var users = await GetUsersWithDisconnectedContacts(cancellationToken ?? CancellationToken.None);
        foreach (var user in users)
        {
            await ClearUserDisconnectedSignalRContacts(user);
        }
    }

    private async Task ClearUserSignalRContacts(Recipient recipient)
    {
        recipient.Contacts.RemoveAll(q => q is SignalRContact signalRContact && signalRContact.ServerId == _serverId);
        await _repository.UpdateAsync(recipient);
    }

    private async Task ClearUserDisconnectedSignalRContacts(Recipient recipient)
    {
        recipient.Contacts.RemoveAll(q => q is SignalRContact signalRContact && !signalRContact.Connected && signalRContact.ServerId == _serverId);
        await _repository.UpdateAsync(recipient);
    }

    private async Task CreateContact(Recipient user, string connectionId, bool connected, string userAgent)
    {
        user.Contacts.Add(new SignalRContact { ConnectionID = connectionId, UserAgent = userAgent, Connected = connected, ServerId = _serverId });
        await _repository.UpdateAsync(user).ConfigureAwait(false);
    }

    private Task Connect(string connectionId, string userAgent, CancellationToken cancellationToken) =>
        UpdateContact(connectionId, true, cancellationToken, userAgent);
    private Task Disconnect(string connectionId, CancellationToken cancellationToken) => 
        UpdateContact(connectionId, false, cancellationToken: cancellationToken);

    private async Task UpdateContact(string connectionId, bool connected, CancellationToken cancellationToken, string? userAgent = null)
    {
        if (cancellationToken.IsCancellationRequested == true) return;
        var user = await GetUserByConnectionId(connectionId, cancellationToken).ConfigureAwait(false);
        if (user == null) return;
        var contact = user.Contacts
            .FirstOrDefault(q => q is SignalRContact signalRContact && signalRContact.ConnectionID == connectionId);
        if (contact is SignalRContact signalRContact)
        {
            signalRContact.Connected = connected;
            if (!string.IsNullOrWhiteSpace(userAgent)) signalRContact.UserAgent = userAgent;
            await _repository.UpdateAsync(user).ConfigureAwait(false);
        }
    }

    private Task<Recipient> GetUserByConnectionId(string connectionId, CancellationToken cancellationToken) =>
        _context.Recipients.Find($"{{\"Contacts\" : {{ \"$elemMatch\" : {{ \"ServerId\" : UUID(\"{_serverId}\"), \"ConnectionID\" : \"{connectionId}\" }} }} }}")
        .SingleOrDefaultAsync(cancellationToken);

    private Task<List<Recipient>> GetUsersWithDisconnectedContacts(CancellationToken cancellationToken) =>
        _context.Recipients.Find($"{{\"Contacts\" : {{ \"$elemMatch\" : {{ \"ServerId\" : UUID(\"{_serverId}\"), \"Connected\" : \"false\" }} }} }}")
        .ToListAsync(cancellationToken);

    private Task<List<Recipient>> GetUsersWithServerContacts(CancellationToken cancellationToken) =>
        _context.Recipients.Find($"{{\"Contacts\" : {{ \"$elemMatch\" : {{ \"ServerId\" : UUID(\"{_serverId}\") }} }} }}")
        .ToListAsync(cancellationToken);
    
    //private Task<List<Recipient>> GetUsersWithServerContacts(CancellationToken cancellationToken) =>
    //    _context.Recipients.Find(q => q.Contacts.Any(q => q is SignalRContact signalRContact && signalRContact.ServerId == _serverId))
    //    .ToListAsync(cancellationToken);
}