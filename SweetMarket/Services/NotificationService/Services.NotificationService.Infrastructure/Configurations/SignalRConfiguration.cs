﻿namespace Services.NotificationService.Infrastructure.Configurations;

public class SignalRConfiguration
{
    public required Guid ServerId { get; set; } = default!;
    public uint? CleanupInterval { get; set; }
    public IntervalUnit? CleanupIntervalUnit { get; set; }
}

public enum IntervalUnit
{
    Seconds,
    Minutes,
    Hours,
    Days
}