﻿using MassTransit;
using MediatR;
using Services.NotificationService.Application.Commands;

namespace Services.NotificationService.Application.Consumers;

public class NotificationConsumer : IConsumer<SendNotificationCommand>
{
    private readonly IMediator _mediator;

    public NotificationConsumer(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Consume(ConsumeContext<SendNotificationCommand> context)
    {
        await _mediator.Send(new SendNotificationCommand
        {
            Text = context.Message.Text,
            Recipients = context.Message.Recipients,
            RecievingDate = DateTimeOffset.Now,
            MessageType = context.Message.MessageType,
        });
    }
}