﻿namespace Services.NotificationService.Application.Models;

public class FileDto
{
    public byte[] Data { get; set; } = null!;
    public string FileName { get; set; } = null!;
}
