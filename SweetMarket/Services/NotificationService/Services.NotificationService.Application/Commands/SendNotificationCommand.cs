﻿using MediatR;

namespace Services.NotificationService.Application.Commands;

public class SendNotificationCommand : IRequest<Unit>
{
    public string? Text { get; set; }
    public string? MessageType { get; set; } //Пока Avertisment/Order, задаём ручками
                                             //public string? Source { get; set; } //Источник (пока не делаем)
    public DateTimeOffset? RecievingDate { get; set; }
    public List<int> Recipients { get; set; } = new List<int>();
}