﻿using MassTransit;
using MediatR;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Application.MessageBroker.Models;
namespace Services.PaymentService.Application.MessageBroker.Consumers;
public class ExecutePaymentConsumer : IConsumer<ExecutePaymentDto>
{
    private readonly IMediator _mediator;

    public ExecutePaymentConsumer(IMediator mediator)
    {
        _mediator = mediator;
    }

    public Task Consume(ConsumeContext<ExecutePaymentDto> context)
    {
        _mediator.Send(new ExecutePaymentCommand
        {
            ProviderId = context.Message.ProviderId,
            Amount = context.Message.Amount,
            OrderId = context.Message.OrderId,
        });
        return Task.CompletedTask;
    }
}