﻿namespace Services.PaymentService.Application.MessageBroker.Models;
public class ExecutePaymentDto
{
    public int ProviderId { get; set; }
    public decimal Amount { get; set; }
    public int OrderId { get; set; }
}