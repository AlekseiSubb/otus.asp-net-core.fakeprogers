﻿namespace Services.PaymentService.Application.Models;
public class PaymentResponseShort
{
    public string PaymentId { get; set; } = default!;
    public Guid? TransactionId { get; set; }
    public TransactionStatus? Status { get; set; }
    public bool IsSuccessStatus { get; set; }
}