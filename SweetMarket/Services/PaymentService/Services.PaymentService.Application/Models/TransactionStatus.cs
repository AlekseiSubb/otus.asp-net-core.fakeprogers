﻿namespace Services.PaymentService.Application.Models;
public enum TransactionStatus
{
    Pending,
    Processing,
    Success,
    Failed
}
