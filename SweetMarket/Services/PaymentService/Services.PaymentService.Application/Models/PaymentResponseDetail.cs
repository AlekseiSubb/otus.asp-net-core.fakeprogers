﻿namespace Services.PaymentService.Application.Models;
public class PaymentResponseDetail
{
    public string PaymentId { get; set; } = default!;
    public int? OrderId { get; set; } = default!;
    public decimal Amount { get; set; } = default!;
    public Guid AccountId { get; set; }
    public int ProviderId { get; set; }
    public Guid? TransactionId { get; set; }
    public TransactionStatus? Status { get; set; }
}