﻿namespace Services.PaymentService.Application.Models;
public class PaymentProviderResponse
{
    public int Id { get; set; }
    public required string Name { get; set; }
    public required string PictureId { get; set; }
}