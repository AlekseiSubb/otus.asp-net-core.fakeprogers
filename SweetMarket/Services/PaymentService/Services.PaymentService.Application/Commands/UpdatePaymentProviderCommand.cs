﻿using MediatR;

namespace Services.PaymentService.Application.Commands;

public class UpdatePaymentProviderCommand : IRequest<bool>
{
    public int ProviderId { get; set; }
    public string? ProviderName { get; set; }
    public string? PictureId { get; set; }
}