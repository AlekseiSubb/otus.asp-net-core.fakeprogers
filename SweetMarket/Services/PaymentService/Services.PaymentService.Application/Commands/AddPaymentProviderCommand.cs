﻿using MediatR;

namespace Services.PaymentService.Application.Commands;

public class AddPaymentProviderCommand : IRequest<int?>
{
    public string ProviderName { get; set; } = default!;
    public string PictureId { get; set; } = default!;
}