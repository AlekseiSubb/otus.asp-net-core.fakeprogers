﻿using MediatR;
using Services.PaymentService.Application.Models;
namespace Services.PaymentService.Application.Commands;
public class ExecutePaymentCommand : IRequest<PaymentResponseShort>
{
    public int ProviderId { get; set; }
    public decimal Amount { get; set; }
    public int OrderId { get; set; }
}