﻿using MediatR;

namespace Services.PaymentService.Application.Commands;

public class DeletePaymentProviderCommand : IRequest<bool>
{
    public int ProviderId { get; set; }
}