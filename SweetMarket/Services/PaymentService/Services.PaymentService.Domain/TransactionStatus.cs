﻿namespace Services.PaymentService.Domain;
public enum TransactionStatus
{
    Pending,
    Processing,
    Success,
    Failed
}