﻿using MongoDB.Bson;
namespace Services.PaymentService.Domain;
public class PaymentProvider
{
    public ObjectId Id { get; set; }
    public int PaymentProviderId { get; set; }
    public required string Name { get; set; }
    public string? PictureId { get; set; }
}