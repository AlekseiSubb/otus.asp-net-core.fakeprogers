﻿using MongoDB.Bson;
namespace Services.PaymentService.Domain;
public class Payment
{
    public ObjectId Id { get; set; }
    public Guid AccountId { get; set; }
    public int ProviderId { get; set; }
    public decimal Amount { get; set; }
    public int? OrderId { get; set; }
    public Guid? TransactionId { get; set; }
    public TransactionStatus Status { get; set; }
}