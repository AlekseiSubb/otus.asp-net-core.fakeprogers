using MongoDB.Bson;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure;
using Services.PaymentService.Services;
using System.Text.Json.Serialization;
using Services.PaymentService.Infrastructure.Extensions;
using Microsoft.AspNetCore.Diagnostics;
using NLog.Web;
using MassTransit;
using Services.PaymentService.Application.MessageBroker.Consumers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IdentityModel.Tokens.Jwt;
using Services.PaymentService.Infrastructure.Repositories;
using Microsoft.OpenApi.Models;
using Services.PaymentService.Configuration;
using Swashbuckle.AspNetCore.SwaggerGen;

var builder = WebApplication.CreateBuilder(args);

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
        options.Audience = "sweetmarket_payment_api";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new()
        {
            RoleClaimType = "role",
            ValidTypes = new[] { "at+jwt" }
        };
    });

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<SwaggerGenOptions>(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/authorize"),
                TokenUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/token"),
                Scopes = new Dictionary<string, string>
                {
                    {"openid", ""},
                    {"email", ""},
                    {"profile", ""},
                    {"roles", ""},
                    {"sweetmarket_payment_api.fullaccess", ""}
                },
            },
        },
    });
    options.OperationFilter<AuthorizeCheckOperationFilter>();
});


//MongoDb
builder.Services.AddMongoDb(builder.Configuration.GetConnectionString("MongoDb"), builder.Configuration.GetValue<string>("MongoDatabaseName"));
builder.Services.AddScoped<ApplicationDbContext>();
builder.Services.AddTransient<IRepository<Payment, ObjectId>, PaymentRepository>();
builder.Services.AddTransient<IRepository<PaymentProvider, ObjectId>, PaymentProviderRepository>();
builder.Services.AddTransient<ILoadFirstDataService, LoadFirstDataService>();

builder.Services.AddScoped<RandomGeneratorService>();
builder.Services.AddScoped<PaymentService>();

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies()));
//builder.Services.AddMediatR(q => q.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

builder.Services.AddMassTransit(q =>
{
    q.AddConsumer<ExecutePaymentConsumer>();
    q.UsingRabbitMq((context, config) =>
    {
        config.Host(builder.Configuration["RabbitMq:Host"], virtualHost: builder.Configuration["RabbitMq:VirtualHost"], q =>
        {
            q.Username(builder.Configuration["RabbitMq:Username"]);
            q.Password(builder.Configuration["RabbitMq:Password"]);
        });

        config.ReceiveEndpoint("cmd.Payment.PaymentService.ExecutePaymentCommand", e =>
        {
            e.ConfigureConsumer<ExecutePaymentConsumer>(context);
            e.UseMessageRetry(r =>
            {
                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
            });
        });
    });
});


builder.Host.UseNLog(new NLogAspNetCoreOptions
{
    //RemoveLoggerFactoryFilter = false, //�� ��������� true, �� ���� ������� ����������� �������, �� � ��, �� ��� �� ������.
    //ReplaceLoggerFactory = true, //������� ��������� �������
});

var app = builder.Build();

app.UseExceptionHandler(c => c.Run(async context =>
{
    var exception = context.Features.Get<IExceptionHandlerPathFeature>()?.Error;
    var factory = c.ApplicationServices.GetService<ILoggerFactory>();
    var logger = factory?.CreateLogger(AppDomain.CurrentDomain.FriendlyName);
    logger?.LogError(new EventId(222), exception, exception?.Message);
    var response = new { error = exception?.Message ?? "Unknown exception" };
    await context.Response.WriteAsJsonAsync(response);
}));

var scope = app.Services.CreateScope();
var service = scope.ServiceProvider.GetRequiredService<ILoadFirstDataService>();
service.InitializeDb();  

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.OAuthClientId("sweetmarket_web_frontend_client");
    c.OAuthClientSecret("LwZjqN6XCPCtnt0ZKYUiD1jGbRbe2aepqtGfxsuk9D");
    c.EnablePersistAuthorization();
});

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
