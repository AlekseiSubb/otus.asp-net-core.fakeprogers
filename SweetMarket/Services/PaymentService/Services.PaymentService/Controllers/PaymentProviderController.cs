using MassTransit;
using MassTransit.Initializers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Application.Models;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Repositories;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace Services.PaymentService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentProviderController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IRepository<PaymentProvider, ObjectId> _repository;

        public PaymentProviderController(IMediator mediator, IRepository<PaymentProvider, ObjectId> repository)
        {
            _mediator = mediator;
            _repository = repository;
        }

        [HttpGet("payment_providers")]
        public async Task<ActionResult<List<PaymentProviderResponse>>> GetPaymentProvidersList()
        {
            var identity = User.Identity;

            var paymentProviders = await _repository.GetAllAsync();
            return Ok(paymentProviders.Select(q => new PaymentProviderResponse
            {
                Id = q.PaymentProviderId,
                Name = q.Name,
                PictureId = q.PictureId ?? string.Empty
            }).ToList());
        }

        [HttpPut]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddNew(AddPaymentProviderCommand command)
        {
            var providerId = await _mediator.Send(command);
            if (providerId == null) return BadRequest();
            return Ok(providerId);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdatePaymentProvider(UpdatePaymentProviderCommand command)
        {
            if (!await _mediator.Send(command)) return BadRequest();
            return Ok();
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeletePaymentProvider(DeletePaymentProviderCommand command)
        {
            if (!await _mediator.Send(command)) return BadRequest();
            return Ok();
        }
    }
}