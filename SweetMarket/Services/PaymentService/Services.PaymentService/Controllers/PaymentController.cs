using MassTransit;
using MassTransit.Initializers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Application.MessageBroker.Models;
using Services.PaymentService.Application.Models;
using System.Net;

namespace Services.PaymentService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly Services.PaymentService _service;

        public PaymentController(IMediator mediator, Services.PaymentService service)
        {
            _mediator = mediator;
            _service = service;
        }

        [HttpGet("payments")]
        public async Task<ActionResult<List<PaymentResponseShort>>> GetOrderPaymentList(int orderId)
        {
            var payments = await _service.GetPaymentsByOrderId(orderId);
            
            return Ok(payments.Select(q => new PaymentResponseShort
            {
                PaymentId = q.Id.ToString(),
                TransactionId = q.TransactionId,
                Status = (TransactionStatus)(int)q.Status,
                IsSuccessStatus = q.Status == Domain.TransactionStatus.Success
            }).ToList());
        }
        
        [HttpGet("payment/{paymentId}")]
        [ProducesResponseType(typeof(PaymentResponseDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPayment(string paymentId)
        {
            var payment = await _service.GetByPaymentId(paymentId);
            if (payment == null) return NotFound();
            return Ok(new PaymentResponseDetail
            {
                PaymentId = paymentId,
                OrderId = payment.OrderId,
                Amount = payment.Amount,
                AccountId = payment.AccountId,
                ProviderId = payment.ProviderId,
                TransactionId = payment.TransactionId,
                Status = (TransactionStatus)(int)payment.Status,
            });
        }

        [HttpPost("Pay")]
        public async Task<ActionResult<PaymentResponseShort>> Pay(ExecutePaymentCommand command) => Ok(await _mediator.Send(command));
    }
}