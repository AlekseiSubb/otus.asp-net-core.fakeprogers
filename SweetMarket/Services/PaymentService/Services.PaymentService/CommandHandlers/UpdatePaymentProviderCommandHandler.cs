﻿using MediatR;
using MongoDB.Bson;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Repositories;

namespace Services.PaymentService.CommandHandlers;

public class UpdatePaymentProviderCommandHandler : IRequestHandler<UpdatePaymentProviderCommand, bool>
{
    private readonly IRepository<PaymentProvider, ObjectId> _repository;

    public UpdatePaymentProviderCommandHandler(IRepository<PaymentProvider, ObjectId> repository)
    {
        _repository = repository;
    }

    public async Task<bool> Handle(UpdatePaymentProviderCommand request, CancellationToken cancellationToken)
    {
        var provider = await _repository.GetFirstOrDefaultWhere(q => q.PaymentProviderId == request.ProviderId);
        if (provider == null) return false;
        if (!string.IsNullOrWhiteSpace(request.ProviderName))
        {
            if (await _repository.AnyWhere(q => q.Name == request.ProviderName && q.Id != provider.Id)) return false;
            provider.Name = request.ProviderName;
        }
        if (!string.IsNullOrWhiteSpace(request.PictureId)) provider.PictureId = request.PictureId;
        await _repository.UpdateAsync(provider);
        return true;
    }

}
