﻿using MediatR;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Application.Models;
using Services.PaymentService.Services;

namespace Services.PaymentService.CommandHandlers;

public class ExecutePaymentCommandHandler : IRequestHandler<ExecutePaymentCommand, PaymentResponseShort>
{
    private readonly Services.PaymentService _service;

    public ExecutePaymentCommandHandler(Services.PaymentService service)
    {
        _service = service;
    }

    public Task<PaymentResponseShort> Handle(ExecutePaymentCommand request, CancellationToken cancellationToken)
    {
        return _service.PayOrder(new PaymentRequest
        {
            ProviderId = request.ProviderId,
            Amount = request.Amount,
            OrderId = request.OrderId,
        });
    }

}
