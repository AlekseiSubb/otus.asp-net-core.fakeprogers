﻿using MediatR;
using MongoDB.Bson;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Repositories;

namespace Services.PaymentService.CommandHandlers;

public class DeletePaymentProviderCommandHandler : IRequestHandler<DeletePaymentProviderCommand, bool>
{
    private readonly IRepository<PaymentProvider, ObjectId> _repository;

    public DeletePaymentProviderCommandHandler(IRepository<PaymentProvider, ObjectId> repository)
    {
        _repository = repository;
    }

    public async Task<bool> Handle(DeletePaymentProviderCommand request, CancellationToken cancellationToken)
    {
        var provider = await _repository.GetFirstOrDefaultWhere(q => q.PaymentProviderId == request.ProviderId);
        if (provider == null) return false;
        await _repository.DeleteAsync(provider);
        return true;
    }

}
