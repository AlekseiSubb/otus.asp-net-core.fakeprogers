﻿using MediatR;
using MongoDB.Bson;
using Services.PaymentService.Application.Commands;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Repositories;

namespace Services.PaymentService.CommandHandlers;

public class AddPaymentProviderCommandHandler : IRequestHandler<AddPaymentProviderCommand, int?>
{
    private readonly IRepository<PaymentProvider, ObjectId> _repository;

    public AddPaymentProviderCommandHandler(IRepository<PaymentProvider, ObjectId> repository)
    {
        _repository = repository;
    }

    public async Task<int?> Handle(AddPaymentProviderCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var newProvider = new PaymentProvider
            {
                Name = request.ProviderName,
                PictureId = request.PictureId,
            };
            await _repository.AddAsync(newProvider);
            return newProvider.PaymentProviderId;
        }
        catch (Exception ex) { return null; }
    }

}
