﻿using MongoDB.Bson;
using Services.PaymentService.Application.Models;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Repositories;

namespace Services.PaymentService.Services;

public class PaymentService
{
    private readonly IRepository<Payment, ObjectId> _repositoiry;
    private readonly RandomGeneratorService _randomGeneratorService;
    private readonly ILogger _logger;

    public PaymentService(IRepository<Payment, ObjectId> repositoiry, RandomGeneratorService randomGeneratorService, ILogger<PaymentService> logger)
    {
        _repositoiry = repositoiry;
        _randomGeneratorService = randomGeneratorService;
        _logger = logger;
    }

    //delete after test
    internal Task<List<Payment>> GetPaymentsByOrderId(int orderId) => _repositoiry.GetWhere(q => q.OrderId == orderId);

    internal Task<Payment?> GetByPaymentId(string paymentId) => _repositoiry.GetFirstOrDefaultWhere(q => q.Id.ToString() == paymentId);


    internal async Task<PaymentResponseShort> PayOrder(PaymentRequest request)
    {
        var isSuccess = _randomGeneratorService.GetTrue(probabilityPercentage: 80d);
        Application.Models.TransactionStatus? status = Application.Models.TransactionStatus.Success;

        if (!isSuccess)
        {
            var random = _randomGeneratorService.NextInt(Enum.GetValues<Application.Models.TransactionStatus>().Count() - 1);
            status = random switch
            {
                0 => Application.Models.TransactionStatus.Pending,
                1 => Application.Models.TransactionStatus.Processing,
                2 => Application.Models.TransactionStatus.Failed,
                _ => throw new NotImplementedException()
            };
        }

        var accountId = Guid.NewGuid();

        var result = new PaymentResponseShort
        {
            TransactionId = Guid.NewGuid(),
            Status = status,
            IsSuccessStatus = isSuccess
        };

        var payment = new Payment
        {
            OrderId = request.OrderId,
            ProviderId = request.ProviderId,
            AccountId = accountId,
            Amount = request.Amount,
            TransactionId = result.TransactionId,
            Status = (Domain.TransactionStatus)(int)result.Status,
        };
        
        await _repositoiry.AddAsync(payment);
        result.PaymentId = payment.Id.ToString();

        if (status != Application.Models.TransactionStatus.Success)
        {
            _logger.LogError("Unsuccess payment: {Status}\r\nPaymentId:{PaymentId}\r\nProviderId: {ProviderId}\r\nAccountId: {AccountId}\r\nOrderId: {OrderId}\r\nAmount: {Amount}",
                status.ToString(), payment.Id.ToString(), request.ProviderId, accountId, request.OrderId, request.Amount);
        }

        return result;
    }
}

internal class PaymentRequest
{
    internal int ProviderId { get; set; }
    internal decimal Amount { get; set; }
    internal int OrderId { get; set; }
}