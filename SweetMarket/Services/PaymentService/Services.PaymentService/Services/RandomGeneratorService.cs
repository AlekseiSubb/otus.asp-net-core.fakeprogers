﻿using System.Security.Cryptography;
namespace Services.PaymentService.Services;
public class RandomGeneratorService : IDisposable
{
    
    private readonly RandomNumberGenerator _random;

    public RandomGeneratorService()
    {
        _random = RandomNumberGenerator.Create();
    }

    internal bool GetBool(bool value, double probabilityPercentage)
    {
        if (probabilityPercentage > 100) probabilityPercentage = 100; 
        if (probabilityPercentage < 0) probabilityPercentage = 0; 
        var data = new byte[sizeof(int)];
        _random.GetBytes(data);
        var intValue = BitConverter.ToInt32(data, 0) & (int.MaxValue - 1);
        if ((double)intValue / (double)int.MaxValue <= probabilityPercentage / 100d) return value;
        return !value;
    }

    internal bool GetTrue(double probabilityPercentage) => GetBool(true, probabilityPercentage);
    
    internal bool GetFalse(double probabilityPercentage) => GetBool(false, probabilityPercentage);

    private static double Map(double value, double from1, double to1, double from2, double to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    internal int NextInt() => NextInt(sizeof(int));
    
    internal int NextInt(int maxValue)
    {
        var data = new byte[sizeof(int)];
        _random.GetBytes(data);
        return BitConverter.ToInt32(data, 0) & (maxValue - 1);
    }

    public void Dispose()
    {
        _random.Dispose();
    }
}