﻿using MongoDB.Bson;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Repositories;

namespace Services.PaymentService.Infrastructure;

public interface ILoadFirstDataService
{
    void InitializeDb();
}

public class LoadFirstDataService : ILoadFirstDataService
{
    private readonly IRepository<PaymentProvider, ObjectId> _repository;

    public LoadFirstDataService(IRepository<PaymentProvider, ObjectId> repository)
    {
        _repository = repository;
    }

    public async void InitializeDb()
    {
        var paymentProviders = new List<PaymentProvider>
        {
            new PaymentProvider{ Name = "Visa", PaymentProviderId = 0, PictureId = "visa_icon.png" },
            new PaymentProvider{ Name = "MasterCard", PaymentProviderId = 1, PictureId = "mastercard_icon.png" },
            new PaymentProvider{ Name = "МИР", PaymentProviderId = 2, PictureId = "mircard_icon.png" },
        };

        foreach (var paymentProvider in paymentProviders)
        {
            var existingProvider = await _repository.GetFirstOrDefaultWhere(q => q.Name == paymentProvider.Name);
            if (existingProvider == null) await _repository.AddAsync(paymentProvider);
            else
            {
                existingProvider.Name = paymentProvider.Name;
                existingProvider.PaymentProviderId = paymentProvider.PaymentProviderId;
                existingProvider.PictureId = paymentProvider.PictureId;
                await _repository.UpdateAsync(existingProvider);
            }
        }
    }
}
