﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Services.PaymentService.Infrastructure.Extensions;

public static class ApplicationDbContextExtension
{
    public static IServiceCollection AddMongoDb(this IServiceCollection services, string? connectionString)
    {
        if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString));
        services.AddSingleton(new MongoClient(connectionString));
        return services;
    }

    public static IServiceCollection AddMongoDb(this IServiceCollection services, string? connectionString, string? databaseName)
    {
        if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString));
        if (string.IsNullOrWhiteSpace(databaseName)) throw new ArgumentNullException(nameof(databaseName));
        services.AddSingleton(new MongoClient(connectionString).GetDatabase(databaseName));
        return services;
    }
}