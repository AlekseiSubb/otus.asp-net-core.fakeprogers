﻿using MongoDB.Bson;
using MongoDB.Driver;
using Services.PaymentService.Domain;
using Services.PaymentService.Infrastructure.Sequences;

namespace Services.PaymentService.Infrastructure
{
    public class ApplicationDbContext
    {
        private readonly IMongoDatabase _database;

        public ApplicationDbContext(IMongoDatabase database)
        {
            _database = database;
        }

        public IMongoCollection<TDocument> GetCollections<TDocument>(string collectionName) => _database.GetCollection<TDocument>(collectionName);

        public IMongoCollection<Payment> Payments => GetCollections<Payment>(nameof(Payment));
        public IMongoCollection<PaymentProvider> PaymentProviders => GetCollections<PaymentProvider>(nameof(PaymentProvider));
        
        private IntSequence _paymentProviderIdSequence = new("PaymentProviderId");

        public Task<int> GetNextPaymentProviderId() => _paymentProviderIdSequence.GetNextSequenceValue(_database);
    }
}