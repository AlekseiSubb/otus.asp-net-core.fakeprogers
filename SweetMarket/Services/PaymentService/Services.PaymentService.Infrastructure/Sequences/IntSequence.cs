﻿using MongoDB.Driver;
namespace Services.PaymentService.Infrastructure.Sequences;

public class IntSequence : Sequence<int>
{
    public IntSequence(string name, int startValue = 0)
    {
        Name = name;
        Value = startValue;
    }

    public override async Task<int> Insert(IMongoDatabase database)
    {
        var collection = database.GetCollection<IntSequence>(Name);
        await collection.InsertOneAsync(this);
        return Value;
    }

    public override async Task<int> GetNextSequenceValue(IMongoDatabase database)
    {
        var collection = database.GetCollection<IntSequence>(Name);
        if (await collection.EstimatedDocumentCountAsync() == 0) return await Insert(database);
        //var a = await collection.Find("{}").ToListAsync();
        //await collection.DeleteOneAsync(q => q.Id == a.First().Id);
        var filter = Builders<IntSequence>.Filter.Eq(a => a.Name, Name);
        var update = Builders<IntSequence>.Update.Inc(a => a.Value, 1);
        var sequence = collection.FindOneAndUpdate(filter, update, new FindOneAndUpdateOptions<IntSequence, IntSequence> { ReturnDocument = ReturnDocument.After });
        return sequence.Value;
        //return sequence?.Value ?? Insert(database);
    }
}
