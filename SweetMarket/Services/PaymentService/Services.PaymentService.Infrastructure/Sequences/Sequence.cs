﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Numerics;

namespace Services.PaymentService.Infrastructure.Sequences;
public abstract class Sequence<T> where T : INumber<T>
{
    public ObjectId Id { get; set; }
    public virtual string Name { get; protected set; } = "sequence";
    public T Value { get; protected set; } = default!;

    public abstract Task<T> Insert(IMongoDatabase database);

    public abstract Task<T> GetNextSequenceValue(IMongoDatabase database);
}