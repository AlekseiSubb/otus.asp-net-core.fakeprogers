﻿using System.Linq.Expressions;

namespace Services.PaymentService.Infrastructure.Repositories;
public interface IRepository<T, TKey>
{
    Task<List<T>> GetAllAsync();

    Task<T> GetByIdAsync(TKey id);
    Task<T?> GetByIdOrDefaultAsync(TKey id);

    Task<List<T>> GetRangeByIdsAsync(List<TKey> ids);

    Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);
    Task<T?> GetFirstOrDefaultWhere(Expression<Func<T, bool>> predicate);

    Task<List<T>> GetWhere(Expression<Func<T, bool>> predicate);

    Task<bool> AnyWhere(Expression<Func<T, bool>> predicate);

    Task AddAsync(T entity);

    Task<bool> UpdateAsync(T entity);

    Task<bool> DeleteAsync(T entity);
}