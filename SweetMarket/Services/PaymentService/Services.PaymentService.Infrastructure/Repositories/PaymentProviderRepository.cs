﻿using MongoDB.Bson;
using MongoDB.Driver;
using Services.PaymentService.Domain;
using System.Linq.Expressions;

namespace Services.PaymentService.Infrastructure.Repositories
{
    public class PaymentProviderRepository : IRepository<PaymentProvider, ObjectId>
    {
        private readonly ApplicationDbContext _context;

        public PaymentProviderRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(PaymentProvider entity)
        {
            if (await _context.PaymentProviders.Find(q => q.Name == entity.Name).AnyAsync()) throw new Exception("Payment provider with same name already exists");
            entity.PaymentProviderId = await _context.GetNextPaymentProviderId();
            await _context.PaymentProviders.InsertOneAsync(entity);
        }

        public async Task<bool> DeleteAsync(PaymentProvider entity)
        {
            var document = await _context.PaymentProviders.FindOneAndDeleteAsync(q => q.PaymentProviderId == entity.PaymentProviderId);
            return document != null;
        }

        public Task<List<PaymentProvider>> GetAllAsync() =>
            _context.PaymentProviders.Find("{}").ToListAsync();

        public Task<PaymentProvider> GetByIdAsync(ObjectId id) =>
            _context.PaymentProviders.Find(q => q.Id == id).SingleAsync();

        public Task<PaymentProvider?> GetByIdOrDefaultAsync(ObjectId id) =>
            _context.PaymentProviders.Find(q => q.Id == id).SingleOrDefaultAsync();

        public Task<PaymentProvider> GetFirstWhere(Expression<Func<PaymentProvider, bool>> predicate) => 
            _context.PaymentProviders.Find(predicate).SingleAsync();
        
        public Task<PaymentProvider?> GetFirstOrDefaultWhere(Expression<Func<PaymentProvider, bool>> predicate) => 
            _context.PaymentProviders.Find(predicate).SingleOrDefaultAsync();

        public Task<List<PaymentProvider>> GetRangeByIdsAsync(List<ObjectId> ids) =>
            _context.PaymentProviders.Find(q => ids.Contains(q.Id)).ToListAsync();

        public Task<List<PaymentProvider>> GetWhere(Expression<Func<PaymentProvider, bool>> predicate) =>
            _context.PaymentProviders.Find(predicate).ToListAsync();

        public Task<bool> AnyWhere(Expression<Func<PaymentProvider, bool>> predicate) =>
            _context.PaymentProviders.Find(predicate).AnyAsync();

        public async Task<bool> UpdateAsync(PaymentProvider entity)
        {
            //var document = await _context.PaymentCollections.FindOneAndReplaceAsync(q => q.Id == entity.Id, entity, new () { ReturnDocument = ReturnDocument.After });
            var document = await _context.PaymentProviders.FindOneAndReplaceAsync(q => q.Id == entity.Id, entity);
            return document != null;
        }
    }
}
