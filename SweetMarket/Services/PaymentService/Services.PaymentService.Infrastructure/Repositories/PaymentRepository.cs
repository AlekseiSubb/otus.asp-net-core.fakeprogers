﻿using MongoDB.Bson;
using MongoDB.Driver;
using Services.PaymentService.Domain;
using System.Linq.Expressions;

namespace Services.PaymentService.Infrastructure.Repositories
{
    public class PaymentRepository : IRepository<Payment, ObjectId>
    {
        private readonly ApplicationDbContext _context;

        public PaymentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task AddAsync(Payment entity) => _context.Payments.InsertOneAsync(entity);

        public async Task<bool> DeleteAsync(Payment entity)
        {
            var document = await _context.Payments.FindOneAndDeleteAsync(q => q.Id == entity.Id);
            return document != null;
        }

        public Task<List<Payment>> GetAllAsync() => 
            _context.Payments.Find("{}").ToListAsync();

        public Task<Payment> GetByIdAsync(ObjectId id) =>
            _context.Payments.Find(q => q.Id == id).SingleAsync();

        public Task<Payment?> GetByIdOrDefaultAsync(ObjectId id) =>
            _context.Payments.Find(q => q.Id == id).SingleOrDefaultAsync();

        public Task<Payment> GetFirstWhere(Expression<Func<Payment, bool>> predicate) => 
            _context.Payments.Find(predicate).SingleAsync();
        
        public Task<Payment?> GetFirstOrDefaultWhere(Expression<Func<Payment, bool>> predicate) => 
            _context.Payments.Find(predicate).SingleOrDefaultAsync();

        public Task<List<Payment>> GetRangeByIdsAsync(List<ObjectId> ids) =>
            _context.Payments.Find(q => ids.Contains(q.Id)).ToListAsync();

        public Task<List<Payment>> GetWhere(Expression<Func<Payment, bool>> predicate) =>
            _context.Payments.Find(predicate).ToListAsync();

        public Task<bool> AnyWhere(Expression<Func<Payment, bool>> predicate) =>
            _context.Payments.Find(predicate).AnyAsync();
        
        public async Task<bool> UpdateAsync(Payment entity)
        {
            //var document = await _context.PaymentCollections.FindOneAndReplaceAsync(q => q.Id == entity.Id, entity, new () { ReturnDocument = ReturnDocument.After });
            var document = await _context.Payments.FindOneAndReplaceAsync(q => q.Id == entity.Id, entity);
            return document != null;
        }
    }
}
