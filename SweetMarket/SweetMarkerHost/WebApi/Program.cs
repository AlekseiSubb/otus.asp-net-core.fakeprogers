using Domain.Entities.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using Infrastructure.EntityFramework;
using Infrastructure.Repositories.Implementations;
using Services.Integration;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NLog.Web;
using Services.Abstractions;
using Services.Implementations;
using Services.Repositories.Abstractions;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using WebApi.Extentions;
using WebApi.Models;
using WebApi.Models.RecipeStepEditAggregate;
using WebApi.Settings;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;

using static WebApi.Extentions.AddCustomerExtentions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Configuration;
using WebApi.Middlewares;

var builder = WebApplication.CreateBuilder(args);

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

AddConfiguration(builder);

builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        b => b.AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());
});

builder.Services.AddAccessTokenManagement();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
        options.Audience = "sweetmarket_market_api";
        options.RequireHttpsMetadata = false;
        //options.SaveToken = true;
        options.TokenValidationParameters = new()
        {
            RoleClaimType = "role",
            ValidTypes = new[] { "at+jwt" }
        };
    });


//���� ������
builder.Services.AddDbContext<DatabaseContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("PostgreSQL"),
        q => q.MigrationsAssembly("Infrastructure.EntityFramework"));
    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true); //�������� UTC-�����.
    //options.UseSqlServer(builder.Configuration.GetConnectionString("SqlServer"));
    //options.UseSqlite(builder.Configuration.GetConnectionString("SqLite"));
    //options.UseLazyLoadingProxies(); // lazy loading
});

builder.Host.UseNLog(new NLogAspNetCoreOptions
{
    //RemoveLoggerFactoryFilter = false, //�� ��������� true, �� ���� ������� ����������� �������, �� � ��, �� ��� �� ������.
    //ReplaceLoggerFactory = true, //������� ��������� �������
});


builder.Services.AddHealthChecks()
    .AddDbContextCheck<DatabaseContext>();

RegisterAppServices(builder);

builder.Services.AddMassTransitServices(builder.Configuration);

builder.Services.AddControllers();

//������������� �� ������������ ������������ ��. ���� �� ������
//https://docs.fluentvalidation.net/en/latest/aspnet.html
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddFluentValidationClientsideAdapters();
builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();
builder.Services.Configure<SwaggerGenOptions>(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Title",
        Description = "Description",
        Contact = new OpenApiContact
        {
            Name = "Example Contact",
            Url = new Uri("https://example.com/contact")
        },
        License = new OpenApiLicense
        {
            Name = "Example License",
            Url = new Uri("https://example.com/license")
        },
        Version = "v1"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/authorize"),
                TokenUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/token"),
                Scopes = new Dictionary<string, string>
                {
                    {"openid", ""},
                    {"email", ""},
                    {"profile", ""},
                    {"roles", ""},
                    {"sweetmarket_market_api.fullaccess", ""}
                },
            },
        },
    });
    options.OperationFilter<AuthorizeCheckOperationFilter>();
});


builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseMiddleware<CustomExceptionMiddleware>();

app.UseSwagger();

app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "MarketAPI V1");
    c.RoutePrefix = string.Empty;
    c.OAuthClientId("sweetmarket_web_frontend_client");
    c.OAuthClientSecret("LwZjqN6XCPCtnt0ZKYUiD1jGbRbe2aepqtGfxsuk9D");
    c.EnablePersistAuthorization();
});

//app.UseCors(); //���� �� ����, ����� ��������

app.UseRouting();
app.UseHttpsRedirection();
//app.UseHealthChecks("/health");

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

//����� ����� � �����, � ����� � ���:
//var provider = app.Services.GetService<IMapper<??>>().ConfigurationProvider;
//provider.AssertConfigurationIsValid();
using (var serviceScope = app.Services.CreateScope())
{
    var initService = serviceScope.ServiceProvider.GetService<ILoadFirstDataService>();
    initService?.InitializeDb();
}
app.Run();

void RegisterAppServices(WebApplicationBuilder webApplicationBuilder)
{
    webApplicationBuilder.Services.AddScoped<ILoadFirstDataService, LoadFirstDataService>();

    webApplicationBuilder.Services.AddScoped<IValidator<RecipeStepCreateModel>, RecipeStepCreateModelValidator>();
    webApplicationBuilder.Services.AddScoped<IValidator<RecipeStepUpdateModel>, RecipeStepUpdateModelValidator>();
    webApplicationBuilder.Services.AddScoped<IValidator<StepOperationCreateModel>, StepOperationCreateModelValidator>();
    webApplicationBuilder.Services.AddScoped<IValidator<StepOperationUpdateModel>, StepOperationUpdateModelValidator>();
    webApplicationBuilder.Services.AddScoped<IValidator<OrderFilterModel>, OrderFilterModelValidator>();

    //����������� ��������
    webApplicationBuilder.Services.AddScoped<IMarketRepository, MarketRepository>();

    //����������� �� ������
    webApplicationBuilder.Services.AddTransient<IOrderRepository, OrderRepository>();

    //����������� �� ��������
    webApplicationBuilder.Services.AddScoped<IProductRepository, ProductRepository>();
    webApplicationBuilder.Services.AddTransient<IRepository<Addon, int>, Repository<Addon, int>>();
    webApplicationBuilder.Services.AddScoped<IProductCategoryRepository, ProductCategoryRepository>();

    //����������� �� ��������
    webApplicationBuilder.Services.AddScoped<IRecipeRepository, RecipeRepository>();

    //������� �� ���������
    webApplicationBuilder.Services.AddTransient<IProductBusinessLayerForOwnersService, ProductBusinessLayerForOwnersService>();

    //����������� �� ��������
    webApplicationBuilder.Services.AddTransient<IRepository<Recipe, int>, Repository<Recipe, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<RecipeStep, int>, Repository<RecipeStep, int>>();
    webApplicationBuilder.Services.AddTransient<IRepository<StepOperation, int>, Repository<StepOperation, int>>();
    webApplicationBuilder.Services.AddScoped<IIngredientRepository, IngredientRepository>();
    webApplicationBuilder.Services.AddScoped<IIngredientCategoryRepository, IngredientCategoryRepository>();
    webApplicationBuilder.Services.AddScoped<IEngUnitRepository, EngUnitRepository>();

    //�������
    webApplicationBuilder.Services.AddCustomerModel(); //����������� �� ���������

    webApplicationBuilder.Services.AddTransient<IRecipeStepService, RecipeStepService>();
    webApplicationBuilder.Services.AddTransient<IRecipeStepRepository, RecipeStepRepository>();
    webApplicationBuilder.Services.AddTransient<IEngUnitService, EngUnitService>();
    webApplicationBuilder.Services.AddTransient<IIngredientCategoryService, IngredientCategoryService>();
    webApplicationBuilder.Services.AddTransient<IIngredientService, IngredientService>();
    webApplicationBuilder.Services.AddTransient<IProductCategoryService, ProductCategoryService>();
    webApplicationBuilder.Services.AddTransient<IOrderService, OrderService>();


    //������� ��������
    webApplicationBuilder.Services.AddTransient<IMarketService, MarketService>();

    webApplicationBuilder.Services.AddTransient<IVendorCodeService, VendorCodeService>();

    webApplicationBuilder.Services.AddHttpClient<IMarketApiToStorageGateway, MarketApiToStorageGateway>(c =>
    {
        c.BaseAddress = new Uri(webApplicationBuilder.Configuration["IntegrationSettings:ImageStorageUrl"]);
    });
}

void AddConfiguration(WebApplicationBuilder builder1)
{
    //Configuration
    //�������� ��������� �� ��������� (��������, �� �����, ������ �� ���: https://learn.microsoft.com/ru-ru/aspnet/core/fundamentals/configuration/?view=aspnetcore-7.0#cp)
    builder1.Configuration.Sources.Clear();
    //��������� ����
    builder1.Configuration
        .AddJsonFile("appsettings.json")
        .AddEnvironmentVariables()
        .AddUserSecrets<Program>();
    builder1.Services.Configure<ApplicationSettings>(builder1.Configuration
        .GetSection("")); //��� ������� ���������� ������
}

