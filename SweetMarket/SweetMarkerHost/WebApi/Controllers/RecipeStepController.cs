﻿using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using WebApi.Models.RecipeStepDetailsAggregate;
using WebApi.Models.RecipeStepEditAggregate;

namespace WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Roles = "Confectioner,SA")]
    public class RecipeStepController : ControllerBase
    {
        private readonly IRecipeStepService _recipeStepService;
        private readonly IValidator<RecipeStepCreateModel> _recipeStepCreateModelValidator;
        private readonly IValidator<RecipeStepUpdateModel> _recipeStepUpdateCreateValidator;
        private readonly IMapper _mapper;


        public RecipeStepController(IRecipeStepService recipeStepService,
            IMapper mapper,
            IValidator<RecipeStepCreateModel> recipeStepCreateModelValidator,
            IValidator<RecipeStepUpdateModel> recipeStepUpdateCreateValidator)
        {
            _recipeStepService = recipeStepService;
            _mapper = mapper;
            _recipeStepCreateModelValidator = recipeStepCreateModelValidator;
            _recipeStepUpdateCreateValidator = recipeStepUpdateCreateValidator;
        }

        /// <summary>
        /// Получить представления шага рецепта для чтения
        /// </summary>
        [HttpGet("details/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            RecipeStepDto recipeStep = await _recipeStepService.GetByIdAsync(id);

            if (recipeStep is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<RecipeStepDto, RecipeStepDetailsModel>(recipeStep));
        }

        /// <summary>
        /// Получить список представлений шагов рецепта для чтения
        /// </summary>
        [HttpPost("details")]
        public async Task<IActionResult> GetList(int recipeId)
        {
            return Ok(_mapper.Map<List<RecipeStepDetailsModel>>(await _recipeStepService.GetStepsByRecipeId(recipeId)));
        }

        /// <summary>
        /// Добавить новый шаг рецепта
        /// </summary>
        //TODO возврат структурированного объекта с ошибками валидации
        [HttpPost]
        public async Task<IActionResult> Add(RecipeStepCreateModel recipeStepCreateModel)
        {
            var validationResult = await _recipeStepCreateModelValidator.ValidateAsync(recipeStepCreateModel);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            return Ok(await _recipeStepService.CreateAsync(_mapper.Map<RecipeStepCreateDto>(recipeStepCreateModel)));
        }

        /// <summary>
        /// Изменить шаг рецепта
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, RecipeStepUpdateModel recipeStepUpdateModel)
        {
            RecipeStepDto recipe = await _recipeStepService.GetByIdAsync(id);

            if (recipe is null)
            {
                return NotFound();
            }

            var validationResult = await _recipeStepUpdateCreateValidator.ValidateAsync(recipeStepUpdateModel);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            await _recipeStepService.UpdateAsync(id, _mapper.Map<RecipeStepUpdateDto>(recipeStepUpdateModel));
            return Ok();
        }

        /// <summary>
        /// Удалить шаг рецепта
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _recipeStepService.DeleteAsync(id);
            return Ok();
        }
    }
}
