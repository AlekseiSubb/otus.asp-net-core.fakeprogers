﻿using AutoMapper;
using Domain.Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories.Abstractions;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Roles = "Confectioner,SA")]
    public class RecipeController : ControllerBase
    {
        private readonly IRecipeRepository _recipeRepository;
        private readonly IMapper _mapper;

        public RecipeController(IRecipeRepository recipeRepository, IMapper mapper)
        {
            _recipeRepository = recipeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить рецепт по Id
        /// </summary>
        //TODO добавить фильтр по удаленным записям, т.к. запись не должна удаляться
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            Recipe recipe = await _recipeRepository.GetAsync(id);

            if (recipe is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<Recipe, RecipeModel>(recipe));
        }

        /// <summary>
        /// Получить все рецепты
        /// </summary>
        //TODO добавить фильтр по удаленным записям, т.к. запись не должна удаляться
        [HttpGet("List")]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<Recipe> recipes = await _recipeRepository.GetAllAsync();

            if (recipes is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<Recipe>, IEnumerable<RecipeModel>>(recipes));
        }

        /// <summary>
        /// Добавить рецепт
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Add(RecipeEditModel recipeModel)
        {
            var recipe = _mapper.Map<Recipe>(recipeModel);
            return Ok(await _recipeRepository.AddAsync(recipe));
        }

        /// <summary>
        /// Изменить рецепт
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, RecipeEditModel recipeEditModel)
        {
            if (!await _recipeRepository.IsExistAsync(id))
            {
                return NotFound();
            }

            var recipe = _mapper.Map<Recipe>(recipeEditModel);
            recipe.Id = id;

            await _recipeRepository.UpdateAsync(recipe);
            return Ok();
        }

        /// <summary>
        /// Удалить рецепт
        /// </summary>
        //TODO продумать удаление, т.к. текущая реализация репозитория просто удаляет сущность из БД
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _recipeRepository.DeleteAsync(id);
            return Ok();
        }
    }
}
