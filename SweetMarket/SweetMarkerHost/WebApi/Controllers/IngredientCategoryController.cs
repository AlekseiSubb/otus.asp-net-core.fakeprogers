﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Roles = "Confectioner,SA")]
    public class IngredientCategoryController : ControllerBase
    {
        private readonly IIngredientCategoryService _ingredientCategoryService;
        private readonly IMapper _mapper;

        public IngredientCategoryController(IIngredientCategoryService ingredientCategoryService, IMapper mapper)
        {
            _ingredientCategoryService = ingredientCategoryService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить категорию ингредиентов
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IngredientCategoryDto ingredientCategory = await _ingredientCategoryService.GetIngredientCategoryByIdAsync(id);

            if (ingredientCategory is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IngredientCategoryDto, IngredientCategoryModel>(ingredientCategory));
        }

        /// <summary>
        /// Получить список категорий ингредиентов
        /// </summary>
        [HttpGet("list")]
        public async Task<IEnumerable<IngredientCategoryModel>> GetAll()
        {
            var items = await _ingredientCategoryService.GetIngredientCategoryAsync();

            return _mapper.Map<IEnumerable<IngredientCategoryDto>, IEnumerable<IngredientCategoryModel>>(items);
        }

        /// <summary>
        /// Добавить категорию ингредиентов
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Add(IngredientCategoryModel ingredientCategoryModel)
        {
            var validator = new IngredientCategoryValidator();
            var validationResult = validator.Validate(ingredientCategoryModel);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);

            var model = _mapper.Map<IngredientCategoryDto>(ingredientCategoryModel);
            model.Id = 0;

            return Ok(await _ingredientCategoryService.CreateIngredientCategoryAsync(model));
        }

        /// <summary>
        /// Изменить категорию ингредиентов
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, IngredientCategoryModel ingredientCategoryModel)
        {
            IngredientCategoryDto item = await _ingredientCategoryService.GetIngredientCategoryByIdAsync(id);

            if (item is null)
            {
                return NotFound();
            }

            var validator = new IngredientCategoryValidator();
            var validationResult = validator.Validate(ingredientCategoryModel);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);


            item.Name = ingredientCategoryModel.Name;
            item.Description = ingredientCategoryModel.Description;

            await _ingredientCategoryService.UpdateIngredientCategoryAsync(id, item);


            return Ok();
        }

        /// <summary>
        /// Удалить категорию ингредиентов
        /// </summary>
        //TODO продумать удаление, т.к. текущая реализация репозитория просто удаляет сущность из БД
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _ingredientCategoryService.DeleteIngredientCategoryAsync(id);
            return Ok();
        }
    }
}
