using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts.Product;
using WebApi.Models;

namespace WebApi.Controllers;

/// <summary>
/// API Product: ключевой функционал для владельцев/кондитеров
/// </summary>
[ApiController]
[Route("/api/v1/[controller]")]
[Authorize(Roles = "Confectioner,SA")]
public class ProductController : Controller
{
    private readonly IProductBusinessLayerForOwnersService _productBusinessLayerForOwnersService;
    private readonly IMapper _mapper;


    /// <inheritdoc />
    public ProductController(IProductBusinessLayerForOwnersService productBusinessLayerForOwnersService,
             IMapper mapper)
    {
        _productBusinessLayerForOwnersService = productBusinessLayerForOwnersService;
        _mapper = mapper;
    }

    /// <summary>
    /// Получить продукт по id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    [ProducesResponseType(typeof(ProductLiteModel), 200)]
    public async Task<IActionResult> Get(int id)
    {
        var product = await _productBusinessLayerForOwnersService.GetById(id);

        return Ok(_mapper.Map<ProductLiteModel>(product));
    }

    /// <summary>
    /// Добавить новую продукцию
    /// </summary>
    /// <param name="addProductModel"></param>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(typeof(ProductLiteModel), 200)]
    public async Task<IActionResult> AddWithRecipe([FromForm] AddProductModel addProductModel)
    {
        var addProductWithRecipeDto = _mapper.Map<AddProductModel, AddProductDto>(addProductModel);
        var productLiteDto = await _productBusinessLayerForOwnersService.AddWithRecipe(addProductWithRecipeDto);
        return productLiteDto is null ? NotFound() : Ok(_mapper.Map<ProductLiteModel>(productLiteDto));
    }

    /// <summary>
    /// Удалить продукцию
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Delete(int id)
    {
        var productLiteDto = await _productBusinessLayerForOwnersService.Delete(id);
        return productLiteDto is null ? NotFound() : Ok();//Ok(_mapper.Map<ProductLiteModel>(productLiteDto));
    }

    /// <summary>
    /// Редактировать продукцию
    /// </summary>
    /// <param name="editProductModel"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(typeof(ProductLiteModel), 200)]
    public async Task<IActionResult> Update([FromForm] ProductEditModel editProductModel, int id)
    {
        var editProductDto = _mapper.Map<ProductEditModel, EditProductDto>(editProductModel);
        var productLiteDto = await _productBusinessLayerForOwnersService.Update(editProductDto, id);

        return productLiteDto is null ? NotFound() : Ok(_mapper.Map <ProductLiteModel> (productLiteDto));
    }
    /// <summary>
    /// Получить список продуктов
    /// </summary>
    [HttpGet("list")]
    [ProducesResponseType(typeof(IEnumerable<ProductLiteModel>), 200)]
    public async Task<IActionResult> GetAll()
    {
        var products = await _productBusinessLayerForOwnersService.GetAll();

        return products is null ? NotFound() : Ok(_mapper.Map<List<ProductLiteModel>>(products.ToList()));
    }

    /// <summary>
    /// Опубликовать на странице магазина
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("publish")]
    public async Task<IActionResult> PublishIntoShop(int id)
    {
        var productLiteDto = await _productBusinessLayerForOwnersService.PublishIntoShop(id);

        return productLiteDto is null ? NotFound() : Ok();
    }

    /// <summary>
    /// Убрать с публикации
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("unpublish")]
    public async Task<IActionResult> UnpublishIntoShop(int id)
    {
        var productLiteDto = await _productBusinessLayerForOwnersService.UnPublishIntoShop(id);

        return productLiteDto is null ? NotFound() : Ok();
    }
}