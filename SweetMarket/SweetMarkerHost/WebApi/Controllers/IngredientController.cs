﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;
using System.Net;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    [Authorize(Roles = "Confectioner,SA")]
    public class IngredientController: ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IIngredientService _ingredientService;

        /// <summary>
        /// API ингредиентов
        /// </summary>
        public IngredientController(
            IIngredientService ingredientService,
            IMapper mapper)
        {
            _mapper = mapper;
            _ingredientService = ingredientService;
        }

        /// <summary>
        /// Получить ингредиент по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]

        public async Task<IActionResult> GetAsync(int id)
        {
            var identity = User.Identity;

            IngredientDto ingredient = await _ingredientService.GetByIdAsync(id);

            if (ingredient is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IngredientResponse>(ingredient));
        }

        /// <summary>
        /// Получить список ингредиентов
        /// </summary>
        [HttpGet("List")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetList()
        {
            return Ok(_mapper.Map<List<IngredientResponse>>(await _ingredientService.GetAllAsync()));
        }

        /// <summary>
        /// Создать новый ингредиент
        /// </summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(IngredientRequest request)
        {
            return Ok(await _ingredientService.CreateAsync(_mapper.Map<IngredientUpdateDto>(request)));
        }

        /// <summary>
        /// Изменить ингредиент
        /// </summary>
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Update(int id, IngredientRequest request)
        {
            if (!await _ingredientService.IsExistAsync(id))
            {
                return NotFound();
            }

            await _ingredientService.UpdateAsync(id, _mapper.Map<IngredientUpdateDto>(request));
            return Ok();
        }

        /// <summary>
        /// Удалить ингредиент
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _ingredientService.DeleteAsync(id))
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
