﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Confectioner,SA")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EngUnitController : ControllerBase
    {
        private readonly IEngUnitService _engUnitService;
        private readonly IMapper _mapper;

        public EngUnitController(IEngUnitService engUnitService, IMapper mapper)
        {
            _engUnitService = engUnitService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить единицу измерения по Id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var item = await _engUnitService.GetEndUnitByIdAsync(id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<EngUnitDto, EngUnitModel>(item));
        }

        /// <summary>
        /// Получить список единиц измерения
        /// </summary>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            var x =User;
            var items = await _engUnitService.GetEndUnitsAsync();
            return Ok(_mapper.Map<List<EngUnitModel>>(items));
        }

        /// <summary>
        /// Добавить единицу измерения
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Add(EngUnitModel engUnitModel)
        {
            var validator = new EngUnitValidator();
            var validationResult = validator.Validate(engUnitModel);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);
            var model = _mapper.Map<EngUnitDto>(engUnitModel);
            return Ok(await _engUnitService.CreateEngUnitAsync(model));
        }

        /// <summary>
        /// Изменить единицу измерения
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, EngUnitModel engUnit)
        {
            EngUnitDto item = await _engUnitService.GetEndUnitByIdAsync(id);
            if (item is null)
            {
                return NotFound();
            }

            var validator = new EngUnitValidator();
            var validationResult = validator.Validate(engUnit);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);

            item.Name = engUnit.Name;
            item.ShortName = engUnit.ShortName;

            await _engUnitService.UpdateEngUnitAsync(id, item);
            return Ok();
        }

        /// <summary>
        /// Удалить единицу измерения
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _engUnitService.DeleteEngUnitAsync(id);
            return Ok();
        }
    }
}


