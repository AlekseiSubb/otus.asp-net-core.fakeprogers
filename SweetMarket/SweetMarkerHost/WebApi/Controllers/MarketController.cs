﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Models;
using IdentityModel;

namespace WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(Roles = "Confectioner,SA")]
    public class MarketController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMarketService _service;
        private readonly IVendorCodeService _vendorCodeService;

        public MarketController(IMarketService service, IMapper mapper, IVendorCodeService vendorCodeService)
        {
            _service = service;
            _mapper = mapper;
            _vendorCodeService = vendorCodeService;
        }

        /// <summary>
        /// Получить магазин
        /// </summary>

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var market = await _service.GetById(id);

            if (market is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MarketModel>(market));
        }

        /// <summary>
        /// Получить магазин по коду
        /// </summary>
        [HttpGet("code")]
        public async Task<IActionResult> Get(string code)
        {
            var dto = await _vendorCodeService.ParseCode(code);
            if (dto is null)
            {
                return NotFound();
            }
            var market = await _service.GetById(dto.MarketId);
            if (market is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MarketModel>(market));
        }

        /// <summary>
        /// Получить список магазинов
        /// </summary>
        [HttpPost("list")]
        public async Task<IActionResult> GetList(PaginationFilterModel filterModel)
        {
            var filterDto = _mapper.Map<PaginationFilterModel, PaginationFilterDto>(filterModel);
            var result = await _service.GetPaged(filterDto);
            return Ok(_mapper.Map<PagingModel<MarketShortDto>>(result));
        }

        [Authorize]
        /// <summary>
        /// Получить список магазинов
        /// </summary>
        [HttpPost("listByUser")]
        public async Task<IActionResult> GetListByUser(PaginationFilterModel filterModel)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (!int.TryParse(identity?.FindFirst("userid")?.Value, out int userId)) return Unauthorized();
            var filterDto = _mapper.Map<PaginationFilterModel, PaginationFilterDto>(filterModel);
            var result = await _service.GetPagedByUser(filterDto, userId);
            return Ok(_mapper.Map<PagingModel<MarketShortDto>>(result));
        }

        [Authorize]
        /// <summary>
        /// Добавить магазин
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Add(MarketModel marketModel)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (!int.TryParse(identity?.FindFirst("userid")?.Value, out int userId)) return Unauthorized();
            var mm = _mapper.Map<MarketDto>(marketModel);
            mm.Code = await _vendorCodeService.GenerateMarketCode();
            var newMarket = await _service.Create(mm, userId/*marketModel.CreatiorId*/);
            return Ok(_mapper.Map<MarketModel>(newMarket));
        }

        /// <summary>
        /// Изменить магазин
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, MarketModel marketModel)
        {
            await _service.Update(id, _mapper.Map<MarketDto>(marketModel));
            return Ok();
        }

        /// <summary>
        /// Удалить магазин
        /// </summary>
        //TODO продумать удаление, т.к. текущая реализация репозитория просто удаляет сущность из БД
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }
    }
}
