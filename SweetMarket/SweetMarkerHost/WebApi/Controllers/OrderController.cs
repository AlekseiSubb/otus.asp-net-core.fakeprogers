﻿using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System.Net;
using WebApi.Models;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер заказов кондитера
    /// </summary>
    [ApiController]
    [Route("/api/v1/[controller]")]
    [Authorize(Roles = "Confectioner,SA")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        private readonly IValidator<OrderFilterModel> _orderFilterModelValidator;

        public OrderController(IOrderService orderService, IMapper mapper, IValidator<OrderFilterModel> orderFilterModelValidator)
        {
            _orderService = orderService;
            _mapper = mapper;
            _orderFilterModelValidator = orderFilterModelValidator;
        }

        /// <summary>
        /// Заказ по Id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IEnumerable<OrderModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            var item = await _orderService.GetOrderByIdAsync(id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<OrderModel>(item));
        }

        /// <summary>
        /// Получить список всех  заказов для тестирования
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(GetAll))]
        [ProducesResponseType(typeof(IEnumerable<OrderModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAll()
        {
            var items = await _orderService.GetOrdersAsync();
            return Ok(_mapper.Map<List<OrderModel>>(items));
        }

        /// <summary>
        /// Получить список заказа постранично
        /// </summary>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public async Task<IActionResult> GetList(OrderFilterModel filterModel)
        {
            var filterDto = _mapper.Map<OrderFilterModel, OrderFilterDto>(filterModel);
            return Ok(_mapper.Map<List<OrderModel>>(await _orderService.GetPaged(filterDto)));
        }

        /// <summary>
        /// Создать заказ
        /// </summary>
        /// <param name="orderModel"></param>
        /// <returns></returns>
        [HttpPost("Add")]
        public async Task<IActionResult> CreateOrder(CreateOrderModel orderModel)
        {
            var model = _mapper.Map<OrderDto>(orderModel);
            return Ok(await _orderService.CreateOrderAsync(model));
        }

        /// <summary>
        /// Изменить заказ
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, OrderModel orderModel)
        {
            OrderDto item = await _orderService.GetOrderByIdAsync(id);
            if (item is null)
            {
                return NotFound();
            }
            var model = _mapper.Map<OrderDto>(orderModel);

            await _orderService.UpdateOrderAsync(model);
            return Ok();

        }

        /// <summary>
        /// Удалить заказ
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var ordertoDelete = await _orderService.GetOrderByIdAsync(id);
            if (ordertoDelete is null) return NotFound();
            await _orderService.DeleteOrderAsync(id);
            return Ok();
        }
    }
}