﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using Services.Contracts;
using WebApi.Models;

namespace CustomerApi.ConsumersMapping
{
    public class OrderMappingsProfile : Profile
    {
        public OrderMappingsProfile()
        {
            CreateMap<OrderCreateCommand, OrderDto>();
            CreateMap<OrderUpdateCommand, OrderDto>();
            CreateMap<OrderItemCommand, OrderItemDto>();
        }
    }
}
