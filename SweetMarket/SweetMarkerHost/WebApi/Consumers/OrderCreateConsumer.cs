﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using Services.Abstractions;
using Services.Contracts;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class OrderCreateConsumer : IConsumer<OrderCreateCommand>
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        private readonly ILogger<OrderCreateConsumer> _logger;
        private readonly IValidator<OrderCreateCommand> _validator;

        public OrderCreateConsumer(
            IOrderService orderService, 
            IMapper mapper, 
            ILogger<OrderCreateConsumer> logger,
            IValidator<OrderCreateCommand> validator)
        {
            _orderService = orderService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<OrderCreateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения OrderCreateCommand начало");

            OrderCreateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);
           
            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации OrderCreateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение OrderCreateCommand");
            }

            OrderDto orderDto = _mapper.Map<OrderCreateCommand, OrderDto>(message);

            await _orderService.CreateOrderAsync(orderDto);

            _logger.LogInformation("Обработка сообщения OrderCreateCommand конец");
        }
    }
}
