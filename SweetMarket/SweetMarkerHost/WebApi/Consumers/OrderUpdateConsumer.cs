﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using Services.Abstractions;
using Services.Contracts;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class OrderUpdateConsumer : IConsumer<OrderUpdateCommand>
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        private readonly ILogger<OrderUpdateCommand> _logger;
        private readonly IValidator<OrderUpdateCommand> _validator;

        public OrderUpdateConsumer(
            IOrderService orderService,
            IMapper mapper,
            ILogger<OrderUpdateCommand> logger,
            IValidator<OrderUpdateCommand> validator)
        {
            _orderService = orderService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<OrderUpdateCommand> context)
        {
            _logger.LogInformation("Обработка сообщения OrderUpdateCommand начало");

            OrderUpdateCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации OrderUpdateCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение OrderUpdateCommand");
            }

            OrderDto orderDto = _mapper.Map<OrderUpdateCommand, OrderDto>(message);

            await _orderService.UpdateOrderAsync(orderDto);

            _logger.LogInformation("Обработка сообщения OrderUpdateCommand конец");
        }
    }
}
