﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using Services.Abstractions;
using Services.Contracts;
using FluentValidation;
using MassTransit;

namespace CustomerApi.Consumers
{
    public class OrderDeleteConsumer : IConsumer<OrderDeleteCommand>
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        private readonly ILogger<OrderDeleteConsumer> _logger;
        private readonly IValidator<OrderDeleteCommand> _validator;

        public OrderDeleteConsumer(
            IOrderService orderService,
            IMapper mapper,
            ILogger<OrderDeleteConsumer> logger,
            IValidator<OrderDeleteCommand> validator)
        {
            _orderService = orderService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }
        public async Task Consume(ConsumeContext<OrderDeleteCommand> context)
        {
            _logger.LogInformation("Обработка сообщения OrderDeleteCommand начало");

            OrderDeleteCommand message = context.Message;

            var validationResult = await _validator.ValidateAsync(message);

            if (!validationResult.IsValid)
            {
                _logger.LogError("Валидации OrderDeleteCommand.\r\n {ValidationMessages}", validationResult.Errors);

                throw new Exception("Не удалось обработать сообщение OrderDeleteCommand");
            }

            await _orderService.DeleteByIdCustomerOrderAsync(message.CustomerOrderId);

            _logger.LogInformation("Обработка сообщения MarketDeleteCommand конец");
        }
    }
}
