﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class OrderDeleteValidator : AbstractValidator<OrderDeleteCommand>
    {
        public OrderDeleteValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.MarketId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.MarketId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.MarketId)} должно быть не меньше {minIdValue}");

            RuleFor(s => s.CustomerOrderId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.CustomerOrderId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.CustomerOrderId)} должно быть не меньше {minIdValue}");
        }
    }
}
