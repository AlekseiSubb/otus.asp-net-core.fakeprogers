﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class OrderCreateValidator : AbstractValidator<OrderCreateCommand>
    {
        public OrderCreateValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.CustomerOrderId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.CustomerOrderId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.CustomerOrderId)} должно быть не меньше {minIdValue}");

            RuleFor(s => s.MarketId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.CustomerOrderId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.CustomerOrderId)} должно быть не меньше {minIdValue}");

            const int maxLength = 4096;
            RuleFor(s => s.Description)
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Description)} не должно быть длиннее {maxLength} символов");

            RuleFor(s => s.DateReady)
                .NotEmpty().WithMessage("Дата не должна быть пустой")
                .LessThan(p => DateTime.Now).WithMessage("Дата готовности должна быть больше текущей даты");

            RuleForEach(s => s.OrderItems).SetValidator(new OrderItemCommandValidator());
        }


    }
}
