﻿using Contracts.MessageBus.Commands;
using FluentValidation;

namespace CustomerApi.ConsumersValidators
{
    public class OrderItemCommandValidator : AbstractValidator<OrderItemCommand>
    {
        public OrderItemCommandValidator()
        {
            int minIdValue = 1;
            RuleFor(s => s.ProductId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.ProductId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.ProductId)} должно быть не меньше {minIdValue}");

            const int maxLength = 4096;
            RuleFor(s => s.Comment)
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Comment)} не должно быть длиннее {maxLength} символов");

            RuleFor(s => s.Quantity)
                .NotEmpty().WithMessage("Колличество не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.ProductId)} должно быть не меньше {minIdValue}");

            RuleFor(s => s.Price)
                .NotEmpty().WithMessage("Колличество не должно быть пустым")
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.ProductId)} должно быть не меньше {minIdValue}");

            //TODO валидация полей продукции

        }

    }
}
