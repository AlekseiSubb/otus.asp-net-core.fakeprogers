﻿using Domain.Entities.Models;
using Infrastructure.Repositories.Implementations;
using Services.Abstractions;
using Services.Implementations;
using Services.Repositories.Abstractions;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore.Infrastructure;
using CustomerApi.Consumers;

namespace WebApi.Extentions;
public static class MassTransitRegistration
{
    public static IServiceCollection AddMassTransitServices(this IServiceCollection serviceCollection, ConfigurationManager configurationManager)
    {
        AddServicesInternal(serviceCollection, configurationManager);

        return serviceCollection;
    }

    private static void AddServicesInternal(IServiceCollection serviceCollection, ConfigurationManager configurationManager)
    {
        serviceCollection.AddMassTransit(q =>
        {
            q.AddConsumer<OrderCreateConsumer>();
            q.AddConsumer<OrderDeleteConsumer>();
            q.AddConsumer<OrderUpdateConsumer>();

            q.UsingRabbitMq((context, config) =>
            {
                config.Host(configurationManager["RabbitMq:Host"],
                    virtualHost: configurationManager["RabbitMq:VirtualHost"], q =>
                    {
                        q.Username(configurationManager["RabbitMq:Username"]);
                        q.Password(configurationManager["RabbitMq:Password"]);
                    });

                config.ReceiveEndpoint("Cmd.Order.MarketApi.OrderCreateCommand", e =>
                {
                    e.ConfigureConsumer<OrderCreateConsumer>(context);
                    e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                });

                config.ReceiveEndpoint("Cmd.Order.MarketApi.OrderDeleteCommand", e =>
                {
                    e.ConfigureConsumer<OrderDeleteConsumer>(context);
                    e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                });

                config.ReceiveEndpoint("Cmd.Order.MarketApi.OrderUpdateCommand", e =>
                {
                    e.ConfigureConsumer<OrderUpdateConsumer>(context);
                    e.UseMessageRetry(r => { r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1)); });
                });

            });
        });
    }
}

