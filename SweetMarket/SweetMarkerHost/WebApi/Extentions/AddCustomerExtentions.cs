using Domain.Entities.Models;
using Infrastructure.Repositories.Implementations;
using Services.Abstractions;
using Services.Implementations;
using Services.Repositories.Abstractions;

namespace WebApi.Extentions;

public static class AddCustomerExtentions
{
    public static IServiceCollection AddCustomerModel(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<IRepository<User, int>, Repository<User, int>>();
        serviceCollection.AddTransient<IRepository<Market, int>, Repository<Market, int>>();
        return serviceCollection;
    }
}