﻿using FluentValidation;

namespace WebApi.Models
{
    public class IngredientCategoryModel
    {
        public int Id { get; set; }

        public string Name { get; set; } = default!;

        public string Description { get; set; } = default!;
    }

    public class IngredientCategoryValidator : AbstractValidator<IngredientCategoryModel>
    {
        public IngredientCategoryValidator()
        {
            RuleFor(e => e.Id).Equal(0)
                                .WithMessage("Поле {Id} должно быть 0!");

            RuleFor(e => e.Name).NotEmpty()
                                .WithMessage("Поле {Name} не должно быть пустым!")
                                .MaximumLength(50)
                                .WithMessage("Поле {Name} не должно быть больше 20 символов!");

            RuleFor(e => e.Description)
                .MaximumLength(50)
                .WithMessage("Поле {ShortName} не должно быть больше 10 символов!");
        }
    }
}
