﻿namespace WebApi.Models
{
    public class RecipeEditModel
    {
        public int Id { get; set; }

        public string Name { get; set; } = default!;

        public string Description { get; set; } = default!;

        public int CookingTime { get; set; }
    }
}
