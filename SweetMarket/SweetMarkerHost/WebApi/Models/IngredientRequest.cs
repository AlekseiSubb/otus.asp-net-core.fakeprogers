﻿namespace WebApi.Models
{
    public class IngredientRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int IngredientCategoryId { get; set; }

        public int EngUnitId { get; set; }
    }
}
