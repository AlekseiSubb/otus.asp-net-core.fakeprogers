﻿namespace WebApi.Models;

public class OrderFilterModel
{
    /// <summary>
    /// поля для фильтрации
    /// </summary>
    public string Name { get; set; } = default!;

    public int? MarketId { get; set; }

    public decimal? Price { get; set; }

    public string? State { get; set; }

    public bool SortByDateCreate { get; set; }

    /// <summary>
    /// заказов на странице
    /// </summary>
    public int ItemsPerPage { get; set; }

    /// <summary>
    /// номер страницы с котороый выводить
    /// </summary>
    public int Page { get; set; }
}

