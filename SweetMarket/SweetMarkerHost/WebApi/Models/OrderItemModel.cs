﻿namespace WebApi.Models;

public class OrderItemModel
{
    //public int OrderId { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }
    public string? Comment { get; set; }
    public decimal Price { get; set; }
}

