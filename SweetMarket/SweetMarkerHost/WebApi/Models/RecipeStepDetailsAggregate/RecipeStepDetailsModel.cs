﻿namespace WebApi.Models.RecipeStepDetailsAggregate
{
    /// <summary>
    /// Корень аггрегата для получения шага операции с деталями
    /// </summary>
    public class RecipeStepDetailsModel
    {
        public RecipeStepDetailsModel()
        {
            StepOperations = new List<StepOperationDetailsModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; } = default!;

        public string? Description { get; set; }

        public string Action { get; set; } = default!;

        public int NumPos { get; set; }

        public int RecipeId { get; set; }


        public ICollection<StepOperationDetailsModel> StepOperations { get; set; } = new List<StepOperationDetailsModel>();
    }
}
