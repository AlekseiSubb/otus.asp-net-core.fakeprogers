﻿namespace WebApi.Models.RecipeStepDetailsAggregate
{
    public class EngUnitDetailsModel
    {
        public string Name { get; set; } = default!;

        public string ShortName { get; set; } = default!;
    }
}
