﻿namespace WebApi.Models.RecipeStepDetailsAggregate
{
    public class IngredientDetailsModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; } = default!;

        public string EngUnitName { get; set; }
    }
}
