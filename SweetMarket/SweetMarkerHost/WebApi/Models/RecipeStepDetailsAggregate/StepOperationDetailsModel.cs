﻿namespace WebApi.Models.RecipeStepDetailsAggregate
{
    public class StepOperationDetailsModel
    {

        public int Id { get; set; }

        /// <summary>
        /// Количество ингредиента в единицах измерения ингредиента
        /// </summary>
        public double Count { get; set; }

        /// <summary>
        /// Порядок в шаге
        /// </summary>
        public int NumPos { get; set; }

        public int IngredientId { get; set; }

        public string IngredientName { get; set; }

        public string EngUnitName { get; set; }

        //public IngredientDetailsModel Ingredient { get; set; }
    }
}
