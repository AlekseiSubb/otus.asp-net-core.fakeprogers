﻿namespace WebApi.Models
{
    public class IngredientResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CategoryName { get; set; }

        public string Description { get; set; }

        public string EngUnitShortName { get; set; }
    }
}
