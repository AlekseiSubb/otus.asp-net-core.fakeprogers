﻿using FluentValidation;

namespace WebApi.Models
{
    public class EngUnitModel
    {
        public int Id { get; set; }

        public string Name { get; set; } = default!;

        public string ShortName { get; set; } = default!;
    }
    public class EngUnitValidator : AbstractValidator<EngUnitModel>
    {
        public EngUnitValidator()
        {
            RuleFor(e => e.Id).Equal(0)
                    .WithMessage("Поле {Id} должно быть 0!");

            RuleFor(e => e.Name).NotEmpty()
                                .WithMessage("Поле {Name} не должно быть пустым!")
                                .MaximumLength(20)
                                .WithMessage("Поле {Name} не должно быть больше 20 символов!");

            RuleFor(e => e.ShortName)
                .MaximumLength(10)
                .WithMessage("Поле {ShortName} не должно быть больше 10 символов!");
        }
    }

}
