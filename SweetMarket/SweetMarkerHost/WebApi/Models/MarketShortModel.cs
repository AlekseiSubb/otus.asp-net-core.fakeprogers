namespace WebApi.Models;

/// <summary>
/// �������� ������ ��������
/// </summary>
public class MarketShortModel
{
    /// <summary>
    /// ������������
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// ��������
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// ������ �� ��������� ��������
    /// </summary>
    public Guid ImageGuid { get; set; }
}