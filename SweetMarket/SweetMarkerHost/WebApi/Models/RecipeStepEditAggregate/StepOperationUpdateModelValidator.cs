﻿using FluentValidation;

namespace WebApi.Models.RecipeStepEditAggregate
{
    public class StepOperationUpdateModelValidator : AbstractValidator<StepOperationUpdateModel>
    {
        public StepOperationUpdateModelValidator()
        {
            ConfigureIdValidation();
            ConfigureCountValidation();
            ConfigureNumPosValidation();
            ConfigureIngredientIdValidation();
        }

        //TODO проверять что редактируемые объекты существуют в БД?
        private void ConfigureIdValidation()
        {
            int minIdValue = 0;
            RuleFor(s => s.Id)
                .GreaterThanOrEqualTo(minIdValue)
                .WithMessage(s => $"Поле {nameof(s.Id)} должно быть не меньше {minIdValue}");
        }

        private void ConfigureCountValidation()
        {
            int minCountValue = 1;
            RuleFor(s => s.Count)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Count)} не должно быть пустым")
                .GreaterThanOrEqualTo(minCountValue)
                .WithMessage(s => $"Поле {nameof(s.Count)} должно быть не меньше {minCountValue}");
        }

        private void ConfigureNumPosValidation()
        {
            int minNumPosValue = 1;
            RuleFor(s => s.NumPos)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.NumPos)} не должно быть пустым")
                .GreaterThanOrEqualTo(minNumPosValue)
                .WithMessage(s => $"Поле {nameof(s.NumPos)} должно быть не меньше {minNumPosValue}");
        }

        //TODO Проверка на наличие в БД ингредиентов с указанными Id
        private void ConfigureIngredientIdValidation()
        {
            int minIngredientIdValue = 1;
            RuleFor(s => s.IngredientId)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.IngredientId)} не должно быть пустым")
                .GreaterThanOrEqualTo(minIngredientIdValue)
                .WithMessage(s => $"Поле {nameof(s.IngredientId)} должно быть не меньше {minIngredientIdValue}");
        }
    }
}
