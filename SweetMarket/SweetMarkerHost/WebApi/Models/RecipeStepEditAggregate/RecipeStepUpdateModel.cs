﻿namespace WebApi.Models.RecipeStepEditAggregate
{
    public class RecipeStepUpdateModel
    {
        public RecipeStepUpdateModel()
        {
            StepOperations = new List<StepOperationUpdateModel>();
        }

        public int? Id { get; set; }

        public string Name { get; set; } = default!;

        public string? Description { get; set; }

        public string Action { get; set; } = default!;

        public int? NumPos { get; set; }

        public int RecipeId { get; set; }

        public ICollection<StepOperationUpdateModel> StepOperations { get; set; }
    }
}
