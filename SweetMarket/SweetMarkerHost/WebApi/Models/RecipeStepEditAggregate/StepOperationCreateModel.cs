﻿namespace WebApi.Models.RecipeStepEditAggregate
{
    public class StepOperationCreateModel
    {
        /// <summary>
        /// Количество ингредиента в единицах измерения ингредиента
        /// </summary>
        public double? Count { get; set; }

        /// <summary>
        /// Порядок в шаге
        /// </summary>
        public int? NumPos { get; set; }

        public int? IngredientId { get; set; }
    }
}
