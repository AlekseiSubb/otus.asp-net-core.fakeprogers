﻿using FluentValidation;

namespace WebApi.Models.RecipeStepEditAggregate
{
    public class RecipeStepCreateModelValidator : AbstractValidator<RecipeStepCreateModel>
    {
        public RecipeStepCreateModelValidator(IValidator<StepOperationCreateModel> stepOperationValidator)
        {
            ConfigureNameValidation();
            ConfigureDescriptionValidation();
            ConfigureActionValidation();
            ConfigureNumPosValidation();
            RuleForEach(s => s.StepOperations).SetValidator(stepOperationValidator);
        }

        private void ConfigureNameValidation()
        {
            const int minLength = 3;
            const int maxLength = 256;
            RuleFor(s => s.Name)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть пустым")
                .MinimumLength(minLength)
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть не короче {minLength} символов")
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Name)} не должно быть длиннее {maxLength} символов");
        }

        private void ConfigureDescriptionValidation()
        {
            const int maxLength = 4096;
            RuleFor(s => s.Description)
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Description)} не должно быть длиннее {maxLength} символов");

        }

        private void ConfigureActionValidation()
        {
            const int minLength = 10;
            const int maxLength = 512;
            RuleFor(s => s.Action)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.Action)} не должно быть пустым")
                .MinimumLength(minLength)
                .WithMessage(s => $"Поле {nameof(s.Action)} не должно быть не короче {minLength} символов")
                .MaximumLength(maxLength)
                .WithMessage(s => $"Поле {nameof(s.Action)} не должно быть длиннее {maxLength} символов");

        }

        private void ConfigureNumPosValidation()
        {
            int minNumPosValue = 1;
            RuleFor(s => s.NumPos)
                .NotEmpty()
                .WithMessage(s => $"Поле {nameof(s.NumPos)} не должно быть пустым")
                .GreaterThanOrEqualTo(minNumPosValue)
                .WithMessage(s => $"Поле {nameof(s.NumPos)} должно быть не меньше {minNumPosValue}");
        }
    }
}
