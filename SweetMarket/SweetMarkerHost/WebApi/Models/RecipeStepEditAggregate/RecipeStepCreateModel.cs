﻿namespace WebApi.Models.RecipeStepEditAggregate
{
    public class RecipeStepCreateModel
    {
        public RecipeStepCreateModel()
        {
            StepOperations = new List<StepOperationCreateModel>();
        }

        public string Name { get; set; } = default!;

        public string? Description { get; set; }

        public string Action { get; set; } = default!;

        public int? NumPos { get; set; }

        public int RecipeId { get; set; }

        public ICollection<StepOperationCreateModel> StepOperations { get; set; }
    }
}
