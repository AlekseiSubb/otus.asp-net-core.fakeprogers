﻿namespace WebApi.Models
{
    /// <summary>
    /// ДТО пейджинации
    /// </summary>
    public class PagingModel<T> where T : class
    {
        /// <summary>
        /// Количество элементов на странице
        /// </summary>
        public int ItemsPerPage { get; set; }
        /// <summary>
        /// Текущая страница
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Страниц всего
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Элементы
        /// </summary>
        public List<T> Items { get; set; } = new List<T>();
    }
}
