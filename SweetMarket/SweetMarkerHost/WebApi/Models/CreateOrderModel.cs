﻿namespace WebApi.Models;

public class CreateOrderModel
{
    public int CustomerOrderId { get; init; }
    public int MarketId { get; set; }
    public string Description { get; set; } = default!;
    public DateTime DateReady { get; set; }

    //public Shipping? Shipping { get; set; } 
    public ICollection<OrderItemModel> OrderItems { get; set; } = new List<OrderItemModel>();
}



