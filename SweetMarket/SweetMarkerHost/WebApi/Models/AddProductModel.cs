﻿using Microsoft.AspNetCore.Http;

namespace Services.Contracts.Product
{
    public class AddProductModel
    {

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int? RecipeId { get; set; }

        public int ProductCategoryId { get; set; }

        public int MarketId { get; set; }

        public bool IsPublishedInMarket { get; set; }

        public IFormFile? Image { get; set; }
    }
}
