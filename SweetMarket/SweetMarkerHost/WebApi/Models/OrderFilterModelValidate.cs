﻿using FluentValidation;

namespace WebApi.Models;

public class OrderFilterModelValidator : AbstractValidator<OrderFilterModel>
{
    public OrderFilterModelValidator()
    {
        {
            RuleFor(e => e.Page).GreaterThanOrEqualTo(1)
                .WithMessage("Поле {Page} должно быть больше или ровно 1!");

            RuleFor(e => e.ItemsPerPage).GreaterThanOrEqualTo(1)
                                .WithMessage("Поле {ItemsPerPage} не должно быть меньше 1!");

        }
    }
}


