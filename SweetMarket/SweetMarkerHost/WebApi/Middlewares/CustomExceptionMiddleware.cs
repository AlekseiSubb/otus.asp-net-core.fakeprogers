﻿namespace WebApi.Middlewares;

public class CustomExceptionMiddleware
{
    public readonly RequestDelegate _next;
    public readonly ILogger _logger;

    public CustomExceptionMiddleware(RequestDelegate next, ILogger<CustomExceptionMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task Invoke(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception ex)
        {
            _logger.LogError(new EventId(222), ex, ex?.Message);
            var response = new { error = ex?.Message ?? "Unknown exception" };
            await httpContext.Response.WriteAsJsonAsync(response);
        }
        
    }
}
