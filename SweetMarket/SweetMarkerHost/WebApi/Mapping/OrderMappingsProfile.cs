using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности заказа.
    /// </summary>
    public class OrderMappingsProfile : Profile
    {
        public OrderMappingsProfile()
        {
            CreateMap<OrderDto, OrderModel>().ReverseMap();
            CreateMap<OrderItemDto, OrderItemModel>().ReverseMap();

            CreateMap<OrderShortModel, OrderDto>();
            CreateMap<OrderFilterModel, OrderFilterDto>();
            CreateMap<CreateOrderModel, OrderDto>();

        }
    }
}
