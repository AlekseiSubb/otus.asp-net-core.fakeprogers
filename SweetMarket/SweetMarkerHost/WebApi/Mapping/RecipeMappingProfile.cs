﻿using AutoMapper;
using Domain.Entities.Models;
using WebApi.Models;

namespace WebApi.Mapping
{
    public class RecipeMappingProfile : Profile
    {
        public RecipeMappingProfile()
        {
            CreateMap<Recipe, RecipeModel>();
            CreateMap<RecipeEditModel, Recipe>()
                .ForMember(d => d.Id, o => o.Ignore());
        }
    }
}
