﻿using AutoMapper;
using Services.Contracts;
using Services.Contracts.Product;
using WebApi.Models;

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности категории продуктов
    /// </summary>
    public class ProductCategoryMappingProfile : Profile
    {
        public ProductCategoryMappingProfile()
        {
            CreateMap<ProductCategoryModel, ProductCategoryDto>().ReverseMap();
            CreateMap<ProductCategoryModel, ProductCategoryDto>().ReverseMap();
        }
    }
}
