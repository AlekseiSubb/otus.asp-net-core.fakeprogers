using AutoMapper;
using Services.Contracts;
using Services.Contracts.Product;
using WebApi.Models;

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности продукта.
    /// </summary>
    public class ProductMappingsProfile : Profile
    {
        public ProductMappingsProfile()
        {
            CreateMap<EditProductDto, ProductEditModel>().ReverseMap();
            CreateMap<ProductLiteDto, ProductLiteModel>().ReverseMap();
            CreateMap<AddProductModel, AddProductDto>().ReverseMap();

        }
    }
}
