using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для фильтра пейджинации.
    /// </summary>
    public class GeneralMappingsProfile : Profile
    {
        public GeneralMappingsProfile()
        {
            CreateMap<PaginationFilterModel, PaginationFilterDto>().ReverseMap();
        }
    }
}
