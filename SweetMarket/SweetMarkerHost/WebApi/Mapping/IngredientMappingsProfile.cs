﻿using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    public class IngredientMappingsProfile: Profile
    {
        public IngredientMappingsProfile()
        {
            CreateMap<IngredientDto, IngredientResponse>();
            CreateMap<IngredientRequest, IngredientUpdateDto>();
        }
    }
}
