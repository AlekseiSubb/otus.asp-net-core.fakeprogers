﻿using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    public class IngredientCategoryMappingProfile : Profile
    {
        public IngredientCategoryMappingProfile()
        {
            CreateMap<IngredientCategoryModel, IngredientCategoryDto>();
            CreateMap<IngredientCategoryDto, IngredientCategoryModel>();
        }
    }
}
