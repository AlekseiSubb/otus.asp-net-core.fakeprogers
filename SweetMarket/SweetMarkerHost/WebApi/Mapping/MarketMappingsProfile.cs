using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для магазина.
    /// </summary>
    public class MarketMappingsProfile : Profile
    {
        public MarketMappingsProfile()
        {
            CreateMap<MarketModel, MarketDto>();
            CreateMap<MarketDto, MarketModel>();
            CreateMap<MarketShortModel, MarketShortDto>();

            CreateMap<PagingDto<MarketShortDto>, PagingModel<MarketShortDto>>();
        }
    }
}
