﻿using AutoMapper;
using Services.Contracts;
using WebApi.Models.RecipeStepDetailsAggregate;
using WebApi.Models.RecipeStepEditAggregate;

namespace WebApi.Mapping
{
    public class RecipeStepMappingProfile: Profile
    {
        public RecipeStepMappingProfile()
        {
            CreateMap<RecipeStepDto, RecipeStepDetailsModel>();
            CreateMap<StepOperationDto, StepOperationDetailsModel>();

            CreateMap<RecipeStepCreateModel, RecipeStepCreateDto>();
            CreateMap<StepOperationCreateModel, StepOperationCreateDto>();

            CreateMap<RecipeStepUpdateModel, RecipeStepUpdateDto>();
            CreateMap<StepOperationUpdateModel, StepOperationUpdateDto>();
        }
    }
}
