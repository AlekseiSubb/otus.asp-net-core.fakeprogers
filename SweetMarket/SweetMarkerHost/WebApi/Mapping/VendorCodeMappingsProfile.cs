using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для магазина.
    /// </summary>
    public class VendorCodeMappingsProfile : Profile
    {
        public VendorCodeMappingsProfile()
        {
            CreateMap<VendorCodeModel, VendorCodeDto>();
            CreateMap<VendorCodeDto, VendorCodeModel>();
        }
    }
}
