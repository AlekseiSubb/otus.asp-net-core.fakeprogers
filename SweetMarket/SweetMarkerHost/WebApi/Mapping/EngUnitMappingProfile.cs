﻿using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    public class EngUnitMappingProfile : Profile
    {
        public EngUnitMappingProfile()
        {
            CreateMap<EngUnitModel, EngUnitDto>();
            CreateMap<EngUnitDto, EngUnitModel>();
        }
    }
}
