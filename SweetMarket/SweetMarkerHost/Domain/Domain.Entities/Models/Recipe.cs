﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

/// <summary>
/// Рецепт
/// </summary>
public class Recipe : IEntity<int>
{
    [Key]
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    /// <summary>
    /// Коэффициент трудоемкости, показывает относительное количество времени и ресурсов необходимых для изготовления продукта по рецепту
    /// </summary>
    public double LaborIntensityFactor { get; set; }

    /// <summary>
    /// Время приготовления в минутах
    /// </summary>
    public int CookingTime { get; set; }

    public virtual ICollection<Product> Products { get; set; } = new HashSet<Product>();

    public virtual ICollection<RecipeStep> RecipeSteps { get; set; } = new HashSet<RecipeStep>();
}