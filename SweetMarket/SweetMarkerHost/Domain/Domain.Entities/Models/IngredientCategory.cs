﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

/// <summary>
/// типы ингридиентов
/// </summary>
public class IngredientCategory : IEntity<int>
{
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

    public string? Description { get; set; }

    public ICollection<Ingredient>? Ingredients { get; set; } = new List<Ingredient>();
}