﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;
    /// <summary>
    /// Позиции в заказе
    /// </summary>
    public class OrderItem : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Колличество единиц выбранной продукции в шт
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Комментарий к позиции заказа
        /// </summary>
        public string? Comment { get; set; }

        /// <summary>
        /// Ссылка на заказ
        /// </summary>
        public int OrderId { get; set; } 
        public Order Order { get; set; } = default!;

        /// <summary>
        /// Ссылка на продукцию
        /// </summary>
        public int ProductId { get; set; } 
        public Product Product { get; set; } = default!;

        public decimal Price { get; set; }

    }

