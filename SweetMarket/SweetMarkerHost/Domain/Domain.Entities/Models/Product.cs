﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

/// <summary>
/// Модель продукции
/// TODO подумать над тем чтобы была возможность загружать продукт без рецепта, не кастомизируемый
/// TODO возможно имеет смысл добавить опциональное поле, некий идентификатор для загрузки из внешней среды
/// TODO возможно стоит добавить возможность архивировать товары в магазине
/// </summary>
public class Product : IEntity<int>
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// Наименование
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// Описание
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// ссылка на хранилище картинок
    /// </summary>
    public Guid? ImageGuid { get; set; }

    /// <summary>
    /// Стоимость
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// TODO ссылка на Publication
    /// TODO PublicationHistory, кто и когда опубликовал и по какой цене?
    /// TODO вопрос как цена товара завязана на рецепт
    /// </summary>
    public bool IsPublishedInMarket { get; set; }

    public DateTime CreatedAt { get; set; }

    public int CreatedBy { get; set; }

    /// <summary>
    /// TODO кем и когда удален и т.п.
    /// </summary>
    public bool IsDeleted { get; set; }

    /// <summary>
    /// Уникальный код продукта внутри системы
    /// </summary>
    public string Code { get; set; } = default!;

    /// <summary>
    /// Ссылка на рецепт
    /// </summary>
    public int? RecipeId { get; set; }
    public Recipe? Recipe { get; set; }

    /// <summary>
    /// Ссылка на магазин
    /// </summary>
    public int MarketId { get; set; }

    /// <summary>
    /// Ссылка на тип продукции 
    /// </summary>
    public int? ProductCategoryId { get; set; }
    public ProductCategory? Category { get; set; } = default!;

    /// <summary>
    /// Список на возможные добавки ? тут many - many 
    /// </summary>
    public ICollection<Addon> Addons { get; set; } = new List<Addon>();

    /// <summary>
    /// Оценка продкута
    /// </summary>
    /// <remarks>
    /// Что-то вроде средней оценки
    /// </remarks>
    public byte Rating { get; set; }

    /// <summary>
    /// Число коментариев под продуктом
    /// </summary>
    public uint CommntsCount { get; set; }

}