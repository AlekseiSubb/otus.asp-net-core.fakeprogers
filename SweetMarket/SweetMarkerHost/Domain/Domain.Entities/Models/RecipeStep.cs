﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

public class RecipeStep : IEntity<int>
{
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

    public string? Description { get; set; }

    /// <summary>
    /// Действие выполняемое в шаге
    /// </summary>
    public string? Action { get; set; }

    /// <summary>
    /// Порядок в рецепте
    /// </summary>
    public int NumPos { get; set; }

    /// <summary>
    /// Ссылка на рецепт
    /// </summary>
    public int RecipeId { get; set; }
    public Recipe Recipe { get; set; }

    /// <summary>
    /// Список шагов
    /// </summary>
    public virtual ICollection<StepOperation> StepOperations { get; set; } = new HashSet<StepOperation>();
}