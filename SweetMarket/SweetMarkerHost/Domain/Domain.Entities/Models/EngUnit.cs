﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

public class EngUnit : IEntity<int>
{
    /// <summary>
    /// Единицы измерения
    /// </summary>
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

    public string ShortName { get; set; }

    public ICollection<Ingredient> Ingredients { get; set; } = new HashSet<Ingredient>();
}