﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

public class Ingredient : IEntity<int>
{
    public int Id { get; set; }

    /// <summary>
    /// Имя ингедиента
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Описание ингредиента
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Ссылка на  типы ингридиентов
    /// </summary>
    public int IngredientCategoryId { get; set; }
    public IngredientCategory IngredientCategory { get; set; }

    /// <summary>
    /// Ссылка на единицы измерения
    /// </summary>
    public int EngUnitId { get; set; }
    public EngUnit EngUnit { get; set; }


    /// <summary>
    /// Список шагов операций рецептов
    /// </summary>
    public ICollection<StepOperation> StepOperations { get; set; } = new List<StepOperation>();

}