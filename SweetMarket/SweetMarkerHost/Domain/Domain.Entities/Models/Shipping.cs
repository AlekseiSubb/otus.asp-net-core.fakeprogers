﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

    /// <summary>
    /// Тут отображаются детали по доставке
    /// </summary>
    public class Shipping : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Ссылка на заказ
        /// </summary>
        public int OrderId { get; set; }
        public Order Order { get; set; }

        /// <summary>
        /// Контактное лицо при доставке
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Адрес доставки
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Телефон для связи
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Примерная дата доставки
        /// </summary>
        public DateTime DateShipping { get; set; }

        /// <summary>
        /// Транспортная компания
        /// </summary>
        public string? TransportCompany { get; set; }

        public string ShippingNumber { get; set; } = string.Empty;

        /// <summary>
        /// Коментарий покупателя
        /// </summary>
        public string? CustomerComment { get; set; } = default!;

        /// <summary>
        /// Время доставки, если превысить, то а-та-та
        /// </summary>
        public TimeSpan DeliveryTime { get; set; } = default!;

        /// <summary>
        /// Начало исполнения доставки (с этого момента начинается отсчёт сроков доставки)
        /// </summary>
        public DateTimeOffset DeliveryStartTime { get; set; } = default!;

        /// <summary>
        /// Статус доставки, над ними надо подумать
        /// </summary>
        public ShippingStatus Status { get; set; } = ShippingStatus.NotStarted;

    }

    /// <summary>
    /// Статус доставки, над ними надо подумать
    /// </summary>
    public enum ShippingStatus
    {
        /// <summary>
        /// Не начата
        /// </summary>
        NotStarted,
        /// <summary>
        /// Доставляется
        /// </summary>
        InProdess,
        /// <summary>
        /// Доставлено
        /// </summary>
        Delivered,
        /// <summary>
        /// Не доставлено, возможно, тут необходим статус "отменено"
        /// </summary>
        NotDelivered
    }

