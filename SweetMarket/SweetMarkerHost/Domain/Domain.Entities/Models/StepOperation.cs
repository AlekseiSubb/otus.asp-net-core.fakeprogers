﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Models;

/// <summary>
/// действия которые нужно выполнить 
/// </summary>
public class StepOperation : IEntity<int>
{
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// Количество ингредиента в единицах измерения ингредиента
    /// </summary>
    public double Count { get; set; }

    /// <summary>
    /// Порядок в шаге
    /// </summary>
    public int NumPos { get; set; }

    /// <summary>
    /// Ссылка на шаг рецепта
    /// </summary>
    public int RecipeStepId { get; set; }
    public RecipeStep RecipeStep { get; set; }

    /// <summary>
    /// Ссылка на ингридиент - по идеи тут может быть несколько 
    /// </summary>
    public int IngredientId { get; set; }
    public Ingredient Ingredient { get; set; }
}