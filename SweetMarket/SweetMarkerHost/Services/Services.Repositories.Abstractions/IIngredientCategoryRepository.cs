﻿using Domain.Entities.Models;

namespace Services.Repositories.Abstractions
{
    public interface IIngredientCategoryRepository : IRepository<IngredientCategory, int>
    {
    }
}
