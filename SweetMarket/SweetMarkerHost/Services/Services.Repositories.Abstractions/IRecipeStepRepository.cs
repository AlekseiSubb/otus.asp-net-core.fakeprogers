﻿using Domain.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    public interface IRecipeStepRepository : IRepository<RecipeStep, int>
    {
        Task<ICollection<RecipeStep>> GetByRecipeIdAsync(int recipeId);
    }
}
