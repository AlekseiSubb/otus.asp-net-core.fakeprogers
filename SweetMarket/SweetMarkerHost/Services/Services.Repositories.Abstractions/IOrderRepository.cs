﻿using Domain.Entities;
using Domain.Entities.Models;
using Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions;
public interface IOrderRepository : IRepository<Order, int>
{
    Task<List<Order>> GetPagedAsync(OrderFilterDto filterDto);
    Task<List<Order>> GetAllAsync();
    Task<Order> GetAsync(int id);
    Task<Order> GetByCustomerIdAsync(int idCustomerOrder);
    Task<bool> DeleteByIdCustomerOrderAsync(int idCustomerOrder);
}
