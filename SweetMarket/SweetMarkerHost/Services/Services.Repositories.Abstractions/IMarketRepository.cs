﻿using Domain.Entities;
using Domain.Entities.Models;
using Services.Contracts;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с магазинами
    /// </summary>
    public interface IMarketRepository : IRepository<Market, int>, ICodeEntityRepository
    {
        public Task<PaginatedEntity<Market, int>> GetPagedAsync(PaginationFilterDto filterDto);
        
        public Task<PaginatedEntity<Market, int>> GetPagedByUserAsync(PaginationFilterDto filterDto, int userId);
    }
}
