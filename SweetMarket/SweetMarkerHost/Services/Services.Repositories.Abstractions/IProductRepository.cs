﻿using Domain.Entities;
using Domain.Entities.Models;
using Services.Contracts;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    public interface IProductRepository : IRepository<Product, int>, ICodeEntityRepository
    {
    }
}
