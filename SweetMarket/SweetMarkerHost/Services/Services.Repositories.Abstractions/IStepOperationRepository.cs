﻿using Domain.Entities.Models;

namespace Services.Repositories.Abstractions
{
    public interface IStepOperationRepository : IRepository<StepOperation, int>
    {
    }
}
