﻿using Domain.Entities.Models;

namespace Services.Repositories.Abstractions
{
    public interface IProductCategoryRepository : IRepository<ProductCategory, int>, ICodeEntityRepository
    {
    }
}
