﻿using Domain.Entities.Models;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    public interface IRecipeRepository : IRepository<Recipe, int>
    {
        Task<bool> IsExistAsync(int id);
    }
}
