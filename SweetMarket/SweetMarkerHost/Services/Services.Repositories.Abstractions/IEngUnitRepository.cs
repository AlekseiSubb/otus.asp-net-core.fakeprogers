﻿using Domain.Entities.Models;

namespace Services.Repositories.Abstractions
{
    public interface IEngUnitRepository : IRepository<EngUnit, int>
    {
    }
}
