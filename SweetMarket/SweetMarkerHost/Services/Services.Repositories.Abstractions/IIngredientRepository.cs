﻿using Domain.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    public interface IIngredientRepository : IRepository<Ingredient, int>
    {
        new Task<ICollection<Ingredient>> GetAllAsync();

        Task<bool> IsExistAsync(int id);

        Task<Ingredient> GetAsync(int id);

    }
}
