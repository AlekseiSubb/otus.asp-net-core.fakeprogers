﻿using AutoMapper;
using Domain.Entities.Models;
using Microsoft.VisualBasic;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations
{
    public class IngredientService : IIngredientService
    {
        private readonly IIngredientRepository _repository;
        private readonly IMapper _mapper;

        public IngredientService(IIngredientRepository ingredientRepository, IMapper mapper) 
        {
            _repository = ingredientRepository;
            _mapper = mapper;
        }

        public async Task<int> CreateAsync(IngredientUpdateDto dto)
        {
            var ingredient = _mapper.Map<Ingredient>(dto);
            return (await _repository.AddAsync(ingredient)).Id;
        }

        public async Task<bool> IsExistAsync(int id)
        {
            return await _repository.IsExistAsync(id);
        }

        public async Task<bool> UpdateAsync(int id, IngredientUpdateDto dto)
        {
            var ingredient = _mapper.Map<Ingredient>(dto);
            ingredient.Id = id;

            return await _repository.UpdateAsync(ingredient);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var ingredient = await _repository.GetAsync(id);
            if(ingredient != null)
            {
                return await _repository.DeleteAsync(ingredient);
            }

            return false;
        }

        public async Task<ICollection<IngredientDto>> GetAllAsync()
        {
            var ingredients = await _repository.GetAllAsync();
            return _mapper.Map<ICollection<IngredientDto>>(ingredients);
        }

        public async Task<IngredientDto> GetByIdAsync(int id)
        {
            var ingredient = await _repository.GetAsync(id);
            return _mapper.Map<IngredientDto>(ingredient);
        }  
    }
}
