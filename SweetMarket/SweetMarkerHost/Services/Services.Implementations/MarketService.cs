﻿using System;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Models;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System.Threading.Tasks;
using Contracts.MessageBus.Commands;
using MassTransit;

namespace Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами
    /// </summary>
    public class MarketService : IMarketService
    {
        private readonly IMapper _mapper;
        private readonly IMarketRepository _marketRepository;
        private readonly IVendorCodeService _vendorCodeService;
        private readonly ISendEndpointProvider _sendEndpointProvider;

        public MarketService(IMapper mapper,
            IMarketRepository marketRepository, 
            IVendorCodeService vendorCodeService, 
            ISendEndpointProvider sendEndpointProvider)
        {
            _mapper = mapper;
            _marketRepository = marketRepository;
            _vendorCodeService = vendorCodeService;
            _sendEndpointProvider = sendEndpointProvider;
        }

        /// <summary>
        /// Получить список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов. </returns>
        public async Task<PagingDto<MarketShortDto>> GetPaged(PaginationFilterDto filterDto)
        {
            var entities = await _marketRepository.GetPagedAsync(filterDto);
            return _mapper.Map<PaginatedEntity<Market, int>, PagingDto<MarketShortDto>>(entities);
        }

        /// <summary>
        /// Получить постраничный список магазинов пользователя
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов. </returns>
        public async Task<PagingDto<MarketShortDto>> GetPagedByUser(PaginationFilterDto filterDto, int userId)
        {
            var entities = await _marketRepository.GetPagedByUserAsync(filterDto, userId);
            return _mapper.Map<PaginatedEntity<Market, int>, PagingDto<MarketShortDto>>(entities);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО курса</returns>
        public async Task<MarketDto> GetById(int id)
        {
            var market = await _marketRepository.GetAsync(id);
            return _mapper.Map<Market, MarketDto>(market);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="courseDto">ДТО курса</param>
        /// <returns>идентификатор</returns>
        //public async Task<int> CreateAsync(MarketDto marketDto, UserDto creator)
        public async Task<int> Create(MarketDto marketDto, int creatorId)
        {
            Market entity = _mapper.Map<MarketDto, Market>(marketDto);
            entity.OwnerId = creatorId;
            entity.Code = await _vendorCodeService.GenerateMarketCode();

            Market market = await _marketRepository.AddAsync(entity);

            if (market is null)
            {
                throw new Exception("Не удалось создать магазин");
            }

            await SendCreateMarketCommand(market);

            return market.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="marketDto">ДТО курса</param>
        public async Task Update(int id, MarketDto marketDto)
        {
            var entity = _mapper.Map<MarketDto, Market>(marketDto);
            entity.Id = id;

            //TODO обработка ошибки
            bool result = await _marketRepository.UpdateAsync(entity);

            if (result)
            {
                await SendUpdateMarketCommand(entity);
            }
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(int id)
        {
            //TODO обработка ошибки
            bool result = await _marketRepository.DeleteAsync(id);
            if (result)
            {
                await SendDeleteMarketCommand(id);
            }
        }

        //TODO refactor
        private async Task SendCreateMarketCommand(Market market)
        {
            MarketCreateCommand command = _mapper.Map<Market, MarketCreateCommand>(market);

            var uri = new Uri("queue:Cmd.Customer.CustomerApi.MarketCreateCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }

        //TODO refactor
        private async Task SendUpdateMarketCommand(Market market)
        {
            MarketUpdateCommand command = _mapper.Map<Market, MarketUpdateCommand>(market);

            var uri = new Uri("queue:Cmd.Customer.CustomerApi.MarketUpdateCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }

        //TODO refactor
        private async Task SendDeleteMarketCommand(int marketId)
        {
            var uri = new Uri("queue:Cmd.Customer.CustomerApi.MarketDeleteCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send<MarketDeleteCommand>(new () { MarketId = marketId });
        }
    }
}