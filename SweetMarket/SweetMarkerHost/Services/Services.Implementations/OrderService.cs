﻿using System;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Models;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System.Threading.Tasks;
using Contracts.MessageBus.Commands;
using MassTransit;
using System.Collections.Generic;
using System.Linq;

namespace Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;

        }
        public async Task<OrderDto> GetOrderByIdAsync(int id)
        {
            var item = await _orderRepository.GetAsync(id);
            return _mapper.Map<Order, OrderDto>(item);
        }

        public async Task<IEnumerable<OrderDto>> GetOrdersAsync()
        {
            var items = await _orderRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(items);
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список заказов. </returns>
        public async Task<IEnumerable<OrderDto>> GetPaged(OrderFilterDto filterDto)
        {
            IEnumerable<Order> entities = await _orderRepository.GetPagedAsync(filterDto);
            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(entities);
        }

        public async Task<int> CreateOrderAsync(OrderDto orderDto)
        {
            var model = _mapper.Map<Order>(orderDto);

            model.OrderNumber = "market" + model.MarketId.ToString().PadLeft(5, '0');

            //посчитать сумму 
            model.Price = model.OrderItems.Sum(oi => oi.Price);

            model.Id = 0;
            model.DateCreate = DateTime.Now;
            model.State = OrderStatusEnum.Created.ToString();
            Order addItem = await _orderRepository.AddAsync(model);

            //генирация номера заказа 
            addItem.OrderNumber = "market" + model.MarketId.ToString().PadLeft(5, '0') + "-" + model.Id.ToString().PadLeft(5, '0');
            await _orderRepository.UpdateAsync(addItem);

            return addItem.Id;
        }
        public async Task UpdateOrderAsync(OrderDto orderDto)
        {
            var model = await _orderRepository.GetByCustomerIdAsync(orderDto.CustomerOrderId);

            var newModel = _mapper.Map<Order>(orderDto);

            newModel.Id = model.Id;
            await _orderRepository.UpdateAsync(newModel);
        }
        public async Task DeleteOrderAsync(int id)
        {
            await _orderRepository.DeleteAsync(id);
        }
        public async Task DeleteByIdCustomerOrderAsync(int idCustomerOrder)
        {
            await _orderRepository.DeleteByIdCustomerOrderAsync(idCustomerOrder);
        }
    }
}




