﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Implementations
{
    public class EngUnitService : IEngUnitService
    {
        private readonly IEngUnitRepository _engUnitRepository;
        private readonly IMapper _mapper;

        public EngUnitService(IEngUnitRepository engUnitRepository, IMapper mapper)
        {
            _engUnitRepository = engUnitRepository;
            _mapper = mapper;
        }

        public async Task<EngUnitDto> GetEndUnitByIdAsync(int id)
        {
            var item = await _engUnitRepository.GetAsync(id);
            return _mapper.Map<EngUnit, EngUnitDto>(item);
        }

        public async Task<IEnumerable<EngUnitDto>> GetEndUnitsAsync()
        {
            var items = await _engUnitRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<EngUnit>, IEnumerable<EngUnitDto>>(items);
        }

        public async Task<int> CreateEngUnitAsync(EngUnitDto engUnitDto)
        {
            var model = _mapper.Map<EngUnit>(engUnitDto);
            model.Id = 0;
            EngUnit addItem = await _engUnitRepository.AddAsync(model);
            return addItem.Id;
        }

        public async Task UpdateEngUnitAsync(int id, EngUnitDto engUnitDto)
        {
            EngUnit item = await _engUnitRepository.GetAsync(id);
            item.Name = engUnitDto.Name;
            item.ShortName = engUnitDto.ShortName;

            await _engUnitRepository.UpdateAsync(item);
        }

        public async Task DeleteEngUnitAsync(int id)
        {
            await _engUnitRepository.DeleteAsync(id);
        }
    }
}




