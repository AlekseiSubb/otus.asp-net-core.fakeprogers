using AutoMapper;
using Domain.Entities;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.Mapping;

public class MarketMappingsProfile : Profile
{
    public MarketMappingsProfile()
    {
        CreateMap<Product, int>(MemberList.Destination).ConstructUsing(q => q.Id); //��������
        CreateMap<User, int>(MemberList.Destination).ConstructUsing(q => q.Id); //��������

        CreateMap<MarketDto, Market>(MemberList.Source);

        CreateMap<Market, MarketDto>(MemberList.Destination);
        CreateMap<Market, MarketShortDto>(MemberList.Destination);

        CreateMap<PaginatedEntity<Market, int>, PagingDto<MarketShortDto>>(MemberList.None);
        //.ForMember(d => d.m1, map => map.Ignore())
    }
}