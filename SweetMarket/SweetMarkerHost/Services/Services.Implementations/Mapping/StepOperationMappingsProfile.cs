﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    public class StepOperationMappingsProfile : Profile
    {
        public StepOperationMappingsProfile()
        {
            CreateMap<StepOperation, StepOperationDto>()
                .ForMember(d => d.IngredientName, o => o.MapFrom(s => s.Ingredient.Name))
                .ForMember(d => d.EngUnitName, o => o.MapFrom(s => s.Ingredient.EngUnit.ShortName));

            CreateMap<StepOperationCreateDto, StepOperation>()
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<StepOperationUpdateDto, StepOperation>(); 
        }
    }
}
