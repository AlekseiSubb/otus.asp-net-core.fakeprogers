﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations.Mapping
{
    public class IngredientMappingsProfile: Profile
    {
        public IngredientMappingsProfile()
        {
            CreateMap<Ingredient, IngredientDto>()
                .ForMember(d => d.CategoryName, map => map.MapFrom(i => i.IngredientCategory.Name));

            CreateMap<IngredientUpdateDto, Ingredient>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.IngredientCategory, map => map.Ignore())
                .ForMember(d => d.EngUnit, map => map.Ignore());
        }
    }
}
