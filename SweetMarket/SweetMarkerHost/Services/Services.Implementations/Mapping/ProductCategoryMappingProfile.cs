﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Contracts;
namespace Services.Implementations.Mapping;

/// <summary>
/// Профиль автомаппера для сущности категории продуктов
/// </summary>
public class ProductCategoryMappingProfile : Profile
{
    public ProductCategoryMappingProfile()
    {
        CreateMap<ProductCategory, ProductCategoryDto>();
        CreateMap<ProductCategoryDto, ProductCategory>();
    }
}

