﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities.Models;
using Services.Contracts;
using Services.Contracts.Product;

namespace Services.Implementations.Mapping
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductLiteDto, Product>().ReverseMap();

            CreateMap<AddProductDto, Product>();

            CreateMap<EditProductDto, Product>()
                .ForAllMembers(o => o.Condition((source, destination, member) => member != null));
        }
    }
}