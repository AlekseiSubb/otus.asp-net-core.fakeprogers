using AutoMapper;
using Domain.Entities;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.Mapping;
/// <summary>
/// Профиль автомаппера для сущности заказа.
/// </summary>
public class OrderMappingsProfile : Profile
{
    public OrderMappingsProfile()
    {
        CreateMap<OrderDto, Order>().ReverseMap();
        CreateMap<OrderItemDto, OrderItem>().ReverseMap();
    }
}

