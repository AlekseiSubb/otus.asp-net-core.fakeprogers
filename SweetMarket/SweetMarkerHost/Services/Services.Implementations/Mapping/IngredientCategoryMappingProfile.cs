﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    public class IngredientCategoryMappingProfile : Profile
    {
        public IngredientCategoryMappingProfile()
        {
            CreateMap<IngredientCategoryDto, IngredientCategory>();
            CreateMap<IngredientCategory, IngredientCategoryDto>();
        }
    }
}
