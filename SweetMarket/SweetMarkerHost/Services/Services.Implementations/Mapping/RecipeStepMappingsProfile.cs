﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    public class RecipeStepMappingsProfile : Profile
    {
        public RecipeStepMappingsProfile()
        {
            CreateMap<RecipeStep, RecipeStepDto>();

            CreateMap<RecipeStepCreateDto, RecipeStep>()
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<RecipeStepUpdateDto, RecipeStep>();
        }
    }
}
