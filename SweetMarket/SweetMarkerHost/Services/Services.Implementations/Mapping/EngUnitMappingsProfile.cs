using AutoMapper;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.Mapping;

public class EngUnitMappingsProfile : Profile
{
    public EngUnitMappingsProfile()
    {
        CreateMap<EngUnitDto, EngUnit>();
        CreateMap<EngUnit, EngUnitDto>();
    }
}