#nullable enable
using AutoMapper;
using Domain.Entities.Models;
using MassTransit;
using Services.Abstractions;
using Services.Contracts.Product;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.MessageBus.Commands;
using Microsoft.AspNetCore.Http;
using static System.Net.Mime.MediaTypeNames;

namespace Services.Implementations;

/// <inheritdoc/>
public class ProductBusinessLayerForOwnersService : IProductBusinessLayerForOwnersService
{
    private readonly IMapper _mapper;
    private readonly IVendorCodeService _vendorCodeService;
    private readonly IProductRepository _productRepository;
    private readonly IRecipeRepository _recipeRepository;
    private readonly IMarketRepository _marketRepository;
    private readonly IProductCategoryRepository _productCategoryRepository;
    private readonly ISendEndpointProvider _sendEndpointProvider;
    private readonly IMarketApiToStorageGateway _marketApiToStorageGateway; 

    public ProductBusinessLayerForOwnersService(
        IProductRepository productRepository,
        IMapper mapper,
        IVendorCodeService vendorCodeService,
        IRecipeRepository recipeRepository,
        IMarketRepository marketRepository,
        IProductCategoryRepository productCategoryRepository,
        ISendEndpointProvider sendEndpointProvider,
        IMarketApiToStorageGateway marketApiToStorageGateway)
    {
        _productRepository = productRepository;
        _mapper = mapper;
        _vendorCodeService = vendorCodeService;
        _recipeRepository = recipeRepository;
        _marketRepository = marketRepository;
        _productCategoryRepository = productCategoryRepository;
        _sendEndpointProvider = sendEndpointProvider;
        _marketApiToStorageGateway = marketApiToStorageGateway;
    }

    /// <inheritdoc/>
    public async Task<ProductLiteDto?> GetById(int id)
    {
        var product = await _productRepository.GetAsync(id);
        var dto = _mapper.Map<Product, ProductLiteDto>(product);

        return dto;
    }

    /// <inheritdoc/>
    public async Task<ProductLiteDto?> AddWithRecipe(AddProductDto addProductWithRecipeDto)
    {
        var recipe = await _recipeRepository.GetAsync(addProductWithRecipeDto.RecipeId);
        if (recipe is null) {throw new Exception("Рецепт не найден");}


        var market = await _marketRepository.GetAsync(addProductWithRecipeDto.MarketId);
        if (market is null) throw new Exception("Магазин не найден");

        var product = _mapper.Map<AddProductDto, Product>(addProductWithRecipeDto);
        product.Recipe = recipe;
        product.Code = await _vendorCodeService.GenerateProductCode();
        product.CreatedAt = DateTime.Now;
        if(addProductWithRecipeDto.Image != null)
            product.ImageGuid = Guid.NewGuid();
        Product createdProduct = await _productRepository.AddAsync(product);

        if (createdProduct is null)
        {
            return null;
        }

        await SendCreateProductCommand(createdProduct);

        await _marketApiToStorageGateway.SendImageToStorage(addProductWithRecipeDto.Image, (Guid)product.ImageGuid);

        return _mapper.Map<Product, ProductLiteDto>(createdProduct);
    }

    /// <inheritdoc/>
    public async Task<ProductLiteDto?> Delete(int id)
    {
        var product = await _productRepository.GetAsync(id);
        if (product is null) throw new Exception("Продукт не найден");

        bool resultDelete = await _productRepository.DeleteAsync(id);

        if (!resultDelete)
        {
            return null;
        }

        await SendDeleteProductCommand(id);

        await _marketApiToStorageGateway.DeleteImageToStorage((Guid)product.ImageGuid);

        return _mapper.Map<Product, ProductLiteDto>(product);

    }

    /// <inheritdoc/>
    public async Task<ProductLiteDto?> Update(EditProductDto editProductDto, int id)
    {
        var productCategory = await _productCategoryRepository.GetAsync(editProductDto.ProductCategoryId);
        if (productCategory is null) throw new Exception("Категория продукта не найдена");

        var product = await _productRepository.GetAsync(id);
        if (product is null) throw new Exception("Продукт не найден");

        Product productNew = _mapper.Map(editProductDto, product);

        bool resultUpdate = await _productRepository.UpdateAsync(productNew);
        if (!resultUpdate)
        {
            return null;
        }

        await SendUpdateProductCommand(productNew);

        var dto = _mapper.Map<Product, ProductLiteDto>(productNew);

        await _marketApiToStorageGateway.UpdateImageByStorage(editProductDto.Image, (Guid)product.ImageGuid);

        return dto;
    }

    /// <inheritdoc/>
    public Task<IEnumerable<ProductLiteDto>?> GetAll()
    {
        var products = _productRepository.GetAll(true);

        var result = products is null
            ? null
            : _mapper.Map<IEnumerable<Product>, IEnumerable<ProductLiteDto>>(products);

        return Task.FromResult(result);
    }

    /// <inheritdoc/>
    public async Task<ProductLiteDto?> PublishIntoShop(int id)
    {
        var product = await _productRepository.GetAsync(id);
        if (product is null) throw new Exception("Продукт не найден");

        product.IsPublishedInMarket = true;

        bool resultUpdate = await _productRepository.UpdateAsync(product);

        if (!resultUpdate)
        {
            return null;
        }

        await SendPublishProductCommand(id);

        return _mapper.Map<Product, ProductLiteDto>(product);
    }

    /// <inheritdoc/>
    public async Task<ProductLiteDto?> UnPublishIntoShop(int id)
    {
        var product = await _productRepository.GetAsync(id);
        if (product is null) throw new Exception("Продукт не найден");

        product.IsPublishedInMarket = false;

        bool resultUpdate = await _productRepository.UpdateAsync(product);

        if (!resultUpdate)
        {
            return null;
        }

        await SendUnPublishProductCommand(id);

        return _mapper.Map<Product, ProductLiteDto>(product);
    }

    //TODO refactor
    private async Task SendCreateProductCommand(Product product)
    {
        ProductCreateCommand command = _mapper.Map<Product, ProductCreateCommand>(product);

        var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductCreateCommand");
        ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
        await sendEndpoint.Send(command);
    }

    //TODO refactor
    private async Task SendUpdateProductCommand(Product product)
    {
        ProductUpdateCommand command = _mapper.Map<Product, ProductUpdateCommand>(product);

        var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductUpdateCommand");
        ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
        await sendEndpoint.Send(command);
    }

    //TODO refactor
    private async Task SendDeleteProductCommand(int productId)
    {
        var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductDeleteCommand");
        ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
        await sendEndpoint.Send<ProductDeleteCommand>(new() { ProductId = productId });
    }

    //TODO refactor
    private async Task SendPublishProductCommand(int productId)
    {
        var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductPublishCommand");
        ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
        await sendEndpoint.Send<ProductPublishCommand>(new() { ProductId = productId });
    }

    //TODO refactor
    private async Task SendUnPublishProductCommand(int productId)
    {
        var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductUnPublishCommand");
        ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
        await sendEndpoint.Send<ProductUnPublishCommand>(new() { ProductId = productId });
    }
}