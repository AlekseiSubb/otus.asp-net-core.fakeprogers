﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Implementations
{
    public class RecipeStepService : IRecipeStepService
    {
        private readonly IMapper _mapper;
        private readonly IRecipeStepRepository _recipeStepRepository;

        public RecipeStepService(IMapper mapper, IRecipeStepRepository recipeStepRepository)
        {
            _mapper = mapper;
            _recipeStepRepository = recipeStepRepository;
        }

        public async Task<int> CreateAsync(RecipeStepCreateDto recipeStepDto)
        {
            var entity = _mapper.Map<RecipeStepCreateDto, RecipeStep>(recipeStepDto);
            var res = await _recipeStepRepository.AddAsync(entity);
            return res.Id;
        }

        public async Task UpdateAsync(int id, RecipeStepUpdateDto recipeStepDto)
        {
            var entity = _mapper.Map<RecipeStepUpdateDto, RecipeStep>(recipeStepDto);
            entity.Id = id;
            await _recipeStepRepository.UpdateAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            await _recipeStepRepository.DeleteAsync(id);
        }

        public async Task<RecipeStepDto> GetByIdAsync(int id)
        {
            var recipeStep = await _recipeStepRepository.GetAsync(id);
            return _mapper.Map<RecipeStep, RecipeStepDto>(recipeStep);
        }

        public async Task<ICollection<RecipeStepDto>> GetStepsByRecipeId(int recipeId)
        {
            ICollection<RecipeStep> entities = await _recipeStepRepository.GetByRecipeIdAsync(recipeId);
            return _mapper.Map<ICollection<RecipeStep>, ICollection<RecipeStepDto>>(entities);
        }

    }
}
