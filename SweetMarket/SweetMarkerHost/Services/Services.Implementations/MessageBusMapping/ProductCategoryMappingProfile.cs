﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.MessageBus.Commands;
using Domain.Entities.Models;

namespace Services.Implementations.MessageBusMapping
{
    internal class ProductCategoryMappingProfile : Profile
    {
        public ProductCategoryMappingProfile()
        {
            CreateMap<ProductCategory, ProductCategoryCreateCommand>();
            CreateMap<ProductCategory, ProductCategoryUpdateCommand>();
        }
    }
}
