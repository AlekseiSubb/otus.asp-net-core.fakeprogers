﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.MessageBus.Commands;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.MessageBusMapping
{
    internal class MarketMappingProfile : Profile
    {
        public MarketMappingProfile()
        {
            CreateMap<Market, MarketCreateCommand>();
            CreateMap<Market, MarketUpdateCommand>();
        }
    }
}
