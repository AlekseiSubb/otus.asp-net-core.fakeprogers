﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.MessageBus.Commands;
using Domain.Entities.Models;
using Services.Contracts;

namespace Services.Implementations.MessageBusMapping
{
    internal class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<Order, OrderCreateCommand>();
            CreateMap<Order, OrderUpdateCommand>();
        }
    }
}
