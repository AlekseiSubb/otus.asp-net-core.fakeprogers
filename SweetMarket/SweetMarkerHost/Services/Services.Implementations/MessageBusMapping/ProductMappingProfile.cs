﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.Models;
using Contracts.MessageBus.Commands;

namespace Services.Implementations.MessageBusMapping
{
    internal class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<Product, ProductCreateCommand>();
            CreateMap<Product, ProductUpdateCommand>();
        }
    }
}
