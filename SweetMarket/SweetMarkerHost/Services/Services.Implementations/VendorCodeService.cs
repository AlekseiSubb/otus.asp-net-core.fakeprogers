﻿using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Services.Implementations;

/// <summary>
/// Артикул продукта.
/// Уникальный в рамках всей площадки.
/// </summary>
/// <seealso cref="ParseCode"/>
public class VendorCodeService : IVendorCodeService
{
    private readonly char[] characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
    private readonly char[] digits = "0123456789".ToCharArray();

    private readonly IMarketRepository _marketRepository;
    private readonly IProductCategoryRepository _productCategoryRepository;
    private readonly IProductRepository _productRepository;
    private readonly char[] restrictedCharacters = { 'I', 'O', 'J' };
    private readonly RandomNumberGenerator _randomNumberGenerator = RandomNumberGenerator.Create();

    public VendorCodeService(IMarketRepository marketRepository, IProductCategoryRepository productCategoryRepository, IProductRepository productRepository)
    {
        _marketRepository = marketRepository;
        _productCategoryRepository = productCategoryRepository;
        _productRepository = productRepository;
    }

    /// <summary>
    /// Генерирует код магазина
    /// </summary>
    /// <remarks>
    /// Длина кода – 5 символов
    /// Пока используется Id магазина.
    /// </remarks>
    /// <param name="dto"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<string> GenerateMarketCode()
    {
        string newCode;
        do { newCode = GenerateRandomString(5, false, @case: AlphabetCase.Upper); }
        while (!await IsCodeUnique(_marketRepository, newCode));
        return newCode;
    }

    public async Task<string> GenerateProductCategoryCode()
    {
        string newCode;
        do { newCode = GenerateRandomString(7, false, @case: AlphabetCase.Upper); }
        while (!await IsCodeUnique(_productCategoryRepository, newCode));
        return newCode;
    }



    public async Task<string> GenerateProductCode()
    {
        string newCode;
        do { newCode = GenerateRandomString(6, true, @case: AlphabetCase.Upper); }
        while (!await IsCodeUnique(_productRepository, newCode));
        return newCode;
    }

    public async Task<string> GetVendorCode(VendorCodeDto dto)
    {
        var market = await _marketRepository.GetAsync(dto.MarketId);
        var productCategory = await _productCategoryRepository.GetAsync(dto.ProductCategoryId);
        var product = await _productRepository.GetAsync(dto.ProductId);

        //var marketCode = string.Format("00000", market.Id);
        var marketCode = market.Code; //TODO: взять MerketCode
        //var productCategoryCode = string.Format("0000000", market.Id);
        var productCategoryCode = productCategory.Code; //TODO: взять ProductCategoryCode
        //var productCode = string.Format("000000", dto.ProductId);
        var productCode = product.Code; //TODO: взять ProductCode
        return $"{marketCode}-{productCategoryCode}-{productCode}";
    }

    /// <summary>
    /// Парсит артикул
    /// </summary>
    /// <remarks>
    /// Пишется латиницей в верхнем регистре и цифрами 0-9 Запрещены символы: I, O, J, то есть всего 33 варианта на символ.
    /// Структура:
    /// 5 символов – магазин (28480320 вариантов), символы не должны повторяться
    /// 7 символов – категория в магазине (21531121920 вариантов, символы не должны повторяться)
    /// 6 символов – рандомное значение, символы могут повторяться (1291467969 вариантов)
    /// Длина – 18 символов
    /// </remarks>
    /// <returns></returns>
    public async Task<VendorCodeDto> ParseCode(string code)
    {
        var generalRegex = new Regex(@"(?<MarketCode>.{5})-(?<ProductCategoryCode>.{7})-(?<ProductCode>.{6})");
        var match = generalRegex.Match(code);
        if (!match.Success) throw new Exception("Bad code");
        var marketCode = match.Groups["MarketCode"].Value;
        if (!ValidateCode(marketCode, false, @case: AlphabetCase.Upper)) throw new Exception("Invalid marketCode");
        var marketId = await GetIdByCode(_marketRepository, marketCode);

        var productCategoryCode = match.Groups["ProductCategoryCode"].Value;
        if (!ValidateCode(productCategoryCode, false, @case: AlphabetCase.Upper)) throw new Exception("Invalid productCategoryCode");
        var productCategoryId = await GetIdByCode(_productCategoryRepository, productCategoryCode);

        var productCode = match.Groups["ProductCode"].Value;
        if (!ValidateCode(productCode, true, @case: AlphabetCase.Upper)) throw new Exception("Invalid productCode");
        var productId = await GetIdByCode(_productRepository, productCode);

        return new VendorCodeDto
        {
            Code = code,
            MarketId = marketId,
            ProductCategoryId = productCategoryId,
            ProductId = productId,
        };
    }

    private bool ValidateCode(string code, bool allowRepeating = true, AlphabetMode mode = AlphabetMode.Both, AlphabetCase @case = AlphabetCase.Both, bool ignoreRestrictedCharacters = false)
    {
        if (string.IsNullOrWhiteSpace(code)) return false;
        var allowedSymbols = GetAlphabet(mode, @case, !ignoreRestrictedCharacters, mode == AlphabetMode.Both ? true : false);
        var restrictedCharactersRegex = new Regex(@$"^[{allowedSymbols}]+$");
        if (restrictedCharactersRegex.IsMatch(code)) return false;
        var isOnlyUniqueCharacters = code.Distinct().Count() == code.Count(); //Вроде не работает корректно с юникодом. Пока не надо, но имеем в виду.
        if (!allowRepeating && !isOnlyUniqueCharacters) return false;
        return true;
    }

    private string GenerateRandomString(int length, bool allowRepeating = true, AlphabetMode mode = AlphabetMode.Both, AlphabetCase @case = AlphabetCase.Both, bool removeRestrictedCharacters = true, bool restrictedCharactersCaseSensitive = false)
    {
        var alphabet = GetAlphabet(mode, @case, removeRestrictedCharacters, restrictedCharactersCaseSensitive);
        byte[] data = new byte[4 * length];
        using (var crypto = RandomNumberGenerator.Create())
        {
            crypto.GetBytes(data);
        }
        StringBuilder result = new StringBuilder(length);

        for (int i = 0; i < length; i++)
        {
            var rnd = BitConverter.ToUInt32(data, i * 4);
            var idx = rnd % alphabet.Length;
            result.Append(alphabet[idx]);
            if (!allowRepeating) alphabet = new string(alphabet).Remove(Convert.ToInt32(idx), 1).ToCharArray();
        }

        return result.ToString();
    }

    private char[] GetAlphabet(AlphabetMode mode = AlphabetMode.Both, AlphabetCase @case = AlphabetCase.Both, bool removeRestrictedCharacters = true, bool restrictedCharactersCaseSensitive = false)
    {
        char[] result = new char[0];
        char[] charAlphabet = GetCharacrersAlphabet(@case, removeRestrictedCharacters, restrictedCharactersCaseSensitive);
        switch (mode)
        {
            case AlphabetMode.Digits:
                result = digits;
                break;
            case AlphabetMode.Characters:
                result = charAlphabet;
                break;
            case AlphabetMode.Both:
                result = charAlphabet;
                Array.Resize(ref result, charAlphabet.Length + digits.Length);
                digits.CopyTo(result, charAlphabet.Length);
                break;
        }
        return result;
    }

    private char[] GetCharacrersAlphabet(AlphabetCase @case = AlphabetCase.Both, bool removeRestrictedCharacters = true, bool restrictedCharactersCaseSensitive = false)
    {
        char[] result = new char[0];
        char[] upper = characters.ToList().Select(q => char.ToUpper(q)).ToArray();
        char[] lower = characters.ToList().Select(q => char.ToLower(q)).ToArray();
        switch (@case)
        {
            case AlphabetCase.Upper:
                result = upper;
                break;
            case AlphabetCase.Lower:
                result = lower;
                break;
            case AlphabetCase.Both:
                result = upper;
                Array.Resize(ref result, result.Length + lower.Length);
                lower.CopyTo(result, upper.Length);
                break;
        }
        if (removeRestrictedCharacters)
        {
            if (restrictedCharactersCaseSensitive) result = string.Join("", new string(result).Split(restrictedCharacters)).ToCharArray();
            else
            {
                char[] restrictedUpper = restrictedCharacters.ToList().Select(q => char.ToUpper(q)).ToArray();
                char[] restrictedLower = restrictedCharacters.ToList().Select(q => char.ToLower(q)).ToArray();
                var restrictedBoth = restrictedUpper;
                Array.Resize(ref restrictedBoth, restrictedBoth.Length + restrictedLower.Length);
                restrictedLower.CopyTo(restrictedBoth, restrictedUpper.Length);
                result = string.Join("", new string(result).Split(restrictedBoth)).ToCharArray();
            }
        }
        return result;
    }

    private async Task<bool> IsCodeUnique(ICodeEntityRepository repository, string code)
    {
        return await repository.GetIdByCode(code) == null;
    }

    private async Task<int> GetIdByCode(ICodeEntityRepository repository, string code)
    {
        var result = await repository.GetIdByCode(code);
        if (!result.HasValue) throw new ArgumentNullException(nameof(result));
        return result.Value;
    }

    public void Dispose()
    {
        _randomNumberGenerator.Dispose();
    }

    private enum AlphabetCase
    {
        Upper,
        Lower,
        Both
    }

    private enum AlphabetMode
    {
        Characters,
        Digits,
        Both
    }
}
