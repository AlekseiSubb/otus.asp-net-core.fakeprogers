﻿using AutoMapper;
using Contracts.MessageBus.Commands;
using Domain.Entities.Models;
using MassTransit;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Implementations
{
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IMapper _mapper;
        private readonly IVendorCodeService _vendorCodeService;
        private readonly ISendEndpointProvider _sendEndpointProvider;

        public ProductCategoryService(IProductCategoryRepository productCategoryRepository,
            IMapper mapper,
            IVendorCodeService vendorCodeService,
            ISendEndpointProvider sendEndpointProvider)
        {
            _productCategoryRepository = productCategoryRepository;
            _mapper = mapper;
            _vendorCodeService = vendorCodeService;
            _sendEndpointProvider = sendEndpointProvider;
        }

        public async Task<ProductCategoryDto> GetProductCategoryByIdAsync(int id)
        {
            var item = await _productCategoryRepository.GetAsync(id);
            return _mapper.Map<ProductCategory, ProductCategoryDto>(item);
        }

        public async Task<IEnumerable<ProductCategoryDto>> GetProductCategoryAsync()
        {
            var items = await _productCategoryRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<ProductCategory>, IEnumerable<ProductCategoryDto>>(items);
        }

        public async Task<int> CreateProductCategoryAsync(ProductCategoryDto productCategoryDto)
        {
            var model = _mapper.Map<ProductCategory>(productCategoryDto);
            model.Id = 0;
            model.Code = await _vendorCodeService.GenerateProductCategoryCode();
            
            ProductCategory createdItem = await _productCategoryRepository.AddAsync(model);

            //TODO логирование
            if (createdItem is null)
            {
                throw new Exception("Ошибка не удалось создать категорию продуктов");
            }

            await SendCreateProductCategoryCommand(createdItem);

            return createdItem.Id;
        }

        public async Task UpdateProductCategoryAsync(int id, ProductCategoryDto productCategoryDto)
        {
            ProductCategory item = await _productCategoryRepository.GetAsync(id);
            item.Name = productCategoryDto.Name;
            item.Description = productCategoryDto.Description;

            //TODO обработка ошибки
            bool result = await _productCategoryRepository.UpdateAsync(item);

            if (result)
            {
                await SendUpdateProductCategoryCommand(item);
            }
        }

        public async Task DeleteProductCategoryAsync(int id)
        {
            bool result = await _productCategoryRepository.DeleteAsync(id);

            //TODO обработка ошибки
            if (result)
            {
                await SendDeleteProductCategoryCommand(id);
            }
        }

        //TODO refactor
        private async Task SendCreateProductCategoryCommand(ProductCategory category)
        {
            ProductCategoryCreateCommand command = _mapper.Map<ProductCategory, ProductCategoryCreateCommand>(category);

            var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductCategoryCreateCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }

        //TODO refactor
        private async Task SendUpdateProductCategoryCommand(ProductCategory category)
        {
            ProductCategoryUpdateCommand command = _mapper.Map<ProductCategory, ProductCategoryUpdateCommand>(category);

            var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductCategoryUpdateCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send(command);
        }

        //TODO refactor
        private async Task SendDeleteProductCategoryCommand(int categoryId)
        {
            var uri = new Uri("queue:Cmd.Customer.CustomerApi.ProductCategoryDeleteCommand");
            ISendEndpoint sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(uri);
            await sendEndpoint.Send<ProductCategoryDeleteCommand>(new() { ProductId = categoryId });
        }
    }
}




