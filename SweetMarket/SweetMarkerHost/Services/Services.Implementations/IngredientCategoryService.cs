﻿using AutoMapper;
using Domain.Entities.Models;
using Services.Abstractions;
using Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Implementations
{
    public class IngredientCategoryService : IIngredientCategoryService
    {
        private readonly IIngredientCategoryRepository _ingredientCategoryRepository;
        private readonly IMapper _mapper;

        public IngredientCategoryService(IIngredientCategoryRepository ingredientCategoryRepository, IMapper mapper)
        {
            _ingredientCategoryRepository = ingredientCategoryRepository;
            _mapper = mapper;
        }

        public async Task<IngredientCategoryDto> GetIngredientCategoryByIdAsync(int id)
        {
            var item = await _ingredientCategoryRepository.GetAsync(id);
            return _mapper.Map<IngredientCategory, IngredientCategoryDto>(item);
        }

        public async Task<IEnumerable<IngredientCategoryDto>> GetIngredientCategoryAsync()
        {
            var items = await _ingredientCategoryRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<IngredientCategory>, IEnumerable<IngredientCategoryDto>>(items);
        }

        public async Task<int> CreateIngredientCategoryAsync(IngredientCategoryDto ingredientCategoryDto)
        {
            var model = _mapper.Map<IngredientCategory>(ingredientCategoryDto);
            model.Id = 0;
            var addItem = await _ingredientCategoryRepository.AddAsync(model);
            return addItem.Id;
        }

        public async Task UpdateIngredientCategoryAsync(int id, IngredientCategoryDto ingredientCategoryDto)
        {
            IngredientCategory item = await _ingredientCategoryRepository.GetAsync(id);
            item.Name = ingredientCategoryDto.Name;
            item.Description = ingredientCategoryDto.Description;

            await _ingredientCategoryRepository.UpdateAsync(item);
        }

        public async Task DeleteIngredientCategoryAsync(int id)
        {
            await _ingredientCategoryRepository.DeleteAsync(id);
        }
    }
}




