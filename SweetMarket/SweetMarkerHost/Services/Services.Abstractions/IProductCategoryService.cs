﻿using Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Services.Abstractions;
public interface IProductCategoryService
{
    Task<int> CreateProductCategoryAsync(ProductCategoryDto productCategoryDto);
    Task DeleteProductCategoryAsync(int id);
    Task<IEnumerable<ProductCategoryDto>> GetProductCategoryAsync();
    Task<ProductCategoryDto> GetProductCategoryByIdAsync(int id);
    Task UpdateProductCategoryAsync(int id, ProductCategoryDto productCategoryDto);
}
