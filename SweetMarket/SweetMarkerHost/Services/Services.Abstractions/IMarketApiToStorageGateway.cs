﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface IMarketApiToStorageGateway
    {
        Task SendImageToStorage(IFormFile image, Guid id);
        Task<FileResult> GetImageByStorage(Guid id);
        Task UpdateImageByStorage(IFormFile image, Guid id);
        Task DeleteImageToStorage(Guid id);
    }
}