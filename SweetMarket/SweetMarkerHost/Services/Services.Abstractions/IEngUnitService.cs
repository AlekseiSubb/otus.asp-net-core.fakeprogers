using Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions;

/// <summary>
/// Сервис работы с единицами измерений
/// </summary>
public interface IEngUnitService
{
    /// <summary>
    /// Получить
    /// </summary>
    Task<EngUnitDto> GetEndUnitByIdAsync(int id);

    /// <summary>
    /// Получить список
    /// </summary>
    Task<IEnumerable<EngUnitDto>> GetEndUnitsAsync();

    /// <summary>
    /// Создать
    /// </summary>
    Task<int> CreateEngUnitAsync(EngUnitDto engUnitDto);

    /// <summary>
    /// Изменить
    /// </summary>
    Task UpdateEngUnitAsync(int id, EngUnitDto engUnitDto);

    /// <summary>
    /// Удалить
    /// </summary>
    Task DeleteEngUnitAsync(int id);
}