using Services.Contracts;
using System.Threading.Tasks;
namespace Services.Abstractions
{
    /// <summary>
    /// Cервис работы с магазинами (интерфейс)
    /// </summary>
    public interface IMarketService
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов. </returns>
        Task<PagingDto<MarketShortDto>> GetPaged(PaginationFilterDto filterDto);
        
        /// <summary>
        /// Получить постраничный список магазинов пользователя
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов. </returns>
        Task<PagingDto<MarketShortDto>> GetPagedByUser(PaginationFilterDto filterDto, int userId);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО магазина</returns>
        Task<MarketDto> GetById(int id);

        ///// <summary>
        ///// Создать
        ///// </summary>
        ///// <param name="marketDto"></param>
        ///// <param name="creator"></param>
        ///// <returns></returns>
        //Task<int> CreateAsync(MarketDto marketDto, UserDto creator);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="marketDto"></param>
        /// <param name="creatorId"></param>
        /// <returns></returns>
        Task<int> Create(MarketDto marketDto, int creatorId);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="marketDto">ДТО магазина</param>
        Task Update(int id, MarketDto marketDto);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task Delete(int id);
    }
}