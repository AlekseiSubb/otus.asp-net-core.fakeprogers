﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions;

public interface IIngredientCategoryService
{
    Task<int> CreateIngredientCategoryAsync(IngredientCategoryDto ingredientCategoryDto);
    Task DeleteIngredientCategoryAsync(int id);
    Task<IEnumerable<IngredientCategoryDto>> GetIngredientCategoryAsync();
    Task<IngredientCategoryDto> GetIngredientCategoryByIdAsync(int id);
    Task UpdateIngredientCategoryAsync(int id, IngredientCategoryDto ingredientCategoryDto);
}
