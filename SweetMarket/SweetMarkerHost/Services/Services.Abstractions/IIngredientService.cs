﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис работы с ингридиентами
    /// </summary>
    public interface IIngredientService
    {
        Task<IngredientDto> GetByIdAsync(int id);
        Task<ICollection<IngredientDto>> GetAllAsync();
        Task<bool> IsExistAsync(int id);
        Task<int> CreateAsync(IngredientUpdateDto dto);
        Task<bool> UpdateAsync(int id, IngredientUpdateDto dto);
        Task<bool> DeleteAsync(int id);
    }
}
