#nullable enable
using Services.Contracts.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions;

/// <summary>
/// ������ � �������� ������������ ��� ����������/����������
/// </summary>
public interface IProductBusinessLayerForOwnersService
{
    /// <summary>
    /// 1. �������� ������� �� id
    /// </summary>
    Task<ProductLiteDto?> GetById(int id);

    /// <summary>
    /// 2. �������� ����� ��������� (������� ��������) (������ ��������������� � ��������� ������� �� ��������� ���������)
    /// </summary>
    Task<ProductLiteDto?> AddWithRecipe(AddProductDto addProductWithRecipeDto);

    /// <summary>
    /// 3. ������� ���������
    /// </summary>
    Task<ProductLiteDto?> Delete(int id);

    /// <summary>
    /// 4. ������������� ���������
    /// </summary>
    Task<ProductLiteDto?> Update(EditProductDto editProductDto, int id);

    /// <summary>
    /// 5. �������� ������ ���������
    /// </summary>
    Task<IEnumerable<ProductLiteDto>?> GetAll();

    /// <summary>
    /// 6. ������������ �� �������� ��������
    /// </summary>
    Task<ProductLiteDto?> PublishIntoShop(int id);

    /// <summary>
    /// 7. ������ � ����������
    /// </summary>
    Task<ProductLiteDto?> UnPublishIntoShop(int id);
}