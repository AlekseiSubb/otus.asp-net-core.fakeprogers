﻿using Services.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface IRecipeStepService
    {
        Task<int> CreateAsync(RecipeStepCreateDto recipeStepDto);
        Task UpdateAsync(int id, RecipeStepUpdateDto recipeStepDto);
        Task DeleteAsync(int id);
        Task<RecipeStepDto> GetByIdAsync(int id);
        Task<ICollection<RecipeStepDto>> GetStepsByRecipeId(int recipeId);
    }
}
