﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions;

public interface IOrderService
{
    Task<int> CreateOrderAsync(OrderDto orderDto);
    Task DeleteOrderAsync(int id);
    Task<OrderDto> GetOrderByIdAsync(int id);
    Task<IEnumerable<OrderDto>> GetOrdersAsync();
    Task<IEnumerable<OrderDto>> GetPaged(OrderFilterDto filterDto);
    Task UpdateOrderAsync(OrderDto orderDto);
    Task DeleteByIdCustomerOrderAsync(int idCustomerOrder);

}
