﻿using Microsoft.AspNetCore.Http;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using static System.Net.Mime.MediaTypeNames;

namespace Services.Integration
{
    public class MarketApiToStorageGateway : ControllerBase, IMarketApiToStorageGateway
    {
        private readonly HttpClient _httpClient;

        public MarketApiToStorageGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task SendImageToStorage(IFormFile image, Guid id)
        {
            var dto = new StorageFileDto()
            {
                Id = id
            };
            using (var stream = new MemoryStream())
            {
                image.CopyTo(stream);
                dto.Data = stream.ToArray();
            }


            var response = await _httpClient.PostAsJsonAsync("ImageStorage", dto);

            response.EnsureSuccessStatusCode();
        }

        public async Task<FileResult> GetImageByStorage(Guid id)
        {
            var response = await _httpClient.GetAsync($"ImageStorage/{id}");

            return File(response.Content.ReadAsByteArrayAsync().Result, "image/jpeg", id.ToString() + ".jpeg");
        }

        public async Task UpdateImageByStorage(IFormFile image, Guid id)
        {
            var dto = new StorageFileDto()
            {
                Id = id
            };
            using (var stream = new MemoryStream())
            {
                image.CopyTo(stream);
                dto.Data = stream.ToArray();
            }

            var response = await _httpClient.PutAsJsonAsync("ImageStorage", dto);

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteImageToStorage(Guid id)
        {

            var response = await _httpClient.DeleteAsync("ImageStorage/{id}");

            response.EnsureSuccessStatusCode();
        }
    }
    
}