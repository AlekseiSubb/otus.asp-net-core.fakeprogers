﻿namespace Contracts.MessageBus.Commands
{
    public record ProductDeleteCommand
    {
        public int ProductId { get; init; }
    }
}
