﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.MessageBus.Commands
{
    public record MarketCreateCommand
    {
        public int Id { get; init; }

        public string Name { get; init; }

        public string? Description { get; init; }

        //может быть картинку лучше не при создании маркета сразу крепить, а подгружать отдельной кнопкой?
        public Guid? ImageGuid { get; init; }

        public string Code { get; init; }
    }
}
