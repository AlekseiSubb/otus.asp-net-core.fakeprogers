﻿namespace Contracts.MessageBus.Commands
{
    public record ProductCategoryCreateCommand
    {
        public int Id { get; init; }

        public string Name { get; init; }

        public string Description { get; init; }

        public string Code { get; init; }
    }
}
