﻿namespace Contracts.MessageBus.Commands
{
    public record MarketDeleteCommand
    {
        public int MarketId { get; init; }
    }
}
