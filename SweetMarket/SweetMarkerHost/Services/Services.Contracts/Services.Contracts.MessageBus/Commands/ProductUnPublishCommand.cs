﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.MessageBus.Commands
{
    public record ProductUnPublishCommand
    {
        public int ProductId { get; init; }
    }
}
