﻿namespace Contracts.MessageBus.Commands
{
    public record ProductCreateCommand
    {
        public int Id { get; init; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; init; }

        /// <summary>
        /// ссылка на хранилище картинок
        /// </summary>
        public Guid? ImageGuid { get; init; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Price { get; init; }

        /// <summary>
        /// TODO ссылка на Publication
        /// TODO PublicationHistory, кто и когда опубликовал и по какой цене?
        /// TODO вопрос как цена товара завязана на рецепт
        /// </summary>
        public bool IsPublishedInMarket { get; init; }

        public DateTime CreatedAt { get; init; }

        public int CreatedBy { get; init; }

        /// <summary>
        /// TODO кем и когда удален и т.п.
        /// </summary>
        public bool IsDeleted { get; init; }

        /// <summary>
        /// Уникальный код продукта внутри системы
        /// </summary>
        public string Code { get; init; }

        /// <summary>
        /// Ссылка на магазин
        /// </summary>
        public int MarketId { get; init; }

        /// <summary>
        /// Ссылка на тип продукции 
        /// </summary>
        public int? ProductCategoryId { get; init; }
    }
}
