﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.MessageBus.Commands
{
    public record OrderItemCommand
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string? Comment { get; set; }
        public decimal Price { get; set; }
    }
}
