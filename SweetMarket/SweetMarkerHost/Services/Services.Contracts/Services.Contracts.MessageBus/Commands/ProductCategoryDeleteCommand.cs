﻿namespace Contracts.MessageBus.Commands
{
    public record ProductCategoryDeleteCommand
    {
        public int ProductId { get; init; }
    }
}
