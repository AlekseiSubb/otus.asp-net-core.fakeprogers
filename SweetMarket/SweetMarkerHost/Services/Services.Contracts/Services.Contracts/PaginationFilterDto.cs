﻿namespace Services.Contracts
{
    public class PaginationFilterDto
    {
        public int Page { get; set; } = 0;

        public int PageSize { get; set; }
    }
}
