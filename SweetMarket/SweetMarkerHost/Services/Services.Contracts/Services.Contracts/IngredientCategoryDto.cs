﻿
namespace Services.Contracts
{
    public class IngredientCategoryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }

}
