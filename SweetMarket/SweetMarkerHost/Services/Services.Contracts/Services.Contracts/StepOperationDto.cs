﻿
namespace Services.Contracts
{
    public class StepOperationDto
    {
        public int Id { get; set; }

        public double Count { get; set; }

        public int NumPos { get; set; }

        public int IngredientId { get; set; }

        public string IngredientName { get; set; }

        public string EngUnitName { get; set; }
    }
}
