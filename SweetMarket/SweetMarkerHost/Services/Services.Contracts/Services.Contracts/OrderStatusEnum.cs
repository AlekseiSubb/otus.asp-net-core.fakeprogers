﻿using System;
using System.Collections.Generic;

namespace Services.Contracts;
public enum OrderStatusEnum
{
        /// <summary>
        /// Создан в Маркете
        /// </summary>
        Created,
        /// <summary>
        /// Изготавливается
        /// </summary>
        Making,
        /// <summary>
        /// Изготовлено
        /// </summary>
        Ready,
        /// <summary>
        /// отгружен
        /// </summary>
        Shipped
}
