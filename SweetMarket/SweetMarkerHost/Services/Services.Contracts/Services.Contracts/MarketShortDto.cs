﻿using System;

namespace Services.Contracts
{
    /// <summary>
    /// Короткая ДТО магазина
    /// </summary>
    public class MarketShortDto
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; } = default!;

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// ссылка на хранилище картинок
        /// </summary>
        public Guid ImageGuid { get; set; }
    }
}