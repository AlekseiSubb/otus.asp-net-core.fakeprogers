﻿using System;
using System.Collections.Generic;

namespace Services.Contracts;
public class OrderDto
{
    public int Id { get; set; }
    public int CustomerOrderId { get; set; }
    public int MarketId { get; set; }
    public string OrderNumber { get; set; } = default!;
    public string Description { get; set; } = default!;
    public decimal Price { get; set; }
    public string State { get; set; } = default!;
    public DateTime DateCreate { get; set; }
    public DateTime DateReady { get; set; }

    //public Shipping? Shipping { get; set; } 
    public ICollection<OrderItemDto> OrderItems { get; set; } = new List<OrderItemDto>();

}
