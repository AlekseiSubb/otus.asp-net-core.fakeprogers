﻿using Microsoft.AspNetCore.Http;
using System;

namespace Services.Contracts.Product
{
    public class EditProductDto
    {
        public string Name { get; set; } = default!;

        public string Description { get; set; } = default!;

        public Guid? ImageGuid { get; set; }

        public decimal Price { get; set; }

        public int? RecipeId { get; set; }

        public int ProductCategoryId { get; set; }

        public bool IsPublishedInMarket { get; set; }
        public IFormFile Image { get; set; }

    }
}

