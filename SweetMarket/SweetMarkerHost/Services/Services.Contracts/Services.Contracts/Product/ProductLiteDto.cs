﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Services.Contracts.Product
{
    public class ProductLiteDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? ImageGuid { get; set; }

        public decimal Price { get; set; }

        public int? RecipeId { get; set; }

        public int MarketId { get; set; }

        public int ProductCategoryId { get; set; }

        public bool IsPublishedInMarket { get; set; }

        public DateTime CreatedAt { get; set; }

        public int CreatedBy { get; set; }
        public byte Rating { get; set; }
        public uint CommntsCount { get; set; }
    }
}
