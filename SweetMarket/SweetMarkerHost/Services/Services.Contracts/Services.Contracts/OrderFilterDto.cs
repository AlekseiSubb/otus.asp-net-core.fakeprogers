﻿namespace Services.Contracts;
public class OrderFilterDto
{

    public string Name { get; set; }

    public int MarketId { get; set; }

    public decimal? Price { get; set; }

    public string? State { get; set; }

    public bool SortByDateReady { get; set; }

    /// <summary>
    /// заказов на странице
    /// </summary>
    public int ItemsPerPage { get; set; }

    /// <summary>
    /// номер страницы с котороый выводить
    /// </summary>
    public int Page { get; set; }

}

