﻿using System.Collections.Generic;

namespace Services.Contracts
{
    public class RecipeStepDto
    {
        public RecipeStepDto()
        {
            StepOperations = new List<StepOperationDto>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public string Action { get; set; }

        public int NumPos { get; set; }

        public int RecipeId { get; set; }

        public ICollection<StepOperationDto> StepOperations { get; set; }
    }
}
