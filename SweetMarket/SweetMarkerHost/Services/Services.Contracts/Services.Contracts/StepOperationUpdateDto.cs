﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class StepOperationUpdateDto
    {
        public int Id { get; set; }

        public double Count { get; set; }

        public int NumPos { get; set; }

        public int IngredientId { get; set; }
    }
}
