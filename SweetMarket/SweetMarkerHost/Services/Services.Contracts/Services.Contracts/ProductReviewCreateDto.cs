﻿using System;

namespace Services.Contracts
{
    public class ProductReviewCreateDto
    {
        public int Score { get; set; }

        public string? Summary { get; set; }

        public Guid? ImageGuid { get; set; }

        public int CustomerId { get; set; }

        public int ProductId { get; set; }
    }
}
