﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class RecipeStepUpdateDto
    {
        public RecipeStepUpdateDto()
        {
            StepOperations = new List<StepOperationUpdateDto>();
        }

        public string Name { get; set; }

        public string? Description { get; set; }

        public string Action { get; set; }

        public int NumPos { get; set; }

        public int RecipeId { get; set; }

        public ICollection<StepOperationUpdateDto> StepOperations { get; set; }
    }
}
