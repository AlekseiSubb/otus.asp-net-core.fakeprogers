﻿using System;

namespace Services.Contracts
{
    public class ProductDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? ImageGuid { get; set; }

        public decimal Price { get; set; }

        public int? RecipeId { get; set; }

        public int MarketId { get; set; }

        public int ProductCategoryId { get; set; }

        public bool IsPublishedInMarket { get; set; }

        public DateTime CreatedAt { get; set; }

        public int CreatedBy { get; set; }

        /// <summary>
        /// Оценка продкута
        /// </summary>
        /// <remarks>
        /// Что-то вроде средней оценки
        /// </remarks>
        public byte Rating { get; set; }

        /// <summary>
        /// Число коментариев под продуктом
        /// </summary>
        public uint CommntsCount { get; set; }
    }
}
