﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class IngredientUpdateDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int IngredientCategoryId { get; set; }

        public int EngUnitId { get; set; }
    }
}
