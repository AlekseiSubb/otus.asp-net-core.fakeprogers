﻿namespace Services.Contracts
{
    public class ProductCategoryFilterDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string MarketName { get; set; }
        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
