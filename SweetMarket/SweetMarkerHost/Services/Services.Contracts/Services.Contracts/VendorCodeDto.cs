﻿namespace Services.Contracts;

public class VendorCodeDto
{
    public string Code { get; set; }
    public int MarketId { get; set; }
    public int ProductCategoryId { get; set; }
    public int ProductId { get; set; }

    //TODO: Сделать постфикс, указывающий на источник запроса, типа почта/поиск яндекса/гугла/конкретно из сайта
    //public тут enum ...Source { get; set; }
}
