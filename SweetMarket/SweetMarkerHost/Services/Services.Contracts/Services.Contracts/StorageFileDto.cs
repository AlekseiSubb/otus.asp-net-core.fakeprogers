using System;

namespace Services.Contracts;

public class StorageFileDto
{
    public Guid Id { get; set; }

    public byte[]? Data { get; set; }

}