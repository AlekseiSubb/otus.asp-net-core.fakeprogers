﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class RecipeStepCreateDto
    {
        public RecipeStepCreateDto()
        {
            StepOperations = new List<StepOperationCreateDto>();
        }

        public string Name { get; set; }

        public string? Description { get; set; }

        public string Action { get; set; }

        public int NumPos { get; set; }

        public int RecipeId { get; set; }

        public ICollection<StepOperationCreateDto> StepOperations { get; set; }
    }
}
