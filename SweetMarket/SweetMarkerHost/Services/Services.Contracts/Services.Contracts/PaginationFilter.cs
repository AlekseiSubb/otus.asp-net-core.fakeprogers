﻿namespace Services.Contracts
{
    public class PaginationFilter
    {
        public int Offset { get; set; } = 0;

        public int PageSize { get; set; } = int.MaxValue;
    }
}
