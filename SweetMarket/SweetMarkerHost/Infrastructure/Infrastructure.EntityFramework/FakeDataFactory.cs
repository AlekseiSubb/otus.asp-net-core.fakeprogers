﻿using Domain.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.EntityFramework
{
    public static class FakeDataFactory
    {
        //звгрузка по пользователям
        public static IEnumerable<Market> Markets => new List<Market>()
        {
            new()
            {
                Id = 1,
                Name = "Белочка",
                Description = "г.Пермь, Комсомольский проспект, 50",
                OwnerId = 2,
                Code = "ABC01"
            },
            new()
            {
                Id = 2,
                Name = "Эрнест",
                Description = "г.Пермь, Сибирская, 30",
                OwnerId = 6,
                Code = "ABC02"
            },
            new()
            {
                Id = 3,
                Name = "Кофесити",
                Description = "г.Пермь, Луночарского, 10",
                OwnerId = 3,
                Code = "ABC03"
            }
        };
        public static IEnumerable<User> Users => new List<User>()
        {
            new()
            {
                Id = 1,
                Login = "admin",
                Password = "123",
                FullName = "admin"
            },
            new()
            {
                Id = 2,
                Login = "ivanovii",
                Password = "123",
                MarketId = 1,
                FullName = "Иванов Иван Иванович"

            },
            new()
            {
                Id = 3,
                Login = "novokovgk",
                Password = "123",
                MarketId = 3,
                FullName = "Новиков Геннадий Константинович"
            },
            new()
            {
                Id = 4,
                Login = "grigorievmd",
                Password = "123",
                FullName = "Григорьев Максим Даниилович"
            },
             new()
            {
                Id = 5,
                Login = "alexeevaa",
                Password = "123",
                FullName = "Алексеев Алексей Алексеевич"
            },
            new()
            {
                Id = 6,
                Login = "zhukov",
                Password = "123",
                MarketId = 2,
                FullName = "Жуков Даниил Денисович"

            }
        };

        //загрузка по продуктам
        public static IEnumerable<ProductCategory> ProductCategores => new List<ProductCategory>()
        {
            new()
            {
                Id = 1,
                Name = "Торты",
                Description = "Общая категория для всех тортов",
                Code = "ABC0123"
            },
            new()
            {
                Id = 2,
                Name = "Пирожное",
                Description = "Общая категория для пирожного",
                Code = "ABC0124"
            },
            new()
            {
                Id = 3,
                Name = "Печенье",
                Description = "Общая категория для пирожного",
                Code = "ABC0125"
            },
            new()
            {
                Id = 4,
                Name = "Конфеты ручной работы",
                Description = "Общая категория для пирожного",
                Code = "ABC0126"
            },
            new()
            {
                Id = 5,
                Name = "Шоколад формовой",
                Description = "Общая категория для пирожного",
                Code = "ABC0127"
            },
            new()
            {
                Id = 6,
                Name = "Мороженое",
                Description = "Общая категория для пирожного",
                Code = "ABC0128"
            }
        };
        public static IEnumerable<Addon> Addons => new List<Addon>()
        {
            new()
            {
                Id = 1,
                Name = "Подарочная упаковка",
                Description = "Упаковка продуката",
                Price = 100
            },
            new()
            {
                Id = 2,
                Name = "Надпись на торте заказчика",
                Description = "Для тортов",
                Price = 50
            },
            new()
            {
                Id = 3,
                Name = "Комплект свечек на торт",
                Description = "Свечки для торта 10 шт в пчке",
                Price = 150
            }
        };
        public static IEnumerable<Product> Products => new List<Product>()
        {
            new()
            {
                Id = 1,
                MarketId = 1,
                Name = "Медовик",
                Description = "Бисквитные медовые коржи с начинкой из вареной сгущенки",
                Price = 800,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA001",
                RecipeId = 1,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("76d95dcc-c48a-48be-8e4b-b5f4da0e491c"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 2,
                MarketId = 1,
                Name = "Торт Пикник",
                Description = "Шоколадный",
                Price = 800,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA002",
                RecipeId = 2,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("e436b78d-57a1-4a3f-aa67-9937ee7ffffd"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 3,
                MarketId = 1,
                Name = "Наполеон",
                Description = "Слоёный торт по классическому рецепту с заварным ванильно-сливочным кремом",
                Price = 1045,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA003",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("9de82376-a6cf-4ff3-9d21-db44fe41d7d4"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 4,
                MarketId = 1,
                Name = "Бурбон шоколадный",
                Description = "Шоколадный бисквит без муки, прослойка воздушного мусса из белого и темного шоколада, покрыт шоколадным велюром, оформлен цельным фундуком и карамелью",
                Price = 1560,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA004",
                RecipeId = null,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("91b6ba58-b765-431b-bf36-603b3c316a8f"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 5,
                MarketId = 1,
                Name = "Ганя",
                Description = "Блаженство шоколадного мусса с сочными ягодами малины",
                Price = 1460,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA005",
                RecipeId = null,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("4091ed71-570d-41d6-b470-f22d8731e7f4"),
                IsPublishedInMarket = false
            },
            new()
            {
                Id = 6,
                MarketId = 1,
                Name = "Жизнь в шоколаде",
                Description = "Шоколадный бисквит пропитан вишневым ликером с прослойкой из шоколадного мусса и свежей вишни",
                Price = 1085,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA006",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("4312b111-00ea-4b8c-8616-8382c1a6cfa0"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 7,
                MarketId = 1,
                Name = "Захер",
                Description = "Шоколадный бисквит пропитан коньячным сиропом с прослойкой из шоколадного заварного крема и фруктового джема",
                Price = 875,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA007",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("2c4bd858-b60a-4f93-b6ea-6c3e90ee437f"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 8,
                MarketId = 2,
                Name = "Клубничка",
                Description = "Молочный бисквит, пропитанный сиропом с прослойкой из нежных взбитых сливок, клубника в желе",
                Price = 510,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA008",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("5f264c3f-5a61-4c0a-baa9-94a08dcc631e"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 9,
                MarketId = 2,
                Name = "Матис с малиной",
                Description = "Шоколадный бисквит с прослойкой низкокалорийного йогуртового мусса с малиной и профитролями с ванильным муссом",
                Price = 810,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA009",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("8bb01c0c-7a4b-4b0e-857f-7282bcb9d55d"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 10,
                MarketId = 2,
                Name = "Матисс с фруктами",
                Description = "Шоколадный бисквит с прослойкой низкокалорийного йогуртового мусса с фруктами и профитролями с творожным муссом",
                Price = 990,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA010",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("026701f9-e6a4-4149-859c-eb47d334a470"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 11,
                MarketId = 2,
                Name = "Сметанный",
                Description = "(Домашний рецепт) медовые коржи с прослойкой из сметанного крема",
                Price = 1075,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA011",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("23235a0b-2fda-4e58-87c3-d15ac0364df1"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 12,
                MarketId = 2,
                Name = "Тирамису",
                Description = "Молочный бисквит, пропитанный кофейным сиропом с коньяком с прослойкой из нежных натуральных сливок с сыром маскарпоне",
                Price = 650,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA012",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("3d36c2ab-a4e6-42b9-bba9-f1fbfe1becaf"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 13,
                MarketId = 2,
                Name = "Тифани",
                Description = "Шоколадный бисквит с коньячным сиропом, прослойка из взбитых сливок с шоколадом",
                Price = 45,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA013",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("4ca7269b-782e-41d4-9279-f9305f8c72f7"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 14,
                MarketId = 3,
                Name = "Финансье",
                Description = "Нежный шоколадный бисквит без муки, сливочный крем с вареным сгущенным молоком, грецкими орехами и кусочками безе, покрыт шоколадным велюром",
                Price = 1300,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA014",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("91b6ba58-b765-431b-bf36-603b3c316a8f"),
                IsPublishedInMarket = true
            },
            new()
            {
                Id = 15,
                MarketId = 3,
                Name = "Чаровница",
                Description = "Нежное ванильно-сливочное суфле с клюквенным парфе",
                Price = 580,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA015",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("c3deb4f1-bfc0-4563-bd42-5c7cf4f10f5e"),
                IsPublishedInMarket = true
            },
            new ()
            {
                Id = 16,
                MarketId = 3,
                Name = "Чизкейк с ягодным соусом",
                Description = "Из сыра Маскарпоне с ягодным соусом",
                Price = 680,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id,
                Code = "AAA016",
                RecipeId = 3,
                CreatedAt = DateTime.Now,
                ImageGuid = Guid.Parse("18a2590a-ff18-47a5-93b7-a480661f3d3c"),
                IsPublishedInMarket = true
            }
        };

        //загрузка по рецептам
        public static IEnumerable<EngUnit> EngUnits => new List<EngUnit>()
        {
            new()
            {
                Id = 1,
                Name = "литры",
                ShortName = "л"
            },
            new()
            {
                Id = 2,
                Name = "миллилитры",
                ShortName = "мл"
            },
            new()
            {
                Id = 3,
                Name = "килограмм",
                ShortName = "кг"
            },
            new()
            {
                Id = 4,
                Name = "грамм",
                ShortName = "г"
            },
            new()
            {
                Id = 5,
                Name = "чайная ложка",
                ShortName = "ч.л"
            },
            new()
            {
                Id = 6,
                Name = "столовая ложка",
                ShortName = "ст.л"
            },
            new()
            {
                Id = 7,
                Name = "щепотка",
                ShortName = "щ"
            },
            new()
            {
                Id = 8,
                Name = "штук",
                ShortName = "шт"
            }
        };
        public static IEnumerable<IngredientCategory> IngredientCategories => new List<IngredientCategory>()
        {
            new()
            {
                Id = 1,
                Name = "специи",
                Description = "все традиционные специи"
            },
            new()
            {
                Id = 2,
                Name = "мука",
                Description = "различные виды муки"
            },
            new()
            {
                Id = 3,
                Name = "яйца",
                Description = "различные виды яиц"
            },
            new()
            {
                Id = 4,
                Name = "Молочные продукты",
                Description = "молочка"
            },
            new()
            {
                Id = 5,
                Name = "Мёд",
                Description = "разные виды меда"
            },
            new()
            {
                Id = 6,
                Name = "фрукты",
                Description = "все фрукты"
            },
            new()
            {
                Id = 7,
                Name = "ягоды",
                Description = "различные ягоды"
            },
            new()
            {
                Id = 8,
                Name = "варенье",
                Description = "различные виды варенья"
            },
             new()
            {
                Id = 9,
                Name = "печенье",
                Description = ""
            },
            new()
            {
                Id = 10,
                Name = "прочие",
                Description = "то что не вошло в другие категории"
            }
        };

        public static IEnumerable<Recipe> Recipes => new List<Recipe>()
        {
            new()
            {
                Id = 1,
                Name = "Фирменный рецепт: Медовый торт",
                Description = "Особый рецепт медового торта.",
            },
            new()
            {
                Id = 2,
                Name = "Торт «Пикник»",
                Description = "Торт «Пикник» по мотивам шоколадного батончика. Рецепт рассчитан на торт диаметром 18 см.",
            },
            new()
            {
                Id = 3,
                Name = "Апельсиновый чизкейк",
                Description = "Вкусный и очень красивый апельсиновый чизкейк. Рецепт рассчитан на диаметр формы 20 см.",
            },
            // new()
            //{
            //    Id = 4,
            //    Name = "Муссовое пирожное «Саммер»",
            //    Description = "Создать себе летнее настроение можно в любое время года, для этого достаточно взять из морозилки немного малины, купить в супермаркете спелый манго и приготовить пирожное «Саммер».",
            //    ProductId = 2
            //}
        };

        public static readonly IEnumerable<RecipeStep> RecipeSteps = new List<RecipeStep>()
        {
            new()
            {
                Id = 1,
                Name = "Приготовление теста",
                Description = "",
                RecipeId = 1,
                NumPos = 1
            },
            new()
            {
                Id = 2,
                Name = "Приготовление начинки",
                Description = "",
                RecipeId = 1,
                NumPos = 2
            },
            new()
            {
                Id = 3,
                Name = "Выпечка коржей",
                Description = "",
                RecipeId = 1,
                NumPos = 3
            },
            new()
            {
                Id = 4,
                Name = "Заправка начинкой",
                Description = "",
                RecipeId = 1,
                NumPos = 4
            },
            new()
            {
                Id = 5,
                Name = "Оформление",
                Description = "",
                RecipeId = 1,
                NumPos = 5
            }
        };

        public static readonly IEnumerable<StepOperation> StepOperations = new List<StepOperation>()
        {
            new()
            {
                Id = 1,
                RecipeStepId = 1,
                Count = 1,
                NumPos = 1,
                IngredientId = 1,
            },
            new()
            {
                Id = 2,
                RecipeStepId = 1,
                Count = 1,
                NumPos = 2,
                IngredientId = 2,
            },
            new()
            {
                Id = 3,
                RecipeStepId = 1,
                Count = 1,
                NumPos = 3,
                IngredientId = 2,
            }

        };

        public static readonly IEnumerable<Ingredient> Ingredients = new List<Ingredient>()
        {
            new()
            {
                Id = 1,
                Name = "пшеничная мука",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 2,
            },
            new()
            {
                Id = 2,
                Name = "сахар",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 1,
            },
            new()
            {
                Id = 3,
                Name = "яйцо",
                Description = "",
                EngUnitId = 8,
                IngredientCategoryId = 3,
            },
            new()
            {
                Id = 4,
                Name = "масло сливочное",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 4,
            },
            new()
            {
                Id = 5,
                Name = "мёд",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 5,
            },
            new()
            {
                Id = 6,
                Name = "сахар",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 1,
            },
            new()
            {
                Id = 7,
                Name = "сода",
                Description = "",
                EngUnitId = 5,
                IngredientCategoryId = 1,
            },
            new()
            {
                Id = 8,
                Name = "молоко",
                Description = "",
                EngUnitId = 2,
                IngredientCategoryId = 4,
            },
            new()
            {
                Id = 9,
                Name = "ванильный сахар",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 1,
            },
            new()
            {
                Id = 10,
                Name = "печенье",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 9,
            },
            new()
            {
                Id = 11,
                Name = "сливки",
                Description = "",
                EngUnitId = 2,
                IngredientCategoryId = 4,
            },
            new()
            {
                Id = 12,
                Name = "лимон",
                Description = "",
                EngUnitId = 8,
                IngredientCategoryId = 6,
            },
            new()
            {
                Id = 13,
                Name = "ванилин",
                Description = "",
                EngUnitId = 6,
                IngredientCategoryId = 1
            },
            new()
            {
                Id = 14,
                Name = "Сахарная пудра",
                Description = "",
                EngUnitId = 6,
                IngredientCategoryId = 1,
            },
            new()
            {
                Id = 15,
                Name = "клубника",
                Description = "",
                EngUnitId = 6,
                IngredientCategoryId = 7,
            }
        };
    }
}
