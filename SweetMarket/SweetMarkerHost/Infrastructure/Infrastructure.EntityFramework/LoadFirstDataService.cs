﻿namespace Infrastructure.EntityFramework
{
    public interface ILoadFirstDataService
    {
        void InitializeDb();
    }
    public class LoadFirstDataService : ILoadFirstDataService
    {
        private readonly DatabaseContext _dataContext;
        public LoadFirstDataService(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {

            if (_dataContext == null)
                return;

            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            //загрузка начальных данные т.к. у нас ключи генерируются БД то мы не можем грузить данные с ID - это делает БД,
            // по этому нужно создавать обект БД и потом его заполнять или использовать начальную загрузку в DBContext через modelBuilder.Entity<T>().HasData()
            //FakeDataFactory.ProductCategores.ToList().ForEach(categores =>
            //{
            //    _dataContext.ProductCategorys.Add(new ProductCategory { Name = categores.Name, Description = categores.Description });
            //});
            //FakeDataFactory.customerAdreses.ToList().ForEach(adreses =>
            //{
            //    _dataContext.CustomerAdresses.Add(new CustomerAdress { Adress = adreses.Adress });
            //});
            //FakeDataFactory.markets.ToList().ForEach(markets =>
            //{
            //    _dataContext.Markets.Add(new Market { Name = markets.Name, Description = markets.Description });
            //});
            //_dataContext.SaveChanges();
            //FakeDataFactory.users.ToList().ForEach(users =>
            //{
            //    _dataContext.Users.Add(new User { 
            //                        Login = users.Login, 
            //                        Password = users.Password, 
            //                        MarketId = _dataContext.Markets.FirstOrDefault(m => m.Id == 1).Id
            //                        });
            //});
            //_dataContext.SaveChanges();

          
            //_dataContext.PaymentDetails.Add(NewPaymentDetail); можно отдельно не грузить т.к. добавится при добавление Orders
            //_dataContext.SaveChanges();
        }
    }
}
