﻿using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    internal class IngredientConfiguration : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.ToTable(nameof(Ingredient));

            builder.HasKey(t => t.Id);

            builder.HasOne(i => i.EngUnit)
                .WithMany(i => i.Ingredients)
                .HasForeignKey(i => i.EngUnitId)
                .OnDelete(DeleteBehavior.NoAction); //свойство-внешний ключ в зависимой сущности получает значение null

            builder.HasOne(i => i.IngredientCategory)
                .WithMany(c => c.Ingredients)
                .HasForeignKey(i => i.IngredientCategoryId)
                .OnDelete(DeleteBehavior.NoAction);

        }
    }
}
