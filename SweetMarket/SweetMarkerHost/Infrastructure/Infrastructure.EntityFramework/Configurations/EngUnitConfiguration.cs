﻿using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    internal class EngUnitConfiguration : IEntityTypeConfiguration<EngUnit>
    {
        public void Configure(EntityTypeBuilder<EngUnit> builder)
        {
            builder.ToTable(nameof(EngUnit));

            builder.HasKey(t => t.Id);
            //связь описана в IngredientConfiguration
            builder.Property(c => c.Name)
            .HasMaxLength(20)
            .IsRequired();

            builder.Property(c => c.ShortName)
            .HasMaxLength(10);
        }
    }
}
