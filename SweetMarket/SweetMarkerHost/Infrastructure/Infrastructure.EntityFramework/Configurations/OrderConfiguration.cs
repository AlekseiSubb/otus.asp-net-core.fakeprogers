﻿using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable(nameof(Order));

            builder.HasKey(t => t.Id); 

            builder.HasMany(q => q.OrderItems)
                .WithOne()
                .HasForeignKey(q => q.OrderId);
                //.OnDelete(DeleteBehavior.Cascade);

        }
    }
}
