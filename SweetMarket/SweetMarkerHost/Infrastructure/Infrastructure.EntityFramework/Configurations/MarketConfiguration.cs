﻿using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    internal class MarketConfiguration : IEntityTypeConfiguration<Market>
    {
        public void Configure(EntityTypeBuilder<Market> builder)
        {
            builder.ToTable(nameof(Market));

            builder.HasKey(t => t.Id); //Возможно потом будем использовать strongly typed id

            builder.Property(t => t.Name)
                .HasMaxLength(64);

            builder.Property(t => t.Description)
                .HasMaxLength(512);

            builder.HasIndex(q => q.Code)
                .IsUnique();
            builder.Property(q => q.Code)
                .HasMaxLength(5);

            //builder.HasOne(q => q.Owner)
            //    .WithOne(u => u.Market)
            //    .HasForeignKey<Market>(q => q.OwnerId);

            builder.HasMany(q => q.Products)
                .WithOne()
                .HasForeignKey(q => q.MarketId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(q => q.Сonfectioners)
                .WithOne(q => q.Market)
                .HasForeignKey(q => q.MarketId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
