﻿using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    internal class IngredientCategoryConfiguration : IEntityTypeConfiguration<IngredientCategory>
    {
        public void Configure(EntityTypeBuilder<IngredientCategory> builder)
        {
            builder.ToTable(nameof(IngredientCategory));

            builder.HasKey(t => t.Id);
            //связь описана в IngredientConfiguration
            builder.Property(c => c.Name)
            .HasMaxLength(50)
            .IsRequired();

            builder.Property(c => c.Description)
            .HasMaxLength(50);

        }
    }
}
