﻿using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityFramework.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));

            builder.HasKey(t => t.Id);

            builder.HasOne(u => u.Market)
                .WithMany(m => m.Сonfectioners)
                .HasForeignKey(u => u.MarketId);

        }
    }
}
