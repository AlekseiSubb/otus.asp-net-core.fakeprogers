﻿using Domain.Entities.Models;
using Infrastructure.EntityFramework.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public sealed class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
        /// <summary>
        /// Таблички по Заказчику
        /// </summary>
        public DbSet<User> Users { get; set; }
        public DbSet<Market> Markets { get; set; }


        /// <summary>
        /// Таблички по продукту
        /// </summary>
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategorys { get; set; }
        public DbSet<Addon> Addons { get; set; }


        /// <summary>
        /// Таблички по рецептам
        /// </summary>
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeStep> RecipeSteps { get; set; }
        public DbSet<StepOperation> StepOperations { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<IngredientCategory> IngredientCategorys { get; set; }
        public DbSet<EngUnit> EngUnits { get; set; }

        /// <summary>
        /// таблички по заказу
        /// </summary>
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Shipping> Shippings { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new IngredientConfiguration());
            modelBuilder.ApplyConfiguration(new IngredientCategoryConfiguration());
            modelBuilder.ApplyConfiguration(new EngUnitConfiguration());
            modelBuilder.ApplyConfiguration(new MarketConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new ProductCategoryConfiguration());
            initDB(modelBuilder);
        }

        private void initDB(ModelBuilder modelBuilder)
        {
            //начальные данные по пользователям
            modelBuilder.Entity<Market>().HasData(FakeDataFactory.Markets);
            modelBuilder.Entity<User>().HasData(FakeDataFactory.Users);

            //начальные данные по продукту
            modelBuilder.Entity<ProductCategory>().HasData(FakeDataFactory.ProductCategores);
            modelBuilder.Entity<Addon>().HasData(FakeDataFactory.Addons);
            modelBuilder.Entity<Product>().HasData(FakeDataFactory.Products);

            //начальные данные по рецептам
            modelBuilder.Entity<EngUnit>().HasData(FakeDataFactory.EngUnits);
            modelBuilder.Entity<IngredientCategory>().HasData(FakeDataFactory.IngredientCategories);
            modelBuilder.Entity<Recipe>().HasData(FakeDataFactory.Recipes);
            modelBuilder.Entity<RecipeStep>().HasData(FakeDataFactory.RecipeSteps);
            modelBuilder.Entity<StepOperation>().HasData(FakeDataFactory.StepOperations);
            modelBuilder.Entity<Ingredient>().HasData(FakeDataFactory.Ingredients);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(message => System.Diagnostics.Debug.WriteLine(message), LogLevel.Information);
        }
    }
}