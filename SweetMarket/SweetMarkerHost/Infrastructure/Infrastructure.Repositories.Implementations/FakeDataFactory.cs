﻿using Domain.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.EntityFramework
{
    public static class FakeDataFactory
    {
        //звгрузка по пользователям
        public static IEnumerable<Market> Markets => new List<Market>()
        {
            new()
            {
                Id = 1,
                Name = "Белочка",
                Description = "г.Пермь, Комсомольский проспект, 50",
                OwnerId = 2
            },
            new()
            {
                Id = 2,
                Name = "Эрнест",
                Description = "г.Пермь, Сибирская, 30",
                OwnerId = 6
            },
            new()
            {
                Id = 3,
                Name = "Алиэкспресс",
                Description = "китайский семейный магазинчик",
                OwnerId = 3
            }
        };
        public static IEnumerable<User> Users => new List<User>()
        {
            new()
            {
                Id = 1,
                Login = "testUserAdmin",
                Password = "admin"

            },
            new()
            {
                Id = 2,
                Login = "testUserMaker1_people1",
                Password = "123",
                MarketId = 1

            },
            new()
            {
                Id = 3,
                Login = "testUserMaker1_people2",
                Password = "123",
                MarketId = 3

            },
            new()
            {
                Id = 4,
                Login = "testUserCustomer1",
                Password = "123"
            },
             new()
            {
                Id = 5,
                Login = "testUserCustomer2",
                Password = "123"
            },
            new()
            {
                Id = 6,
                Login = "testUserMaker1_people2",
                Password = "123",
                MarketId = 2

            }
        };

        //загрузка по продуктам
        public static IEnumerable<ProductCategory> ProductCategores => new List<ProductCategory>()
        {
            new()
            {
                Id = 1,
                Name = "Торты",
                Description = "Общая категория для всех тортов"
            },
            new()
            {
                Id = 2,
                Name = "Пирожное",
                Description = "Общая категория для пирожного"
            }
        };
        public static IEnumerable<Addon> Addons => new List<Addon>()
        {
            new()
            {
                Id = 1,
                Name = "Подарочная упаковка",
                Description = "Упаковка продуката",
                Price = 100
            },
            new()
            {
                Id = 2,
                Name = "Надпись на торте заказчика",
                Description = "Для тортов",
                Price = 50
            },
            new()
            {
                Id = 3,
                Name = "Комплект свечек на торт",
                Description = "Свечки для торта 10 шт в пчке",
                Price = 150
            }
        };
        public static IEnumerable<Product> Products => new List<Product>()
        {
            new()
            {
                Id = 1,
                MarketId = 1,
                Name = "Баннановый торт",
                Description = "Бисквитные коржи с сметанно банановым кремом",
                Price = 500,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id
            },
            new()
            {
                Id = 2,
                MarketId = 1,
                Name = "Медовый",
                Description = "Бисквитные медовые коржи с начинкой из вареной сгущенки",
                Price = 600,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Торты").Id
            },
            new()
            {
                Id = 3,
                MarketId = 1,
                Name = "Пирожное корзинка",
                Description = "Песочная корзинка с вареной сгущенкой черносливом и грецким орехом",
                Price = 45,
                ProductCategoryId = ProductCategores.FirstOrDefault(pc => pc.Name == "Пирожное").Id
            }
        };

        //загрузка по рецептам
        public static IEnumerable<EngUnit> EngUnits => new List<EngUnit>()
        {
            new()
            {
                Id = 1,
                Name = "литры",
                ShortName = "л"
            },
            new()
            {
                Id = 2,
                Name = "милллитры",
                ShortName = "мл"
            },
            new()
            {
                Id = 3,
                Name = "килограмм",
                ShortName = "кг"
            },
            new()
            {
                Id = 4,
                Name = "грамм",
                ShortName = "г"
            },
            new()
            {
                Id = 5,
                Name = "чайная ложка",
                ShortName = "ч.л"
            },
            new()
            {
                Id = 6,
                Name = "столовая ложка",
                ShortName = "ст.л"
            },
            new()
            {
                Id = 7,
                Name = "щипотка",
                ShortName = "щ"
            }
        };
        public static IEnumerable<IngredientCategory> IngredientCategories => new List<IngredientCategory>()
        {
            new()
            {
                Id = 1,
                Name = "соль",
                Description = "все традицилнные специи"
            },
            new()
            {
                Id = 2,
                Name = "мука",
                Description = "различные виды муки"
            },
            new()
            {
                Id = 3,
                Name = "яица",
                Description = "различные виды яиц"
            },
            new()
            {
                Id = 4,
                Name = "молочные продукты",
                Description = "молочка"
            }
        };

        public static IEnumerable<Recipe> Recipes => new List<Recipe>()
        {
            new()
            {
                Id = 1,
                Name = "Фирменный рецепт: Медовый торт",
                Description = "первый тестовый рецепт",
            }
        };

        public static readonly IEnumerable<RecipeStep> RecipeSteps = new List<RecipeStep>()
        {
            new()
            {
                Id = 1,
                Name = "Приготовление теста",
                Action = "AA",
                Description = "",
                RecipeId = 1,
                NumPos = 1
            },
            new()
            {
                Id = 2,
                Name = "Приготовление начинки",
                Action = "AA",
                Description = "",
                RecipeId = 1,
                NumPos = 2
            },
            new()
            {
                Id = 3,
                Name = "Выпечка коржей",
                Action = "AA",
                Description = "",
                RecipeId = 1,
                NumPos = 3
            },
            new()
            {
                Id = 4,
                Name = "Заправка начинкой",
                Action = "AA",
                Description = "",
                RecipeId = 1,
                NumPos = 4
            },
            new()
            {
                Id = 5,
                Name = "Оформление",
                Action = "AA",
                Description = "",
                RecipeId = 1,
                NumPos = 5
            }
        };

        public static readonly IEnumerable<StepOperation> StepOperations = new List<StepOperation>()
        {
            new()
            {
                Id = 1,
                RecipeStepId = 1,
                Count = 1,
                NumPos = 1
            },
            new()
            {
                Id = 2,
                RecipeStepId = 1,
                Count = 1,
                NumPos = 2
            },
            new()
            {
                Id = 3,
                RecipeStepId = 1,
                Count = 1,
                NumPos = 3
            }

        };

        public static readonly IEnumerable<Ingredient> Ingredients = new List<Ingredient>()
        {
            new()
            {
                Id = 1,
                Name = "пшеничная мука",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 2,
            },
            new()
            {
                Id = 2,
                Name = "сахар",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 2,
            },
            new()
            {
                Id = 3,
                Name = "яйцо",
                Description = "",
                EngUnitId = 4,
                IngredientCategoryId = 2,
            }
        };
    }
}
