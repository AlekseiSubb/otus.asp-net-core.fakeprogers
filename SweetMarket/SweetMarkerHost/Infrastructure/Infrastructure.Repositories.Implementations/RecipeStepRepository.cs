﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class RecipeStepRepository : Repository<RecipeStep, int>, IRecipeStepRepository
    {
        public RecipeStepRepository(DatabaseContext context) : base(context)
        {
        }

        public async override Task<RecipeStep> GetAsync(int id)
        {
            return await Context.RecipeSteps.Where(x => x.Id == id)
                .Include(x => x.StepOperations)
                .ThenInclude(x => x.Ingredient.EngUnit)
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }

        public async Task<ICollection<RecipeStep>> GetByRecipeIdAsync(int recipeId)
        {
            var query = Context.Set<RecipeStep>().AsQueryable();
            query = query
                .Where(c => c.RecipeId == recipeId)
                .Include(c => c.StepOperations)
                .ThenInclude(so => so.Ingredient.EngUnit)
                .AsNoTracking();

            return await query.ToListAsync();
        }

        //TODO не помню как работает в EF Core удаление связанных сущностей, посмотреть и переделать
        public override async Task<bool> DeleteAsync(int id)
        {
            var query = Context.Set<RecipeStep>().AsQueryable()
                .Include(rs => rs.StepOperations)
                .Where(c => c.Id == id);

            RecipeStep recipeStep = await query.FirstOrDefaultAsync();

            foreach (var stepOperation in recipeStep.StepOperations)
            {
                recipeStep.StepOperations.Remove(stepOperation);
            }

            await Context.SaveChangesAsync();

            return await base.DeleteAsync(id);
        }

        public override async Task<bool> UpdateAsync(RecipeStep entity)
        {
            Context.Update(entity);

            var stepOperationsIds = entity.StepOperations.Select(x => x.Id).ToList();
            var stepOperationsToRemove = await Context.StepOperations
                .Where(x => x.RecipeStepId == entity.Id && !stepOperationsIds.Contains(x.Id))
                .ToListAsync();

            if (stepOperationsToRemove.Any())
                Context.StepOperations.RemoveRange(stepOperationsToRemove);

            await Context.SaveChangesAsync();
            return true;
        }
    }
}
