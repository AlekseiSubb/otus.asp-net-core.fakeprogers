﻿using Domain.Entities;
using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с магазинами
    /// </summary>
    public class MarketRepository : Repository<Market, int>, IMarketRepository
    {
        public MarketRepository(DatabaseContext context) : base(context) { }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов </returns>
        public async Task<PaginatedEntity<Market, int>> GetPagedAsync(PaginationFilterDto filterDto)
        {
            //var query = GetAll().ToList().AsQueryable(); //не работает
            var query = Context.Set<Market>().AsQueryable();
            query = query
                .Skip((filterDto.Page - 1) * filterDto.PageSize)
                .Take(filterDto.PageSize);
            var items = await query.ToListAsync();
            var itemsCount = await GetAll(asNoTracking: true).CountAsync();
            var totalPages = Convert.ToInt32(Math.Ceiling((double)itemsCount / (double)filterDto.PageSize));
            return new PaginatedEntity<Market, int>
            {
                Items = items,
                CurrentPage = filterDto.Page,
                ItemsPerPage = filterDto.PageSize,
                TotalPages = totalPages == 0 ? 1 : totalPages,
            };
        }

        /// <summary>
        /// Получить постраничный список пользователя.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список магазинов </returns>
        public async Task<PaginatedEntity<Market, int>> GetPagedByUserAsync(PaginationFilterDto filterDto, int userId)
        {
            var query = Context.Set<Market>().AsQueryable();
            query = query
                .Where(q => q.OwnerId == userId)
                .Skip((filterDto.Page - 1) * filterDto.PageSize)
                .Take(filterDto.PageSize);
            var items = await query.ToListAsync();
            var itemsCount = await GetAll(asNoTracking: true).CountAsync();
            var totalPages = Convert.ToInt32(Math.Ceiling((double)itemsCount / (double)filterDto.PageSize));
            return new PaginatedEntity<Market, int>
            {
                Items = items,
                CurrentPage = filterDto.Page,
                ItemsPerPage = filterDto.PageSize,
                TotalPages = totalPages == 0 ? 1 : totalPages,
            };
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<Market> GetAsync(int id)
        {
            var query = Context.Set<Market>().AsQueryable();
            query = query
                .Include(c => c.Products)
                .Include(q => q.Сonfectioners)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
        public async Task<int?> GetIdByCode(string code) => (await Context.Set<Market>().FirstOrDefaultAsync(q => q.Code == code))?.Id;
    }
}
