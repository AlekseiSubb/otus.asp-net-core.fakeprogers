﻿using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class IngredientCategoryRepository : Repository<IngredientCategory, int>, IIngredientCategoryRepository
    {
        public IngredientCategoryRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
