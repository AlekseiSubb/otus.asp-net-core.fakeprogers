﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class IngredientRepository : Repository<Ingredient, int>, IIngredientRepository
    {
        public IngredientRepository(DatabaseContext context) : base(context)
        {
        }

        public new async Task<ICollection<Ingredient>> GetAllAsync()
        {
            return await Context.Ingredients
                .Include(x => x.EngUnit)
                .Include(x => x.IngredientCategory)
                .AsNoTracking()
                .ToListAsync();
        }

        public override Task<Ingredient> GetAsync(int id)
        {
            var query = Context.Set<Ingredient>().AsQueryable();
            query = query
                .Include(x => x.EngUnit)
                .Include(x => x.IngredientCategory)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }

        public async Task<bool> IsExistAsync(int id)
        {
            return await Context.Ingredients.AnyAsync(x => x.Id == id);
        }
    }
}
