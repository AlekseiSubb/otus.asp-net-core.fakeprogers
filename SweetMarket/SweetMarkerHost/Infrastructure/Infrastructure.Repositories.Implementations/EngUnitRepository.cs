﻿using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class EngUnitRepository : Repository<EngUnit, int>, IEngUnitRepository
    {
        public EngUnitRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
