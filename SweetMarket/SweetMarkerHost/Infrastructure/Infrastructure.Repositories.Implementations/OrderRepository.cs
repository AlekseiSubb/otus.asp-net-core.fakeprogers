﻿using Domain.Entities;
using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System.Linq;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Infrastructure.Repositories.Implementations
{
    public class OrderRepository : Repository<Order, int>, IOrderRepository
    {
        public OrderRepository(DatabaseContext context) : base(context)
        {
        }
        public async Task<List<Order>> GetPagedAsync(OrderFilterDto filterDto)
        {
            var query = Context.Set<Order>().AsQueryable();

            query = query.Where(c =>  c.MarketId == filterDto.MarketId);

            if (!string.IsNullOrWhiteSpace(filterDto.Name))
            {
                query = query.Where(c => filterDto.Name.Contains(c.OrderNumber));
            }

            if (filterDto.Price.HasValue && filterDto.Price > 0)
            {
                query = query.Where(c => c.Price == filterDto.Price);
            }

            if (!string.IsNullOrWhiteSpace(filterDto.State))
            {
                query = query.Where(c => c.State == filterDto.State);
            }

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            query = query
                    .Include(o => o.OrderItems);

            if (filterDto.SortByDateReady)
                query.OrderByDescending(s => s.DateCreate);
            else
                query.OrderByDescending(s => s.Id);

            return await query.ToListAsync();
        }
        public override Task<Order> GetAsync(int id)
        {
            var query = Context.Set<Order>().AsQueryable();
            query = query
                .Include(o => o.OrderItems)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }

        public Task<Order> GetByCustomerIdAsync(int idCustomerOrder)
        {
            var query = Context.Set<Order>().AsQueryable();
            query = query
                .Include(o => o.OrderItems)
                .Where(c => c.CustomerOrderId == idCustomerOrder);

            return query.SingleOrDefaultAsync();
        }

        public override async Task<List<Order>> GetAllAsync()
        {
            var query = Context.Set<Order>().AsQueryable();
            query = query
                .Include(o => o.OrderItems);
            return await query.ToListAsync();
        }

        public async Task<bool> DeleteByIdCustomerOrderAsync(int idCustomerOrder)
        {
            var query = Context.Set<Order>().AsQueryable();
            query = query
                .Where(c => c.CustomerOrderId == idCustomerOrder);
            var orders = await query.ToListAsync();

            bool res = true;
            foreach(var o in orders)
            {
                res |= await DeleteAsync(o.Id);
            }
            return res;
        }
    }
}
