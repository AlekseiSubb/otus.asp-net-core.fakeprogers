﻿using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
    public class RecipeRepository : Repository<Recipe, int>, IRecipeRepository
    {
        public RecipeRepository(DatabaseContext context) : base(context)
        {

        }

        public async Task<bool> IsExistAsync(int id)
        {
            return await Context.Recipes.AnyAsync(r => r.Id == id);
        }
    }
}
