﻿using Domain.Entities;
using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
    public class ProductRepository : Repository<Product, int>, IProductRepository
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<int?> GetIdByCode(string code) => (await Context.Set<Product>().FirstOrDefaultAsync(q => q.Code == code))?.Id;
    }
}
