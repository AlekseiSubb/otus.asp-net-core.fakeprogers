﻿using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class StepOperationRepository : Repository<StepOperation, int>, IStepOperationRepository
    {
        public StepOperationRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
