﻿using Domain.Entities.Models;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
    public class ProductCategoryRepository : Repository<ProductCategory, int>, IProductCategoryRepository
    {
        public ProductCategoryRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<int?> GetIdByCode(string code) => (await Context.Set<ProductCategory>().FirstOrDefaultAsync(q => q.Code == code))?.Id;
    }
}
