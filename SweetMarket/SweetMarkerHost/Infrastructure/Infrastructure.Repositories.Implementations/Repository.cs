﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий чтения и записи
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    /// <typeparam name="TPrimaryKey">Основной ключ</typeparam>
    public class Repository<T, TPrimaryKey> : IRepository<T, TPrimaryKey>
        where T : class, IEntity<TPrimaryKey>
    {
        protected readonly DatabaseContext Context;
        private readonly DbSet<T> _entitySet;

        public Repository(DatabaseContext context)
        {
            Context = context;
            _entitySet = Context.Set<T>();
        }

        #region Get

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public virtual async Task<T> GetAsync(TPrimaryKey id)
        {
            return await _entitySet.FindAsync((object)id);
        }

        #endregion

        #region GetAll

        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
        /// <returns>IQueryable массив сущностей</returns>
        public virtual IQueryable<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? _entitySet.AsNoTracking() : _entitySet;
        }

        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
        /// <returns>Список сущностей</returns>
        public async Task<List<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false)
        {
            return await GetAll().ToListAsync(cancellationToken);
        }

        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        /// <returns>Список сущностей</returns>
        public virtual async Task<List<T>> GetAllAsync()
        {
            return await GetAll().ToListAsync();
        }

        #endregion

        #region Create

        /// <summary>
        /// Добавить в базу одну сущность
        /// </summary>
        /// <param name="entity">сущность для добавления</param>
        /// <returns>добавленная сущность</returns>
        public virtual async Task<T> AddAsync(T entity)
        {
            var objToReturn = _entitySet.Add(entity);
            await Context.SaveChangesAsync();
            return objToReturn.Entity as T;
        }

        /// <summary>
        /// Добавить в базу массив сущностей
        /// </summary>
        /// <param name="entities">массив сущностей</param>
        public virtual async Task<bool> AddRangeAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return false;
            }
            await _entitySet.AddRangeAsync(entities);
            await Context.SaveChangesAsync();
            return true;
        }

        #endregion

        #region Update

        /// <summary>
        /// Для сущности проставить состояние - что она изменена
        /// </summary>
        /// <param name="entity">сущность для изменения</param>
        public virtual async Task<bool> UpdateAsync(T entity)
        {
            var itemEf = _entitySet.Update(entity);
            if (itemEf is { })
            {
                await Context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="id">ID удалённой сущности</param>
        /// <returns>была ли сущность удалена</returns>
        public virtual async Task<bool> DeleteAsync(TPrimaryKey id)
        {
            var obj = _entitySet.Find(id);
            if (obj == null)
            {
                return false;
            }
            _entitySet.Remove(obj);

            await Context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity">сущность для удаления</param>
        /// <returns>была ли сущность удалена</returns>
        public virtual async Task<bool> DeleteAsync(T entity)
        {
            if (entity == null)
            {
                return false;
            }
            _entitySet.Remove(entity);
            await Context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Удалить сущности
        /// </summary>
        /// <param name="entities">Коллекция сущностей для удаления</param>
        /// <returns>была ли операция завершена успешно</returns>
        public virtual async Task<bool> DeleteRangeAsync(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return false;
            }
            _entitySet.RemoveRange(entities);
            await Context.SaveChangesAsync();
            return true;
        }

        #endregion
    }
}
