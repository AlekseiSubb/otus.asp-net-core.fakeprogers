namespace ImageStorageService
{
    public class StorageFileResponse
    {
        public string Key { get; set; }

        public byte[]? Data { get; set; }

    }
}