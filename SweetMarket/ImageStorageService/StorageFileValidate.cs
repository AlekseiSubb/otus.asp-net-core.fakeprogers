using FluentValidation;

namespace ImageStorageService
{
    public class StorageFileValidate : AbstractValidator<StorageFile>
    {
        public StorageFileValidate()
        {
            {
                RuleFor(e => e.Id).NotNull().NotEmpty()
                    .WithMessage("���� {Id} �� ������ ���� ������!")
                    .Must(x => (x is Guid) && Guid.Empty != (Guid)x)
                    .WithMessage("���� {Id} ������ ���� �� ������ Guid!");

                RuleFor(e => e.Data).NotNull().NotEmpty()
                                    .WithMessage("���� {Data} �� ������ ���� ������!")
                                    .Must(x => x is Byte[]).WithMessage("���� {Data} ������ ���� ������ ����!");

            }
        }
    }
}