namespace ImageStorageService
{
    public class StorageFileView
    {

        public required string Key { get; set; }

        public string? Size { get; set; }

    }
}