using Amazon.S3;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using System.Drawing;
using Microsoft.Extensions.Configuration;
using FluentValidation;
using System.Xml.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;

namespace ImageStorageService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImageStorageController : ControllerBase
    {
        private readonly ILogger<ImageStorageController> _logger;
        private IAmazonS3 _s3Client;
        private IConfiguration _configuration;
        private string _defoltBaket = "fakeprogerscloudimages";
        private bool _encryption = true;
        private IValidator<StorageFile> _storageFileValidate;


        public ImageStorageController(ILogger<ImageStorageController> logger, IAmazonS3 s3Client, IConfiguration Configuration, IValidator<StorageFile> storageFileValidate)
        {
            _logger = logger;
            _s3Client = s3Client;
            _configuration = Configuration;
            _storageFileValidate = storageFileValidate;

            _defoltBaket = _configuration.GetValue<string>("AWS:DefoltBaket") ?? string.Empty;
            _encryption = _configuration.GetValue<bool>("AWS:Encryption");
        }

        [HttpGet("{id:Guid}")]
        public async Task<FileResult> Get(Guid id)
        {
            GetObjectRequest getObjectRequest = new GetObjectRequest
            {
                BucketName = _defoltBaket,
                Key = id.ToString() + ".jpg"
            };
            using var objResp = await _s3Client.GetObjectAsync(getObjectRequest);
            using var ms = new MemoryStream();
            await objResp.ResponseStream.CopyToAsync(ms);

            //return Ok(new StorageFileResponse
            //{
            //    Key = id.ToString(),
            //    Data = ms.ToArray()
            //});
            FileResult res = File(ms.ToArray(), "image/jpeg", id.ToString() + ".jpg");
            return res;
        }

        [HttpGet("{key}")]
        public async Task<FileResult> Get(string key)
        {
            GetObjectRequest getObjectRequest = new GetObjectRequest
            {
                BucketName = _defoltBaket,
                Key = key.ToString()
            };
            using var objResp = await _s3Client.GetObjectAsync(getObjectRequest);
            using var ms = new MemoryStream();
            await objResp.ResponseStream.CopyToAsync(ms);

            //return Ok(new StorageFileResponse
            //{
            //    Key = key,
            //    Data = ms.ToArray()
            //});
            FileResult res = File(ms.ToArray(), "image/jpeg", key);
            return res;
        }

        /// <summary>
        /// �������� ��� ����� ��� �����
        /// </summary>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            var identity = User.Identity;

            var listObjectsV2Paginator = _s3Client.Paginators.ListObjectsV2(new ListObjectsV2Request
            {
                BucketName = _defoltBaket,
            });

            List<StorageFileView> res = new List<StorageFileView>();
            await foreach (var response in listObjectsV2Paginator.Responses)
            {
                foreach (var entry in response.S3Objects)
                {
                    res.Add(new StorageFileView { Key = entry.Key, Size = $"Size =  {entry.Size}" });
                }
            }
            return Ok(res);
        }

        /// <summary>
        /// �������� ���� ��� ������������
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Add(StorageFile addedFile)
        {
            //string fullName = Path.Combine("C:\\test", "label3.emf");
            //FileStream fs = new FileStream(fullName,
            //                               FileMode.Open,
            //                               FileAccess.Read);
            //BinaryReader br = new BinaryReader(fs);
            //long numBytes = new FileInfo(fullName).Length;
            //addedFile.Data = br.ReadBytes((int)numBytes);

            if (addedFile == null)
                BadRequest();

            var validationResult = await _storageFileValidate.ValidateAsync(addedFile);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);

            var putRequest = new PutObjectRequest
            {
                BucketName = _defoltBaket,
                Key = addedFile.Id.ToString() + ".jpg",
            };

            using (var ms = new MemoryStream(addedFile.Data))
            {
                putRequest.InputStream = ms;
                var putResponse = await _s3Client.PutObjectAsync(putRequest);
            }

            return Ok();
        }

        /// <summary>
        /// ������� ����
        /// </summary>
        [HttpDelete("{id:Guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            DeleteObjectRequest delRequest = new DeleteObjectRequest
            {
                BucketName = _defoltBaket,
                Key = id.ToString() + ".jpg"
            };

            DeleteObjectResponse response = await _s3Client.DeleteObjectAsync(delRequest);
            return Ok();
        }

        /// <summary>
        /// ������� ����
        /// </summary>
        [HttpDelete("{key}")]
        public async Task<IActionResult> Delete(string key)
        {
            DeleteObjectRequest delRequest = new DeleteObjectRequest
            {
                BucketName = _defoltBaket,
                Key = key
            };

            DeleteObjectResponse response = await _s3Client.DeleteObjectAsync(delRequest);
            return Ok();
        }


        /// <summary>
        /// ��������
        /// </summary>
        [HttpPut()]
        public async Task<IActionResult> Update(StorageFile addedFile)
        {
            if (addedFile == null)
                BadRequest();

            var validationResult = await _storageFileValidate.ValidateAsync(addedFile);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors.FirstOrDefault()?.ErrorMessage);

            //�������
            DeleteObjectRequest delRequest = new DeleteObjectRequest
            {
                BucketName = _defoltBaket,
                Key = addedFile.Id.ToString() + ".jpg"
            };
            await _s3Client.DeleteObjectAsync(delRequest);

            //��������
            var putRequest = new PutObjectRequest
            {
                BucketName = _defoltBaket,
                Key = addedFile.Id.ToString() + ".jpg"
            };

            using (var ms = new MemoryStream(addedFile.Data))
            {
                putRequest.InputStream = ms;
                var putResponse = await _s3Client.PutObjectAsync(putRequest);
            }

            return Ok();
        }
    }
}