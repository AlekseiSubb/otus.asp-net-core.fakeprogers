namespace ImageStorageService
{
    public class StorageFile
    {
        public Guid Id { get; set; }

        public byte[]? Data { get; set; }

    }
}