using System.IdentityModel.Tokens.Jwt;
using Amazon.Extensions.NETCore.Setup;
using Amazon.Runtime;
using Amazon.S3;
using FluentValidation;
using ImageStorageService;
using ImageStorageService.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

var builder = WebApplication.CreateBuilder(args);

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
        options.Audience = "sweetmarket_imagestorage_api";
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new()
        {
            RoleClaimType = "role",
            ValidTypes = new[] { "at+jwt" }
        };
    });

builder.Services.AddScoped<IValidator<StorageFile>, StorageFileValidate>();
builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<SwaggerGenOptions>(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/authorize"),
                TokenUrl = new Uri($"{builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"]}/connect/token"),
                Scopes = new Dictionary<string, string>
                {
                    {"openid", ""},
                    {"email", ""},
                    {"profile", ""},
                    {"roles", ""},
                    {"sweetmarket_imagestorage_api.fullaccess", ""}
                },
            },
        },
    });
    options.OperationFilter<AuthorizeCheckOperationFilter>();
});

//����� � �������
//string accessKey = "YCAJES7TKO27jigMxhucY4o-q";
//string secretKey = "YCMCmEYqKo9ewml64WK_2_c1OSA1Lb8yScQVzuiq";
AWSOptions awsOptions = builder.Configuration.GetAWSOptions();
awsOptions.Credentials = new BasicAWSCredentials(builder.Configuration["aws_access_key_id"], builder.Configuration["aws_secret_access_key"]);
builder.Services.AddDefaultAWSOptions(awsOptions);
builder.Services.AddAWSService<IAmazonS3>();

var app = builder.Build();

//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.OAuthClientId("sweetmarket_web_frontend_client");
        c.OAuthClientSecret("LwZjqN6XCPCtnt0ZKYUiD1jGbRbe2aepqtGfxsuk9D");
        c.EnablePersistAuthorization();
    });
//}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
