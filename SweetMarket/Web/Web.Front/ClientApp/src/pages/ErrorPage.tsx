import Error, { type ErrorProps } from "@/components/Error";
import { Center } from "@mantine/core";

export default function ErrorPage(props: ErrorProps) {
  return (
    <Center mx="auto" h="100vh">
      <Error {...props} />
    </Center>
  );
}
