import { Center, Stack, Text, Title } from "@mantine/core";
import { Link } from "react-router-dom";

export default function NotFoundPage() {
  return (
    <Center mx="auto" h="100vh">
      <Stack align="center">
        <Title size="h2" color="red.7">
          404
        </Title>
        <Text size={"xl"}>Страница не найдена</Text>
      </Stack>
    </Center>
  );
}
