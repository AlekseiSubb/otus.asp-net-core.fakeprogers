import {
  Container,
  Stack,
  Group,
  Button,
  TextInput,
  Textarea,
  NumberInput,
  Flex,
  Select,
  Title,
  Image,
  Checkbox,
  FileButton,
} from "@mantine/core";
import { notifications } from "@mantine/notifications";
import { useForm } from "@mantine/form";

import { useEffect, useMemo, useContext, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import useAxios from "axios-hooks";

import SaveButton from "@/components/SaveButton";
import PageLoader from "@/components/PageLoader";
import ErrorPage from "@/pages/ErrorPage";
import { MarketContext } from "@/context/marketContext";
import { RecipeData } from "@/types/recipe";
import { MarketProduct, ProductCategory } from "@/types/product";
import PageBox from "@/components/PageBox";
import AxiosErrorBox from "@/components/AxiosError";

interface FormValues {
  name: string;
  description: string;
  productCategoryId?: string;
  price?: number;
  recipeId?: string;
  isPublishedInMarket: boolean;
}

const productUrl = "/marketapi/v1/Product";
const recipesUrl = "/marketapi/v1/Recipe/List";
const categoriesUrl = "/marketapi/v1/ProductCategory/list";

export default function ProductForm() {
  const { id } = useParams();
  const market = useContext(MarketContext);
  const navigate = useNavigate();

  const isNewProduct = id === "new";
  const [image, setImage] = useState<File | null>(null);

  const [{ data: product, loading, error }] = useAxios<MarketProduct>(
    { url: `${productUrl}/${id}` },
    { manual: isNewProduct },
  );

  const [{ data: recipes }] = useAxios<RecipeData[]>(recipesUrl);
  const [{ data: categories }] = useAxios<ProductCategory[]>(categoriesUrl);

  const [{ loading: updateLoading }, addOrEditProduct] = useAxios(
    {
      url: isNewProduct ? productUrl : `${productUrl}/${id}`,
      method: isNewProduct ? "POST" : "PUT",
    },
    { manual: true },
  );

  const iconUrl = image
    ? URL.createObjectURL(image)
    : product?.imageGuid
    ? `/imageapi/v1/ImageStorage/${product.imageGuid}`
    : "";

  const recipesSelect = useMemo(
    () => recipes?.map(rec => ({ value: rec.id.toString(), label: rec.name })),
    [recipes],
  );

  const categoriesSelect = useMemo(
    () =>
      categories?.map(c => ({
        value: c.id.toString(),
        label: c.name,
      })),
    [categories],
  );

  const form = useForm<FormValues>({
    validate: {
      name: value => (value?.trim()?.length > 0 ? null : "заполните название"),
      description: value =>
        value?.trim()?.length > 0 ? null : "заполните описание",
      productCategoryId: value =>
        value && Number(value) > 0 ? null : "укажите категорию продукта",
      price: value =>
        value && value > 0 ? null : "укажите корректную цену продукта",
    },
  });

  useEffect(() => {
    form.setValues({
      name: product?.name ?? "",
      description: product?.description ?? "",
      productCategoryId: product?.productCategoryId?.toString(),
      price: product?.price,
      recipeId: product?.recipeId?.toString(),
      isPublishedInMarket: product?.isPublishedInMarket ?? false,
    });
  }, [product, recipes]);

  async function handleSubmit(values: FormValues) {
    const formData = new FormData();
    Object.entries(values).forEach(item => {
      formData.append(item[0], item[1]);
    });

    formData.append("MarketId", market.id.toString());

    if (image) {
      formData.append("Image", image);
    }

    try {
      await addOrEditProduct({
        data: formData,
      });

      notifications.show({
        message: "Данные успешно сохранены",
      });
      navigate("/market/products");
    } catch (error) {
      notifications.show({
        message: "Ошибка при попытке сохранить данные продукта",
        color: "red",
      });
    }
  }

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  return (
    <PageBox
      title={isNewProduct ? "Создание продукта" : "Редактирование продукта"}
      loading={loading || updateLoading}
      size="sm"
      p="md"
      shadow
    >
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Stack>
          <Flex direction="row" gap="lg">
            <Stack sx={{ flex: "1 1 auto" }}>
              <TextInput
                label="Название"
                withAsterisk
                {...form.getInputProps("name")}
              />

              <Select
                label="Категория"
                withAsterisk
                data={categoriesSelect ?? []}
                {...form.getInputProps("productCategoryId")}
              />

              <Select
                label="Рецепт"
                data={recipesSelect ?? []}
                {...form.getInputProps("recipeId")}
              ></Select>
            </Stack>

            <Group position="center">
              <Stack spacing="sm">
                <Image
                  width={200}
                  height={200}
                  mt={10}
                  withPlaceholder
                  style={{ border: "1px solid lightgray" }}
                  src={iconUrl}
                  imageProps={{
                    onLoad: () => {
                      URL.revokeObjectURL(iconUrl);
                    },
                  }}
                />
                <FileButton onChange={setImage} accept="image/jpeg">
                  {props => (
                    <Button variant="outline" compact {...props}>
                      выбрать файл...
                    </Button>
                  )}
                </FileButton>
              </Stack>
            </Group>
          </Flex>

          <Textarea
            label="Описание"
            withAsterisk
            placeholder="описание продукта"
            autosize
            minRows={5}
            maxRows={10}
            {...form.getInputProps("description")}
          />

          <Group spacing="lg">
            <NumberInput
              label="Цена (в руб.)"
              withAsterisk
              placeholder="введите цену в рублях"
              min={0}
              step={1}
              {...form.getInputProps("price")}
            />

            <Checkbox
              label="Опубликовать продукт"
              mt="xl"
              {...form.getInputProps("isPublishedInMarket", {
                type: "checkbox",
              })}
            />
          </Group>

          <SaveButton disabled={updateLoading} />
        </Stack>
      </form>
    </PageBox>
  );
}
