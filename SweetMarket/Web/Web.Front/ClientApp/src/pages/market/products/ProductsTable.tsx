import { MarketProduct, ProductCategory } from "@/types/product";
import {
  Group,
  Table,
  Text,
  Image,
  ActionIcon,
  Box,
  Switch,
  Center,
} from "@mantine/core";
import { useCallback, useMemo } from "react";
import { BiEdit, BiTrash } from "react-icons/bi";
import { BsBook } from "react-icons/bs";
import { Link } from "react-router-dom";

const imageSorageUrl = "/imageapi/v1/ImageStorage";

interface ProductsTableProps {
  products: MarketProduct[];
  productCategories: ProductCategory[];
  onPublishStatusChange: (productId: number, status: boolean) => void;
}

export default function ProductsTable({
  products,
  productCategories,
  onPublishStatusChange,
}: ProductsTableProps) {
  const columns = useMemo(
    () => (
      <tr>
        <th></th>
        <th>Название</th>
        <th>Цена</th>
        <th>Категория</th>
        <th>Описание</th>
        <th>Дата создания</th>
        <th>Рецепт</th>
        <th>Опубликовано</th>
        <th></th>
      </tr>
    ),
    [],
  );

  const getCategoryName = useCallback(
    (categoryId: number) => {
      const category = productCategories.find(c => c.id === categoryId);
      return category?.name ?? "";
    },
    [productCategories],
  );

  const rows = useMemo(
    () =>
      products?.map(p => (
        <tr key={p.id}>
          <td>
            <Box sx={{ width: 60, height: 60 }} m={5}>
              <Image
                width={60}
                height={60}
                src={p.imageGuid ? `${imageSorageUrl}/${p.imageGuid}` : ""}
                withPlaceholder
              />
            </Box>
          </td>
          <td>
            <Text lineClamp={2}>{p.name}</Text>
          </td>
          <td width="120">
            <Text lineClamp={1}>{p.price} руб.</Text>
          </td>
          <td>{getCategoryName(p.productCategoryId)}</td>
          <td>
            <Text lineClamp={2}>{p.description}</Text>
          </td>
          <td>
            <Text miw={150}>{new Date(p.createdAt).toLocaleString()}</Text>
          </td>
          <td>
            <ActionIcon
              disabled={!p.recipeId}
              component={Link}
              to={`/market/recipes/${p.recipeId}`}
            >
              <BsBook size="1.5rem" />
            </ActionIcon>
          </td>
          <td>
            <Switch
              checked={p.isPublishedInMarket}
              onChange={event =>
                onPublishStatusChange(p.id, event.currentTarget.checked)
              }
            />
          </td>
          <td>
            <Group noWrap>
              <ActionIcon component={Link} to={`${p.id}`}>
                <BiEdit size="1.5rem" />
              </ActionIcon>
              <ActionIcon color="red.5" onClick={() => {}}>
                <BiTrash size="1.5rem" />
              </ActionIcon>
            </Group>
          </td>
        </tr>
      )),
    [products, productCategories, onPublishStatusChange],
  );

  if (!rows || rows.length === 0) {
    return (
      <Center mih={100} w="100%" mx="auto">
        <Text>продукты не найдены</Text>
      </Center>
    );
  }

  return (
    <Table highlightOnHover withBorder bg="white">
      <thead>{columns}</thead>
      <tbody>{rows}</tbody>
    </Table>
  );
}
