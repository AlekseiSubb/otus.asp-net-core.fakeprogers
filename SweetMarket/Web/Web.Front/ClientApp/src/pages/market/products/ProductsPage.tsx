import { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import useAxios from "axios-hooks";

import {
  Button,
  Container,
  Group,
  SegmentedControl,
  Select,
  Stack,
  Title,
} from "@mantine/core";
import { notifications } from "@mantine/notifications";
import { BiPlus } from "react-icons/bi";

import ProductsTable from "./ProductsTable";
import PageLoader from "@/components/PageLoader";
import ErrorPage from "../../ErrorPage";
import { MarketProduct, ProductCategory } from "@/types/product";
import Error from "@/components/Error";
import PageBox from "@/components/PageBox";
import AxiosErrorBox from "@/components/AxiosError";

const apiUrl = "/marketapi/v1/Product";

export default function ProductsPage() {
  const [{ data, loading, error }, getProducts] = useAxios<MarketProduct[]>(
    `${apiUrl}/list`,
  );

  const [{ data: categories }] = useAxios<ProductCategory[]>(
    "/marketapi/v1/ProductCategory/list",
  );

  const [_, sendPublication] = useAxios(
    {
      method: "PUT",
    },
    { manual: true },
  );

  const [products, setProducts] = useState<MarketProduct[]>([]);
  const [publishStatus, setPublishStatus] = useState<string | undefined>();
  const [categoryId, setCategoryId] = useState<number | null>(null);

  async function changePubliсationStatus(productId: number, status: boolean) {
    const changedProductIdx = products.findIndex(p => p.id === productId);
    products[changedProductIdx] = {
      ...products[changedProductIdx],
      isPublishedInMarket: status,
    };
    setProducts([...products]);

    const response = await sendPublication({
      url: status ? `${apiUrl}/publish` : `${apiUrl}/unpublish`,
      params: {
        id: productId,
      },
    });

    if (response.status === 200) {
      notifications.show({
        message: "Статус публикации продукта изменен",
      });
    } else {
      notifications.show({
        message: "Не удалось изменить статус публикации продукта",
        color: "red",
      });

      getProducts();
    }
  }

  useEffect(() => {
    setProducts(data ?? []);
  }, [data]);

  const filteredProducts = useMemo(() => {
    if (!publishStatus && !categoryId) {
      return products;
    }

    return products.filter(
      p =>
        (publishStatus === "all" ||
          p.isPublishedInMarket === Boolean(publishStatus)) &&
        (!categoryId || p.productCategoryId === categoryId),
    );
  }, [products, publishStatus, categoryId]);

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  return (
    <PageBox loading={loading} size="lg">
      <Stack>
        <Group
          position="apart"
          bg={"white"}
          p={5}
          sx={{
            boxShadow: "0px 0px 5px 0px rgba(97, 119, 137, 0.2);",
          }}
        >
          <Group spacing="xl">
            <Title size={"h3"}>Список продуктов</Title>
            <SegmentedControl
              size="sm"
              color="blue.5"
              value={publishStatus}
              onChange={setPublishStatus}
              data={[
                { label: "Все", value: "all" },
                { label: "Опубликованные", value: "1" },
                { label: "Неопубликованные", value: "" },
              ]}
            />
            <Select
              placeholder="категория"
              clearable
              value={categoryId?.toString()}
              onChange={value => setCategoryId(Number(value))}
              data={
                categories?.map(c => ({
                  label: c.name,
                  value: c.id.toString(),
                })) ?? []
              }
            />
          </Group>

          <Button
            size="xs"
            leftIcon={<BiPlus size="1.2rem" />}
            variant="filled"
            component={Link}
            to="new"
          >
            Создать
          </Button>
        </Group>

        <ProductsTable
          products={filteredProducts ?? []}
          productCategories={categories ?? []}
          onPublishStatusChange={changePubliсationStatus}
        />
      </Stack>
    </PageBox>
  );
}
