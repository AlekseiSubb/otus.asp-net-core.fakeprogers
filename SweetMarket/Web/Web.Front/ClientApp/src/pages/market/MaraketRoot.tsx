import { Outlet } from "react-router-dom";

import MarketLayout from "@/layouts/market/MarketLayout";
import MarketHeader from "@/layouts/market/MarketHeader";
import MarketNavbar from "@/layouts/market/MarketNavbar";

import { MarketProvider } from "@/context/marketContext";

export default function MaraketRoot() {
  return (
    <MarketProvider>
      <MarketLayout header={<MarketHeader />} navbar={<MarketNavbar />}>
        <Outlet />
      </MarketLayout>
    </MarketProvider>
  );
}
