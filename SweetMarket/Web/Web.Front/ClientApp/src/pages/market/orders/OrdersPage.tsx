import PageBox from "@/components/PageBox";
import { Stack, Group, Button, Center, Container, Text } from "@mantine/core";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import useAuthUser from "@/hooks/useAuthUser";
import Error from "@/components/Error";
import ErrorPage from "@/pages/ErrorPage";
import OrderItem from "./OrderItem";
import useAxios from "axios-hooks";
import { MarketContext } from "@/context/marketContext";
import AxiosErrorBox from "@/components/AxiosError";

interface OrderItem {
  productId: number;
  quantity: number;
  price: number;
}

interface OrderData {
  orderNumber: number;
  price: number;
  state: string;
  orderItems: OrderItem[];
}

export default function OrderPage() {
  const { isLoggedIn } = useAuthUser();
  const market = useContext(MarketContext);

  const [{ data, loading, error }] = useAxios<OrderData[]>({
    url: "/marketapi/v1/Order/list",
    method: "POST",
    data: {
      name: "",
      marketId: market.id,
      price: 0,
      state: "created",
      SortByDateCreate: true,
      itemsPerPage: 20,
      page: 1,
    },
  });

  if (!isLoggedIn) {
    return (
      <Error
        error={{ status: 401, message: "необходимо войти в свой аккаунт" }}
      />
    );
  }

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  return (
    <PageBox title="Заказы" size="md" p="1rem" loading={loading}>
      <Stack spacing="sm">
        {data && data.length > 0 ? (
          data.map(order => (
            <Container fluid w="100%" bg="white" p={20}>
              <Stack>
                <Text weight={500}>№ {order.orderNumber}</Text>
                <Text>Статус: {order.state}</Text>
                {order.orderItems.map(item => (
                  <OrderItem
                    productId={item.productId}
                    quantity={item.quantity}
                    price={item.price}
                  />
                ))}
              </Stack>
            </Container>
          ))
        ) : (
          <Center m="auto" p="2rem">
            нет заказов
          </Center>
        )}
      </Stack>
    </PageBox>
  );
}
