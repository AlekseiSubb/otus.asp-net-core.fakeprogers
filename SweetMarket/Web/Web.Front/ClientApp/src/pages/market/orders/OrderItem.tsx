import React from "react";
import { Container, Stack, Loader, Text } from "@mantine/core";
import { Link } from "react-router-dom";
import useAxios from "axios-hooks";
import { MarketProduct } from "@/types/product";

interface OrderItemProps {
  productId: number;
  quantity: number;
  price: number;
}

export default function OrderItem({
  productId,
  quantity,
  price,
}: OrderItemProps) {
  const [{ data: product, loading }, getProducts] = useAxios<MarketProduct>(
    `/marketapi/v1/Product/${productId}`,
  );

  if (loading) {
    return <Loader className="loader" />;
  }

  return (
    <Container fluid w="100%" p={20}>
      <Stack>
        <Link to={`/market/product/${product?.id}`}>{product?.name}</Link>
        <Text>Кол-во: {quantity}</Text>
        <Text>Цена: {price}</Text>
      </Stack>
    </Container>
  );
}
