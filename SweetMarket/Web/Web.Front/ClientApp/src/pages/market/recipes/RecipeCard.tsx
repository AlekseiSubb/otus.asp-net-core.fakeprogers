import { Card, Text, Group, Image, Stack, ActionIcon } from "@mantine/core";
import { useNavigate } from "react-router-dom";
import { BiTime, BiTrash } from "react-icons/bi";

const fakeImgSrc =
  "https://cdn.xxl.thumbs.canstockphoto.ru/%D0%BA%D0%BD%D0%B8%D0%B3%D0%B0-%D1%80%D0%B5%D1%86%D0%B5%D0%BF%D1%82-%D0%B2%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%BD%D1%8B%D0%B9-%D0%BA%D0%BB%D0%B8%D0%BF%D0%B0%D1%80%D1%82_csp7112265.jpg";

interface RecipeCardProps {
  id: number;
  name: string;
  cookingTime: number;
  onDeleteClick: (id: Number) => void;
}

export default function RecipeCard({
  id,
  name,
  cookingTime,
  onDeleteClick,
}: RecipeCardProps) {
  const navigate = useNavigate();

  function handleRecipeClick() {
    navigate(`${id}`);
  }

  function handleDeleteClick(event: React.MouseEvent<HTMLElement>) {
    event.stopPropagation();
    onDeleteClick(id);
  }

  return (
    <Card
      shadow="sm"
      padding="lg"
      radius="md"
      onClick={handleRecipeClick}
      sx={{
        border: "0.1rem solid transparent",
        "&:hover": {
          border: "0.1rem solid lightgray",
        },
      }}
    >
      <Card.Section>
        <Image src={fakeImgSrc} height={150} alt="Norway" />
      </Card.Section>

      <Stack mt="md" mb="xs">
        <Text size="sm" weight={500} h="2.4rem" lineClamp={2}>
          {name}
        </Text>
        <Group position="apart">
          <Group spacing="xs">
            <BiTime />
            <Text>{cookingTime} мин.</Text>
          </Group>
          <ActionIcon variant="transparent" onClick={handleDeleteClick}>
            <BiTrash size="1.5rem" />
          </ActionIcon>
        </Group>
      </Stack>
    </Card>
  );
}
