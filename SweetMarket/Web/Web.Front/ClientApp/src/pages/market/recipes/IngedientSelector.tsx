import React, { useMemo } from "react";
import { Stack, Select, NumberInput, Box, Button } from "@mantine/core";
import { useForm } from "@mantine/form";
import { ModalFormProps } from "@/types/modalForm";
import { count } from "console";
import { valueGetters } from "@mantine/core/lib/Box/style-system-props/value-getters/value-getters";
import SaveButton from "@/components/SaveButton";
import { Ingredient, StepIngredient } from "@/types/recipe";

interface FormValues {
  value: string | null;
  count: number;
}

interface IngedientSelectorProps
  extends ModalFormProps<StepIngredient | null, StepIngredient> {
  ingredients: Ingredient[];
}

export default function IngedientSelector({
  data,
  onConfirm,
  ingredients,
}: IngedientSelectorProps) {
  const form = useForm<FormValues>({
    initialValues: {
      value: data?.ingredientId?.toString() ?? null,
      count: data?.count ?? 1,
    },
    validate: {
      value: (value) =>
        value && value.length > 0 ? null : "выберите ингредиент",
      count: (value) => (value > 0 ? null : "укажите количество"),
    },
  });

  function handleSubmit(values: FormValues) {
    onConfirm({
      id: data?.id ?? 0,
      ingredientId: Number(values.value),
      count: values.count,
    });
  }

  const selectOptions = ingredients.map((item) => ({
    value: item.id.toString(),
    label: item.name,
  }));

  const selectedIngredient = useMemo(
    () => ingredients.find((item) => item.id === Number(form.values.value)),
    [form.values.value, ingredients]
  );

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <Stack justify="space-around" spacing="lg" mih={250}>
        <Select
          withAsterisk
          label="Ингредиент"
          placeholder="Выберите ингредиент"
          zIndex={200}
          searchable
          maxDropdownHeight={150}
          {...form.getInputProps("value")}
          data={selectOptions}
        />
        <NumberInput
          withAsterisk
          label={`Кол-во ${
            selectedIngredient ? `(${selectedIngredient.engUnitShortName})` : ""
          }`}
          step={1}
          min={1}
          w={200}
          {...form.getInputProps("count")}
        />

        <SaveButton />
      </Stack>
    </form>
  );
}
