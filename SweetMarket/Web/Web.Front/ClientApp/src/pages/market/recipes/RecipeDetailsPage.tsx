import {
  Text,
  Group,
  Container,
  Stack,
  Image,
  Title,
  Divider,
  Button,
  Modal,
  Center,
} from "@mantine/core";
import { useParams } from "react-router-dom";
import useAxios from "axios-hooks";

import useModalForm from "@/hooks/useModalForm";
import { RecipeData } from "@/types/recipe";
import RecipeForm from "./RecipeForm";
import ErrorPage from "../../ErrorPage";
import RecipeSteps from "./RecipeSteps";
import PageLoader from "@/components/PageLoader";
import AxiosErrorBox from "@/components/AxiosError";
import PageBox from "@/components/PageBox";

const fakeImgSrc =
  "https://cdn.xxl.thumbs.canstockphoto.ru/%D0%BA%D0%BD%D0%B8%D0%B3%D0%B0-%D1%80%D0%B5%D1%86%D0%B5%D0%BF%D1%82-%D0%B2%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%BD%D1%8B%D0%B9-%D0%BA%D0%BB%D0%B8%D0%BF%D0%B0%D1%80%D1%82_csp7112265.jpg";

export default function RecipeDetailsPage() {
  const { recipeId } = useParams();

  const [{ data, loading, error }, getRecipe] = useAxios<RecipeData>(
    `/marketapi/v1/Recipe/${recipeId}`,
  );

  const { showEdit, modalProps, formProps } = useModalForm<RecipeData>({
    editTitle: "Редактирование рецепта",
    onConfirm: () => getRecipe(),
  });

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  return (
    <PageBox size="lg" loading={loading}>
      {data ? (
        <Stack spacing={"md"}>
          <Container
            fluid
            m={0}
            p={0}
            bg={"white"}
            sx={{
              boxShadow: "0px 0px 5px 0px rgba(97, 119, 137, 0.2);",
            }}
          >
            <Group position="apart" p="0.2rem">
              <Title p={5} size="1.2rem">
                Описание рецепта
              </Title>
              <Button compact onClick={() => showEdit(data)}>
                Редактировать
              </Button>
            </Group>

            <Divider />

            <Group noWrap align="top" p={16}>
              <Image src={fakeImgSrc} height={300} width={300} alt="Norway" />
              <Stack m={0} spacing="sm">
                <Stack spacing="0.2rem">
                  <Text weight={500}>{data?.name}</Text>
                  <Text>{data?.description}</Text>
                </Stack>
                {/* <Stack spacing="0.2rem">
                <Text weight={500}>Состав</Text>
                {ingredients}
              </Stack> */}
                <Stack spacing="0.2rem">
                  <Text weight={500}>Время приготовления</Text>
                  <Text>{data?.cookingTime} мин.</Text>
                </Stack>
              </Stack>
            </Group>

            <Divider mt={"1rem"} />
          </Container>

          <Container
            fluid
            m={0}
            bg={"white"}
            p={0}
            sx={{
              boxShadow: "0px 0px 5px 0px rgba(97, 119, 137, 0.2);",
            }}
          >
            {recipeId && <RecipeSteps recipeId={Number(recipeId)} />}
          </Container>
          <Modal {...modalProps}>
            <RecipeForm {...formProps} />
          </Modal>
        </Stack>
      ) : (
        <Center>нет данных</Center>
      )}
    </PageBox>
  );
}
