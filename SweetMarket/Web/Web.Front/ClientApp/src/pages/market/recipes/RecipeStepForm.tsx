import {
  Container,
  Divider,
  Stack,
  Text,
  Group,
  Button,
  ActionIcon,
  Box,
  TextInput,
  Textarea,
  NumberInput,
  Flex,
  Modal,
  Select,
  Title,
  Center,
  Loader,
} from "@mantine/core";
import { BiEdit, BiTrash, BiPlus } from "react-icons/bi";
import { useNavigate, useParams } from "react-router-dom";
import {
  Ingredient,
  RecipeStepData,
  StepIngredient,
  StepOperation,
} from "@/types/recipe";
import { useForm } from "@mantine/form";
import { useEffect, useState } from "react";
import SaveButton from "@/components/SaveButton";
import useModalForm from "@/hooks/useModalForm";
import IngedientSelector from "./IngedientSelector";
import useAxios from "axios-hooks";
import ErrorPage from "../../ErrorPage";
import { notifications } from "@mantine/notifications";
import PageLoader from "@/components/PageLoader";
import PageBox from "@/components/PageBox";
import AxiosErrorBox from "@/components/AxiosError";

interface FormValues {
  numPos: number;
  name: string;
  action: string;
}

const url = "/marketapi/v1/RecipeStep";

export default function RecipeStepForm() {
  const { recipeId, stepId } = useParams();
  const navigate = useNavigate();

  const isEditMode = Number(stepId) > 0;

  const [{ data: recipeStep, loading: getLoading, error: getError }] =
    useAxios<RecipeStepData>(
      {
        url: `${url}/details/${stepId}`,
      },
      { manual: !isEditMode },
    );

  const [{ data: ingredients, loading: ingredientsLoading }] = useAxios<
    Ingredient[]
  >("/marketapi/v1/Ingredient/List");

  const [{ loading: updateLoading, error: updateError }, updateRequest] =
    useAxios(
      {
        url: isEditMode ? `${url}/${stepId}` : url,
        method: isEditMode ? "PUT" : "POST",
      },
      { manual: true },
    );

  const loading = getLoading || updateLoading;
  const [stepOperations, setStepOperations] = useState<StepOperation[]>([]);

  const { showCreate, showEdit, modalProps, formProps } = useModalForm<
    StepIngredient,
    StepIngredient
  >({
    createTitle: "Выберите ингредиент",
    editTitle: "Изменение игредиента",
    onConfirm: stepIngredient => {
      if (stepIngredient) {
        const ingredient = ingredients?.find(
          ing => ing.id === stepIngredient.ingredientId,
        );

        const newStepOperation = {
          ingredientId: stepIngredient.ingredientId,
          count: stepIngredient.count,
          ingredientName: ingredient?.name ?? "",
          engUnitName: ingredient?.engUnitShortName ?? "",
        };

        if (stepIngredient.id > 0) {
          const oldValues = stepOperations[stepIngredient.id];
          stepOperations[stepIngredient.id] = {
            ...oldValues,
            ...newStepOperation,
          };
          setStepOperations([...stepOperations]);
        } else {
          stepOperations.push({
            ...newStepOperation,
            id: 0,
            numPos: stepOperations.length + 2,
          });
        }
      }
    },
  });

  const form = useForm<FormValues>({
    initialValues: {
      numPos: 1,
      name: "",
      action: "",
    },
    validate: {
      numPos: value => (value > 0 ? null : "введите корректный номер позиции"),
      name: value =>
        value.trim().length > 3
          ? null
          : "заполните имя шага (не менее 3 символов)",
      action: value =>
        value && value.trim().length > 10
          ? null
          : "поле действия должно быть длинее 10 символов",
    },
  });

  function handleSubmit(values: FormValues) {
    updateRequest({
      data: {
        id: isEditMode ? Number(stepId) : undefined,
        recipeId: Number(recipeId),
        name: values.name,
        action: values.action,
        numPos: values.numPos,
        stepOperations: stepOperations.map(so => {
          if (isEditMode) {
            return {
              id: so.id,
              numPos: so.numPos,
              count: so.count,
              ingredientId: so.ingredientId,
            };
          }

          return {
            numPos: so.numPos,
            count: so.count,
            ingredientId: so.ingredientId,
          };
        }),
      },
    }).then(() =>
      notifications.show({
        message: "Изменения успешно сохранены",
      }),
    );
  }

  function handleDeleteStepOperation(index: number) {
    stepOperations.splice(index, 1);
    setStepOperations([...stepOperations]);
  }

  useEffect(() => {
    if (recipeStep) {
      form.setValues({
        name: recipeStep.name,
        action: recipeStep.action,
        numPos: recipeStep.numPos,
      });

      setStepOperations(recipeStep.stepOperations ?? []);
    }
  }, [recipeStep]);

  const axiosError = getError || updateError;
  if (axiosError) {
    return <AxiosErrorBox error={axiosError} />;
  }

  return (
    <PageBox loading={loading || ingredientsLoading} size="sm" p="md" shadow>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Stack spacing="md">
          <Title size="h3">
            {isEditMode ? "Редактирование шага рецепта" : "Новый шаг рецепта"}
          </Title>
          <Stack>
            <Flex gap="md">
              <TextInput
                label="Название"
                placeholder="введите название шага"
                withAsterisk
                sx={{ flex: "1" }}
                {...form.getInputProps("name")}
              />
              <NumberInput
                label="Номер шага"
                step={1}
                min={1}
                w={100}
                {...form.getInputProps("numPos")}
              />
            </Flex>

            <Textarea
              label="Действие"
              placeholder="введите подробное описания действия шага"
              autosize
              minRows={5}
              {...form.getInputProps("action")}
            />
          </Stack>

          <Group>
            <Text weight={500}>Необходимые ингредиенты</Text>
            <Button compact onClick={showCreate}>
              <BiPlus size="1.2rem" />
            </Button>
          </Group>

          <Divider />

          {stepOperations.length > 0 ? (
            stepOperations.map((so, index) => (
              <Box
                key={index}
                sx={{
                  background: "white",
                  border: "1px solid lightgray",
                  padding: "1rem",
                }}
              >
                <Group position="apart">
                  <Group spacing="xs">
                    <Text>{index + 1}.</Text>
                    <Text>{so.ingredientName} - </Text>
                    <Text>
                      {so.count} {so.engUnitName}
                    </Text>
                  </Group>
                  <Group spacing="sm">
                    <ActionIcon
                      onClick={() =>
                        showEdit({
                          id: index,
                          ingredientId: so.ingredientId,
                          count: so.count,
                        })
                      }
                    >
                      <BiEdit size="1.2rem" />
                    </ActionIcon>
                    <ActionIcon
                      onClick={() => handleDeleteStepOperation(index)}
                    >
                      <BiTrash size="1.2rem" />
                    </ActionIcon>
                  </Group>
                </Group>
              </Box>
            ))
          ) : (
            <Center bg="white" p="2rem">
              без ингредиентов
            </Center>
          )}

          <SaveButton disabled={loading} />
        </Stack>
      </form>

      <Modal {...modalProps}>
        <IngedientSelector ingredients={ingredients ?? []} {...formProps} />
      </Modal>
    </PageBox>
  );
}
