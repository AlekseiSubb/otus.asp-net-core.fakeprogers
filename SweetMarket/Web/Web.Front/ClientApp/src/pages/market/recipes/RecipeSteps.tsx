import {
  Group,
  Title,
  Text,
  Button,
  Divider,
  Loader,
  Center,
} from "@mantine/core";
import { BiPlus } from "react-icons/bi";
import RecipeStep from "./RecipeStep";
import { RecipeStepData } from "@/types/recipe";
import useAxios from "axios-hooks";
import { Link } from "react-router-dom";
import ErrorPage from "../../ErrorPage";

interface RecipeStepsProps {
  recipeId: number;
}

export default function RecipeSteps({ recipeId }: RecipeStepsProps) {
  const [{ data: steps, loading, error }, getSteps] = useAxios<
    RecipeStepData[]
  >({
    url: "/marketapi/v1/RecipeStep/details",
    method: "POST",
    params: {
      recipeId,
    },
  });

  const [{}, deleteStep] = useAxios<RecipeStepData[]>({ method: "Delete" });

  function handleDeleteClick(stepId: number) {
    deleteStep(`/marketapi/v1/RecipeStep/${stepId}`).then(() => getSteps());
  }

  if (loading) {
    return <Loader className="loader" />;
  }

  if (error) {
    return <ErrorPage />;
  }

  return (
    <>
      <Group position="apart" p="0.5rem">
        <Title size="1.2rem">Шаги рецепта</Title>
        <Button
          compact
          leftIcon={<BiPlus size="1.2rem" />}
          component={Link}
          to="step/0"
        >
          Добавить
        </Button>
      </Group>

      <Divider />

      <div>
        {steps && steps.length > 0 ? (
          steps.map(step => (
            <RecipeStep
              key={step.id}
              {...step}
              onDeleteClick={() => handleDeleteClick(step.id)}
            />
          ))
        ) : (
          <Center p="2rem">
            <Text>шаги не определены</Text>
          </Center>
        )}
      </div>
    </>
  );
}
