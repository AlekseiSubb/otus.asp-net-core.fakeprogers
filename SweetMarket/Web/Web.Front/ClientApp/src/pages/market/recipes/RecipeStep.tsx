import { RecipeStepData } from "@/types/recipe";
import {
  Container,
  Divider,
  Stack,
  Text,
  Group,
  ActionIcon,
} from "@mantine/core";
import { useMemo } from "react";
import { BiEdit, BiTrash } from "react-icons/bi";
import { Link } from "react-router-dom";

interface RecipeStepProps extends RecipeStepData {
  onDeleteClick: () => void;
}

function RecipeStep({
  id,
  numPos,
  name,
  action,
  stepOperations,
  onDeleteClick,
}: RecipeStepProps) {
  const ingredients = useMemo(
    () =>
      stepOperations?.map((so) => (
        <Text
          color="blue.8"
          size="sm"
        >{`${so.ingredientName} - ${so.count} ${so.engUnitName}.`}</Text>
      )),
    [stepOperations]
  );

  return (
    <Container fluid p={0}>
      <Stack spacing={"sm"} p={15}>
        <Text
          size="md"
          component={Link}
          underline
          weight={500}
          to={`step/${id}`}
          sx={{ cursor: "pointer" }}
        >
          Шаг №{numPos}. {name}
        </Text>
        <Text>{action}</Text>

        {ingredients && ingredients.length > 0 ? (
          <Stack spacing={5} mt={10}>
            <Text size="sm" weight={500}>
              Необходимые ингридиенты:
            </Text>
            <Stack spacing={0}>{ingredients}</Stack>
          </Stack>
        ) : null}

        <Group position="right">
          <ActionIcon component={Link} to={`step/${id}`}>
            <BiEdit size="1.5rem" />
          </ActionIcon>
          <ActionIcon color="red.5" onClick={onDeleteClick}>
            <BiTrash size="1.5rem" />
          </ActionIcon>
        </Group>
      </Stack>
      <Divider />
    </Container>
  );
}

export default RecipeStep;
