import { TextInput, Textarea, NumberInput } from "@mantine/core";
import { useForm } from "@mantine/form";
import useAxios from "axios-hooks";

import SaveButton from "@/components/SaveButton";
import { ModalFormProps } from "@/types/modalForm";
import { RecipeData } from "@/types/recipe";

export interface FormValues {
  name: string;
  description?: string;
  cookingTime: number;
}

const url = "/marketapi/v1/Recipe";

const RecipeForm: React.FC<ModalFormProps<RecipeData | null>> = ({
  data,
  isEditMode,
  onConfirm,
  onError,
}) => {
  const [{ loading }, request] = useAxios<RecipeData>(
    {
      url: isEditMode ? `${url}/${data?.id}` : url,
      method: isEditMode ? "PUT" : "POST",
    },
    { manual: true },
  );

  const form = useForm<FormValues>({
    initialValues: {
      name: data?.name ?? "",
      description: data?.description ?? "",
      cookingTime: data?.cookingTime ?? 0,
    },

    validate: {
      name: value => (value.trim().length > 0 ? null : "введите название"),
    },
  });

  function handleSubmit(values: FormValues) {
    request({ data: values })
      .then(() => onConfirm())
      .catch(() => onError());
  }

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <TextInput
        label="Название"
        withAsterisk
        placeholder="название рецепта"
        {...form.getInputProps("name")}
      />
      <NumberInput
        label="Время приготовления (мин)"
        min={0}
        step={1}
        defaultValue={0}
        mt="md"
        {...form.getInputProps("cookingTime")}
      />
      <Textarea
        label="Описание"
        placeholder="описание рецепта"
        autosize
        minRows={3}
        maxRows={10}
        {...form.getInputProps("description")}
      />
      <SaveButton disabled={loading} />
    </form>
  );
};

export default RecipeForm;
