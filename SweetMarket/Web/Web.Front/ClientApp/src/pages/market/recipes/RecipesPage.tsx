import {
  Button,
  Container,
  SimpleGrid,
  Group,
  Title,
  Modal,
  Center,
} from "@mantine/core";
import ErrorPage from "@/pages/ErrorPage";

import useAxios from "axios-hooks";
import RecipeCard from "./RecipeCard";
import RecipeForm from "./RecipeForm";
import useModalForm from "@/hooks/useModalForm";
import { RecipeData } from "@/types/recipe";
import { BiPlus } from "react-icons/bi";
import PageLoader from "@/components/PageLoader";
import PageBox from "@/components/PageBox";
import AxiosErrorBox from "@/components/AxiosError";

export default function RecipesPage() {
  const [{ data, loading, error }, getData] = useAxios<RecipeData[]>(
    "/marketapi/v1/Recipe/List",
  );

  const [{}, deleteRecipe] = useAxios({ method: "DELETE" }, { manual: true });
  const { showCreate, modalProps, formProps } = useModalForm({
    createTitle: "Новый рецепт",
    onConfirm: () => getData(),
  });

  function handleDelete(id: Number) {
    deleteRecipe(`/marketapi/v1Recipe/${id}`).then(() => getData());
  }

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  return (
    <PageBox loading={loading} size="lg">
      <Group
        position="apart"
        bg={"white"}
        p={5}
        sx={{
          boxShadow: "0px 0px 5px 0px rgba(97, 119, 137, 0.2);",
        }}
      >
        <Title size={"h3"}>Список рецептов</Title>
        <Button
          onClick={showCreate}
          size="xs"
          leftIcon={<BiPlus size="1.2rem" />}
          variant="filled"
        >
          Создать
        </Button>
      </Group>

      <SimpleGrid
        spacing="md"
        mt={"md"}
        cols={4}
        breakpoints={[
          { maxWidth: "lg", cols: 4, spacing: "md" },
          { maxWidth: "md", cols: 3, spacing: "sm" },
          { maxWidth: "sm", cols: 2, spacing: "sm" },
        ]}
      >
        {data && data.length > 0 ? (
          data?.map(recipe => (
            <RecipeCard
              key={recipe.id}
              id={recipe.id}
              name={recipe.name}
              cookingTime={recipe.cookingTime}
              onDeleteClick={handleDelete}
            />
          ))
        ) : (
          <Center p={30}>нет доступных рецептов</Center>
        )}
      </SimpleGrid>

      <Modal {...modalProps}>
        <RecipeForm {...formProps} />
      </Modal>
    </PageBox>
  );
}
