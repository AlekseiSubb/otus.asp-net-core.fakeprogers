import AxiosErrorBox from "@/components/AxiosError";
import PageBox from "@/components/PageBox";
import { CustomerOrder } from "@/types/order";
import { Container, Group, Stack, Text } from "@mantine/core";
import useAxios from "axios-hooks";
import { error } from "console";
import React, { useEffect } from "react";
import { customerApiUrls } from "@/env";
import useAuthUser from "@/hooks/useAuthUser";
import Error from "@/components/Error";

const apiUrl = customerApiUrls.orders;

interface Customer {
  id: number;
}

export default function OrdersPage() {
  const { user, isLoggedIn } = useAuthUser();
  const [{ data: customer, loading: customerLoading }] = useAxios<Customer>(
    `${customerApiUrls.customer}/by_user_id/${user?.id}`,
  );

  const [{ data: orders, loading, error }, requestOrders] = useAxios<
    CustomerOrder[]
  >({}, { manual: true });

  useEffect(() => {
    requestOrders({
      url: `${apiUrl}/${customer?.id}`,
    });
  }, [customer]);

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  if (!isLoggedIn) {
    return (
      <Error
        error={{ status: 401, message: "необходимо войти в свой аккаунт" }}
      />
    );
  }

  return (
    <PageBox title="Заказы" size="sm" loading={loading || customerLoading}>
      <Stack>
        {orders?.map(order => (
          <Container fluid w="100%">
            <Stack>
              <Group position="apart">
                <Text>№ {order.orderNumber}</Text>
                <Text>{order.price}</Text>
              </Group>
              {order.orderItems.map(item => (
                <Group>
                  <Text>{item.productName}</Text>
                  <Text>{item.quantity}</Text>
                  <Text>{item.marketName}</Text>
                </Group>
              ))}
            </Stack>
          </Container>
        ))}
      </Stack>
    </PageBox>
  );
}
