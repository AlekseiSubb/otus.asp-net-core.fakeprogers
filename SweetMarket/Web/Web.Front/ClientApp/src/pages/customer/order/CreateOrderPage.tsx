import AxiosErrorBox from "@/components/AxiosError";
import Error from "@/components/Error";
import PageBox from "@/components/PageBox";
import { ShoppingSession } from "@/types/shoppingSession";
import { Container, Group, Stack, Title, Text, Button } from "@mantine/core";
import useAxios from "axios-hooks";
import React, { useContext } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import OrderPosition from "./OrderPosition";
import { customerApiUrls } from "@/env";
import { CartContext } from "@/context/cartContext";

const apiUrl = customerApiUrls.orders;
const cartApiUrl = customerApiUrls.cart;

export default function CreateOrderPage() {
  const navigate = useNavigate();
  const { shoppingSessionId } = useParams();
  const { removeCart } = useContext(CartContext);
  //   const shoppingSessionId = urlParams.get("shoppingSession");

  const [{ data, loading, error }] = useAxios<ShoppingSession>({
    url: cartApiUrl,
    params: {
      id: shoppingSessionId,
    },
  });

  const [{ loading: postLoading, error: postError }, createOrderRequest] =
    useAxios(
      {
        url: `${apiUrl}`,
        method: "POST",
      },
      { manual: true },
    );

  async function createOrder() {
    const res = await createOrderRequest({
      data: {
        idShoppingSession: Number(shoppingSessionId),
        paymentType: "наличными при получении",
        description: "",
        promocode: "",
      },
    });

    if (res.status === 200) {
      removeCart();
      navigate("/orders");
    }
  }

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  if (!shoppingSessionId) {
    return <Error error={{ message: "Нет активной сессии покупателя" }} />;
  }

  return (
    <PageBox
      title="Оформление заказа"
      size="sm"
      loading={loading || postLoading}
    >
      <Stack>
        <Title size="h4">Товары</Title>
        <Container fluid bg="white" w="100%">
          {data?.cartItems.map(item => (
            <OrderPosition
              productId={item.productId}
              quantity={item.qu}
              price={item.price}
            />
          ))}
        </Container>

        <Title size="h4">Способ оплаты</Title>
        <Container fluid bg="white" w="100%" p={20}>
          <Text>наличными при получении</Text>
        </Container>

        <Title size="h4">Оформление</Title>
        <Container fluid bg="white" w="100%" mih={100} p={20}>
          <Group position="apart">
            <Text weight={500}>Итоговая цена: </Text>
            <Text weight={500}>
              {data?.cartItems
                .map(item => item.price * item.qu)
                .reduce((prev, current) => prev + current)
                .toFixed(2) ?? ""}{" "}
              руб.
            </Text>
          </Group>
          <Group position="right" mt={20}>
            <Button onClick={createOrder}>Оформить</Button>
          </Group>
        </Container>
      </Stack>
    </PageBox>
  );
}
