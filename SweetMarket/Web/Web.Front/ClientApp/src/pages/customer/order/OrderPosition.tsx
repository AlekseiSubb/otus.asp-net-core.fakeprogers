import useAxios from "axios-hooks";
import { Loader, Container, Group, Image, Text } from "@mantine/core";
import React from "react";
import { MarketProduct } from "@/types/product";
import { customerApiUrls, imageApiUrl } from "@/env";

interface OrderPositionProps {
  productId: number;
  quantity: number;
  price: number;
}

const productApiUrl = customerApiUrls.product;
const imageStorageUrl = imageApiUrl;

export default function OrderPosition({
  productId,
  quantity,
  price,
}: OrderPositionProps) {
  const [{ data: product, loading }] = useAxios<MarketProduct>(
    `${productApiUrl}/${productId}`,
  );

  if (loading) {
    return <Loader className="loader" />;
  }

  return (
    <Container fluid p={10}>
      <Group position="apart" w="100%">
        <Group align="start">
          <Image
            width={60}
            height={60}
            src={
              product?.imageGuid
                ? `${imageStorageUrl}/${product.imageGuid}`
                : ""
            }
            withPlaceholder
          />
          <Text weight={500}>{product?.name}</Text>
        </Group>
        <Group>
          <Text>x{quantity}</Text>
          <Text weight={500}>{(price * quantity).toFixed(2)} руб.</Text>
        </Group>
      </Group>
    </Container>
  );
}
