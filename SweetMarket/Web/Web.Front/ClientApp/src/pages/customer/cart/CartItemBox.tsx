import { CartItem } from "@/types/shoppingSession";
import { Link } from "react-router-dom";
import {
  Button,
  Container,
  Divider,
  Group,
  NumberInput,
  Stack,
  Text,
} from "@mantine/core";
import React from "react";

interface CartItemBoxProps {
  cartItem: CartItem;
  onDeleteClick: () => void;
  onQuChange: (value: number) => void;
}

export default function CartItemBox({
  cartItem,
  onQuChange,
  onDeleteClick,
}: CartItemBoxProps) {
  return (
    <Container fluid bg="white" p={10} w="100%">
      <Stack>
        <Group position="apart">
          <Link to={`/product/${cartItem.productId}`}>
            {cartItem.productName}
          </Link>
          <Text weight={100}>Цена: {cartItem.price}</Text>
        </Group>

        <Text>Маркет: {cartItem.marketName}</Text>
        <Divider />
        <Group position="apart">
          <Group spacing="sm">
            <Text>Кол-во: </Text>
            <NumberInput
              value={cartItem.qu}
              onChange={onQuChange}
              w={100}
              step={1}
              min={1}
            />
            <Button onClick={onDeleteClick}>удалить</Button>
          </Group>
        </Group>
      </Stack>
    </Container>
  );
}
