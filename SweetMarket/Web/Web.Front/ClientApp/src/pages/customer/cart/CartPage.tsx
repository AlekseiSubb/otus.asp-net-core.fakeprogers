import PageBox from "@/components/PageBox";
import { CartContext } from "@/context/cartContext";
import { Stack, Group, Button, Center } from "@mantine/core";
import React, { useContext } from "react";
import CartItemBox from "./CartItemBox";
import { Link } from "react-router-dom";
import useAuthUser from "@/hooks/useAuthUser";
import Error from "@/components/Error";
import ErrorPage from "@/pages/ErrorPage";

export default function CartPage() {
  const { id, totalItems, items, removeCartItem, editCartItemQu } =
    useContext(CartContext);
  const { isLoggedIn } = useAuthUser();

  const hasItems = totalItems > 0;

  if (!isLoggedIn) {
    return (
      <Error
        error={{ status: 401, message: "необходимо войти в свой аккаунт" }}
      />
    );
  }

  return (
    <PageBox title="Корзина" size="md" p="1rem">
      <Stack spacing="sm">
        {items.length > 0 ? (
          items.map(item => (
            <CartItemBox
              cartItem={item}
              onQuChange={value => editCartItemQu(item.id, value)}
              onDeleteClick={() => removeCartItem(item.id)}
            />
          ))
        ) : (
          <Center m="auto" p="2rem">
            корзина пуста
          </Center>
        )}
        {hasItems && (
          <Group position="center">
            <Button component={Link} to={`/orders/create/${id}`}>
              Перейти к оформлению
            </Button>
          </Group>
        )}
      </Stack>
    </PageBox>
  );
}
