import { Outlet } from "react-router-dom";

import CustomerHeader from "@/layouts/customer/CustomerHeader";
import CustomerLayout from "@/layouts/customer/CustomerLayout";
import CustomerNavbar, { LinkProps } from "@/layouts/customer/CustomerNavbar";
import useAxios from "axios-hooks";
import { useContext, useEffect, useState } from "react";
import { ProductCategory } from "@/types/product";
import CartProvider from "@/context/cartContext";
import { AuthContext } from "@/context/authContext";
import useAuthUser from "@/hooks/useAuthUser";
import { customerApiUrls } from "@/env";

export default function CustomerRoot() {
  const [links, setLinks] = useState<LinkProps[]>();
  const [{ data: categories, loading }] = useAxios<ProductCategory[]>({
    url: customerApiUrls.productCategories,
  });

  const { user, loginUrl, logoutUrl } = useContext(AuthContext);

  useEffect(() => {
    setLinks(
      categories?.map(cat => ({
        id: cat.id,
        to: `/catalog/${cat.name}`,
        text: cat.name,
      })),
    );
  }, [categories]);

  return (
    <CartProvider>
      <CustomerLayout
        header={
          <CustomerHeader
            userName={user?.name}
            isLoggedIn={user !== null}
            loginUrl={loginUrl}
            logoutUrl={logoutUrl}
          />
        }
        navbar={<CustomerNavbar links={links} loading={loading} />}
      >
        <Outlet />
      </CustomerLayout>
    </CartProvider>
  );
}
