import { Card, Text, Group, Image, Stack, Button } from "@mantine/core";
import { BiBasket } from "react-icons/bi";
import { MarketProduct } from "@/types/product";
import { imageApiUrl } from "@/env";

interface ProductCardProps {
  product: MarketProduct;
  showBasketButton: boolean;
  isAddedIntoBasket: boolean;
  onAddingIntoBasket?: (id: number) => void;
  onClick?: (id: number) => void;
}

export default function ProductCard({
  product,
  showBasketButton,
  isAddedIntoBasket,
  onAddingIntoBasket,
  onClick,
}: ProductCardProps) {
  const { id, name, imageGuid } = product;

  function handleAddIntoBasket(event: React.MouseEvent<HTMLElement>) {
    event.stopPropagation();
    onAddingIntoBasket?.(id);
  }

  return (
    <Card
      shadow="sm"
      padding="lg"
      radius="md"
      onClick={() => onClick?.(id)}
      sx={{
        border: "0.1rem solid transparent",
        "&:hover": {
          border: "0.1rem solid lightgray",
        },
        cursor: "pointer",
      }}
    >
      <Card.Section>
        <Image src={`${imageApiUrl}/${imageGuid}`} height={150} alt="Norway" />
      </Card.Section>

      <Stack mt="md" mb="xs">
        <Text size="sm" h="2.4rem" lineClamp={2}>
          {name}
        </Text>
        {/* <Text lineClamp={3}>{description}</Text> */}
        <Text weight={500}>{product.price} руб.</Text>
        {showBasketButton && (
          <Group position="right">
            <Button
              onClick={handleAddIntoBasket}
              rightIcon={<BiBasket size={20} />}
              disabled={isAddedIntoBasket}
              color={isAddedIntoBasket ? "red.7" : undefined}
            >
              {isAddedIntoBasket ? "уже в корзине" : "в корзину"}
            </Button>
          </Group>
        )}
      </Stack>
    </Card>
  );
}
