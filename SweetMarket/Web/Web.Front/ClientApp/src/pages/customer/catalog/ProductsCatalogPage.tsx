import PageBox from "@/components/PageBox";
import {
  ActionIcon,
  Center,
  Pagination,
  SimpleGrid,
  TextInput,
} from "@mantine/core";
import useAxios from "axios-hooks";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ProductCard from "./ProductCard";
import { MarketProduct } from "@/types/product";
import { BsSearch } from "react-icons/bs";
import AxiosErrorBox from "@/components/AxiosError";
import useAuthUser from "@/hooks/useAuthUser";
import { CartContext } from "@/context/cartContext";

import { customerApiUrls } from "@/env";

interface ProductsCatalog {
  itemsPerPage: number;
  currentPage: number;
  totalPages: number;
  items: MarketProduct[];
}

export default function ProductsCatalogPage() {
  const { categoryName } = useParams();
  const navigate = useNavigate();
  const [searchText, setSearchText] = useState("");
  // const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);

  const cartContext = useContext(CartContext);

  const { user, isLoggedIn } = useAuthUser();

  const [{ data, loading, error }, getData] = useAxios<ProductsCatalog>(
    {
      url: customerApiUrls.productCatalog + "/list",
      method: "POST",
    },
    { manual: true },
  );

  function loadProducts(page: number) {
    getData({
      data: {
        name: searchText,
        price: 0,
        marketName: "",
        categoryName: categoryName ?? "",
        page: page,
        pageSize: pageSize,
      },
    });
  }

  function handleSearchClick() {
    loadProducts(1);
  }

  useEffect(() => {
    loadProducts(1);
  }, [categoryName]);

  if (error) {
    return <AxiosErrorBox error={error} />;
  }

  return (
    <PageBox loading={loading} size="lg">
      <Center>
        <TextInput
          value={searchText}
          onChange={event => setSearchText(event.currentTarget.value)}
          miw={{ base: 300, md: 500, xl: 800 }}
          placeholder="введите наименование продукта"
          rightSection={
            <ActionIcon onClick={handleSearchClick}>
              <BsSearch size={22} />
            </ActionIcon>
          }
        />
      </Center>

      {data?.items && data.items.length > 0 ? (
        <SimpleGrid
          spacing="md"
          mt={"md"}
          cols={4}
          breakpoints={[
            { maxWidth: "lg", cols: 4, spacing: "md" },
            { maxWidth: "md", cols: 3, spacing: "sm" },
            { maxWidth: "sm", cols: 2, spacing: "sm" },
          ]}
        >
          {data.items.map(product => (
            <ProductCard
              key={product.id}
              product={product}
              onClick={() => navigate(`/product/${product.id}`)}
              onAddingIntoBasket={() => cartContext.addCartItem(product.id, 1)}
              showBasketButton={isLoggedIn}
              isAddedIntoBasket={cartContext.isProductExist(product.id)}
            />
          ))}
        </SimpleGrid>
      ) : (
        <Center p={30}>не удалось найти продукты</Center>
      )}

      {data && data.totalPages > 1 && (
        <Center>
          <Pagination
            value={data.currentPage}
            onChange={val => loadProducts(val)}
            total={data.totalPages}
            mt="md"
          />
        </Center>
      )}
    </PageBox>
  );
}
