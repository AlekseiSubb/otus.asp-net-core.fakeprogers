import PageBox from "@/components/PageBox";
import { MarketProduct } from "@/types/product";
import useAxios from "axios-hooks";
import React from "react";
import { useParams } from "react-router-dom";
import { Container } from "@mantine/core";
import { customerApiUrls } from "@/env";

export default function ProductDetailsPage() {
  const { id } = useParams();
  const [{ data, loading, error }, getProductDetails] = useAxios<MarketProduct>(
    `${customerApiUrls.product}/${id}`,
  );

  return (
    <PageBox loading={loading} size="md" p="1rem" shadow>
      <Container w="100%" bg="white"></Container>
    </PageBox>
  );
}
