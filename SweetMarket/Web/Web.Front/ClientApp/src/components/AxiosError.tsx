import { Center } from "@mantine/core";
import { AxiosError } from "axios";
import Error from "./Error";

interface AxiosErrorProps {
  error: AxiosError;
}

export default function AxiosErrorBox({ error }: AxiosErrorProps) {
  function getErrorInfo(error: AxiosError) {
    if (error.response) {
      return {
        status: error.response?.status,
        message: error.response?.statusText,
      };
    }

    return {
      message: error.message,
    };
  }

  return (
    <Center mx="auto" h="calc(100vh - var(--mantine-header-height))">
      <Error error={getErrorInfo(error)} />
    </Center>
  );
}
