import { Box, Button } from "@mantine/core";
import React from "react";

interface SaveButtonProps {
  disabled?: boolean;
}

export default function SaveButton({ disabled = false }: SaveButtonProps) {
  return (
    <Box mt="md" sx={{ display: "flex", justifyContent: "flex-end" }}>
      <Button type="submit" disabled={disabled}>
        Сохранить
      </Button>
    </Box>
  );
}
