import React from "react";
import PageLoader from "./PageLoader";
import { SystemProp, SpacingValue, Container, Title } from "@mantine/core";

interface PageBoxProps {
  title?: string;
  loading?: boolean;
  children?: React.ReactNode;
  size?: "lg" | "md" | "sm";
  p?: SystemProp<SpacingValue>;
  fluid?: boolean;
  shadow?: boolean;
}

export default function PageBox({
  title,
  loading,
  children,
  size,
  p,
  fluid,
  shadow,
}: PageBoxProps) {
  if (loading) {
    return <PageLoader />;
  }

  return (
    <Container p="1rem" fluid>
      <Container
        m="auto"
        p={p ?? 0}
        size={size}
        fluid={fluid}
        className={shadow ? "box-shadow" : ""}
      >
        {title && (
          <Title size="h3" mb="md">
            {title}
          </Title>
        )}
        {children}
      </Container>
    </Container>
  );
}
