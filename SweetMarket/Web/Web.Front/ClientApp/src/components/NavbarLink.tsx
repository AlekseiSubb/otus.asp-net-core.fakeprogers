import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { Group, Text } from "@mantine/core";

interface NavbarLinkProps {
  to: string;
  end?: boolean;
  text: string;
  icon?: React.ReactElement;
}

export default function NavbarLink({
  to,
  text,
  icon,
  end = false,
}: NavbarLinkProps) {
  const linkStyle = ({ isActive }: { isActive: boolean }) => {
    return {
      fontWeight: isActive ? "bold" : "",
      // color: isPending ? "red" : "black",
    };
  };

  return (
    <NavLink to={to} style={linkStyle} end={end}>
      <Group spacing={10}>
        {icon}
        <Text size={"xl"}>{text}</Text>
      </Group>
    </NavLink>
  );
}
