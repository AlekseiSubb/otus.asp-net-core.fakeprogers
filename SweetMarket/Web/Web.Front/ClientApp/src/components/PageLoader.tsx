import { Loader, Center } from "@mantine/core";

export default function PageLoader() {
  return (
    <Center h="calc(100vh - var(--mantine-header-height))" w="100%">
      <Loader h="4rem" w="100%" />
    </Center>
  );
}
