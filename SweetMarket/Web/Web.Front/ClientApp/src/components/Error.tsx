import { Stack, Title, Text } from "@mantine/core";
import { AxiosError } from "axios";
import { Link } from "react-router-dom";

export interface ErrorInfo {
  status?: number;
  message?: string;
}

export interface ErrorProps {
  error?: ErrorInfo;
  redirect?: {
    url: string;
    text: string;
  } | null;
}

export default function Error({ error, redirect }: ErrorProps) {
  return (
    <Stack align="center">
      <Title size="h2" color="red.7">
        {error?.status ?? "Ошибка"}
      </Title>
      <Text size={"xl"}>
        {error?.message ?? "В приложении возникла непредвиденная ошибка"}
      </Text>
      {redirect && (
        <Link to={redirect.url}>
          <Text>{redirect.text}</Text>
        </Link>
      )}
    </Stack>
  );
}
