import { RouterProvider, createBrowserRouter } from "react-router-dom";

import MarketRoot from "./pages/market/MaraketRoot";
import CustomerRoot from "./pages/customer/CustomerRoot";

import HomePage from "./pages/market/home/HomePage.js";
import RecipesPage from "./pages/market/recipes/RecipesPage.js";
import RecipeDetailsPage from "./pages/market/recipes/RecipeDetailsPage.js";
import ProductsPage from "./pages/market/products/ProductsPage.js";
import MarketOrdersPage from "./pages/market/orders/OrdersPage.js";
import SettingsPage from "./pages/market/settings/SettingsPage.js";
import ErrorPage from "./pages/ErrorPage.js";
import UserProfilePage from "./pages/user/UserProfilePage.jsx";
import RecipeStepForm from "./pages/market/recipes/RecipeStepForm.js";
import ProductForm from "./pages/market/products/ProductForm.js";

import NotFoundPage from "./pages/NotFoundPage";

import "./App.css";
import ProductsCatalogPage from "./pages/customer/catalog/ProductsCatalogPage";
import ProductDetailsPage from "./pages/customer/catalog/ProductDetailsPage";
import CartPage from "./pages/customer/cart/CartPage";
import CreateOrderPage from "./pages/customer/order/CreateOrderPage";
import OrdersPage from "@/pages/customer/order/OrdersPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <CustomerRoot />,
    errorElement: (
      <ErrorPage redirect={{ url: "/", text: "вернуться на главную" }} />
    ),
    children: [
      {
        index: true,
        element: <ProductsCatalogPage />,
      },
      {
        path: "catalog/:categoryName",
        element: <ProductsCatalogPage />,
      },
      {
        path: "product/:id",
        element: <ProductDetailsPage />,
      },
      {
        path: "cart",
        element: <CartPage />,
      },
      {
        path: "orders",
        children: [
          {
            index: true,
            element: <OrdersPage />,
          },
          {
            path: "create/:shoppingSessionId",
            element: <CreateOrderPage />,
          },
        ],
      },
    ],
  },
  {
    path: "market",
    element: <MarketRoot />,
    errorElement: (
      <ErrorPage redirect={{ url: "/market", text: "вернуться на главную" }} />
    ),
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: "products",
        element: <ProductsPage />,
      },
      {
        path: "products/:id",
        element: <ProductForm />,
      },
      {
        path: "orders",
        element: <MarketOrdersPage />,
      },
      {
        path: "recipes",
        element: <RecipesPage />,
      },
      {
        path: "settings",
        element: <SettingsPage />,
      },
      {
        path: "user",
        element: <UserProfilePage />,
      },
      {
        path: "recipes/:recipeId",
        children: [
          {
            index: true,
            element: <RecipeDetailsPage />,
          },
          {
            path: "step/:stepId",
            element: <RecipeStepForm />,
          },
        ],
      },
    ],
  },
  {
    path: "*",
    element: <NotFoundPage />,
  },
]);

export default function App() {
  return <RouterProvider router={router} />;
}
