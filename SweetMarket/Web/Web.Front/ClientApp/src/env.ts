//API Urls

export const customerApiUrls = {
  productCatalog: "/customerapi/v1/ProductCatalog",
  product: "/customerapi/v1/Product",
  productCategories: "/customerapi/v1/ProductCategory/list",
  cart: "/customerapi/v1/ShoppingSession",
  orders: "/customerapi/v1/Order",
  customer: "/customerapi/v1/Customer",
};

export const imageApiUrl = "/imageapi/v1/ImageStorage";
