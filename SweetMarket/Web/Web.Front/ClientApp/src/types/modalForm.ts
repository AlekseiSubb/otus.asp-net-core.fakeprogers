export interface ModalFormProps<T, O = undefined> {
  data: T;
  isEditMode: boolean;
  onConfirm: (data?: O) => void;
  onError: () => void;
}
