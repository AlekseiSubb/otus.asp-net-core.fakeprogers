export interface CartItem {
  id: number;
  qu: number;
  productId: number;
  productName: string;
  marketName: string;
  price: number;
}

export interface ShoppingSession {
  id: number;
  customerId: number;
  created: string;
  cartItems: CartItem[];
}
