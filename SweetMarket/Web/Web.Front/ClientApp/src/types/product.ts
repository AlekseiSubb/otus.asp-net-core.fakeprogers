export interface MarketProduct {
  id: number;
  name: string;
  description?: string;
  imageGuid?: string;
  price: number;
  recipeId?: number;
  marketId: number;
  productCategoryId: number;
  isPublishedInMarket: boolean;
  createdAt: string;
}

export interface ProductCategory {
  id: number;
  name: string;
}
