export interface RecipeData {
  id: number;
  name: string;
  description?: string;
  cookingTime: number;
}

export interface RecipeStepData {
  id: number;
  name: string;
  action?: string;
  numPos?: number;
  recipeId: number;
  stepOperations?: StepOperation[];
}

export interface StepOperation {
  id: number;
  numPos: number;
  ingredientId: number;
  count: number;
  ingredientName: string;
  engUnitName: string;
}

export interface StepIngredient {
  id: number;
  ingredientId: number;
  count: number;
}

export interface Ingredient {
  id: number;
  name: string;
  categoryName: string;
  description?: string;
  engUnitShortName: string;
}
