export interface OrderItem {
  productName: string;
  marketName: string;
  quantity: number;
  price: number;
}

export interface CustomerOrder {
  orderNumber: number;
  price: number;
  state: string;
  orderItems: OrderItem[];
}
