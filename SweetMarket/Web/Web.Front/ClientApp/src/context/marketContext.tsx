import { createContext } from "react";
import useAxios from "axios-hooks";
import { Center, Loader } from "@mantine/core";
import ErrorPage from "@/pages/ErrorPage";

interface MarketProviderProps {
  children: React.ReactNode;
}

export interface Market {
  id: number;
  name?: string;
}

export const MarketContext = createContext<Market>({
  id: 0,
  name: "",
});

export function MarketProvider({ children }: MarketProviderProps) {
  const [{ data: market, loading, error }] = useAxios<Market>(
    "/marketapi/v1/Market/1",
  );

  if (loading) {
    return (
      <Center h={"100vh"} w={"100vw"} mx="auto">
        <Loader h="4rem" w="100%" />
      </Center>
    );
  }

  if (error) {
    return <ErrorPage error={error} />;
  }

  const provadingValues: Market = {
    id: 1,
    name: market?.name,
  };

  return (
    <MarketContext.Provider value={provadingValues}>
      {children}
    </MarketContext.Provider>
  );
}
