import PageLoader from "@/components/PageLoader";
import useAuthUser from "@/hooks/useAuthUser";
import { CartItem, ShoppingSession } from "@/types/shoppingSession";
import useAxios from "axios-hooks";
import { createContext, useEffect, useState } from "react";
import { customerApiUrls } from "@/env";

export interface CartContextProps {
  id: number;
  totalItems: number;
  items: CartItem[];
  addCartItem: (productId: number, byCount: number) => void;
  removeCartItem: (cartItemId: number) => void;
  editCartItemQu: (cartItemId: number, qu: number) => void;
  isProductExist: (id: number) => boolean;
  removeCart: () => void;
}

export const CartContext = createContext<CartContextProps>({
  id: 0,
  totalItems: 0,
  items: [],
  addCartItem: () => {},
  removeCartItem: () => {},
  editCartItemQu: () => {},
  isProductExist: () => false,
  removeCart: () => {},
});

interface CartProviderProps {
  children: React.ReactNode;
}

interface Customer {
  id: number;
}

const apiUrl = customerApiUrls.cart;

export default function CartProvider({ children }: CartProviderProps) {
  const { user } = useAuthUser();

  const [{ data: customer }, requestCustomer] = useAxios<Customer>(
    `${customerApiUrls.customer}/by_user_id/${user?.id}`,
  );

  const [{ data: shoppingSessionData }, requestActiveSession] =
    useAxios<ShoppingSession>(
      {
        url: `${apiUrl}/GetByIdCastomer`,
        params: {
          id: customer?.id,
        },
      },
      { manual: true },
    );

  const [{}, cartRequest] = useAxios<ShoppingSession>(
    {
      method: "POST",
    },
    { manual: true },
  );

  const [shoppingSession, setShoppingSession] = useState<
    ShoppingSession | undefined
  >();

  function productExist(id: number) {
    if (shoppingSession?.cartItems && shoppingSession?.cartItems.length > 0) {
      return (
        shoppingSession.cartItems.find(p => p.productId === id) !== undefined
      );
    }

    return false;
  }

  async function createCart() {
    const customerRes = await requestCustomer({
      url: `${customerApiUrls.customer}/by_user_id/${user?.id}`,
    });

    const newSession = await cartRequest({
      url: apiUrl,
      params: {
        idCustomer: customerRes.data.id,
      },
    });

    return newSession.data.id;
  }

  async function addCartItem(productId: number, byCount: number = 1) {
    let shoppingSessionId = shoppingSession?.id;

    if (!shoppingSessionId) {
      var newSessionId = await createCart();
      shoppingSessionId = newSessionId;
    }

    const res = await cartRequest({
      url: `${apiUrl}/items`,
      params: {
        idShoppingSession: shoppingSessionId,
        addedProductId: productId,
        qu: byCount,
      },
    });

    if (res.status === 200) {
      requestActiveSession();
    }
  }

  async function removeCartItem(cartItemId: number) {
    if (!shoppingSession) {
      return;
    }

    const res = await cartRequest({
      url: `${apiUrl}/items/${cartItemId}`,
      method: "DELETE",
      params: {
        idShoppingSession: shoppingSession.id,
      },
    });

    if (res.status === 200) {
      if (res.data.cartItems.length === 0) {
        await removeCart();
      } else {
        requestActiveSession();
      }
    }
  }

  async function removeCart() {
    if (shoppingSession) {
      const res = await cartRequest({
        url: apiUrl,
        method: "DELETE",
        params: {
          idShoppingSession: shoppingSession.id,
        },
      });

      if (res.status === 200) {
        setShoppingSession(undefined);
      }
    }
  }

  async function editCartItemQu(cartItemId: number, qu: number) {
    if (shoppingSession) {
      const res = await cartRequest({
        url: `${apiUrl}/items/${cartItemId}`,
        method: "PUT",
        params: {
          idShoppingSession: shoppingSession.id,
          qu: qu,
        },
      });

      if (res.status === 200) {
        requestActiveSession();
      }
    }
  }

  const totalItems = shoppingSession?.cartItems
    ? shoppingSession?.cartItems.length + 1
    : 0;

  useEffect(() => {
    requestActiveSession();
  }, [customer]);

  useEffect(() => {
    setShoppingSession(shoppingSessionData);
  }, [shoppingSessionData]);

  //   if (loading) {
  //     return <PageLoader />;
  //   }

  return (
    <CartContext.Provider
      value={{
        id: shoppingSession?.id ?? 0,
        totalItems,
        items: shoppingSession?.cartItems ?? [],
        addCartItem,
        removeCartItem,
        editCartItemQu,
        isProductExist: productExist,
        removeCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
}
