import PageLoader from "@/components/PageLoader";
import useAxios from "axios-hooks";
import { userInfo } from "os";
import React, { createContext, useEffect } from "react";

const idClaimType = "userid";
const roleClaimType = "role";
const nameClaimType = "email";

interface UserClaim {
  type: string;
  value: any;
}

export interface User {
  id: number;
  name: string;
  role: string;
}

export interface AuthContextProps {
  user: User | null;
  loginUrl: string;
  logoutUrl: string;
}

export const AuthContext = createContext<AuthContextProps>({
  user: null,
  loginUrl: "",
  logoutUrl: "",
});

interface AuthProviderProps {
  children: React.ReactNode;
}

const loginUrl = "/bff/login";
const logoutUrl = "/bff/logout";

export default function AuthProvider({ children }: AuthProviderProps) {
  const [{ data: userInfo }] = useAxios<UserClaim[]>({
    url: "/bff/user",
    headers: {
      "X-CSRF": "1",
    },
  });

  const user = userInfo
    ? {
        id: userInfo.find(x => x.type === idClaimType)?.value ?? 0,
        name: userInfo.find(x => x.type === nameClaimType)?.value ?? "",
        role: userInfo.find(x => x.type === roleClaimType)?.value ?? "",
      }
    : null;

  //   const user = {
  //     id: 4,
  //     name: "Dima",
  //     role: "customer",
  //   };

  return (
    <AuthContext.Provider value={{ user, loginUrl, logoutUrl }}>
      {children}
    </AuthContext.Provider>
  );
}
