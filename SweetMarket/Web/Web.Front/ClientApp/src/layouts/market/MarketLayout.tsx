import React from "react";
import { AppShell, Navbar, Header, ScrollArea } from "@mantine/core";

interface MarketLayoutProps {
  header: React.ReactNode;
  navbar: React.ReactNode;
  children: React.ReactNode;
}

export default function MarketLayout(props: MarketLayoutProps) {
  return (
    <AppShell
      padding={0}
      navbar={
        <Navbar width={{ base: 220 }} p="1rem">
          {props.navbar}
        </Navbar>
      }
      header={
        <Header height={60}>
          <div className="app-header">{props.header}</div>
        </Header>
      }
      styles={theme => ({
        main: {
          backgroundColor:
            theme.colorScheme === "dark"
              ? theme.colors.dark[8]
              : theme.colors.gray[0],
        },
      })}
    >
      <ScrollArea
        style={{
          height: "calc(100vh - var(--mantine-header-height)",
        }}
      >
        {props.children}
      </ScrollArea>
    </AppShell>
  );
}
