import { AiOutlineHome } from "react-icons/ai";
import { IoTimerOutline } from "react-icons/io5";
import { BsBook, BsCart2, BsGear } from "react-icons/bs";
import { Stack } from "@mantine/core";

import NavbarLink from "@/components/NavbarLink";

export default function MarketNavbar() {
  const iconSize = 20;

  return (
    <Stack spacing="1rem" p={0}>
      <NavbarLink
        to="/market"
        text="Главная"
        icon={<AiOutlineHome size={iconSize} />}
        end
      />
      <NavbarLink
        to="products"
        text="Товары"
        icon={<BsCart2 size={iconSize} />}
      />
      <NavbarLink
        to="recipes"
        text="Рецепты"
        icon={<BsBook size={iconSize} />}
      />
      <NavbarLink
        to="orders"
        text="Заказы"
        icon={<IoTimerOutline size={iconSize} />}
      />
      {/* <NavbarLink
        to="settings"
        text="Настройки"
        icon={<BsGear size={iconSize} />}
      /> */}
    </Stack>
  );
}
