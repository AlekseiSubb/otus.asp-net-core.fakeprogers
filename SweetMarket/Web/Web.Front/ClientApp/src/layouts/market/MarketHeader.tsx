// import React from 'react'
import { useContext } from "react";
import { Group, Image, Space, Text, Flex } from "@mantine/core";
import { NavLink } from "react-router-dom";
import { HiOutlineUserCircle } from "react-icons/hi";
import { Market, MarketContext } from "@/context/marketContext";
import icon from "@/assets/confectionery-40.png";
import useAuthUser from "@/hooks/useAuthUser";

export default function Header() {
  const market = useContext<Market>(MarketContext);
  const user = useAuthUser();

  return (
    <Flex align="center" h="100%" wrap="nowrap">
      <Group position="apart" noWrap w="100%">
        <NavLink to="/market" end>
          <Group noWrap>
            <Image width={40} height={40} withPlaceholder src={icon} />
            <Text size="2rem" lineClamp={1} color="#535bf2">
              Sweet Market
            </Text>
          </Group>
        </NavLink>

        <Group spacing={"sm"} noWrap>
          <Text size="1rem" lineClamp={1}>
            Кондитерская: {market?.name}
          </Text>
          <Space />
          <NavLink to="user">
            <Group spacing={"xs"} noWrap>
              <HiOutlineUserCircle size={20} />
              <Text size="1rem" lineClamp={1}>
                {user.user?.name}
              </Text>
            </Group>
          </NavLink>
        </Group>
      </Group>
    </Flex>
  );
}
