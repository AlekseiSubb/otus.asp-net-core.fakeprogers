import NavbarLink from "@/components/NavbarLink";
import { Stack, Loader } from "@mantine/core";
import React from "react";

export interface LinkProps {
  id: number;
  to: string;
  text: string;
}

interface CustomerNavbarProps {
  links?: LinkProps[];
  loading: boolean;
}

export default function CustomerNavbar({
  links,
  loading,
}: CustomerNavbarProps) {
  return (
    <Stack spacing="0.5rem" p={0}>
      <NavbarLink to="/" text="Главная" end />
      {loading ? (
        <Loader className="loader" />
      ) : (
        links?.map(link => (
          <NavbarLink key={link.id} to={link.to} text={link.text} />
        ))
      )}
    </Stack>
  );
}
