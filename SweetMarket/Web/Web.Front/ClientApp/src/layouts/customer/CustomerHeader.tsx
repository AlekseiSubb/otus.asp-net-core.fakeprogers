import {
  Flex,
  Group,
  Image,
  Text,
  Button,
  Space,
  ActionIcon,
  Stack,
  Indicator,
} from "@mantine/core";
import { Link, NavLink } from "react-router-dom";
import icon from "@/assets/confectionery-40.png";
import { PiMapPin } from "react-icons/pi";
import { BsCart, BsMinecart } from "react-icons/bs";
import { IoNotificationsOutline } from "react-icons/io5";
import { useContext } from "react";
import { CartContext } from "@/context/cartContext";
import { HiOutlineUserCircle } from "react-icons/hi";

interface CustomerHeaderProps {
  userName?: string;
  isLoggedIn: boolean;
  loginUrl: string;
  logoutUrl: string;
}

export default function CustomerHeader({
  userName,
  isLoggedIn,
  loginUrl,
  logoutUrl,
}: CustomerHeaderProps) {
  const { totalItems } = useContext(CartContext);
  const cartHasItems = totalItems ? totalItems > 0 : false;

  return (
    <Flex align="center" h="100%" wrap="nowrap">
      <Group position="apart" noWrap w="100%">
        <Group noWrap>
          <NavLink to="/" end>
            <Group align="center" noWrap>
              <Image width={40} height={40} withPlaceholder src={icon} />
              <Text size="2rem" color="#535bf2" lineClamp={1}>
                Sweet Market
              </Text>
            </Group>
          </NavLink>
          {/* <NavLink to="/x">
            <Group noWrap spacing={2}>
              <PiMapPin size={20} />
              <Text size="sm">город</Text>
            </Group>
          </NavLink> */}
        </Group>

        <Group align="center" spacing={"sm"} noWrap>
          <NavLink to="/cart" end>
            <Stack spacing={2} align="center">
              <Indicator
                size={10}
                inline
                //offset={5}
                position="bottom-center"
                color="red"
                disabled={!cartHasItems}
              >
                <BsMinecart size={25} />
              </Indicator>
            </Stack>
          </NavLink>
          {/* <NavLink to={"/notify"} end>
            <Indicator
              size={10}
              inline
              //   offset={5}
              position="bottom-center"
              label="новые"
              color="red"
            >
              <IoNotificationsOutline size={22} />
            </Indicator>
          </NavLink> */}

          <Space w="2rem" />

          {isLoggedIn ? (
            <Group spacing={"xs"} noWrap>
              <HiOutlineUserCircle size={20} />
              <Text size="1rem" lineClamp={1}>
                {userName}
              </Text>
              <a href={logoutUrl}>Выйти</a>
            </Group>
          ) : (
            <a href={loginUrl}>Войти</a>
          )}
        </Group>
      </Group>
    </Flex>
  );
}
