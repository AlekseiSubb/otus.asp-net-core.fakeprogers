import React from "react";
import ReactDOM from "react-dom/client";
import { MantineProvider } from "@mantine/core";

import Axios from "axios";
import { configure } from "axios-hooks";
import LRU from "lru-cache";

import { Notifications } from "@mantine/notifications";

import App from "./App";

import "./index.css";
import AuthProvider from "./context/authContext";

const axios = Axios.create({
  baseURL: "/",
});

const cache = new LRU({ max: 10 });

configure({ axios, cache: false });

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <MantineProvider withGlobalStyles withNormalizeCSS>
    <Notifications />
    <React.StrictMode>
      <AuthProvider>
        <App />
      </AuthProvider>
    </React.StrictMode>
  </MantineProvider>,
);
