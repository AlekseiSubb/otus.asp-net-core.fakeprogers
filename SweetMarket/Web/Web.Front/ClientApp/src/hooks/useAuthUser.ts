import { AuthContext } from "@/context/authContext";
import { useContext } from "react";

export default function useAuthUser() {
  const { user } = useContext(AuthContext);

  return {
    isLoggedIn: user !== null,
    user,
  };
}
