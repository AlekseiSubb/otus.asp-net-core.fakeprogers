import { useState } from "react";
import { notifications } from "@mantine/notifications";

interface ModalFormOptions<T> {
  showNotifications?: boolean;
  editTitle?: string;
  createTitle?: string;
  onConfirm?: (data?: T) => void;
}

export default function useModalForm<T = any, O = any>(
  config?: ModalFormOptions<O>
) {
  const [opened, setOpened] = useState(false);
  const [isEditMode, setEditMode] = useState(false);
  const [data, SetData] = useState<T | null>(null);

  function showCreate() {
    setOpened(true);
  }

  function showEdit(data: T) {
    setOpened(true);
    setEditMode(true);
    SetData(data);
  }

  function close() {
    setOpened(false);
    SetData(null);
    setEditMode(false);
  }

  function confirm(data?: O) {
    if (config?.showNotifications) {
      notifications.show({ message: "Изменения успешно сохранены" });
    }

    close();
    config?.onConfirm?.(data);
  }

  function error() {
    if (config?.showNotifications) {
      notifications.show({ message: "Ошибка при сохранении", color: "red" });
    }

    setOpened(false);
  }

  return {
    showCreate,
    showEdit,
    modalProps: {
      opened,
      onClose: close,
      title: isEditMode ? config?.editTitle : config?.createTitle,
      centered: true,
    },
    formProps: {
      data,
      isEditMode,
      onConfirm: confirm,
      onError: error,
    },
  };
}
