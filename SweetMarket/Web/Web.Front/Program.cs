using Duende.Bff.Yarp;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();

builder.Services.AddReverseProxy()
    .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"))
    .AddBffExtensions();

builder.Services.AddBff(options =>
    {
        //options.EnforceBffMiddleware = false;
        //options.AntiForgeryHeaderName = string.Empty;
        //options.AntiForgeryHeaderValue = string.Empty;
    })
   .AddServerSideSessions();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();


builder.Services.AddAuthentication(options =>
{
    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
}).AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
{
    //options.Cookie.SameSite = SameSiteMode.Strict;
    // set session lifetime
    options.ExpireTimeSpan = TimeSpan.FromHours(8);

    // sliding or absolute
    options.SlidingExpiration = false;

    // host prefixed cookie name
    //   options.Cookie.Name = "__Host-spa-yarp";

    // strict SameSite handling
    options.Cookie.SameSite = SameSiteMode.Strict;

    options.Cookie.HttpOnly = false;
})
.AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
{
    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.Authority = builder.Configuration["IdentityConfiguration:IdentityServerBaseUrl"];
    options.ClientId = builder.Configuration["IdentityConfiguration:ClientId"];
    options.ClientSecret = builder.Configuration["IdentityConfiguration:ClientSecret"];
    options.ResponseType = "code";

    options.SaveTokens = true;
    options.GetClaimsFromUserInfoEndpoint = true;

    options.ClaimActions.Remove("aud");
    options.ClaimActions.DeleteClaim("sid");
    options.ClaimActions.DeleteClaim("idp");
    options.Scope.Add("roles");
    options.Scope.Add("sweetmarket_customer_api.fullaccess");
    options.Scope.Add("sweetmarket_market_api.fullaccess");
    options.Scope.Add("sweetmarket_geolocation_api.fullaccess");
    options.Scope.Add("sweetmarket_payment_api.fullaccess");
    options.Scope.Add("sweetmarket_notification_api.fullaccess");
    options.Scope.Add("sweetmarket_imagestorage_api.fullaccess");
    options.Scope.Add("userid");
    options.Scope.Add("email");
    options.ClaimActions.MapJsonKey("role", "role");
    options.ClaimActions.MapJsonKey("email", "email");
    options.ClaimActions.MapUniqueJsonKey("userid", "userid");
    options.TokenValidationParameters = new()
    {
        RoleClaimType = "role",
    };
    options.RequireHttpsMetadata = false;

    //��������� cookie ����� �������� ��� https
    options.NonceCookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
    options.NonceCookie.SameSite = SameSiteMode.Strict;

    options.CorrelationCookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
    options.CorrelationCookie.SameSite = SameSiteMode.Strict;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    //app.UseHsts();
}

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseBff();

app.UseAuthentication();
app.UseAuthorization();

app.MapBffManagementEndpoints();

app.MapReverseProxy(proxyApp =>
{
    //proxyApp.UseAntiforgeryCheck();
});

app.MapFallbackToFile("index.html");

//app.UseCors(x => x
//    .AllowAnyMethod()
//    .AllowAnyHeader()
//    .AllowCredentials()
//    //.WithOrigins("https://localhost:44351")); // Allow only this origin can also have multiple origins seperated with comma
//    .SetIsOriginAllowed(origin => true));

app.Run();
