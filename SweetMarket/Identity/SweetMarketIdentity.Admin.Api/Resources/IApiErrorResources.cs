﻿// Copyright (c) Jan Škoruba. All Rights Reserved.
// Licensed under the Apache License, Version 2.0.

using SweetMarketIdentity.Admin.Api.ExceptionHandling;

namespace SweetMarketIdentity.Admin.Api.Resources
{
    public interface IApiErrorResources
    {
        ApiError CannotSetId();
    }
}







